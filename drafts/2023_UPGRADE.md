MateriaMusica2023
=================

Base de datos
-------------

Instrumentos:

- Laúd
- Vihuela
- Guitarra clásica/romántica
- Guitarra moderna
- Adaptaciones/transcripciones
- Otros

Períodos/marcos estilísticos:

- Renacimiento
  - España
  - Inglaterra
  - Otros
- Barroco
- Clasicismo
- Romanticismo
  - Temprano
  - Tardío
- Siglo XX
  - Impresionismo
  - Nacionalismo
  - Atonalismo
  - Neoclasicismo
- Folklore
  - Argentino
  - Latinoamericano
  - Otros
- Música popular urbana
  - Tango
  - Jazz
  - Rock/pop/otros
- Otros

