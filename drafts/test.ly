\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \fill-line {
\score {
  \layout {
    ragged-right = ##t
    indent = 0
    \context {
      \Staff
      \remove Bar_engraver
      \remove Stem_engraver
      \remove Time_signature_engraver
    }
    \context {
      \Voice
      \omit Stem
    }
  }
  \new Staff \relative c' {
\once \override TextScript #'extra-offset = #'(4.8 . 0)
s4^\markup {
		\override #'(size . 1.8) {
		\override
		#'(fret-diagram-details . (
			(barre-type . none)
			(number-type . roman-upper)
			(orientation . landscape)
			(label-dir . LEFT)
		) )
\fret-diagram-verbose #'(
(mute 6)
(place-fret 5 3)
(place-fret 5 5)
(place-fret 5 6)
(place-fret 4 3)
(place-fret 4 5)
(place-fret 3 2)
(place-fret 3 4)
(place-fret 3 5)
)
  } }
    c d ees f g a b! c
} } } }

\markup { \vspace #3 }

\markup { \fill-line {
\score {
  \layout {
    ragged-right = ##t
    indent = 0
    \context {
      \Staff
      \remove Bar_engraver
      \remove Stem_engraver
      \remove Time_signature_engraver
    }
    \context {
      \Voice
      \omit Stem
    }
  }
  \new Staff \relative c' {
\once \override TextScript #'extra-offset = #'(4.8 . 0)
s4^\markup {
		\override #'(size . 1.8) {
		\override
		#'(fret-diagram-details . (
			(barre-type . none)
			(number-type . roman-upper)
			(orientation . landscape)
			(label-dir . LEFT)
		) )
\fret-diagram-verbose #'(
(open 6)
(open 5)
(open 4)
(open 3)
(open 2)
(open 1)
(place-fret 6 1)
(place-fret 6 3)
(place-fret 6 5)
(place-fret 6 7)
(place-fret 6 8)
(place-fret 6 10)
(place-fret 6 12)
(place-fret 5 2)
(place-fret 5 3)
(place-fret 5 5)
(place-fret 5 7)
(place-fret 5 8)
(place-fret 5 10)
(place-fret 5 12)
(place-fret 4 2)
(place-fret 4 3)
(place-fret 4 5)
(place-fret 4 7)
(place-fret 4 9)
(place-fret 4 10)
(place-fret 4 12)
(place-fret 3 2)
(place-fret 3 4)
(place-fret 3 5)
(place-fret 3 7)
(place-fret 3 9)
(place-fret 3 10)
(place-fret 3 12)
(place-fret 2 1)
(place-fret 2 3)
(place-fret 2 5)
(place-fret 2 6)
(place-fret 2 8)
(place-fret 2 10)
(place-fret 2 12)
(place-fret 1 1)
(place-fret 1 3)
(place-fret 1 5)
(place-fret 1 7)
(place-fret 1 8)
(place-fret 1 10)
(place-fret 1 12)
)
  } }
    e f g a b c d e
} } } }

