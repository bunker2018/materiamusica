#!/bin/bash

. lib/common.sh

directories="scores materiel exercises books"

for d in $directories
do
	msg "Examining $d"
	cat $d/* | while read l
	do
		id="$(echo $l | cut -d: -f1)"
		attrs="$(echo $l | cut -d: -f2-)"
		title="$(echo $attrs | cut -d@ -f1-2 | sed -e 's/@/ - /')"
		remainder="$(echo $attrs | cut -d@ -f3-)"
  	i=0; while true; do
  		i=$((${i}+1))
  		s="$(echo $remainder | cut -d@ -f${i})"
  		[[ "$s" == '' ]] && break
  		button="$(echo "$s" | cut -d' ' -f1)"
  		case $button in
				":")
					echo -n "$id:"
					for t in $s
					do
						case $t in
							:) continue;;
							*) echo -n " @$t";;
						esac
					done
					echo
					break
				;;
				PDF|YOUTUBE|MP4)
					continue
				;;
				*|"EOL")
					break
				;;
			esac
  	done
	done
done
