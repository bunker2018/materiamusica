#!/bin/bash

. lib/site.sh

#read_config() {
	# TODO: this does not seem to work :(
	msg "Reading default config"
	. assets/config.default
	if [ -f site/config ]
	then
		msg "Reading site config"
		. site/config
	fi
#}

msg Building site

prepare_build_dir
generate_bibindex
generate_materiel
generate_exercises
generate_bookindex
generate_linksindex
prepare_output
publish_site

ok
