#!/bin/bash

. lib/common.sh

cat scores/* | cut -d: -f1 | while read t; do
	cat site/pages/{foba3,cs*}.mmus | grep ^@$t$ >___checking
	size=$(du ___checking | cut  -f1)
	case $size in 0) msg "@$t is unchecked";; esac
done

ok
