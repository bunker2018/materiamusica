#!/bin/bash

. lib/db.sh

check_cwd
check_not_root

msg Building database

dbdir=./site/db
wdir=/tmp/mmdb

# Store the database contents in RAM
for f in $dbdir/*.lst
do
  _varname=$(basename $f | sed -e 's/.lst//')
  eval ${_varname}=\"$(cat $f | cut -d"@" -f1)\"
done

make_clean_wdir
cat $dbdir/tables | while read line
do
  # Parse table
  table_id=$(echo $line | cut -d@ -f1)
  table_label="$(echo $line | cut -d@ -f2)"
  table_fields="$(echo $line | cut -d@ -f3)"

  msg2 "Table $table_id"

  # Make a pass over the entire scores database
  cat scores/* | while read score
  do
    # Check this score for matches against table
    # definitions
    c=0
    while true
    do
      c=$(($c+1))
      _field="$(echo $score | cut -d@ -f$c)"
      case $_field in
        EOL) break;;
        :*)
          _data="${_field/:/}"
          _state=1
          for _j in $table_fields
          do
            _jstate=
            for _d in $_data
            do
              # Check each score tag ($_d) against table
              # definitions ($_j)
              if eval echo \$$_j | grep -E -- " ._$_d" >/dev/null
              then
                _jstate=1
                break
              fi
            done
            _state=$_jstate
            [ $_state ] || break
          done
          [ $_state ] || break

          # Score matches table definitions
          score_id=$(echo $score | cut -d: -f1)
          msg3 "$score_id (${_data/ /}) matches $table_fields"
          
          for _j in $table_fields
          do
            my_data="$(extract_data $_j "$_data" | cut -d' ' -f1)"
            eval my_${_j}=$my_data
          done
          
          _this_path=""
          for _j in $table_fields
          do
            eval _this_path=${_this_path}@\$my_${_j}
          done
          #_this_path=${_this_path/_/}
          _this_dump=${wdir}/tables/${table_id}
          echo ${_this_path}@:$score_id >>$_this_dump
        ;;
      esac
    done # with this score
  done # with this table
#    score_id=$(echo $score | cut -d@ -f1)
#    score_composer="$(echo $score | cut -d@ -f2)"
#    score_title="$(echo $score | cut -d@ -f3)"
done # reading tables line by line

msg "Preparing sources"

cat $dbdir/tables | while read line
do
  # Parse table
  table_id=$(echo $line | cut -d@ -f1)
  table_label="$(echo $line | cut -d@ -f2)"
  table_fields="$(echo $line | cut -d@ -f3)"

  s=${wdir}/tables/$table_id
  this_source=${wdir}/sources/$table_id
  stack=()
  
  cat $s | sort | while read line
  do
    tier=0
    flag=
    while true
    do
      _f=$(echo $line | cut -d@ -f$(($tier+1)))
      case $_f in
        :*)
          break;;   # Last field; exit bucle and keep it as score id
        *)
          # Check field against the stack
          if [[ $_f != ${stack[$tier]} ]] \
            || [ $flag ]
          then
            echo %${tier}%$_f >>$this_source
            stack[$tier]=$_f
            flag=1
          fi
          tier=$(($tier+1))
        ;;
      esac
    done
    echo ${_f/:/} >>$this_source   # Write score id in the source
  done
done

msg "Generating output"

out=${wdir}/out

cat $dbdir/tables | while read line
do
  # Parse table
  table_id=$(echo $line | cut -d@ -f1)
  table_label="$(echo $line | cut -d@ -f2)"
  table_fields="$(echo $line | cut -d@ -f3)"
  table_jobs="$(echo $line | cut -d@ -f4)"
  table_source=${wdir}/sources/$table_id
  
  for j in $table_jobs
  do
    msg2 "Generating $j for table $table_id"
    eval make_$j
  done
done

msg "Saving output"
cp -rv ${out} ${dbdir}

ok
