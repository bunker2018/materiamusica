[[_TOC_]]

## Mejoras 2022

```
- [ ] Contenidos por prácticos/grupos de obras
  - [x] Mostrar unidades -> contenidos en una sola página.
  - [x] Reemplazar prácticos por material de apoyo/estudio (opcional, otorga créditos).
  - [ ] Distribuir/recortar contenidos en cada obra del programa.
- [ ] Programa de obras
  - [ ] Repertorio de obras.
  - [ ] Armar programas de obras prediseñados para cada nivel (completo con recorte de contenidos, recorrido, material de
        apoyo, etc.); generar link de descarga en pdf y eventualmente texto para material de Classroom.
  - [ ] Recorte de contenidos para cada obra.
  - [ ] Recorrido.
  - [ ] Material de apoyo.
  - [ ] Propósitos; objetivos. (Definidos, claros, concisos)
  - [ ] Temporalidad.
- [ ] Calificación/acreditación
  - [ ] Items que se evaluarán en cada obra (contenidos, propósitos).
  - [ ] Créditos por: temporalidad, calidad, proceso, compromiso.
  - [ ] Penalizaciones por: fuera de término, falta de compromiso, trampas.
  - [ ] Algoritmos (varios, para permitir ajustes durante el proceso; ej: suma, promedio, mixtos, ...); margen discrecional.
- [ ] Reemplazar WhatsApp
  - [ ] Discord: socializar, experimentar, diseñar, implementar, probar:
    - [ ] Con otros docentes.
    - [ ] Con alumnos cursantes.
    - [ ] Con grupos heterogéneos (consulta, actividades).
  - [ ] Telegram: probar (no ofrece muchas mejoras).
- [ ] Seguimiento - Google Spreadsheet: mejoras en la integración.
  - [x] Generar CSV.
  - [x] Parsear (cf. Cuadernillo).
    - [x] Generar pdf (alojar en sitio web en forma privada).
    - [ ] Generar texto ad hoc para integrar como Material de Classroom.
- [ ] Mejoras varias
  - [x] Anotaciones en Zoom: Stutter.
  - [ ] Seguir adelante con la transcripción/traducción/elaboración de material de apoyo.
  - [x] Archivar página 2021.
  - [x] Rediseñar/resubir página 2022.
  - [ ] Linkear entre sí: Classroom / Página / Seguimientos / Calificaciones / Discord.
  - [ ] Investigar a fondo: Classroom, Shutter, Zoom, Discord, etc.
  - [ ] Agregar tags a la biblioteca virtual.
  - [ ] Eventualmente: generar páginas de biblioteca prefiltradas (ej. todas las obras de FOBA 3).
  - [ ] Horarios: optimizar socialización (con los alumnos, con la escuela, con otros docentes, etc.).
  - [x] Página de inicio: muy ambiciosa, razonada fuera del recipiente: repensar.
```

## Recorrido (prioridades)

### Urgentes

- Programa de obras
  - Repertorio de obras: armar para cada alumno.
  - Armar programas de obras prediseñados para cada nivel (completo con recorte de contenidos, recorrido, material de
        apoyo, etc.); generar link de descarga en pdf y eventualmente texto para material de Classroom.
  - Recorte de contenidos para cada obra.
  - Recorrido.
  - Material de apoyo.
  - Propósitos; objetivos. (Definidos, claros, concisos)
  - Temporalidad.
- Rediseñar/resubir página 2022.
  - Página de inicio: muy ambiciosa, razonada fuera del recipiente: repensar.
- Archivar página 2021.
- Contenidos por prácticos/grupos de obras
  - Mostrar unidades -> contenidos en una sola página.
  - Reemplazar prácticos por material de apoyo/estudio (opcional, otorga créditos).
  - Distribuir/recortar contenidos en cada obra del programa.
- Seguimiento - Google Spreadsheet: mejoras en la integración.
  - Generar CSV.
  - Parsear (cf. Cuadernillo).
    - Generar pdf (alojar en sitio web en forma privada).
    - Generar texto ad hoc para integrar como Material de Classroom.

### Importantes

- Calificación/acreditación
  - Items que se evaluarán en cada obra (contenidos, propósitos).
  - Créditos por: temporalidad, calidad, proceso, compromiso.
  - Penalizaciones por: fuera de término, falta de compromiso, trampas.
  - Algoritmos (varios, para permitir ajustes durante el proceso; ej: suma, promedio, mixtos, ...); margen discrecional.
- Seguir adelante con la transcripción/traducción/elaboración de material de apoyo.
- Agregar tags a la biblioteca virtual.

### Otras

- Linkear entre sí: Classroom / Página / Seguimientos / Calificaciones / Discord.
- Investigar a fondo: Classroom, Shutter, Zoom, Discord, etc.
- Eventualmente: generar páginas de biblioteca prefiltradas (ej. todas las obras de FOBA 3).
- Horarios: optimizar socialización (con los alumnos, con la escuela, con otros docentes, etc.).
- Agregar ejemplos de YT a la biblioteca.

- Reemplazar WhatsApp
  - Discord: socializar, experimentar, diseñar, implementar, probar:
    - Con otros docentes.
    - Con alumnos cursantes.
    - Con grupos heterogéneos (consulta, actividades).
  - Telegram: probar (no ofrece muchas mejoras).

