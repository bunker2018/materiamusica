@pandoc -Ss --template=/usr/lib/lymd/tex/template-oneside.tex
Escuela de Arte N°01 ``Alcides Biagetti''%%
__Carrera y nivel:__ Profesorado en Música Orientación Instrumento -- 2° año%%
__Espacio curricular:__ Instrumento Guitarra II%%
__Docente:__ Marcelo López Minnucci%%
__Año:__ 2019

## Contrato pedagógico

El presente contrato describe las condiciones académicas y administrativas requeridas para
la continuidad de la cursada y su consecuente acreditación y promoción, así como a la
adecuada inserción del curso en la carrera y la Institución, en los planos académico,
administrativo y vincular.

A estos efectos, el alcance de este contrato se halla enmarcado por la Resolución 4043/09
(Régimen Académico Marco) y el Proyecto Institucional (PI) de la Escuela de Arte ``Alcides
Biagetti''; las partes vinculadas por el presente contrato se referirán a dichos documentos
para resolver toda situación no contemplada en el contrato o donde hubiera discrepancias
en su interpretación.

Es responsabilidad del estudiante:

- conocer y cumplimentar las condiciones de regularidad (régimen de inasistencias),
  plan de estudios, correlatividades,
  plazos y demás disposiciones y acuerdos institucionales;
- cumplir con los plazos y las formalidades requeridas para la inscripción tanto a las
  cursadas como a las instancias de examen final;
- conocer y cumplimentar los requerimientos de la cátedra en relación a la aprobación de
  la unidad curricular y del correspondiente examen final;
- conocer y respetar los acuerdos de convivencia vigentes.

Es responsabilidad de la cátedra:

- dar a conocer en tiempo y forma los proyectos de cátedra, trabajos prácticos, material de
  trabajo, objetivos, requerimientos y plazos relevantes para el buen desarrollo del curso;
- realizar en tiempo y forma la correspondiente devolución de las actividades propuestas;
- propiciar siempre que sea posible la continuidad de la cursada,
recurriendo a
los recursos pedagógicos,
académicos e institucionales disponibles a tal efecto.

Son condiciones necesarias para la aprobación de la cursada regular:

- cumplir con las entregas acordadas con la cátedra, en tiempo y forma;
- asistir al 80% de las clases como mínimo;
- cumplir con el régimen de correlatividades dispuesto por el plan de estudios al momento del
  cierre de calificaciones al final del ciclo lectivo;
- obtener una calificación igual o superior a 4 (cuatro) para acceder a la aprobación de la cursada
  con derecho a rendir examen final.

Los siguientes puntos no son vinculantes a la aprobación de la materia,
sino que apuntan a la construcción de un clima de buena convivencia
en el aula y la Institución:

- colaborar entre todos con la limpieza, el orden y el respeto de los espacios comunes;
- observar la puntualidad necesaria para un desarrollo ordenado y armonioso de la clase;
- comprometernos entre tod@s a propiciar y practicar la resolución de dife\\-rencias y conflictos
  que pudieran surgir, siempre a través del diálogo y el respeto por el otro, con el propósito
  no de la supremacía de mis propias ideas, sino en busca de un consenso que nos incluya a todos;
- predisponernos, tanto docentes como alumn@s, a recibir la calificación numérica o la
  devolución oral o escrita con una mirada autocrítica, cons\\-tructiva y proyectiva, entendiendo
  estas instancias como una oportunidad de crecimiento y autosuperación;
- participar de, y colaborar con, los proyectos de extensión propuestos por la cátedra y la institución:
  conciertos, muestras, audiciones, registro de material audiovisual, etc.

El presente contrato tendrá validez hasta el fin del ciclo lectivo 2019 en relación a la cursada de
la materia, con continuidad hasta la fecha del examen final en relación a los puntos que a éste
refieren.

