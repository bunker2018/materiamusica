Asociación Cooperadora E.A.A.B. -- 3/3/2019

## Orden del día para la 1° Reunión (día 8/3/19)

- Socializar y asentar en acta lo hablado en los últimos encuentros.
- Presentar el balance de Marzo, o determinar un plazo para su presentación
si no llegáramos a elaborarlo.
- Determinar el plazo para la elaboración del Estatuto y los Libros.
- Determinar las prioridades y las actividades a realizar en lo inmediato, a saber:
- Presentación del Equipo de Cooperadora (definición de lineamientos y propósitos).
- Cancelación de las deudas existentes.
- Relevamiento del patrimonio.
- Cálculo de los gastos fijos de la Escuela (limpieza, librería, etc.).

