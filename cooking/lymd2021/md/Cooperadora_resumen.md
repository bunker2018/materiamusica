Asociación Cooperadora E.A.A.B. -- 3/3/2019

# Estatuto marco (resumen)

## Objetivos de la Cooperadora

Los Objetivos de la Entidad serán los siguientes,
condicionados a las necesidades del medio social en que se desenvuelve
y las particularidades de los ciclos de enseñanza
(ver Artículo 1° del Decreto 4767/72 y su modificatorio Decreto
355/73):

- Coordinar su actividad con los sectores económicos de la comunidad, a efectos de
servir de agente laboral de l@s alumn@s cuando lo soliciten o las
circunstancias lo requieran.
- Establecer un sistema de becas que propicien la continuidad de los estudios de los
alumnos que pudiesen necesitar ese apoyo.
- Distribuir entre los alumnos que no puedan adquirirlos, útiles, libros y demás
elementos didácticos y procurar su venta a un precio razonable entre el resto
(ver Resolución Ministerial N° 695/69).
- Promover actividades extra - escolares para los alumnos, en los cuales, el aspecto
pedagógico se hallará a cargo de los docentes que designe la Inspección de
Enseñanza respectiva.
- Contribuir al quehacer educacional mediante la adquisición del material necesario y
mantenimiento del edificio escolar.
- Encarar o propiciar toda otra forma de asistencia al educando o sus familias, que
tienda a hacer cumplir con lo enunciado.

Además (no aplicarían a la E.A.A.B. en principio):

- Financiar, administrar o participar en Comedores Escolares Unitarios o
Centralizados, o en los Servicios que los sustituyan.
- Establecer, donde los hubiese, Servicio de Asistencia Médica integral, total o
parcialmente gratuitos, de acuerdo a las posibilidades de cada educando.
- Financiar o cooperar en la financiación de los Planes de Turismo o excursiones
educativas.

## Conformación de la Comisión Directiva

La Administración de la Entidad estará a cargo de la Comisión
Directiva integrada por: un Presidente, un Secretario, un Tesorero, tres Vocales
Titulares y dos Vocales Suplentes.

La Comisión Directiva durará en sus funciones dos (2) años,
renovándose por mitades cada año en la Asamblea Anual Ordinaria de socios,
determinándose por sorteo la duración de los mandatos y en adelante se respetará
teniendo en cuenta este primero.

Los miembros de la Comisión Directiva podrán ser reelectos.

Los cargos de Presidente, Tesorero, Revisor de Cuentas y Asesor
son incompatibles para cónyuges y parientes hasta segundo grado de
consanguinidad.

## Deberes y atribuciones de la Comisión Directiva

### Presidente

- Representar legalmente a la Cooperadora, actuando a su nombre ante las
autoridades Judiciales y Administrativas en cualquier lugar y jurisdicción.
- Reunir la Comisión Directiva para las sesiones de la misma, fijando previamente el
Orden del Día y presidiendo el debate.
- Convenir con el Asesor los días y horarios de las reuniones de Comisión Directiva.
- Presidir las Asambleas y Reuniones de Comisión Directiva y desempatar con su voto
en caso de empate.
- Firmar conjuntamente con Tesorero o Secretario ordenes de pago, cheques y
demás documentos que importen a la creación de derechos y obligaciones.
- Revisar mensualmente los balances de Tesorería.
- Cumplir y hacer cumplir el Estatuto y las Resoluciones de las Asambleas y de la
Comisión Directiva.
- Delegar temporalmente el mandato en un Vocal Titular durante las sesiones o
Asambleas a fin de participar del debate.
- En caso de renuncia o cualquier otro impedimento, será reemplazado por el
miembro Titular de la Comisión Directiva que corresponda (VOCAL TITULAR 1°)

### Secretari@

- Redactar y firmar las Actas conjuntamente con el Presidente.
- Redactar las comunicaciones, correspondencias y documentación, conservando
copia de los mismos en los Archivos respectivos.
- Refrendar la firma del Presidente en todo documento que emane de la Asociación
Cooperadora.
- Citar para reunión de Comisión Directiva a los miembros de manera fehaciente y
darles cuenta de las comunicaciones que se reciban en virtud del cargo que inviste.
- Firmar conjuntamente con Presidente o Tesorero ordenes de pago, cheques y
demás documentos que importen a la creación de derechos y obligaciones.
- Cumplir y hacer cumplir el Estatuto y las Resoluciones de las Asambleas y de la
Comisión Directiva.
- En caso de renuncia o cualquier otro impedimento, será reemplazado por el
miembro Titular de la Comisión Directiva que corresponda (VOCAL TITULAR 1°)

### Tesorer@

- Recibir dinero que ingrese en Caja, siendo responsable directo de lo que se
recaude.
- Recibir de su antecesor, por intermedio del Presidente, bajo inventario, los
documentos de crédito y débito, dinero, muebles, útiles, etc.
- Depositar los fondos en el Banco en el cual tengan abierta una cuenta corriente a
esos efectos (art.14).
- Presentar a la Comisión Directiva un Balance mensual con comprobantes, el que
será refrendado por Presidente, Secretario, Revisores de Cuentas Titular y Docente
y Asesor.
- Organizar el servicio financiero de la Entidad de acuerdo a los propios recursos y
llevar un Libro de Tesorería, otro de Registro de Socios (Rubricados éstos por el
Consejo Escolar del distrito) y la documentación auxiliar que estime necesaria.
- Verificar los pagos ya autorizados por la Comisión Directiva, exigiendo facturas que
se encuadren dentro de la legislación vigente (A.F.I.P.).
- Conservar en su poder la suma del dinero en efectivo (Caja Chica).
- Firmar conjuntamente con Presidente o Secretario ordenes de pago, cheques y
demás documentos que importen a la creación de derechos y obligaciones.
- Cumplir y hacer cumplir el Estatuto y las Resoluciones de las Asambleas y de la
Comisión Directiva.
- En caso de renuncia o cualquier otro impedimento, será reemplazado por el
miembro Titular de la Comisión Directiva que corresponda (VOCAL TITULAR 1°).

### Vocales Titulares

- Concurrir a las sesiones convocadas.
- Desempeñar las comisiones que se le designen.
- Suplir a los miembros de Comisión Directiva que por renuncia u otras circunstancias
dejare vacante el cargo.

### Vocales Suplentes

- Reemplazar a los Vocales Titulares en caso de renuncia u otros impedimentos.
- Desempeñar las comisiones que se les designen.

### Comisión Revisora de Cuentas

El Órgano de Supervisión de la Comisión Directiva será la
Comisión Revisora de Cuentas, la cual estará integrada por dos miembros Titulares
y uno Suplente. Uno de los Titulares será designado por el Director del
establecimiento entre el Personal Docente del mismo. El otro Revisor de Cuentas
Titular, será elegido por la Asamblea y debe ser Socio de la Entidad. Durarán en sus
funciones un (1) año, pudiendo ser confirmados en nuevos períodos.

## Capital Social

La Asociación Cooperadora integrará su fondo (CAPITAL SOCIAL)
de la siguiente forma:

- Con la cuota social que abonen sus Asociados.
- Con los beneficios de actos, festivales, rifas, bonos, quermeses, etc.
- Con donaciones y legados.
- Con el producto de la venta de los Bienes Muebles que sean dados de baja, de
acuerdo a lo determinado en el Artículo 17° inc. a).
- Con el producto de quiosco, venta de libros, útiles, instalado en el local Escolar, con
sujeción a las reglamentaciones vigentes.
- Con Subsidios Estatales y de otros orígenes.
- Con el producto de la venta de objetos o maquinarias fabricadas por los alumnos o
la Asociación Cooperadora, __siempre que esta haya aportado la materia prima__.
- Con los Fondos recaudados en cualquier otro concepto que en ninguna manera
entorpezca el logro del fin y los objetivos de la entidad.

Los Fondos ingresados por cualquier concepto, deberán ser
depositados dentro del plazo de cinco (5) días hábiles en el Banco de la Provincia de
Buenos Aires con el cual operan, a la orden conjunta del Presidente, Secretario y
Tesorero, procediendo a su extracción con la firma indistinta de dos de dichos
miembros.

## Bienes muebles e inmuebles

Los bienes muebles adquiridos a cualquier titulo por la Asociación
Cooperadora integraran su patrimonio. La Comisión Directiva podrá afectarlos a los
siguientes usos:

- Por la Entidad en el cumplimiento de sus fines y objetivos o en su desenvolvimiento
interno.
- A otras Asociaciones Cooperadoras que participen en programas o servicios de
ayuda escolar que beneficien a alumnos del establecimiento.
- A la escuela en su actividad educativa cuando sea necesidad imperiosa y a petición
expresa del asesor.
- A instituciones de bien público, cuando razones de necesidad social lo hagan
imperioso, debiendo mediar la anuencia del Ministerio de Educación, a través de la
Dirección de Cooperación Escolar, cuando fuesen otorgados por un período mayor
de treinta días.

Los bienes muebles podrán egresar del patrimonio de la Entidad
cuando se cumplan las siguientes condiciones:

- Por venta mediante el voto de los dos tercios de la Comisión Directiva reunida en
sesión plenaria.
- Por donación gratuita, sólo mediante la aprobación de la Dirección General de
Cultura y Educación y en beneficio de instituciones de bien público reconocidas
oficialmente.

Los bienes inmuebles adquiridos a cualquier título por la entidad,
ingresarán al patrimonio fiscal, con afectación a la Dirección General de Cultura y
Educación. Cuando por razones de bien común, la entidad considere necesaria su
tenencia precaria - con sujeción a los fines que motivaron su adquisición - la
Cooperadora solicitará su autorización a la Dirección General de Cultura y
Educación.

## Socios

Las categorías de los Socios serán las siguientes:

- __ACTIVOS:__ Todos los ciudadanos mayores de edad, que por residencia, actividad o
escolaridad de sus hijos, se hallen vinculados a la comunidad en que el
establecimiento escolar desenvuelve su actividad, que abonen la Cuota Social
establecida y cumplan las normas fijadas por el presente Estatuto Social.
- __HONORARIOS:__ Las agrupaciones profesionales de Trabajadores, las Empresas,
personas o Instituciones en general que colaboren con el cumplimiento de los
objetivos de la Entidad y se hagan acreedores al reconocimiento de tales por parte
de la Comisión Directiva, ad - referéndum de la Asamblea siguiente.
- __ADHERENTES:__ Menores de veintiún (21) años de edad que abonen una cuota
inferior a la fijada para los Socios Activos.

## Asambleas

La Asamblea Ordinaria se celebrará anualmente en la segunda
quincena de mayo, a los efectos de:

- Aprobar o rechazar la Memoria y Balance del Ejercicio anterior.
- Renovación de la Comisión Directiva y Comisión Revisora de Cuentas.
- Determinación de monto de Cuota de Socios y Efectivo que podrá mantener el
Tesorero para gastos menores y urgentes (Caja Chica).

Las Asambleas Extraordinarias deberán ser convocadas por la
Comisión Directiva en un plazo no mayor de quince (15) días, ni menor de cinco
(5), cuando ocurra alguna de las siguientes condiciones:

- Lo soliciten por escrito más del diez (10%) por ciento de los socios.
- Lo soliciten por escrito dos (2) miembros titulares de la Comisión Directiva.
- Lo establezca la Dirección de Cooperación Escolar.

## De los Acuerdos Institucionales Extraordinarios

En la última reunión se abordó el tema de ciertos bienes patrimoniales que se han adquirido
con fondos de los propios estudiantes o de la cátedra, y que se entiende son para uso
exclusivo o preferencial de determinada carrera o espacio curricular (por ejemplo,
televisores, herramientas, materiales).

En el espíritu de que la Cooperadora pueda contribuir a regular y cuidar dichos
elementos sin pasar por arriba estos acuerdos, se propuso agregar al Estatuto una serie
de acuerdos institucionales a este respecto, que permitirían observar el buen uso
del patrimonio y a la vez proporcionar la flexibilidad necesaria para la coexistencia
de las diferentes carreras, atendiendo a sus especificidades y necesidades.

Queda por determinarse la manera de enunciar estos acuerdos, de forma tal que el
Consejo Escolar apruebe el Estatuto formalmente.

