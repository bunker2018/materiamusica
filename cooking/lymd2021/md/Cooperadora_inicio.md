Cooperadora EAAB -- 29/3/2019

## Equipo

- __Presidente:__ Marcelo López Minnucci
- __Secretaria:__ Carolina Tellería
- __Tesorero:__ Javier Agama Barragán
- __Pro Tesorero:__ Héctor Garrafa
- __1° Vocal:__ Susana Biondi
- __2° Vocal:__ Yesica Moller
- __3° Vocal:__ Mayra Valeria Álvarez
- __1° Vocal Suplente:__ Agustín Ávila
- __2° Vocal Suplente:__ Betina Ragaini
- __Asesora:__ Mariana Giorgetti

### Comisión Revisora de Cuentas

- __Titular:__ Mirko Rodríguez
- __Suplente:__ María de los Ángeles Caputo

## Libros

- Registro de Socios
- Libro de Tesorería y Registro de Caja Chica
- Libro de Inventario (Patrimonio)
- Libro de Actas
- Estatuto
- Carpetas de comprobantes y comunicados
- Cuaderno
- Caja

## Propuestas de trabajo y temario

- Leer y socializar la __documentación__ que regula el funcionamiento de la Cooperadora:
roles y obligaciones correspondientes a cada cargo, proce\\-dimientos, plazos.
- __Presentación del equipo__ ante los diferentes claustros:
estudiantes, docentes, auxiliares, preceptor@s, equipo directivo.
Escuchar inquietudes, ideas, necesidades, prioridades.
Socializar la situación actual de la Coope\\-radora y la propuesta del grupo para la gestión.
Armar la presentación como un __evento__.
- Dar participación a los __delegados__ y al __Centro de Estudiantes__. Recibir proyectos y
propuestas de todos los actores de la C.E. (__convocatoria__).
- Proponer a 3° año de Diseño la confección de un __logo__ para la Cooperadora.
- Proyectar la confección de los __libros__: socios, patrimonio, actas, contabilidad, etc.
- Proponer y elaborar estrategias para __juntar fondos__:
peñas, varietés, subastas, conciertos;
urnas para colaboraciones voluntarias;
bingo;
insistir en la importancia del pago de la cuota y colaboraciones;
conseguir colaboraciones voluntarias de privados y Municipio (descuentos, donaciones);
kiosco/buffet;
considerar el cobro de una cuota o colaboración mensual;
cobrar rendimientos académicos, etc., a quienes no paguen Cooperadora;
cenas solidarias (Casino).
- Elaborar una lista de los __comercios__ donde se realizan compras habitualmente.
- Elaborar una lista de los __gastos fijos__ mensuales/anuales de la Escuela:
insumos de limpieza, librería, conexión a Internet, etc.,
de manera que nos sirva para prever gastos y a la vez para agilizar las compras y que nunca falte lo importante.
- Programar día y horario de las __reuniones periódicas__ (mensuales -- es función del Presidente y de Dirección).
- Resolver la deuda con __Librería Estudiando__ y pasar por Leone.
- Realizar un __relevamiento del patrimonio__ de la Cooperadora.
- __Puesta en valor:__ elaborar una lista de los arreglos y mejoras más urgentes:
paredes del pasillo que se estropearon al armar la muestra,
burlete de la puerta de calle del SUM, etc.
- Recuperar __proyectos__ en lista de espera o dormidos:
acustización de aulas, división de Biblioteca y aulas, etc.
- Implementar cursos, talleres, seminarios, traer gente y propuestas de afuera, articular con Universidad.
- Fortalecer el vínculo con la Escuela N°8 a través del trabajo conjunto de ambas Cooperadoras.
- __Fotocopiadora o impresora__ como servicio y como una manera de recaudar fondos.
- Establecer __redes de intercambio__ con otras entidades (comerciales, etc.).
- Estatuto: inclusión de acuerdos _ad hoc_ (ej., sobre el uso de ciertos elementos).
- Enfatizar la __transparencia__ y la __participación__ en la gestión de la A.C.
- Compra de elementos (propiedad de Cooperadora) para uso de los estudiantes que no puedan
costearlos (ej. gubias).

