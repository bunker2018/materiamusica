## Modelo de Acta de Asamblea 
 
  En la ciudad de ............................ a los ................. días del mes de ............... de 20.., siendo las ... hs., se constituye en la sede del Establecimiento Escolar, la Asamblea General ORDINARIA, con la presencia de un total de .... socios en condiciones de votar, se deja constancia que al no lograrse el quorum requerido (la mitad más uno) a la hora mencionada para el inicio de la asamblea, se realizará una vez transcurridos ........ minutos de la hora estipulada, vencido el plazo se da lectura al orden del día:

1. Designación de dos asociados para firmar el acta conjuntamente con el Presidente y Secretario.
2. Consideración de la Memoria, Balance General e informe de la Comisión Revisora de Cuentas.
3. Elección de: Comisión Directiva, en los cargos de Presidente, Vicepresidente, Tesorero, Secretario, Prosecretario, cuatro vocales titulares y cuatro suplentes, una Comisión Revisora de Cuentas con sus tres miembros titulares y un suplente.

Se designa los dos asociados:

Sr. .....................................................................D.N.I. ..................................... y al Sr. ....................................

 D.N.I. ...................que firmarán el acta de la Asamblea.

Seguidamente se da lectura a la Memoria, Balance e Informe de la Comisión Revisora de Cuentas para su consideración y aprobación.

Luego de considerado y aprobado el punto anterior, se lleva a cabo el acto eleccionario, quedando registrada la nómina de los miembros que conforman la nueva Comisión Directiva y Revisora de Cuentas.

Finalizado el tratamiento del orden del día, se da por concluído el acto, previo agradecimiento a los asistentes del mismo.

 

Secretario                                                                 

Presidente

Asambleísta                                                               

Asambleísta
