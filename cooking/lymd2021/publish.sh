#!/bin/bash

d="../../musicabiagetti/public"
[ -d "$d" ] || {
	echo "Wrong path"
	exit 1
}

cat publish.lst | while read line; do
	file="$(echo $line | cut -d'|' -f1)"
	dest="$(echo $line | cut -d'|' -f2)"
	cp -v "$file" "${d}/${dest}"
done

