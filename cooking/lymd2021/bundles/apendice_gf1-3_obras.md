@pandoc -Ss --chapters --toc --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-vertical Obras_de_referencia Material_de_consulta_elaborado_por_la_cátedra Guitarra_--_FOBA_1--3 2019@

\\pagebreak

# Prefacio

@lymd include src/obras_gf1-3_prefacio.md @

# FOBA 1

## Música para vihuela y laúd

@lymd include src/obras_gf1_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf1_s19.md @

## Música del siglo XX

@lymd include src/obras_gf1_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf1_popu.md @

# FOBA 2

## Música para vihuela y laúd

@lymd include src/obras_gf2_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf2_s19.md @

## Música del siglo XX

@lymd include src/obras_gf2_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf2_popu.md @

# FOBA 3

## Música para vihuela y laúd

@lymd include src/obras_gf3_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf3_s19.md @

## Música del siglo XX

@lymd include src/obras_gf3_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf3_popu.md @

## Selección de canciones[^1]

@lymd include src/obras_gf3_canc.md @

[^1]: Ver prefacio.
