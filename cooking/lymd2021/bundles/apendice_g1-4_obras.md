@pandoc -Ss --chapters --toc --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-vertical Obras_de_referencia Material_de_consulta_elaborado_por_la_cátedra Guitarra_--_Profesorado_1--4 2019@

\\pagebreak

# Prefacio

@lymd include src/obras_g1-4_prefacio.md @

# Primer año

## Música para vihuela y laúd

@lymd include src/obras_g1_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g1_s19.md @

## Música del siglo XX

@lymd include src/obras_g1_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g1_popu.md @

# Segundo año

## Música para vihuela y laúd

@lymd include src/obras_g2_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g2_s19.md @

## Música del siglo XX

@lymd include src/obras_g2_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g2_popu.md @

# Tercer año

## Música para vihuela y laúd

@lymd include src/obras_g3_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g3_s19.md @

## Música del siglo XX

@lymd include src/obras_g3_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g3_popu.md @

# Cuarto año

## Música para vihuela y laúd

@lymd include src/obras_g4_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g4_s19.md @

## Música del siglo XX

@lymd include src/obras_g4_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g4_popu.md @

