@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-eaab FOBA_de_Música Nivel_3 Instrumento_--_Guitarra 6 2019@

# Marco referencial

@lymd include src/gf3_fund.md@

# Propósitos

@lymd include src/gf3_prop.md@

# Expectativas de logro

@lymd include src/gf3_expe.md@

# Contenidos

@lymd include src/gf3_cont.md@

# Marco metodológico

@lymd include src/gf3_meto.md@

# Cronograma

@lymd include src/gf3_cron.md@

# Acreditación y evaluación

@lymd include src/gf3_eval.md@

