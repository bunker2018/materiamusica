@pandoc -Ss --chapters --toc --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-vertical Obras_de_referencia Material_de_consulta_elaborado_por_la_cátedra Guitarra_--_todos_los_niveles 2019@

# Prefacio

@lymd include src/obras_prefacio.md @

# FOBA -- Primer nivel

## Música para vihuela y laúd

@lymd include src/obras_gf1_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf1_s19.md @

## Música del siglo XX

@lymd include src/obras_gf1_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf1_popu.md @

# FOBA -- Segundo nivel

## Música para vihuela y laúd

@lymd include src/obras_gf2_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf2_s19.md @

## Música del siglo XX

@lymd include src/obras_gf2_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf2_popu.md @

# FOBA -- Tercer nivel

## Música para vihuela y laúd

@lymd include src/obras_gf3_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_gf3_s19.md @

## Música del siglo XX

@lymd include src/obras_gf3_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_gf3_popu.md @

## Selección de canciones[^1]

[^1]: Ver prefacio.

@lymd include src/obras_gf3_canc.md @

# Profesorado -- Primer año

## Música para vihuela y laúd

@lymd include src/obras_g1_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g1_s19.md @

## Música del siglo XX

@lymd include src/obras_g1_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g1_popu.md @

# Profesorado -- Segundo año

## Música para vihuela y laúd

@lymd include src/obras_g2_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g2_s19.md @

## Música del siglo XX

@lymd include src/obras_g2_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g2_popu.md @

# Profesorado -- Tercer año

## Música para vihuela y laúd

@lymd include src/obras_g3_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g3_s19.md @

## Música del siglo XX

@lymd include src/obras_g3_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g3_popu.md @

# Profesorado -- Cuarto año

## Música para vihuela y laúd

@lymd include src/obras_g4_laud.md @

## Música para guitarra del siglo XIX

@lymd include src/obras_g4_s19.md @

## Música del siglo XX

@lymd include src/obras_g4_s20.md @

## Música de raíz folklórica y popular

@lymd include src/obras_g4_popu.md @

