@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-vertical Guitarra_1_--_4 Trayecto_formativo_de_la_Orientación_Instrumento Prof._Marcelo_López_Minnucci 2019@

# Marco referencial

En este documento se proponen los lineamientos generales para el trayecto formativo de la materia
Instrumento -- Guitarra,
correspondiente al Profesorado Orientación Instrumento
y a la Tecnicatura en Capacitación Instrumental,
así como las propuestas de articulación con otros espacios curriculares de la carrera
de Música y eventualmente un trabajo interdisciplinar con las demás carreras.

La propuesta consiste en articular con las materias de producción en cada año de la carrera
(talleres), o bien con espacios curriculares cuyos contenidos puedan aportar al proceso
de aprendizaje de la guitarra desde otros encuadres (Trabajo Corporal).
La articulación con las materias troncales de la carrera
(Lenguaje Musical, Historia de la Música, Espacio de la Práctica Docente)
se piensa como una continuidad durante el trayecto formativo, de 1° a 4° año.

La secuenciación y el recorte de los contenidos propios del espacio curricular
se harán en función de este plan de articulación,
de manera de otorgar coherencia a los procesos de aprendizaje;
eventualmente esto permitiría trabajar un mismo repertorio en diferentes materias,
aportando significatividad y dinamismo a las actividades
y una mayor profundidad en el trabajo sobre los materiales.

Al hacer más permeable la materia a los materiales y encuadres de otros espacios y disciplinas,
se espera alcanzar una renovación del repertorio de obras,
y asimismo una resignificación de los textos musicales que habitualmente se trabajan,
con el aporte de las diversas miradas y encuadres de cada cátedra.

La articulación con el Espacio de la Práctica Docente se piensa en función de la
incorporación progresiva del estudiante al trabajo de aula y la transmisión de saberes,
a través de la socialización de proyectos áulicos y experiencias en los primeros años,
llegando hacia el final de la carrera a una síntesis de lo adquirido orientada a la Formación
Básica, desde la producción de material musical con finalidad pedagógica
y la elaboración de estrategias adaptadas al contexto histórico y cultural en el que se inserta la Escuela.

# Secuenciación de contenidos y  articulación

__Primer año__ -- Articulación con Trabajo Corporal.%%
El cuerpo. Adquisición y construcción razonada de los mecanismos básicos.%%
Organicidad, conciencia del cuerpo, observación.%%
Rutinas y estrategias de estudio y práctica.%%
Preparación de obra en función de la presentación en audición o concierto.

__Segundo año__ -- Articulación con los talleres: Música Popular, Folklore.%%
Encuentro de las técnicas, lenguajes (en el sentido de códigos estilísticos),
marcos teóricos y valores musicales inherentes a
diferentes escuelas y vertientes.%%
Elaboración de arreglos, adaptaciones, reelaboración de pasajes.%%
La técnica en función del contexto y del discurso: apropiación de las técnicas
propias del lenguaje folklórico y popular (rasgueos, recursos técnicos y expresivos
propios del campo popular).

__Tercer año__ -- 
La elaboración de la obra.%%
Progresiva complejización del lenguaje y de la demanda técnica.%%
Acercamiento al repertorio académico de concierto.
Exploración de diversos géneros y repertorios.%%
Operadores. Ejercicio cero (ref. Eduardo Fernández).
Profundización del trabajo técnico e interpretativo.%%
Articulación con Técnicas de Improvisación.

__Cuarto año__ -- Articulación con las materias de medios e integradoras.%%
Síntesis de lo adquirido a lo largo de la carrera proyectado hacia el aula de música y el medio local.%%
Gran obra. Nuevos lenguajes. Polifonía.%%
Resolución de pasajes a través del análisis (ref. Eduardo Fernández).
Estudio integral de la técnica instrumental y de los recursos expresivos.%%
Síntesis: mirada sobre FOBA 1 y elaboración de material de aula (obra, estudio, adaptación, material audiovisual).

__A lo largo de la carrera__ -- Articulación con las materias troncales.%%
Espacio de la Práctica Docente:
a través de la socialización de proyectos y experiencias áulicas,
la participación activa de la cátedra en la discusión de estrategias metodológicas,
la producción de material musical para el aula.%%
Lenguaje Musical: a través del trabajo en el aula de Guitarra sobre la composición de obra,
la socialización del repertorio de la materia para el análisis armónico y formal.%%
Historia de la Música: a través del aporte del repertorio de obras de diferentes épocas a la
bibliografía.

# Marco metodológico

## Selección del repertorio

- En el aula, en un contexto de exploración y descubrimiento.
- Participación activa del alumno en la selección de obras y la elaboración de arreglos.
- A través de diversos formatos: partitura, audio, video.
- Inclusión de diferentes géneros: académicos, populares, folklóricos, urbanos, nuevas tendencias.
Permeabilidad a los materiales provenientes de otros espacios curriculares.

## Diagnóstico

- A través de la audición de material previamente trabajado.
- Discusión de técnicas de estudio, estrategias de abordaje de obra.
- Conocimiento y apropiación de materiales, técnicas, nomenclatura.

## Seguimiento

- Acuerdo de pautas de trabajo, tiempos, estrategias para el corto, mediano y largo plazo.
- Seguimiento: participativo, consensuado, autoevaluación y autogestión de tiempos y metodologías de estudio.
- Instancias de audición en el aula: como evaluación parcial de proceso, experiencia de tocar música en vivo,
recalibración de expectativas, tiempos y métodos.

# Conformación de los programas de examen

## Primer año

- Una obra grande, en un movimiento (preferentemente tripartita, duración mínima 4 minutos).
- 5 obras breves o microformas.
- Participación en una audición a fin de año.

## Segundo año

- Una obra grande, razonablemente compleja y demandante, de lenguaje accesible (en relación a
los contenidos de Lenguaje Musical del nivel).
- Una obra de raíz popular adaptada o arreglada durante el curso.
- 4 obras breves.
- Participación en una audición y/o concierto.

## Tercer año

- 2 obras grandes (duración mínima: 4 minutos), o una obra en varios movimientos,
técnicamente demandantes y de una complejidad acorde a las expectativas del nivel.
- 4 obras breves, técnicamente demandantes y complejas de acuerdo a las
expectativas del nivel.
- Participación en una audición y/o un concierto, como organizador del ciclo o intérprete.

## Cuarto año

- Una obra grande en varios movimientos.
- 2 obras grandes, demandantes y complejas en relación a las expectativas del nivel.
- 3 obras breves que presenten características y profundidad de trabajo de acuerdo al nivel.
- Una obra breve, o una producción de material de aula o contenido multimedia.
- Participación en una audición y/o concierto y/o en la organización del ciclo de Conciertos.

# Formato de los exámenes

Se proponen los siguientes formatos para los exámenes finales:

## Examen final

__Opción 1:__ Tocar las obras del programa frente a una mesa examinadora.%%
__Opción 2:__ Tocar parte del programa en audición o concierto, el resto frente a una mesa examinadora.  

## Examen libre

Tocar las obras del programa frente a una mesa examinadora.

## Promoción directa

Tocar las obras del programa en una o varias (no más de tres) audiciones o conciertos antes del cierre de año.%%
Se accedería a la promoción sólo si todo el material se audiciona en condiciones de aprobación con nota de promoción (7).  

__Opción 1:__ mesa examinadora presente en la audición.%%
__Opción 2:__ registro fílmico que permita una posterior evaluación por el equipo de cátedra.  

