@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-eaab %CARRERA% %NIVEL% %MATERIA% %HORAS% %ANHO%@

# Marco referencial

@lymd include src/%ID%_fund.md@

# Propósitos

@lymd include src/%ID%_prop.md@

# Contenidos

@lymd include src/%ID%_cont.md@

# Marco metodológico

@lymd include src/%ID%_meto.md@

# Cronograma

@lymd include src/%ID%_cron.md@

# Acreditación y evaluación

@lymd include src/%ID%_eval.md@

