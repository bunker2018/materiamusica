@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex

@lymd maketitlepage-eaab FOBA_de_Música Nivel_3 Instrumento_--_Guitarra 5 2019@

# Marco referencial

Acá va todo lo que pica y hace bailar.

Lo que enoja se mira de cerca, de lejos, un buen rato. Después se opera.

# Propósitos

Adónde vamos.

# Contenidos

## Por un lado

- Qué cosas,
- qué saberes,
- Qué saber haceres.

## Por otro lado

- Otra cosa,
- y otra,
- y otra.

# Marco metodológico

Cómo.

Dónde.

Con qué.

Qué o qué según qué.

# Cronograma

## Primera semana

### Segundo mes

#### Cierre de año

...y se nos va...

# Acreditación y evaluación

## Cursada regular

Estos son los que vienen a portarse bien. Así les va.

## Paso de los Libres

La gran mayoría.

## Semipresenciales e integrados

Cinco pa\\'l peso.

