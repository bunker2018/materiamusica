@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-eaab Profesorado_en_Música_--_Orientación_Instrumento Nivel_2 Instrumento_--_Guitarra 2 2019@

# Marco referencial

@lymd include src/g2_fund.md@

# Propósitos

@lymd include src/g2_prop.md@

# Contenidos

@lymd include src/g2_cont.md@

# Marco metodológico

@lymd include src/g2_meto.md@

# Cronograma

@lymd include src/g2_cron.md@

# Acreditación y evaluación

@lymd include src/g2_eval.md@

