@pandoc -Ss --chapters --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-eaab Profesorado_en_Música_--_Orientación_Instrumento Nivel_3 Instrumento_--_Guitarra 2 2019@

# Marco referencial

@lymd include src/g3_fund.md@

# Propósitos

@lymd include src/g3_prop.md@

# Contenidos

@lymd include src/g3_cont.md@

# Marco metodológico

@lymd include src/g3_meto.md@

# Cronograma

@lymd include src/g3_cron.md@

# Acreditación y evaluación

@lymd include src/g3_eval.md@

