@pandoc -Ss --chapters --toc --base-header-level 2 --template=/usr/lib/lymd/tex/template-oneside.tex -H /usr/lib/lymd/tex/academic-header.tex -B ./generated_titlepage.tex
@lymd maketitlepage-vertical Obras_de_referencia Material_de_consulta_ordenado_por_género Guitarra_--_todos_los_niveles 2019@


# Música para vihuela y laúd

## FOBA -- Primer nivel

@lymd include src/obras_gf1_laud.md @

## FOBA -- Segundo nivel

@lymd include src/obras_gf2_laud.md @

## FOBA -- Tercer nivel

@lymd include src/obras_gf3_laud.md @

## Profesorado -- Primer año

@lymd include src/obras_g1_laud.md @

## Profesorado -- Segundo año

@lymd include src/obras_g2_laud.md @

## Profesorado -- Tercer año

@lymd include src/obras_g3_laud.md @

## Profesorado -- Cuarto año

@lymd include src/obras_g4_laud.md @

# Música para guitarra del siglo XIX

## FOBA -- Primer nivel

@lymd include src/obras_gf1_s19.md @

## FOBA -- Segundo nivel

@lymd include src/obras_gf2_s19.md @

## FOBA -- Tercer nivel

@lymd include src/obras_gf3_s19.md @

## Profesorado -- Primer año

@lymd include src/obras_g1_s19.md @

## Profesorado -- Segundo año

@lymd include src/obras_g2_s19.md @

## Profesorado -- Tercer año

@lymd include src/obras_g3_s19.md @

## Profesorado -- Cuarto año

@lymd include src/obras_g4_s19.md @

# Música del siglo XX

## FOBA -- Primer nivel

@lymd include src/obras_gf1_s20.md @

## FOBA -- Segundo nivel

@lymd include src/obras_gf2_s20.md @

## FOBA -- Tercer nivel

@lymd include src/obras_gf3_s20.md @

## Profesorado -- Primer año

@lymd include src/obras_g1_s20.md @

## Profesorado -- Segundo año

@lymd include src/obras_g2_s20.md @

## Profesorado -- Tercer año

@lymd include src/obras_g3_s20.md @

## Profesorado -- Cuarto año

@lymd include src/obras_g4_s20.md @

# Música de raíz folklórica y popular

## FOBA -- Primer nivel

@lymd include src/obras_gf1_popu.md @

## FOBA -- Segundo nivel

@lymd include src/obras_gf2_popu.md @

## FOBA -- Tercer nivel

@lymd include src/obras_gf3_popu.md @

## Profesorado -- Primer año

@lymd include src/obras_g1_popu.md @

## Profesorado -- Segundo año

@lymd include src/obras_g2_popu.md @

## Profesorado -- Tercer año

@lymd include src/obras_g3_popu.md @

## Profesorado -- Cuarto año

@lymd include src/obras_g4_popu.md @

## Selección de canciones[^1]

[^1]: Ver prefacio del documento \`\`Obras de referencia''.

@lymd include src/obras_gf3_canc.md @

