- Acompañar al grupo en la construcción, progresiva y razonada, de una técnica
instrumental con proyección a las demandas de la orientación.
- Acercar al grupo a las diversas posibilidades estilísticas, técnicas y musicales
de la guitarra, desde la audición, la lectura y escritura y la ejecución.
- Incorporar las posibilidades que ofrecen las nuevas tecnologías como un vector
de renovación e intercambio de prácticas áulicas, material musical y de estudio,
estrategias metodológicas, etc., y de articulación con otros espacios curriculares,
carreras y lenguajes.
- Integrar la producción del curso a las instancias de muestra a lo largo del año
(audiciones, conciertos, muestra anual) y a la generación de contenidos.
