La implementación del presente espacio curricular en el contexto de la situación compleja,
coyuntural, que atraviesa la Institución, plantea una serie de desafíos e interrogantes.

En primer lugar, proyectar el desarrollo de un curso de Guitarra a lo largo de un año, en el
marco de la profunda crisis económica y social que afecta tanto al país y a la región como
al medio local, en las condiciones estipuladas por las normativas vigentes: condiciones de
regularidad, acreditación y promoción, regímenes de cursada, etc., supone serios problemas de
sustentabilidad, si lo que se quiere es propiciar la continuidad pedagógica y la igualdad
de oportunidades. Sin olvidar que buena parte de la matrícula de la Escuela proviene de las
capas socio--económicas más desfavorecidas, con las consiguientes dificultades de traslado,
adquisición de materiales y elementos para la cursada, disponibilidad de tiempo, entre otras.

Sumado a esto, la situación particular que vive la Escuela en el presente, a un año de instalarse
en la nueva Sede, sufriendo carencias apremiantes de recursos de todo tipo: espacio físico,
infraestructura, recursos económicos y humanos; todo esto en medio de un proceso de renovación
y cambio de paradigmas y de funcionamiento de la institución, de redefinición de sus propósitos
y alcances.

En este contexto, las nuevas tecnologías y canales de comunicación funcionan de hecho
como catalizador de la actividad pedagógica y de la interacción coordinada entre las diferentes
áreas, espacios curriculares, carreras y lenguajes.
En la actualidad existen medios de producción y registro del material musical, escrito o audiovisual,
con herramientas que vemos a diario en el aula;
a través de los canales de comunicación que provee internet
(mensajería, páginas web, plataformas y aplicaciones), personas y grupos pueden compartir y hacer accesible
en tiempo real materiales, procesos, ideas y pautas de trabajo, para así
coordinar e integrar el trabajo y la producción de las diferentes disciplinas.

Este cambio de paradigma, naturalmente, pone en cuestión la
pertinencia de las viejas prácticas metodológicas:
el planteo de una cursada presencial con la actividad centralizada en el aula física;
la selección _a priori_ del material de obras y estudios, tomados de un repertorio cuya
adecuación al medio local y al tiempo presente es, cuanto menos, forzada y difícil;
un recorte de contenidos realizado sobre este mismo repertorio de obras, y diseñado
también _a priori_ para satisfacer las demandas de interpretación y resolución técnica
de estas mismas obras, mirando de soslayo las prácticas instrumentales y los valores
musicales del repertorio popular y del material propio del aula de música;
la des--integración de la carrera en islas curriculares, impermeables
al intercambio y el diálogo entre prácticas, materiales, lenguajes.

En el caso particular del 3° nivel de la Formación Básica, se nos presenta la necesidad imperiosa
de replantear las prácticas y los materiales del curso
en relación a las especificidades de la orientación en
Educación Musical. Además de la necesaria revisión del repertorio de obras, se impone acercar el
trabajo de la materia a otras del nivel y de la carrera, en especial los Instrumentos
Armónicos, las Prácticas de Conjunto y los talleres.

A partir de lo expuesto, la propuesta consiste en plantear un espacio curricular permeable y adaptable,
con propósitos claros en relación a contenidos bien definidos, pero sobre todo abierta y alerta a las
posibilidades, algunas insospechadas, que se nos presentarán a diario, en el aula y en los espacios
virtuales y extracurriculares. Utilizando todos los medios al alcance de la cátedra y del alumno
para resolver las dificultades que nos plantee el desarrollo del curso: medios tecnológicos, estrategias
de articulación e integración, flexibilidad en los regímenes de cursada, en función de garantizar la continuidad
pedagógica, la calidad educativa y la adecuada inserción de la materia Guitarra en el contexto de la FOBA
y de las carreras. Todo esto en función del propósito central, troncal, de la materia Instrumento:
_hacer música_ con la guitarra, música de guitarra en el aula de música y en los espacios físicos
y virtuales que le son afines. Colaborar también con las actividades que involucren a la guitarra en
otros espacios, curriculares y extracurriculares, físicos y virtuales, a través de la producción y
socialización de contenidos diversos en su alcance y posibilidades.

