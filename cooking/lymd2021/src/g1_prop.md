- Acompañar al alumno en la profundización y consolidación
de las herramientas técnicas, expresivas y conceptuales adquiridas
durante la Formación Básica.
- Proponer un conjunto de estrategias y herramientas
para el estudio de la técnica instrumental
con énfasis en la conciencia del cuerpo y la observación y registro de sensaciones.
- Acercar al alumno a las diversas posibilidades estilísticas, técnicas y musicales
de la guitarra, desde la audición, la lectura y escritura y la ejecución.
- Generar y llevar a cabo un proyecto musical consistente en la realización
de una audición o concierto sobre el final del ciclo.
- Incorporar las posibilidades que ofrecen las nuevas tecnologías como un vector
de renovación e intercambio de prácticas áulicas, material musical y de estudio,
estrategias metodológicas, etc., y de articulación con otros espacios curriculares,
carreras y lenguajes.
- Integrar la producción del curso a las instancias de muestra a lo largo del año
(audiciones, conciertos, muestra anual) y a la generación de contenidos.

