En el cuarto y último año de la Orientación Instrumento,
la madurez técnica y musical lograda a lo largo de la carrera
nos permite abordar un repertorio de obras de gran envergadura,
en particular la música para vihuela y laúd (antepasados de la
guitarra romántica y moderna), inaccesibles para el estudiante en los primeros años de la formación
ya sea por la demanda técnica que presentan o por la difícil comprensión del texto musical.
Este repertorio, en particular la música para laúd de J. S. Bach y
del Renacimiento, se
reservan para esta etapa de la carrera, en la que nos hallamos en condiciones
de abordar apropiadamente obras en varios movimientos (suites),
y de trabajar y realizar las ornamentaciones típicas de la música de este período.

La música para guitarra del siglo XX presenta similares características,
al presentar exigencias técnicas considerables y un lenguaje hermético y difícil.
Se presenta en este cuarto año una oportunidad para abordar este repertorio,
cuando en Lenguaje Musical se trabajan precisamente los lenguajes no
tonales y los planteamientos formales y estéticos de esta música,
proporcionándonos la posibilidad de articular e integrar el trabajo no sólo
de la interpretación de obras sino, y sobre todo, de la composición de material
original.

Al igual que en el tercer año de la carrera, lo que se busca no es
hacer más difícil y trabajoso el recorrido del curso, sino establecer una
conexión entre los diversos espacios curriculares del nivel,
las herramientas y capacidades ya adquiridas
y el material que se trabaja en Guitarra, que se pretende sea significativo
para el trayecto formativo y articulable entre las diferentes disciplinas de la carrera.
De igual manera se buscará, como continuación del trabajo de tercer año,
hacer lugar a la reflexión crítica y constructiva sobre los materiales, estrategias y metodologías
existentes.

La propuesta para este cuarto año de la Orientación consistirá, además del trabajo
sobre el material de obras del curso, en buscar una síntesis de lo adquirido a lo largo de la carrera
volviendo la mirada al aula de guitarra de la Formación Básica,
a través de la producción de material de aula que sirva de apoyo para
el trabajo en la Práctica Docente, tanto para las actividades de aula durante
las prácticas como en forma de insumos para la discusión y construcción de metodologías
y estrategias para el curso. De igual manera, el estudio de la técnica instrumental
se referirá en este punto del trayecto a los desafíos que se presentan al docente
de Instrumento, más que a la adquisición de una destreza técnica considerable.

El presente proyecto de cátedra, así como los correspondientes a los años anteriores de la
carrera, se pondrá a consideración del estudiante, que en su calidad de profesional en el último
estadío de su formación estará en condiciones de participar activamente en la
discusión y elaboración de alternativas y soluciones para la carrera y la institución.
Colaborando así con la construcción colectiva de un trayecto formativo
de excelencia y adecuadamente posicionado en el medio local y en la Escuela.
