En el primer año del trayecto formativo de la Orientación Instrumento,
se consideran adquiridos los mecanismos y rudimentos básicos de la técnica instrumental,
un nivel aceptable de lectura a primera vista y abordaje de obra,
y un manejo de los elementos del lenguaje musical acorde al nivel.

Se ha señalado además, como representativo de los estudiantes de Música en general,
una tendencia a la introversión, la poca participación en clase y en el debate grupal, y
una clara "desconexión" con el cuerpo y con la _sensación_.

A partir de esta base, el propósito del presente curso consiste en
propiciar la consolidación y apropiación por parte del alumno
de las capacidades técnicas y musicales ya adquiridas,
y asimismo una apertura _hacia afuera_
(la música como una experiencia compartida y como un canal de comunicación)
y _hacia adentro_
(la exploración individual, interior, de las sensaciones involucradas tanto
en la ejecución --la técnica, como en el acto expresivo
--el discurso musical y la ejecución en vivo).

El objetivo concreto del curso (el _proyecto_) será la preparación, hacia el final del ciclo, de una
presentación en vivo (audición o concierto).
En este trabajo se pondrán en juego el compromiso individual del
alumno con el proyecto musical, en sus aspectos técnicos, estéticos y conceptuales,
y con su realización concreta.
En este marco se buscará dar protagonismo a la autogestión
en el diseño de estrategias de estudio, tiempos y objetivos parciales,
y en la elaboración del repertorio.

De esta manera, a través de un proyecto musical que involucra al estudiante
de Música como protagonista y no como un mero "reproductor",
y que además incluye al público como una pieza esencial y prevista desde el comienzo,
se espera otorgar significatividad a la producción musical de la carrera en relación
al medio local y a la comunidad de la Escuela, y asimismo que sirva de preparación
al estudiante para las demandas de los años posteriores de la carrera.

Por último, al trabajo sobre la técnica instrumental se le sumará una experiencia
de articulación con el espacio de Trabajo Corporal, con la mirada puesta sobre
la conciencia del cuerpo, la observación interior y el registro de las sensaciones,
con el propósito de enriquecer y renovar las herramientas conceptuales y expresivas
de que disponemos para el estudio del instrumento.
%% Aportando a la
%% significatividad de Trabajo Corporal en la carrera y abriendo eventualmente el
%% juego a la articulación con otros espacios.

