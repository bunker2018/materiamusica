## Unidad 1: El cuerpo y el instrumento. Técnica básica de ambas manos.

- Grupos musculares. Concepto de fijación.
- Construcción de la posición. Apoyos. Relajación, flexibilidad, movilidad, tonicidad. Posiciones altas y bajas.
- Construcción de la posición del brazo. Cambios de presentación. Traslados de mano izquierda: transversal y longitudinal. Traslados de mano derecha: registros tímbricos.

## Unidad 2: Técnica de ambas manos.

- Técnica de mano izquierda. Cejilla. Distensión y contracción. Posición fija. Escalas. Escalas con dedos fijos. Ligados.
- Técnica de mano derecha. Trabajo del pulgar. Toque apoyado de pulgar y dedos. Rasgueos. Arpeggiato. Toque libre de pulgar y dedos. Línea. Arpegios. Figueta. Pizzicato.
- Traslados de mano derecha: registros tímbricos.
- Armónicos naturales y artificiales. Diferentes posibilidades de realización.

## Unidad 3: Criterios de digitación

- Digitación del pasaje. Criterios técnicos y musicales. La cuerda al aire. Solución de continuidad.

## Unidad 4: Recursos musicales

- Recursos interpretativos en relación a los siguientes parámetros:
	%TT- Intensidad: crescendo, decrescendo, niente, subito, pp, ff.
	%TT- Timbre: ponticello, sul tasto. Toques de uña y de yema. Cuerda al aire. Armónicos. Valor tímbrico del intervalo. Registros tímbricos según cuerda y posición.
	%TT- Organización temporal: accelerando, rallentando, ritenuto, fermata, cambios de tempo, rubato.
	%TT- Articulación: staccato, legato, non legato, acentos.
- Regularidad. Continuidad. Gradualidad. Contraste. Extremos.
- Textura: diferenciación de planos sonoros; polifonía a dos voces; melodía con acompañamiento; diseños.
- Ornamentación. Mordente, trino, apoyatura, glissando, portamento.
- Carácter de la obra. Planteos estilísticos, técnicos, estéticos.

## Unidad 5: Especificidades del instrumento

- La escritura para guitarra. Transposición de octava. Sonidos armónicos. Digitación de ambas manos.
Ligadura de frase.
- Topografía del instrumento. Las notas en toda la extensión del diapasón. Reducción del material
(mapeo del material de alturas al diapasón). Registros dinámicos, tímbricos, modos de acción.
Afinación tradicional. Afinaciones alternativas.
- Técnicas extendidas. Modos de acción propios de la música popular, folklórica y urbana. La percusión
sobre el cuerpo del instrumento y sobre el encordado. Instrumentos de cuerda pulsada afines a la guitarra.

