La propuesta para el segundo año de la carrera de Instrumento
consiste en una adaptación de los materiales y metodologías existentes
a los espacios curriculares de producción del año,
en especial el Taller de Música Popular,
con el propósito de
renovar el repertorio de obras
y otorgar significatividad al trayecto formativo en este punto de la carrera.

Se buscará incluir en el programa de obras un material predominantemente de raíz popular,
que permita un trabajo conjunto y articulado del repertorio en diversos espacios,
de manera de lograr una convergencia de las diversas miradas y usos característicos:
recursos de análisis, herramientas técnicas, criterios expresivos y formales,
autogestión en el proceso de preparación, ensayo y arreglo.

En este contexto, se hará especial énfasis en la lectura de la partitura
como un "texto abierto", no ya como una obra cerrada que espera ser reproducida tal como es,
sino como una invitación a resignificarla, reelaborarla y adaptarla a una interpretación
propia, personal, haciendo uso de los aportes del campo popular.
De igual manera se recurrirá a las herramientas conceptuales trabajadas en Lenguaje Musical
en el análisis y la lectura comprensiva del texto.

La producción musical del curso se volcará en un concierto o audición sobre el final del ciclo,
continuando el proceso iniciado en el primer año de la carrera, orientando el trabajo
a una realización musical más relajada y libre y a la producción de material propio,
a través de la generación de arreglos, la reelaboración del texto, la inclusión
de pasajes _ad libitum_ (improvisados) y la diversificación del recurso técnico--instrumental
(rasgueos, técnicas extendidas, criterios de interpretación).
Se espera lograr de esta forma un trabajo musical más adecuado a las expectativas y demandas
de la institución y del alumno,
y a la vez introducir un elemento de renovación y cambio a la estructura de la
carrera de Instrumento, tanto en el repertorio de obras como en las metodologías de trabajo
y los criterios de análisis y realización musical.

