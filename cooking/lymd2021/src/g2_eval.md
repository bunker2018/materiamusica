## Acreditación de la materia

### Régimen de cursada presencial

Para acreditar el espacio curricular se deberá cumplir con la entrega y aprobación
en tiempo y forma de los trabajos prácticos
de cada unidad, y registrar la asistencia a un 80% de las clases. 

La calificación necesaria para lograr la acreditación es de 4 (cuatro).

### Régimen de cursada semipresencial

Se deberá acordar con la cátedra no menos de ocho encuentros a lo largo del ciclo lectivo
(tres encuentros por cuatrimestre como mínimo), separados entre sí por un máximo de seis (6) semanas.
Para acreditar el espacio curricular se requerirá la asistencia a la totalidad de los encuentros
pautados, así como la entrega en tiempo y forma de los trabajos indicados por la cátedra.

La calificación necesaria para lograr la acreditación es de 4 (cuatro).

## Programa de examen

- Una obra grande, razonablemente compleja y demandante, de lenguaje accesible (en relación a
los contenidos de Lenguaje Musical del nivel).
- Una obra de raíz popular adaptada o arreglada durante el curso.
- 4 obras breves.
- Participación en una audición y/o concierto.

El armado del programa de examen podrá adaptarse a cada caso particular, según el criterio de
la cátedra y tomando como referencia el modelo aquí propuesto.

### Examen libre

En el caso de rendir en condición de libre, al programa se le agregará una pieza breve o microforma.

## Examen final

El examen final consistirá en la ejecución del repertorio trabajado durante el año ante una mesa examinadora
integrada por un mínimo de dos docentes. Se evaluará en esta instancia el desempeño técnico y musical en
relación a los contenidos del nivel, teniendo en cuenta el proceso de aprendizaje en cada caso particular.

