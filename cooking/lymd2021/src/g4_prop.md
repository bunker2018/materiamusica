- Proponer un repertorio de obras acorde a las capacidades adquiridas
en las instancias previas de la formación,
que posibilite además un trabajo conjunto y articulado con los
espacios curriculares afines del nivel.
- Acompañar al alumno en la elaboración de una síntesis
de las herramientas técnicas, expresivas y conceptuales adquiridas
en los años previos de la carrera, con la mirada puesta en el
desempeño del futuro profesional de la educación en Música.
- Ofrecer al alumno la oportunidad de discutir las propuestas de la cátedra
para el trayecto formativo de la Orientación y de participar activamente
en la construcción del proyecto de la carrera de Música.
- Incorporar las posibilidades que ofrecen las nuevas tecnologías como un vector
de renovación e intercambio de prácticas áulicas, material musical y de estudio,
estrategias metodológicas, etc., y de articulación con otros espacios curriculares,
carreras y lenguajes.
- Integrar la producción del curso a las instancias de muestra a lo largo del año
(audiciones, conciertos, muestra anual) y a la generación de contenidos.

