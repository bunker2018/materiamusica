La materia consistirá en una clase semanal individual de dos horas.
Se iniciará con la presentación del proyecto anual
y la preselección de un repertorio de material de obras para guitarra solista de diversos géneros,
que servirá de base para la preparación del material del curso.
Se destinarán una o dos semanas más para un primer acercamiento al material (lectura, indicaciones
y ajustes generales)
y para realizar el diagnóstico inicial.
En esta primera etapa se acordarán los lineamientos y objetivos puntuales del curso,
que surgirán del diálogo entre el proyecto propuesto por la cátedra
y la objetivación por parte del alumno de sus inquietudes, expectativas y desafíos individuales
en este punto de su formación.

Durante el transcurso del año, se alternará el trabajo de los contenidos del curso
con las instancias de preparación y muestra del material.
Cada unidad o bloque de contenidos se abordará en función de un cronograma flexible,
consistente en una planificación tentativa de plazos y objetivos parciales
que se ajustará orgánicamente a la evolución del proyecto en cada caso particular.

El resultado del trabajo sobre cada contenido específico se trasladará
a la elaboración y preparación del repertorio de obras
(_marcado_ progresivo y acumulativo de la partitura)
y a la construcción y planificación de rutinas de estudio
y estrategias de abordaje de obra;
conjuntamente, se buscará integrar el trabajo estrictamente musical
con una proyección al desempeño del docente de música
desde la reflexión y discusión de metodologías y actividades,
recorte de contenidos sobre diferentes materiales musicales,
y la elaboración de material de aula como parte del programa de obras del curso.

Se buscará promover una cursada dinámica, cuya continuidad no dependa de los imponderables
que eventualmente alteren el cronograma anual,
haciendo accesible _online_ la totalidad del material del curso y propiciando espacios virtuales que
permitan al alumno y a la cátedra canalizar consultas, realizar entregas, compartir material,
etc.,
aun cuando las circunstancias dificulten o imposibiliten los encuentros pautados.

