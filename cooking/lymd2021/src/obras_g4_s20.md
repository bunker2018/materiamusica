- Manuel de Falla: _Homenaje a Debussy_.
- Toru Takemitsu: _Beatles for guitar_.
- M. M. Ponce: _Sonata III; Sonatina Meridional_.
- L. Brouwer: _El Decamerón Negro; Elogio de la danza; Tarantos_.
- R. Dyens: _Libra Sonatine_.
- J. Rodrigo: _En los trigales_.
- C. Domeniconi: _Suite Koyunbaba_.
- Abel Carlevaro: _Preludio N o 5; Cronomías_.
- Ginastera: _Sonata op.47_.
