- Proponer un repertorio de obras acorde a las capacidades adquiridas
en las instancias previas de la formación,
que posibilite además un trabajo conjunto y articulado con los
espacios curriculares afines del nivel.
- Acompañar al alumno en la profundización y consolidación
de las herramientas técnicas, expresivas y conceptuales adquiridas
en los años previos de la carrera.
- Proponer un conjunto de estrategias y herramientas
para un estudio profundo y razonado de la técnica instrumental,
posibilitando a la vez un posicionamiento reflexivo y activo sobre
la enseñanza de la técnica y las posibilidades expresivas y musicales del instrumento.
- Generar y llevar a cabo un proyecto musical consistente en la preparación
de una obra de extensión y complejidad acordes al nivel.
- Incorporar las posibilidades que ofrecen las nuevas tecnologías como un vector
de renovación e intercambio de prácticas áulicas, material musical y de estudio,
estrategias metodológicas, etc., y de articulación con otros espacios curriculares,
carreras y lenguajes.
- Integrar la producción del curso a las instancias de muestra a lo largo del año
(audiciones, conciertos, muestra anual) y a la generación de contenidos.

