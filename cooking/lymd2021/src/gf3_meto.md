La materia consistirá en una clase semanal de dos horas. en grupos de hasta cuatro alumnos.
Se iniciará con la presentación del proyecto anual
y la preselección de un repertorio de material de obras y/o canciones en diversos formatos,
que servirá de base al grupo para la preparación del material del curso.
Se destinarán una o dos semanas más para un primer acercamiento al material (lectura, indicaciones
y ajustes generales)
y para realizar el diagnóstico inicial del grupo.

Durante el transcurso del año, se alternará el trabajo de las unidades de la materia
(dos al inicio del primer cuatrimestre, una promediando el segundo cuatrimestre)
con las instancias de preparación y muestra del material. Cada unidad consistirá en
una serie de trabajos prácticos, en cada uno de los cuales se profundizará un contenido
o bloque de contenidos
integrando en lo posible recepción, conceptualización, interpretación y producción
(improvisación/composición);
el resultado de estas experiencias se trasladará al trabajo sobre
el repertorio del grupo (_marcado_ progresivo y acumulativo de la partitura)
y a la construcción y planificación de rutinas de estudio.

Se prevé incluir en el repertorio de obras todo aquel material musical que se adecúe al trabajo de los
contenidos propuestos: obras y estudios para guitarra solista, adaptaciones realizadas en clase o en
otras materias, material para voz y guitarra, realización de cifrados de acordes, trabajos
rítmicos y texturales pautados (especies rítmicas, diseños texturales). Siempre considerando el
abordaje de la música escrita como fundamental en la formación musical y profesional.

Se buscará promover una cursada dinámica y resiliente a los avatares del grupo y del año,
haciendo accesible _online_ la totalidad del material del curso y propiciando espacios virtuales que
permitan al grupo y a la cátedra canalizar consultas, realizar entregas, compartir material,
etc.,
aun cuando las circunstancias dificulten o imposibiliten los encuentros pautados.

