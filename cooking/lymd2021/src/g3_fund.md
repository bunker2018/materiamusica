En el tercer año de la Orientación Instrumento
nos hallamos en posesión de una serie de herramientas y habilidades
ya desarrolladas y afianzadas, en los aspectos técnico--instrumentales,
musicales y expresivos, y en el campo de la formación docente.
Es en este año donde comenzamos a realizar prácticas en el aula,
poniendo en juego metodologías y estrategias de enseñanza--aprendizaje
lo que posibilita un posicionamiento más activo, reflexivo y crítico
del alumno hacia la propuesta de la cátedra de Instrumento.

En otros espacios curriculares del nivel (Lenguaje Musical, Técnicas de
Improvisación) se amplían, profundizan y diversifican las herramientas
conceptuales y de análisis y las estrategias de abordaje y estudio de los
materiales. Accedemos así al lenguaje armónico del Romanticismo
(tonalidad ampliada o extendida), en coincidencia con el trabajo en
Técnicas de Improvisación sobre los materiales armónicos y escalísticos del _jazz_.

Todo esto nos posibilita acceder a un repertorio de obras de mayor extensión y complejidad,
con el desafío y la posibilidad de crecimiento que esto implica para la formación musical.
No se pretende con esto agregar simplemente "dificultad" o "exigencia" al repertorio,
sino hacer posible una transversalidad de contenidos, posicionando la materia en
relación a los demás espacios del nivel de manera que resulte significativa
para el trayecto formativo, y a la vez proporcionando desafíos acordes a las
capacidades musicales y técnicas del alumno.

El proyecto musical propuesto para este momento de la carrera consiste en la
preparación de una "gran obra", esto es, una pieza musical razonablemente extensa,
con una complejidad técnica y musical adecuada al nivel, y que por sus
características (material armónico y melódico, forma, marco estilístico)
propicie la articulación del trabajo con otros espacios curriculares.

Se propone también incluir en el programa de la materia, además de la interpretación
de obras, la elaboración de material musical apropiado para el aula,
donde el alumno pondrá en juego lo adquirido durante su formación
apuntando al desempeño docente en la adaptación y renovación de los materiales
y el diseño de sus propias estrategias metodológicas.

