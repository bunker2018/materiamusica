Este trabajo se propone como un material de consulta, tanto para el docente como para el
estudiante de Guitarra del Profesorado (Orientación Instrumento) y Tecnicatura, y consiste
en una selección de estudios y obras de referencia, con el propósito de colaborar con el
armado del repertorio de los correspondientes cursos y/o programas de examen de los diferentes
niveles.

Cabe aclarar que las obras seleccionadas aquí no suponen una imposición de un repertorio en
particular; deben entenderse más bien como una referencia de las pautas técnicas y musicales
que representan los diferentes niveles de la carrera. Vale decir que un programa de examen
que no contenga ninguna obra de las propuestas aquí, puede ser enteramente válido en tanto
cumpla con las exigencias propias del nivel.

El material se ha seleccionado y secuenciado de acuerdo a los siguientes criterios:

- dificultad de ejecución: operadores y mecanismos técnicos;
- tempo, recursos métricos y temporales;
- extensión;
- accesibilidad del lenguaje (recursos tonales, texturales, formales);
- variedad de géneros y estilos de composición;
- inclusión de los géneros, recursos técnicos, planteos texturales, etc., más representativos del repertorio guitarrístico.

La secuenciación del material supone la dificultad de encuadrar una determinada obra en un único
nivel de la carrera; de hecho, lo más habitual es que una obra sea apropiada para dos o tres niveles
consecutivos de la carrera, incluso más. Por lo tanto, la secuenciación propuesta es bastante flexible,
y se recomienda consultar, además del nivel de referencia, los niveles previos y posteriores. El tercer
nivel de la Formación Básica, por ejemplo, incluye obras que bien pueden trabajarse en el primer o
segundo año de la carrera, atendiendo a las necesidades de cada caso en particular.

Una recomendación a los estudiantes que recurran a este material para elaborar un programa de examen libre
o terminar de armar un repertorio incompleto:
**siempre** consultar a la cátedra antes de sentarse a estudiar las obras.
De esta forma nos ahorraremos (todos) la frustración de tirar a la basura semanas o meses de trabajo
por ahorrarnos la molestia de realizar una consulta a tiempo.

Por último, cualquier sugerencia o corrección que contribuya a la mejora de este trabajo será bienvenida y
agradecida.

\\flushright
Matías Diego\\linebreak
Marcelo López Minnucci\\linebreak
Diego Magnín\\linebreak
Sebastián Molteni
\\flushleft
\\pagebreak
