## Unidad 1: Lectura y abordaje de obra

- Lectura comprensiva del texto musical en función del marco estilístico.
- Interpretación del material rítmico: fórmulas rítmicas, claves, ornamentaciones típicas, planteos texturales.
- Interpretación armónica del texto: reducción, cifrado de acordes, material escalístico.
- Recorte y diversificación del recurso técnico--instrumental en función del género (reelaboración del pasaje).
- Abordaje de la obra como una realización provisional de la composición (texto ``abierto'').

## Unidad 2: Aspectos técnico--instrumentales

- Recursos técnico--instrumentales típicos de la música de raíz popular:
%TT- rasgueos;
%TT- percusión sobre encordado y caja;
%TT- articulaciones;
%TT- _bends_, _tapping_;
%TT- ornamentaciones típicas.
- _Pizzicato_, armónicos, técnicas extendidas: notación y realización.

## Unidad 3: Aspectos musicales y preparación de la obra

- Plan formal: exploración de las posibilidades formales del género.
Introducción, coda, puentes.
Secciones _ad libitum_.
Estructura y ornamentación.
- Plan textural: exploración y diversificación de las texturas típicas.
Realización del ritmo con un mínimo de material (reducción).
Aplicación de las técnicas extendidas como recurso tímbrico y textural.
- Reelaboración/replanteo de la partitura para guitarra sola como parte de un ensamble.
- Elaboración de una segunda guitarra sobre la obra a solo.

