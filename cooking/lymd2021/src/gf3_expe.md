- Que el grupo desarrolle un conocimiento y dominio de la técnica de la guitarra que le permita
abordar los contenidos de la orientación con solvencia e independencia.
- Que el grupo adquiera los hábitos y técnicas de estudio necesarios para proyectar y administrar
el trayecto propuesto en el plan de estudios de la orientación.
- Que el grupo desarrolle un manejo de las posibilidades estilísticas y musicales de la guitarra
que le permita lograr una producción musical de calidad, en el marco de las demandas de la
orientación.
- Que el grupo logre producir un material musical para guitarra solista
en el que se pongan de manifiesto los saberes y habilidades adquiridos a lo largo del curso.
- Que el grupo adquiera la solvencia y confianza necesarias para socializar el resultado del trabajo
musical del curso, en audiciones, en el aula de música y en las instancias de examen.

