## Primer cuatrimestre

### Diagnóstico y presentación de la materia.

Exploración y selección del material. Diagnóstico.%%
Lectura preliminar del material. Digitación.

### Lectura y abordaje de obra

Lectura y digitación del material.%%
Preparación y marcado de la partitura. Planificación del trabajo.%%
Plan de interpretación, marco estilístico y estético.

### Aspectos técnico--instrumentales

Estudio integral de la técnica del instrumento.%%
Recorte y secuenciación de los mecanismos y operadores técnicos.%%
Rutinas y estrategias de estudio.

### Cierre del cuatrimestre

Selección del material del segundo cuatrimestre.%%
Audición y cierre de cuatrimestre.%%
Evaluación de proceso, pautas de trabajo. Indicaciones locales.

## Segundo cuatrimestre

### Aspectos musicales

Trabajo interpretativo: puesta en obra de los recursos técnicos y musicales en función del
plan de interpretación.%%
Elaboración y realización del plan de obra.

### Elaboración de material de aula

Composición y adaptación de estudios y obras breves.%%
Propuestas de trabajo. Recorte y secuenciación de contenidos.%%
Diseño de materiales originales y adaptación de obras de diversa procedencia.

### Cierre del año

Revisión y preparación del repertorio.%%
Evaluación de proceso. Indicaciones locales.

Audición y cierre del curso.%%
Instancia de recuperación.



# Destinatarios

Estudiantes de 4° año del Profesorado en Música -- Orientación Instrumento de la Escuela de Arte "Alcides Biagetti".

Estudiantes de 4° año de la Tecnicatura en Capacitación Instrumental de la Escuela de Arte "Alcides Biagetti".

# Recursos

- Guitarras; banquito; atriles.
- Material de estudio (partituras; trabajos prácticos y apuntes elaborados por la cátedra).
- Plataformas _online_ organizadas y/o elaboradas por la cátedra.

