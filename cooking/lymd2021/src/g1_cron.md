## Primer cuatrimestre

### Diagnóstico y presentación de la materia.

Exploración y selección del material. Diagnóstico.%%
Lectura preliminar del material. Digitación.

### Unidad 1: Lectura y abordaje de obra

Lectura y digitación del material.%%
Preparación y marcado de la partitura. Planificación del trabajo.%%
Plan de interpretación, marco estilístico y estético.

### Unidad 2: Aspectos técnico--instrumentales

Mecanismos. Operadores técnicos. Grupos musculares.%%
Rutinas y estrategias de estudio.

### Cierre del cuatrimestre

Selección del material del segundo cuatrimestre.%%
Audición y cierre de cuatrimestre.%%
Evaluación de proceso, pautas de trabajo. Indicaciones locales.

## Segundo cuatrimestre

### Unidad 3: Aspectos musicales

Trabajo interpretativo: puesta en obra de los recursos técnicos y musicales en función del
plan de interpretación.%%
Elaboración del plan de obra en función de la situación de concierto.

### Cierre del año

Revisión y preparación del repertorio.%%
Evaluación de proceso. Indicaciones locales.

Audición y cierre del curso.%%
Instancia de recuperación.

# Destinatarios

Estudiantes de 1° año del Profesorado en Música -- Orientación Instrumento de la Escuela de Arte "Alcides Biagetti".

Estudiantes de 1° año de la Tecnicatura en Capacitación Instrumental de la Escuela de Arte "Alcides Biagetti".

# Recursos

- Guitarras; banquito; atriles.
- Material de estudio (partituras; trabajos prácticos y apuntes elaborados por la cátedra).
- Plataformas _online_ organizadas y/o elaboradas por la cátedra.

