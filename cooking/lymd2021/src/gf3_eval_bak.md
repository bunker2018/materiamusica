## Acreditación de la materia

Para acreditar el espacio curricular se deberá cumplir con la entrega y aprobación de los trabajos prácticos
de cada unidad.

La calificación necesaria para lograr la acreditación es de 4 (cuatro). En caso de obtener una calificación de 7 (siete) o más se podrá acceder a la promoción directa de la materia (ver más abajo).

## Programas de examen

### FOBA orientado a Instrumento

- Una obra relativamente extensa, técnicamente demandante de acuerdo a los contenidos del nivel.
- Cuatro piezas breves o estudios.
- Variedad de recursos técnicos, géneros y contextos musicales.

### FOBA orientado a Educación Musical

- Un total de seis obras.
- Una selección de canciones representativas de los contenidos y expectativas del nivel.
- Una selección de piezas breves o estudios que complementen la preparación técnica y musical
de acuerdo a los requerimientos de la orientación.

### Examen libre

En el caso de rendir en condición de libre, al programa se le agregará una pieza breve, estudio o canción.

## Examen final

El examen final consistirá en la ejecución del repertorio trabajado durante el año ante una mesa examinadora
integrada por un mínimo de dos docentes. Se evaluará en esta instancia el desempeño técnico y musical en
relación a los contenidos del nivel, teniendo en cuenta el proceso de aprendizaje en cada caso particular.

## Promoción directa

Para acceder a la promoción directa de la materia, el alumno deberá presentar el repertorio trabajado durante
el curso en una o dos audiciones o conciertos antes del cierre de año.

Se accederá a la promoción sólo si todo el material se audiciona en condiciones de aprobación con nota de promoción (7).

Para que la presentación del material valide la promoción se deberá cumplir con los siguientes requisitos:

__Opción 1:__ mesa examinadora presente en la audición.

__Opción 2:__ registro fílmico que permita una posterior evaluación por el equipo de cátedra.

