## Aspectos técnico--instrumentales

- Operadores. Ejercicio cero. Construcción del _ejercicio_ (cf. E. Fernández).
- Realización de pasajes técnicamente complejos. Integración en el discurso musical.
- Técnicas extendidas: trémolo, ornamentaciones, diferentes tipos de _glissandi_,
técnicas específicas de diferentes géneros y épocas.

## Aspectos musicales y preparación de obra

- Gran obra: lectura, digitación y preparación del texto.
- La música de guitarra en el Romanticismo (Tárrega, Llobet).
Notaciones específicas.
Ornamentación (glissando, portamento).
- Realización de texturas complejas.
Polifonía.
Tonalidad extendida.
- Realización de la forma en obras grandes. Relación entre secciones y movimientos.
Del planteo formal global a las decisiones locales.

## Contenidos transversales a otros espacios curriculares

- Análisis y mapeo del material melódico/armónico.
- Reelaboración de pasajes y secciones a partir de la improvisación.
- Reelaboración del material armónico a través del análisis.

