## Aspectos técnico--instrumentales

- Estudio integral de la técnica del instrumento. Análisis, digitación y resolución de pasajes complejos.
Diseño de ejercicios y estrategias de práctica.
- Análisis técnico y caracterización de obras y pasajes en relación al trayecto del FOBA y de la carrera.
- Elaboración de material de aula. Composición y adaptación de estudios y obras breves.
Recorte y secuenciación de los mecanismos y operadores técnicos.

## Aspectos musicales y acercamiento al repertorio

- La obra en varios movimientos. Planteo formal global y decisiones locales.
Integración de los movimientos individuales en el plan formal.
- La música para vihuela y laúd. Acercamiento al repertorio. Ornamentaciones típicas.
- Diseño y realización de pasajes ornamentados.
- Nuevos lenguajes: desarticulación/deconstrucción de la forma.
El timbre como parámetro dominante.
Discursos no tonales.
- Notación de la música de guitarra en el siglo XX. Nuevos lenguajes. Técnicas extendidas.

