## Primer cuatrimestre

### Diagnóstico y presentación de la materia.

Exploración y selección de material. Diagnóstico.%%

### Unidad 1: Aspectos técnico--instrumentales

Exposición y adquisición de la técnica del instrumento.%%
Lectura preliminar del material. Digitación.
_Marcado: ejercicios, rutinas._

### Unidad 2: Aspectos musicales

Globalidad. Forma. Gesto. Carácter. Abordaje de obra. Estrategias de estudio.%%
_Marcado: forma, parámetros globales, carácter._%%

Realización técnica. Ejercicios. Lectura y construcción de estudios.%%
_Marcado: pasajes; resolución; ejercicios. Digitación. Pautas de trabajo._

### Cierre del cuatrimestre

Selección del material del segundo cuatrimestre.%%
_Marcado: forma, parámetros globales, carácter. Digitación, pasajes. Pautas de trabajo._

Audición y cierre de cuatrimestre.%%
_Marcado: seguimiento, pautas de trabajo. Indicaciones locales._

## Segundo cuatrimestre

### Unidad 3: Especificidades del instrumento

La escritura para guitarra. Transposición de octava. Sonidos armónicos.%%
Topografía del instrumento. Registros dinámicos, tímbricos, modos de acción.
Afinaciones.%%
_Marcado: pautas texturales, resolución de cifrados, material, mapeos._

### Cierre del año

Revisión y preparación del repertorio.%%
_Marcado: seguimiento. Indicaciones locales._

Audición y cierre del curso.%%
Instancia de recuperación.


# Destinatarios

Estudiantes de 3° año de la Formación Básica en Música de la Escuela de Arte "Alcides Biagetti".

# Recursos

- Guitarras; banquito; atriles.
- Material de estudio (partituras; trabajos prácticos y apuntes elaborados por la cátedra).
- Plataformas _online_ organizadas y/o elaboradas por la cátedra.

