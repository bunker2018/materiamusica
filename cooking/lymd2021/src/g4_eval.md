## Acreditación de la materia

### Régimen de cursada presencial

Para acreditar el espacio curricular se deberá cumplir con la entrega y aprobación
en tiempo y forma de los trabajos prácticos
de cada unidad, y registrar la asistencia a un 80% de las clases. 

La calificación necesaria para lograr la acreditación es de 4 (cuatro).

### Régimen de cursada semipresencial

Se deberá acordar con la cátedra no menos de ocho encuentros a lo largo del ciclo lectivo
(tres encuentros por cuatrimestre como mínimo), separados entre sí por un máximo de seis (6) semanas.
Para acreditar el espacio curricular se requerirá la asistencia a la totalidad de los encuentros
pautados, así como la entrega en tiempo y forma de los trabajos indicados por la cátedra.

La calificación necesaria para lograr la acreditación es de 4 (cuatro).

## Programa de examen

- Una obra grande en varios movimientos.
- 2 obras grandes, demandantes y complejas en relación a las expectativas del nivel.
- 3 obras breves que presenten características y profundidad de trabajo de acuerdo al nivel.
- Una obra breve, o una producción de material de aula o contenido multimedia.
- Participación en una audición y/o concierto y/o en la organización del ciclo de Conciertos.

El armado del programa de examen podrá adaptarse a cada caso particular, según el criterio de
la cátedra y tomando como referencia el modelo aquí propuesto.

### Examen libre

En el caso de rendir en condición de libre, al programa se le agregará una obra grande o dos piezas breves.

## Examen final

El examen final consistirá en la ejecución del repertorio trabajado durante el año ante una mesa examinadora
integrada por un mínimo de dos docentes. Se evaluará en esta instancia el desempeño técnico y musical en
relación a los contenidos del nivel, teniendo en cuenta el proceso de aprendizaje en cada caso particular.

