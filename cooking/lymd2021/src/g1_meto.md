La materia consistirá en una clase semanal individual de una hora.
Se iniciará con la presentación del proyecto anual
y la preselección de un repertorio de material de obras para guitarra solista de diversos géneros,
que servirá de base para la preparación del material del curso.
Se destinarán una o dos semanas más para un primer acercamiento al material (lectura, indicaciones
y ajustes generales)
y para realizar el diagnóstico inicial.
En esta primera etapa se acordarán los lineamientos y objetivos puntuales del curso,
que surgirán del diálogo entre el proyecto propuesto por la cátedra
y la objetivación por parte del alumno de sus inquietudes, expectativas y desafíos individuales
en este punto de su formación.

Durante el transcurso del año, se alternará el trabajo de las unidades de la materia
(dos al inicio del primer cuatrimestre, una promediando el segundo cuatrimestre)
con las instancias de preparación y muestra del material.
Cada unidad o bloque de contenidos se abordará en función de un cronograma flexible,
consistente en una planificación tentativa de plazos y objetivos parciales
que se ajustará orgánicamente a la evolución del proyecto en cada caso particular.
El resultado del trabajo sobre cada contenido específico se trasladará
a la elaboración y preparación del repertorio de obras
(_marcado_ progresivo y acumulativo de la partitura)
y a la construcción y planificación de rutinas de estudio,
estrategias de abordaje de obra
y eventualmente al esbozo de un plan de trabajo con proyección a los estadíos posteriores
de la formación musical del alumno.
%%Cada unidad consistirá en
%%una serie de trabajos prácticos, en cada uno de los cuales se profundizará un contenido
%%o bloque de contenidos
%%integrando en lo posible recepción, conceptualización, interpretación y producción
%%(improvisación/composición);
%%el resultado de estas experiencias se trasladará al trabajo sobre
%%el repertorio del grupo (_marcado_ progresivo y acumulativo de la partitura)
%%y a la construcción y planificación de rutinas de estudio.

%%Se prevé incluir en el repertorio de obras todo aquel material musical que se adecúe al trabajo de los
%%contenidos propuestos: obras y estudios para guitarra solista, adaptaciones realizadas en clase o en
%%otras materias, material para voz y guitarra, realización de cifrados de acordes, trabajos
%%rítmicos y texturales pautados (especies rítmicas, diseños texturales). Siempre considerando el
%%abordaje de la música escrita como fundamental en la formación musical y profesional.

Se buscará promover una cursada dinámica, cuya continuidad no dependa de los imponderables
que eventualmente alteren el cronograma anual,
haciendo accesible _online_ la totalidad del material del curso y propiciando espacios virtuales que
permitan al alumno y a la cátedra canalizar consultas, realizar entregas, compartir material,
etc.,
aun cuando las circunstancias dificulten o imposibiliten los encuentros pautados.

