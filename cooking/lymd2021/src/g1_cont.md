## Unidad 1: Lectura y abordaje de obra

- Lectura razonada e integral de la partitura. Aspectos rítmicos, melódico/armónicos,
  formales, técnico/instrumentales.
- Digitación del pasaje. Criterios técnicos y musicales. 
  Preparación de la partitura para su posterior estudio.
- Marcado progresivo y acumulativo de la partitura:
  digitación y trabajo técnico,
  articulación de la forma, trabajo sobre los diferentes
  parámetros, plan de interpretación.
- Marco estilístico y planteo estético:
  recorte de recursos técnicos e interpretativos.

## Unidad 2: Aspectos técnico--instrumentales

- Concepto de mecanismo. Operadores técnicos.
- Grupos musculares: trabajo razonado sobre los grupos musculares mayores y menores.
- Organicidad. Conciencia del cuerpo. Observación.
- Rutinas y estrategias de estudio: elaboración, discusión, realización y autoevaluación.

## Unidad 3: Aspectos musicales

- Recursos interpretativos en función del plan de la obra: intensidad, timbre, articulación, temporalidad.
- Carácter de la obra. Planteos estilísticos, técnicos, estéticos.
- Realización de la textura. Ornamentación.
- Elaboración del plan de obra en función de la situación de concierto.

