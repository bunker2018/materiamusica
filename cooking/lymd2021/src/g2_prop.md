- Trabajar durante el curso un repertorio de obras de raíz predominantemente
popular, reelaborando y resignificando los textos en función de los
requerimientos del nivel.
- Proponer una metodología de trabajo que permita articular con los
espacios curriculares afines del nivel.
- Acompañar al alumno en la profundización, consolidación y expansión
de las herramientas técnicas, expresivas y conceptuales adquiridas
previamente, incluyendo los usos y recursos propios del campo popular.
- Acercar al alumno a las diversas posibilidades estilísticas, técnicas y musicales
de la guitarra, desde la audición, la lectura y escritura y la ejecución.
- Generar y llevar a cabo un proyecto musical consistente en la elaboración
de una audición o concierto sobre el final del ciclo.
- Incorporar las posibilidades que ofrecen las nuevas tecnologías como un vector
de renovación e intercambio de prácticas áulicas, material musical y de estudio,
estrategias metodológicas, etc., y de articulación con otros espacios curriculares,
carreras y lenguajes.
- Integrar la producción del curso a las instancias de muestra a lo largo del año
(audiciones, conciertos, muestra anual) y a la generación de contenidos.

