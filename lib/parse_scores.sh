#!/bin/bash

parse_scorestableterse() {
	l="$1"
	add="$2"
	auth="$(echo $l | cut -d@ -f1)"
	title="$(echo $l | cut -d@ -f2)"
	if [ "$add" ]; then title="$title $add"; fi
	echo "<td>$auth</td>"
	echo "<td>$title</td><td>"

	i=2; while true; do
		i=$(($i+1))
		s="$(echo $l | cut -d@ -f$i)"
		[ "$s" ] || break
		button="$(echo $s | cut -d' ' -f1)"
		case $button in ":"|"EOL") break;; esac
		url="$(echo $s | cut -d' ' -f2-)"
		cat templates/tablebutton-${button}.htm \
			| sed -e "s@%URL%@$url@"
	done

	echo "</td></tr><tr>"
}

resolve_scoresdb() {
	_t="$1"
	tag="$(echo $_t | cut -d+ -f1)"
	case $_t in
		*+*) add="$(echo $_t | cut -d+ -f2)";;
		*) add=;;
	esac
	l="$(cat scores/* exercises/* materiel/* books/* | grep -E -- "^$tag:" | cut -d: -f2-)"
	if [ "$l" ]
	then
		parse_scorestableterse "$l" "$add"
	else
		echo "<td>$tag</td><td>NOT FOUND</td><td></td></tr>"
	fi
}

