#!/bin/bash

. lib/common.sh

process_lili_input() {
	if [ -f "$1" ]
	then
		_f="$(echo $f | sed -e "s/.li//")"
		msg "[process_lili_input] Processing $1"
		lili "$1" >"${_f}.ly"
		msg "[process_lili_input] Running lilypond"
		lilypond -o "${_f}" "${_f}.ly"
	else
		warn "[process_lili_input] $1: file not found, skipping"
	fi
}

