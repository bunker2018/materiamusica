#!/bin/bash

generate_bookindex() {
	msg "Generating bookindex"
	bookindex=site/pages/bookindex.mmus
	echo -n >$bookindex
	cat >>$bookindex<<!
.PAGETITLE MateriaMusica | Libros

.PAGEHEADER Libros

.SCORESTABLEFILTERABLE myBooks myBooksInput
!
	c=0
	ls books/* 2>/dev/null | while read f
	do
		case $f in
			*.noindex)
				continue
			;;
			*)
				cat $f | while read l
				do
					p="$(echo "$l" | cut -d: -f1)"
					echo "@$p" >>$bookindex
				done
			;;
		esac
	done
	cat >>$bookindex<<!
.ENDSCORESTABLE

!
cat templates/bibfooter.htm >>$bookindex
}

generate_bibindex() {
	msg "Generating bibindex"
	bibindex=site/pages/bibindex.mmus
	echo -n >$bibindex
	cat >>$bibindex <<!
.PAGETITLE MateriaMusica | Partituras

.PAGEHEADER Índice de partituras

.SCORESTABLEFILTERABLE myScores myScoresInput
!
	c=0
	ls scores/* 2>/dev/null | while read f
	do
		case $f in
			*.noindex)
				continue
			;;
			*)
				cat $f | while read l
				do
					p="$(echo "$l" | cut -d: -f1)"
					echo "@$p" >>$bibindex
				done
			;;
		esac
	done
	cat >>$bibindex<<!
.ENDSCORESTABLE

!
cat templates/bibfooter.htm >>$bibindex
}

generate_exercises() {
	msg "Generating exercises"
	exercises=site/pages/exercises.mmus
	echo -n >$exercises
	cat >>$exercises<<!
.PAGETITLE MateriaMusica | Ejercicios

.PAGEHEADER Ejercicios

.SCORESTABLEFILTERABLE myExercises myExercisesInput
!
	c=0
	ls exercises/* 2>/dev/null | while read f
	do
		case $f in
			*.noindex)
				continue
			;;
			*)
				cat $f | while read l
				do
					p="$(echo "$l" | cut -d: -f1)"
					echo "@$p" >>$exercises
				done
			;;
		esac
	done
	cat >>$exercises<<!
.ENDSCORESTABLE

!
cat templates/bibfooter.htm >>$exercises
}

generate_materiel() {
	msg "Generating materiel"
	materiel=site/pages/materiel.mmus
	echo -n >$materiel
	cat >>$materiel<<!
.PAGETITLE MateriaMusica | Material de estudio

.PAGEHEADER Material de estudio

.SCORESTABLEFILTERABLE myMateriel myMaterielInput
!
	c=0
	ls materiel/* 2>/dev/null | while read f
	do
		case $f in
			*.noindex)
				continue
			;;
			*)
				cat $f | while read l
				do
					p="$(echo "$l" | cut -d: -f1)"
					echo "@$p" >>$materiel
				done
			;;
		esac
	done
	cat >>$materiel<<!
.ENDSCORESTABLE

!
cat templates/bibfooter.htm >>$materiel
}

generate_linksindex() {
	msg "Generating linksindex"
	linksindex=site/pages/linksindex.mmus
	echo -n >$linksindex
	cat >>$linksindex<<!
.PAGETITLE MateriaMusica | Enlaces

.PAGEHEADER Enlaces a sitios externos

.HR

<table class="table table-striped"><tbody>
!
	c=0
	cat site/linksindex | while read l
	do
		c=$(($c+1))
		case $c in
			1) url="$l";;
			2) title="$l";;
			3) desc="$l";;
			*) cat >>$linksindex<<!
<tr><td><a href="${url}" target="_blank">
${title}
</a></td><td>
${desc}
</td></tr>

!
				c=0
			;;
		esac
	done
	cat >>$linksindex<<!
</tbody></table>

!
cat templates/bibfooter.htm >>$linksindex
}

