#!/bin/bash

make_latex() {
  latex_outdir=${out}/latex
  mkdir -p $latex_outdir
  tex=${latex_outdir}/${table_id}.tex
  cat lib/templates/latex-header.tex \
    | sed -e "s/%LABEL%/$table_label/" >$tex

  cat $table_source | while read _l
  do
    case $_l in
      %1*)
        _s="$(echo $_l | cut -d% -f3-)"
        echo "\\section*{$(pick_label ${_s})}" >>$tex
      ;;
      %2*)
        _s="$(echo $_l | cut -d% -f3-)"
        echo "\\subsection*{$(pick_label ${_s})}" >>$tex
      ;;
      %3*)
        _s="$(echo $_l | cut -d% -f3-)"
        echo "\\subsubsection*{$(pick_label ${_s})}" >>$tex
      ;;
      %4*)
        _s="$(echo $_l | cut -d% -f3-)"
        echo "\\subsubsubsection*{$(pick_label ${_s})}" >>$tex
      ;;
      *)
        _sc="$(cat scores/* | grep -E -- "^${_l}:")"
        _auth="$(echo $_sc | cut -d@ -f1 | cut -d: -f2 | sed -e '
          s/<i>//g;
          s/<\/i>//g;
          s/__ITALIC__//g;
          s/__ENDITALIC__//g;
          s/__BOLD__//g;
          s/__ENDBOLD__//g;
        ')"
        _wrk="$(echo $_sc | cut -d@ -f2 | sed -e '
          s/<i>//g;
          s/<\/i>//g;
          s/__ITALIC__//g;
          s/__ENDITALIC__//g;
          s/__BOLD__//g;
          s/__ENDBOLD__//g;
        ')"
        echo "\\paragraph{}" >>$tex
        echo "\$\\bullet\$ ${_auth}: \\textit{${_wrk}}." >>$tex
      ;;
    esac
  done

  cat lib/templates/latex-footer.tex >>$tex
  env LANG=es_AR.UTF-8 \
    pdflatex --output-directory $latex_outdir $tex >/dev/null
}

pick_label() {
  cat ${dbdir}/*.lst \
    | grep -E -- "^${1}@" \
    | cut -d@ -f2
}

