#!/bin/bash

parse_page() {
	infile=$1
	bn=$(basename $infile | sed -e 's/.mmus//')
	outbn=${bn}.html
	outfile=build/out/${outbn}
	prefile=build/out/${outbn}_pre
	state=plain
	cat $infile \
		| sed -e 's/ ___:.:___ //g' \
		| while read line; do case $line in

		#-----------------------------------------------------------
		
		"___:"*) continue;;
		"___STARTCOMMENT___"|":::STARTCOMMENT:::") state="comment"; continue;;
		"___ENDCOMMENT___"|":::ENDCOMMENT:::") state="plain"; continue;;

		#-----------------------------------------------------------
		
		".PAGETITLE "*)
			case $state in comment) continue;; esac
			# Write header and navigation bar
			string="$(echo $line | cut -d' ' -f2-)"
			if [ -f site/${bn}.header.htm ]
			then
				cat site/${bn}.header.htm
			else
				cat templates/header.htm \
					| sed -e "s/%PAGETITLE%/$string/"
			fi

			if [ -f site/${bn}.navbar.htm ]
			then
				cat site/${bn}.navbar.htm
			else
				cat templates/navbar.htm \
					| sed -e "s/a href=\"$outbn\"/a href=\"#\"/"
			fi

			cat templates/startpage.htm
		;;

		#-----------------------------------------------------------
		
		".PAGEHEADER "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<div class=\"page-header\"><h1>$string</h1></div>"
		;;

		#-----------------------------------------------------------
		
		".H1 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h1>$string</h1>"
		;;

		#-----------------------------------------------------------
		
		".H2 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h2>$string</h2>"
		;;

		#-----------------------------------------------------------
		
		".H3 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h3>$string</h3>"
		;;

		#-----------------------------------------------------------
		
		".H4 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h4>$string</h4>"
		;;

		#-----------------------------------------------------------
		
		".H5 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h5>$string</h5>"
		;;

		#-----------------------------------------------------------
		
		".H6 "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<hr/><h6>$string</h6>"
		;;

		#-----------------------------------------------------------
		
		".HR")
			case $state in comment) continue;; esac
			echo "<hr/>"
		;;

		#-----------------------------------------------------------
		
		".BR")
			case $state in comment) continue;; esac
			echo "<br/>"
		;;

		#-----------------------------------------------------------
		
		".BR2")
			case $state in comment) continue;; esac
			echo "<br/><br/>"
		;;

		#-----------------------------------------------------------
		
		".LINK "*)
			case $state in comment) continue;; esac
			url="$(echo $line | cut -d' ' -f2- | cut -d@ -f1 )"
			label="$(echo $line | cut -d@ -f2-)"
			echo -n "<a href=\"$url\">$label</a>"
		;;

		#-----------------------------------------------------------
		
		".INDENT")
			case $state in comment) continue;; esac
			echo -n "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
		;;

		#-----------------------------------------------------------
		
		".PI "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<p><i>$string</i></p>"
		;;

		#-----------------------------------------------------------
		
		".PANEL "*)
			case $state in comment) continue;; esac
			panel="$(echo $line | cut -d' ' -f2)"
			string="$(echo $line | cut -d' ' -f3-)"
			cat <<!
<hr />
<div class="panel panel-${panel}">
<div class="panel-heading">${string}</div>
<div class="panel-body">
!
		;;

		#-----------------------------------------------------------
		
		".ENDPANEL "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "</div><div class=\"panel-footer\">${string}</div></div>"
		;;

		#-----------------------------------------------------------
		
		".ENDPANEL")
			case $state in comment) continue;; esac
			echo "</div></div>"
		;;

		#-----------------------------------------------------------
		
		".P "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<p>$string</p>"
		;;

		#-----------------------------------------------------------
		
		".PROF "*)
			case $state in comment) continue;; esac
			string="$(echo $line | cut -d' ' -f2-)"
			echo "<h4><b>Docente: </b>$string</h4>"
			echo "<hr class="invisible" />"
		;;

		#-----------------------------------------------------------
		
		".ME")
			case $state in comment) continue;; esac
			echo "<h4><b>Docente: </b>Prof. Marcelo López Minnucci</h4>"
			echo '<hr class="invisible" />'
		;;

		#-----------------------------------------------------------
		
		".BUTTON "*)
			case $state in comment) continue;; esac
			thisclass="$(echo $line | cut -d' ' -f2)"
			thissize="$(echo $line | cut -d' ' -f3)"
			thisurl="$(echo $line | cut -d' ' -f4)"
			thislabel="$(echo $line | cut -d' ' -f5-)"
			cat templates/button.htm \
				| sed -e "s/%CLASS%/$thisclass/" \
				| sed -e "s/%SIZE%/$thissize/" \
				| sed -e "s/%URL%/$thisurl/" \
				| sed -e "s/%LABEL%/$thislabel/"
		;;

		#-----------------------------------------------------------

		".UNITJUMBO"*)
			case $state in comment) continue;; esac
			thistitle="$(echo $line | cut -d' ' -f2- | cut -d@ -f1)"
			thistext="$(echo $line | cut -d@ -f2)"
			cat templates/unitjumbo.htm \
				| sed -e "s/%TITLE%/$thistitle/" \
				| sed -e "s/%TEXT%/$thistext/"
		;;

		#-----------------------------------------------------------
		
		".TPJUMBO"*)
			case $state in comment) continue;; esac
			thistitle="$(echo $line | cut -d' ' -f2- | cut -d@ -f1)"
			thistext="$(echo $line | cut -d@ -f2)"
			cat templates/tpjumbo.htm \
				| sed -e "s/%TITLE%/$thistitle/" \
				| sed -e "s/%TEXT%/$thistext/"
		;;

		#-----------------------------------------------------------
		
		".BLOCKBUTTON "*)
			case $state in comment) continue;; esac
			thisclass="$(echo $line | cut -d' ' -f2)"
			thissize="$(echo $line | cut -d' ' -f3)"
			thisurl="$(echo $line | cut -d' ' -f4)"
			thislabel="$(echo $line | cut -d' ' -f5-)"
			cat templates/blockbutton.htm \
				| sed -e "s/%CLASS%/$thisclass/" \
				| sed -e "s/%SIZE%/$thissize/" \
				| sed -e "s/%URL%/$thisurl/" \
				| sed -e "s/%LABEL%/$thislabel/"
		;;

		#-----------------------------------------------------------
		
		".LISTGROUPBORDERLESS"*)
			case $state in comment) continue;; esac
			echo '<ul class="list-group">'
			state="listgroupborderless"
		;;

		#-----------------------------------------------------------
		
		".LISTGROUP"*)
			case $state in comment) continue;; esac
			echo '<ul class="list-group">'
			state="listgroup"
		;;

		#-----------------------------------------------------------
		
		".ENDLISTGROUP"*)
			case $state in comment) continue;; esac
			echo "</ul>"
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".SCORESTABLE "*)
			case $state in comment) continue;; esac
			thistable="$(echo $line | cut -d' ' -f2)"
			cat templates/scorestableheader.htm \
				| sed -e "s/%tableid%/$thistable/"
			state="scorestableterse"
		;;

		#-----------------------------------------------------------

		".SCORESTABLEFILTERABLE "*)
			case $state in comment) continue;; esac
			thistable="$(echo $line | cut -d' ' -f2)"
			thisinput="$(echo $line | cut -d' ' -f3)"
			cat templates/scorestableheaderfilterable.htm \
				| sed -e "s/%tableid%/$thistable/" \
				| sed -e "s/%inputid%/$thisinput/"
			state="scorestableterse"
		;;

		#-----------------------------------------------------------
		
		".ENDSCORESTABLE")
			case $state in comment) continue;; esac
			cat templates/scorestablefooter.htm
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".PAGER")
			case $state in comment) continue;; esac
			cat templates/pagerheader.htm
			state="pager"
		;;

		#-----------------------------------------------------------
		
		".ENDPAGER")
			case $state in comment) continue;; esac
			cat templates/pagerfooter.htm
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".STARTNAVTABS"*)
			case $state in comment) continue;; esac
			thistag="$(echo $line | cut -d' ' -f2)"
			thislabel="$(echo $line | cut -d' ' -f3-)"
			cat templates/navtabsheader.htm \
				| sed -e "s/%TAG%/$thistag/" \
				| sed -e "s/%LABEL%/$thislabel/"
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".NEWNAVTAB"*)
			case $state in comment) continue;; esac
			thistag="$(echo $line | cut -d' ' -f2)"
			thislabel="$(echo $line | cut -d' ' -f3-)"
			cat templates/navtabsnewtab.htm \
				| sed -e "s/%TAG%/$thistag/" \
				| sed -e "s/%LABEL%/$thislabel/"
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".NAVTABACTIVE"*)
			case $state in comment) continue;; esac
			thistag="$(echo $line | cut -d' ' -f2)"
			cat templates/navtabsactive.htm \
				| sed -e "s/%TAG%/$thistag/"
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".NAVTABINACTIVE"*)
			case $state in comment) continue;; esac
			thistag="$(echo $line | cut -d' ' -f2)"
			cat templates/navtabsinactive.htm \
				| sed -e "s/%TAG%/$thistag/"
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".ENDNAVTABS")
			case $state in comment) continue;; esac
			cat templates/navtabsfooter.htm
			state="plain"
		;;

		#-----------------------------------------------------------
		
		".MENATWORK")
			case $state in comment) continue;; esac
			cat templates/menatwork.htm
			state="plain"
		;;

		#-----------------------------------------------------------
		
		*)
			# Anything else: parse according to current state
			case $state in

				#-------------------------------------------

				comment) continue;;

				#-------------------------------------------

				scorestableterse)
					case $line in
						@*) resolve_scoresdb "${line/@/}";;
						*) parse_scorestableterse "$line";;
					esac
				;;

				#-------------------------------------------

				listgroup)
					case $line in
						"- "*)
							_l="$(echo $line | sed -e 's/- //')"
							echo "  <li class="list-group-item">&nbsp;&nbsp;&nbsp;&nbsp;&#9702;&nbsp;&nbsp;$_l</li>"
						;;
						*)
							echo "  <li class="list-group-item">&nbsp;&#8226;&nbsp;&nbsp;$line</li>"
						;;
					esac
				;;

				#-------------------------------------------

				listgroupborderless)
					case $line in
						"  - "*)
							_l="$(echo $line | sed -e 's/  - //')"
							echo "  <li class=\"list-group-item\" style=\"border: none\">&nbsp;&nbsp;&nbsp;&nbsp;&#8826;&nbsp;&nbsp;$_l</li>"
						;;
						"- "*)
							_l="$(echo $line | sed -e 's/- //')"
							echo "  <li class=\"list-group-item\" style=\"border: none\">&nbsp;&nbsp;&nbsp;&nbsp;&#9702;&nbsp;&nbsp;$_l</li>"
						;;
						*)
							echo "  <li class=\"list-group-item\" style=\"border: none\">&nbsp;&#8226;&nbsp;&nbsp;$line</li>"
						;;
					esac
				;;

				#-------------------------------------------

				pager)
					_url="$(echo $line | cut -d@ -f1)"
					_label="$(echo $line | cut -d@ -f2)"
					cat templates/pager.htm | sed -e "s|%URL%|${_url}|; s|%LABEL%|${_label}|"
				;;

				#-------------------------------------------

				plain)
					echo "$line"
				;;

				#-------------------------------------------

			esac
		
		#-----------------------------------------------------------

		;;
	esac;	done >>$prefile

	cat templates/endpage.htm >>$prefile

	cat $prefile | sed -e '
		s|__BOLD__|<b>|g;
		s|__ENDBOLD__|</b>|g;
		s|__ITALIC__|<i>|g;
		s|__ENDITALIC__|</i>|g;
		s|__SLASH__|/|g;
		s|__QUOT__|\&quot;|g;
		s|__LAQUO__|\&laquo;|g;
		s|__RAQUO__|\&raquo;|g;
		s|<TAB>|\&nbsp;\&nbsp;\&nbsp;\&nbsp;\&nbsp;\&nbsp;\&nbsp;|g;
	' >$outfile
	rm $prefile
}

