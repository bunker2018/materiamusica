#!/bin/bash

publish_site() {
	msg "Publishing site content"
	rm public/*.html 2>/dev/null
	cp -rv build/out/* public/
}

