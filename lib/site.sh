#!/bin/bash

. lib/common.sh
. lib/prepare.sh
. lib/generate_indexes.sh
. lib/parse_scores.sh
. lib/publish.sh
. lib/parse_page.sh

