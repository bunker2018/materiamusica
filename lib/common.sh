#!/bin/bash

########################
### Helper functions ###

msg() { [ $QUIET ] || echo "[$(basename $0 | sed -e s/.sh//)] $@" >>/dev/stderr; }
msg2() { [ $QUIET ] || echo "[$(basename $0 | sed -e s/.sh//)] :: $@" >>/dev/stderr; }
msg3() { [ $QUIET ] || echo "[$(basename $0 | sed -e s/.sh//)] -> $@" >>/dev/stderr; }
warn() { msg "WARNING: $@" >>/dev/stderr; }
err() { msg "ERROR: $@" >>/dev/stderr; }
debug() { [ $DEBUG ] && msg "<<DEBUG>> $@"; }
ok() { msg "All OK."; }
die() { err "$@"; exit 1; }

check_cwd() {
	[ -f .rootdir ] || die "Not in root dir, I refuse to proceed any further."
}

check_not_root() {
	[[ $USER == 'root' ]] && die "I refuse to proceed as root."
}

