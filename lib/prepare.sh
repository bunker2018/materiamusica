#!/bin/bash

prepare_build_dir() {
	check_cwd
	msg "Preparing build directory"
	rm -r build/* 2>/dev/null
	mkdir -p build/{out,css}
}

prepare_output() {
	msg "Preparing output"
	# TODO: use theme from config
	cp -v assets/custom.css build/out
	# TODO: use template from config
	#cp -v assets/bootstrap.html build/out/index.html
	#cp -v site/pages/*.html build/out/
	msg "Parsing pages"
	for f in site/pages/*.mmus
	do
		msg2 "Processing page: $f"
		parse_page $f
	done
}

