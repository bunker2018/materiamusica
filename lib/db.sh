#!/bin/bash

. lib/common.sh
. lib/db_parsers.sh

make_clean_wdir() {
  rm -r $wdir/* 2>/dev/null
  mkdir -p ${wdir}/{tables,sources,out}
}

extract_data() {
  _field=$1
  _data="$2"
  _this=""
  for _d in $_data
  do
    _DBG="$(eval echo -n \$$_field) <> $_d"
    debug "extract_data: $_DBG"
    if eval echo \" \$$_field\" | grep -E -- " ._$_d" >/dev/null
    then
      _this="$_this $(cat $dbdir/*.lst | grep -E -- "^._${_d}@" | cut -d@ -f1)"
    fi
  done
  echo $_this
}

