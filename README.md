_This README is old and mostly deprecated._

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

[Visit site](https://bunker2018.gitlab.io/materiamusica/)

[Bootstrap template](https://bunker2018.gitlab.io/materiamusica/devtools/bootstrap.html)

[GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)

---

**Bucket list: completion tree**

- [x] Admin
  - [x] Planillas y cuadros
    - [x] Datos generales por estudiante
    - [x] Programa de obras
    - [x] Asistencia
    - [x] Seguimiento y proyección
    - [x] Entregas y acreditación
- [ ] Project
  - [ ] Inicio
  - [ ] FOBA
    - [ ] FOBA 3
      - [ ] **Intro**
      - [x] _Unidad 1_
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [ ] Unidad 4
      - [x] _Programa de obras_
  - [ ] Prof./Tec.
    - [ ] CS 1
      - [ ] **Intro**
      - [x] _Unidad 1_
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [ ] Unidad 4
      - [x] _Programa de obras_
    - [ ] CS 2
      - [ ] **Intro**
      - [ ] **Unidad 1**
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [ ] Unidad 4
      - [x] _Programa de obras_
    - [ ] CS 3
      - [ ] **Intro**
      - [ ] **Unidad 1**
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [ ] Unidad 4
      - [x] _Programa de obras_
    - [ ] CS 4
      - [ ] **Intro**
      - [ ] **Unidad 1**
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [x] _Programa de obras_
    - [ ] CS 1
      - [ ] **Intro**
      - [ ] **Unidad 1**
      - [ ] Unidad 2
      - [ ] Unidad 3
      - [x] _Programa de obras_
    - [x] _Biblioteca_
      - [x] Partituras
      - [x] Ejercicios
      - [x] Material
      - [x] Libros
      - [x] Enlaces

**TODO**

- Check for material -> TPs:
    - Carcassi: 25 studies
    - Sor: 20 studies
- Add option when parsing scores to *mark* oeuvre (add bold asterisk to title or -better- change color to table row)
- Create presentation for all curricula
- Create general presentation page
- Add an introduction to "Programa de obras" page
- Add bulk download functionality
- Upload some sonatas for guitar to support keikomeiji
- Add some studies collections as .noindex for suggested works (e.g. "20 estudios de Sor: 3 a elección")
- Consider removing entire books that are already split in several parts, if space becomes an issue.
- MANDATORY WORKS: include in some TP as "suggested work", then mandate that the work selected for that TP MUST be in the program :-P
- Add musical pieces (songs) adapted to Ed.Mus. (La nostalgiosa, etc). - cf. misc.md
- Capture examples to every technique and content (photo and video)
- Optimize scores tag system according to curricula contents

_TODO later_

- Add table column for tags in library
- Split page to allow ather teachers to upload their material and share library

---

About converting pages to downloadable pdf files
------------------------------------------------

Relevant options for wkhtmltopdf(1):

- *-g* generates pdf in grayscale
- *-l* lower quality pdf, which means less disk space
- *-B|-L|-T|-R* set bottom|left|right|top margins (doesn't seem to work)
- *-O* sets orientation (Landscape|Portrait)
- *s* sets page size (A4, Letter, etc.)
- *--page-height|--page-width*
- *--title*

Page options:

- *--no-images*
- *--minimum-font-size*
- *--page-offset* sets the starting page number
- *--zoom* sets the zoom factor (float)

There's a problem: TPs are organized as navtabs, so only TP1 will show in u\* pages.

So anyway, either a dedicated parser will be needed for those pages,
or the page design itself needs to be changed.

_EDIT: TPs are no longer organized as navtabs, so this is not an issue anymore._

---

Old ideas
=========

- Repository of musical material and exercises, for pedagogical purposes. **DONE**
- Each piece of material should be self-contained (redundancy). **DONE**
- Make the material accessible and organized (API?).

Possible categories for the material
------------------------------------

- Lyrics (with chords, fretboards, or both).
- Technical analysis, ad-hoc exercises, about a work or technical issue. *(Extract from reference books)*
- Reference texts, glossaries, etc.
- Fretboards/scales collection.
- Indexes, hierarchy trees (alphabetical, by difficulty, sequential learning...).
- Audio, video, images.
- Links.
- Source files (such as lilypond or LaTeX sources).

Meta
----

- Tags
- Level (perhaps with nested sublevels to define hierarchies better)

Workflow
--------

Say I collect some chords and generate a pdf. Or produce a video. Or take a series of photos.
I could take that material, give it a name, tag and comment it, fill some forms.
Then, with some API, just upload it to the repo.
Then I should have some interface to access the repo from any source.
A simple webpage might be the best to start: accessible from any device with a browser, easy to run and maintain.

(This above is not so clear, perhaps Google Cloud might be an alternative.)

---

Original GitLab HTML example page follows
=========================================

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
