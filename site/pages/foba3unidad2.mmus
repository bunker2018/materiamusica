.PAGETITLE MateriaMusica | FOBA 3 - Unidad 2

.PAGEHEADER Guitarra | FOBA 3
.ME

.UNITJUMBO Unidad 2@ Desarrollo de la técnica de ambas manos

.H2 Contenidos

.H4 Técnica de mano izquierda

.LISTGROUP
Escalas. Escalas con dedos fijos. Traslados de mano izquierda.
Distensión y contracción. Posiciones fijas.
Cejilla.
Ligados.
.ENDLISTGROUP

.H4 Técnica de mano derecha

.LISTGROUP
Toque libre de pulgar y dedos.
Toque apoyado de pulgar y dedos.
Ejecución de líneas melódicas.
Técnica de arpegios. Apagadores ___:A:___ . Campanella.
Rasgueos. Rasgueo con índice. Arpeggiato.
___: Figueta.
.ENDLISTGROUP

___: -A-: Estudio 7 Carcassi
.H4 Recursos tímbricos

.LISTGROUP
Traslados de mano derecha; registros tímbricos.
Pizzicato. Pizzicato alla Bartók.
Armónicos naturales y artificiales. Diferentes posibilidades de realización.
.ENDLISTGROUP

.HR
.TPJUMBO Líneas melódicas. Escalas con dedos fijos. Traslados de mano izquierda. Toques libre y apoyado.@Actividades complementarias
___: Figueta.

.H3 Material de estudio

.H4 Lecturas sugeridas

.SCORESTABLE myTable
M. López Minnucci@FOBA 3 - Unidad 2: Trabajo Práctico Nª1 __ITALIC__(ejercicios)__ENDITALIC__ __BOLD__(1)__ENDBOLD__@PDF static/ejercicios_f3u2tp1.pdf@EOL
@carcassiest6
.ENDSCORESTABLE

.H4 Fragmentos musicales sugeridos

.SCORESTABLE myTable
@triston+(compases 1 a 12)
@elsueñodelamuñeca+(compases 1 a 7)
@undiadenov+(compases 1 a 8 y 9 a 17)
@plmeltrebol+(compases 5 a 12)
@vlprel3+(compases 13 a 15 y 23 a 28)
@mendezesperandote+(compases 1 a 12)
.ENDSCORESTABLE

.H4 Obras y estudios sugeridos

.SCORESTABLE myTable
M. Carcassi@Estudio Nº1 (para las escalas)@PDF static/Op.60-25_Estudios.pdf@EOL
@brouwerestudio10+(para las escalas)
.ENDSCORESTABLE

.H3 Actividades

.LISTGROUP
Realizar los ejercicios propuestos en __BOLD__(1)__ENDBOLD__ aplicando las variantes de digitación sugeridas.
Leer y resolver los primeros 4 sistemas del Estudio Nº6 de M. Carcassi.
Aplicar los diferentes tipos de toque a los fragmentos sugeridos (trabajar sobre al menos 2 fragmentos).
.ENDLISTGROUP

.HR
.TPJUMBO Toques de pulgar. Arpegios. Apagadores. Campanella.@Actividades complementarias

.H3 Material de estudio

.H4 Fragmentos musicales sugeridos

.H5 Toques de pulgar

.SCORESTABLE myTable
@vlprel4+(compases 1 a 9)
@mendezhoyxhoy+(compases 1 a 8)
@tevasmilonga+(compases 5 a 12 y 13 a 16)
@milongueodelayer+(compases 29 a 36)
@laprimavera+(compases 5 a 10)
@mmoreira+(comapses 5 a 12)
@brouwerestudio8+(compases 10 a 17)
@consolacao+(compases 5 a 14)
.ENDSCORESTABLE

.H5 Apagadores

.SCORESTABLE myTable
@carcassiest7+(compases 1 a 8)
@bachbourreemim+(compases 1 a 8)
@vlprel3+(compases 1 a 2 y 6 a 7)
.ENDSCORESTABLE

.H4 Obras y estudios sugeridos

.SCORESTABLE myTable
@carcassiest7+(para los arpegios y apagadores)
@brouwerestudio6+(para los arpegios)
@vlprel4+(para los arpegios y toques de pulgar)
@bachpreludiorem+(para los arpegios y toques de pulgar)
.ENDSCORESTABLE

.H3 Actividades

.LISTGROUP
Realizar los ejercicios de arpegios propuestos en __BOLD__(1)__ENDBOLD__.
Elaborar  al menos 3 fórmulas de arpegios sobre el Estudio Nº6 de Brouwer, utilizando diferentes compases, diseños y patrones rítmicos en el pulgar.
Resolver los fragmentos sugeridos en en el apartado __ITALIC__Apagadores__ENDITALIC__ (trabajar al menos dos fragmentos).
Aplicar los diferentes tipos de toque de pulgar a los fragmentos sugeridos en el apartado __ITALIC__Toques de pulgar__ENDITALIC__ (trabajar sobre al menos 2 fragmentos).
.ENDLISTGROUP

.HR
.TPJUMBO Distensión y contracción. Posiciones fijas. Cejilla. Acordes plaqué y __ITALIC__arpeggiato__ENDITALIC__.@Actividades complementarias

.H3 Material de estudio

.H4 Fragmentos musicales sugeridos

.H5 Distensión y contracción

.SCORESTABLE MyTable
@bachpreludiocello1+(compases 16 a 17)
@ututa+(compases 7 a 14)
@testamentdamelia+(compases 1 a 16)
.ENDSCORESTABLE

.H5 Posiciones fijas

.SCORESTABLE MyTable
@vlprel3+(compases 9 a 12)
@vlprel4+(compases 19 a 24)
@ututa+(compases 7  a 14)
.ENDSCORESTABLE

.H5 Cejilla

.SCORESTABLE MyTable
@ututa+(compases 38 a 41)
@elsueñodelamuñeca+(compases 1  a 16)
@guajiracriolla+(compases 33 a 34)
@ponceprel6+(compases 1 a 13)
@candombeenmi+(compases 5 a 8)
@testamentdamelia+(compases 1 a 16)
.ENDSCORESTABLE

.H4 Obras y estudios sugeridos

.SCORESTABLE myTable
@testamentdamelia
@tarregaprel2
@tarregaprel5
@lagrima
@adelita
.ENDSCORESTABLE

.H3 Actividades

.LISTGROUP
Trabajar sobre al menos 2 fragmentos musicales del apartado __ITALIC__Distensión y contracción__ENDITALIC__.
Trabajar sobre al menos 2 fragmentos musicales del apartado __ITALIC__Posiciones fijas__ENDITALIC__.
Trabajar sobre al menos 2 fragmentos musicales del apartado __ITALIC__Cejilla__ENDITALIC__.
.ENDLISTGROUP

.HR
.TPJUMBO Ligados ascendentes y descendentes. Ligados  con cuerda al aire. Adornos.@Actividades complementarias

.H3 Material de estudio

.H4 Lecturas sugeridas

.SCORESTABLE myTable
@adornos1
@ex_ligados1
.ENDSCORESTABLE

.H4 Fragmentos musicales seleccionados

.H5 Ligados
.SCORESTABLE myTable
@tarregaprel1+(compases 1 a 3)
.ENDSCORESTABLE

.H5 Mordente
.SCORESTABLE myTable
@adelita
@brouwerberceuse+(compás 21)
@undiadenov+(compás 34)
@soundsofbells+(compases 13 a 14 y 22 a 23)
.ENDSCORESTABLE

.H5 Acciaccatura
.SCORESTABLE myTable
@epigramaticos+(Nº1, primer sistema)
@ututa+(compases 1 a 6 y 9 a 10)
@fleuryestilopampeano+(compás 20)
.ENDSCORESTABLE

.H5 Grupeto
.SCORESTABLE myTable
@fleuryestilopampeano+(compases 26 a 29)
.ENDSCORESTABLE

.H5 Glissando
.SCORESTABLE myTable
@adelita
@soundsofbells+(compás 24)
.ENDSCORESTABLE

.H5 Portamento
.SCORESTABLE myTable
@lagrima
@buenayunta+(compases 18 a 19)
@aflordellanto+(compás 28)
.ENDSCORESTABLE

.H4 Obras y estudios seleccionados

.SCORESTABLE myTable
@carcassiest4+(para los ligados)
@brouwerestudio7+(para los ligados)
@brouwerestudio9+(para los ligados)
@brouwerestudio11
@4comets1+(para los ligados)
@candombeenmi+(para los ligados)
@kovats1+(para los ligados)
.ENDSCORESTABLE

.HR
.TPJUMBO Traslados de mano derecha: registros tímbricos. Pizzicato. Pizzicato __ITALIC__alla Bartók__ENDITALIC__. Armónicos naturales y artificiales.@Actividades complementarias

.H3 Material de estudio

.H4 Fragmentos musicales seleccionados

.H5 Registros tímbricos
.SCORESTABLE myTable
@brouwerberceuse+(compases 1 a 4 y 20 a 27)
@undiadenov+(compases 28 a 35)
@brouwerestudio7+(compases 19 a 20)
@plmeltrebol+(compases 1 a 12)
@plmrioabajo+(compases 48 a 49)
@triston+(Tempo primo, compases 46 a 49)
.ENDSCORESTABLE

.H5 Pizzicato
.SCORESTABLE myTable
@brouwerberceuse+(compases 1 a 4)
@guajiracriolla+(compases 1 a 8)
@plmmadameivonne+(compases 1 a 3)
@caminodelastropas+(compases 1 a 4)
.ENDSCORESTABLE

.H5 Armónicos naturales
.SCORESTABLE myTable
@plmrioabajo+(compases 1 a 8)
@vlprel4+(compases 10 y 27 a 32)
@testamentdamelia+(compases 7 a 8)
@epigramaticos+(1º sistema)
@watermusic+(Último movimiento __ITALIC__-Reflecting waters-__ENDITALIC__: 2ª guitarra)
.ENDSCORESTABLE

.H5 Armónicos artificiales
.SCORESTABLE myTable
@testamentdamelia+(compases 17 a 24)
@elsueñodelamuñeca+(parte B)
@pleasepleasemeplm+(compases 3 a 4)
@triston+(últimos 3 compases)
@tarregaprel1+(últimos 4 compases)
@plmmadameivonne+(compases 34 a 35)
.ENDSCORESTABLE

.H4 Obras y estudios sugeridos

.SCORESTABLE myTable
@triston+(para los registros tímbricos)
@tristango+(para los registros tímbricos)
@ponceprel6+(para los registros tímbricos)
@brouwerberceuse+(para el pizzicato y los registros tímbricos)
@guajiracriolla+(para el pizzicato)
@vlprel4+(para los armónicos naturales y los registros tímbricos)
@elsueñodelamuñeca+(parte B: para los armónicos artificiales)
.ENDSCORESTABLE

.HR

.PAGER
foba3.html@Volver
foba3unidad1.html@Anterior
foba3unidad3.html@Siguiente
.ENDPAGER
.PAGER
foba3obras.html@Ir al programa de obras
.ENDPAGER
.HR
