\\time 3/4
{
	<< \\relative c {
		a16-0 __RH #1 e''-0 __RH #4 dis-1\\2 __RH #3 cis-3\\3 __RH #2 e __RH #4 dis __RH #3 cis __RH #2 a-4\\4 __RH #1 dis __RH #4 cis __RH #3 a __RH #2 a, __RH #1 
		a16 __RH #1 e'' dis cis e dis cis a dis cis a a,
		a16 e'' dis cis e dis cis g! dis' cis g a,
		a16 e'' dis cis e dis cis g! dis' cis g a,
		s8^\\markup { "..." }
	} \\\\ \\relative c {
		a4 s4 s8. a16
		a4 s4 s8. a16
		a4 s4 s8. a16
		a4 s4 s8. a16
	} >>
}

