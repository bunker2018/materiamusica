\version "2.18.2"


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\paper {
  bottom-margin = 0
}

#(define guitar-basic (make-fretboard-table))


\storePredefinedDiagram #guitar-basic
\chordmode {c} #guitar-tuning
"x;3;2;o;1;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {d} #guitar-tuning
"x;x;o;2;3;2;"
\storePredefinedDiagram #guitar-basic
\chordmode {e} #guitar-tuning
"o;2;2;1;o;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {f} #guitar-tuning
"1-(;3;3;2;1;1-);"
\storePredefinedDiagram #guitar-basic
\chordmode {g} #guitar-tuning
"3;2;o;o;3;3;"
\storePredefinedDiagram #guitar-basic
\chordmode {a} #guitar-tuning
"x;o;2;2;2;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {b} #guitar-tuning
"x;2-(;4;4;4;2-);"
\storePredefinedDiagram #guitar-basic
\chordmode {c:m} #guitar-tuning
"x;3-(;5;5;4;3-);"
\storePredefinedDiagram #guitar-basic
\chordmode {d:m} #guitar-tuning
"x;x;o;2;3;1;"
\storePredefinedDiagram #guitar-basic
\chordmode {e:m} #guitar-tuning
"o;2;2;o;o;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {f:m} #guitar-tuning
"1-(;3;3;1;1;1-);"
\storePredefinedDiagram #guitar-basic
\chordmode {g:m} #guitar-tuning
"3-(;5;5;3;3;3-);"
\storePredefinedDiagram #guitar-basic
\chordmode {a:m} #guitar-tuning
"x;o;3;3;2;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {b:m} #guitar-tuning
"x;2-(;4;4;3;2-);"
\storePredefinedDiagram #guitar-basic
\chordmode {c:7} #guitar-tuning
"x;3;2;3;1;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {d:7} #guitar-tuning
"x;x;o;2;1;2;"
\storePredefinedDiagram #guitar-basic
\chordmode {e:7} #guitar-tuning
"o;2;o;1;o;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {f:7} #guitar-tuning
"1-(;3;1;2;1;1-);"
\storePredefinedDiagram #guitar-basic
\chordmode {g:7} #guitar-tuning
"3;2;o;o;o;1;"
\storePredefinedDiagram #guitar-basic
\chordmode {a:7} #guitar-tuning
"x;o;2;o;2;o;"
\storePredefinedDiagram #guitar-basic
\chordmode {b:7} #guitar-tuning
"x;2;1;2;o;2;"

#(define guitar-basic-alt (make-fretboard-table))


\storePredefinedDiagram #guitar-basic-alt
\chordmode {c} #guitar-tuning
"3;3;2;o;1;o;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {d} #guitar-tuning
"2;x;o;2;3;o;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {e:m} #guitar-tuning
"o;2;2;o;o;3;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {d:7} #guitar-tuning
"2;x;o;2;1;o;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {e:7} #guitar-tuning
"o;2;o;1;3;o;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {a:7} #guitar-tuning
"x;o;2;o;2;3;"
\storePredefinedDiagram #guitar-basic-alt
\chordmode {b:7} #guitar-tuning
"2;x;1;2;o;2;"

#(define guitar-basic-alt-barre (make-fretboard-table))


\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {c} #guitar-tuning
"x;3-(;5;5;5;3-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {d} #guitar-tuning
"x;5-(;7;7;7;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {g} #guitar-tuning
"3-(;5;5;4;3;3-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {a} #guitar-tuning
"5-(;7;7;6;5;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {c:m} #guitar-tuning
"x;3-(;5;5;4;3-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {d:m} #guitar-tuning
"x;5-(;7;7;6;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {a:m} #guitar-tuning
"5-(;7;7;5;5;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {c:7} #guitar-tuning
"x;3-(;5;3;5;3-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {d:7} #guitar-tuning
"x;5-(;7;5;7;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {g:7} #guitar-tuning
"3-(;5;3;4;3;3-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {a:7} #guitar-tuning
"5-(;7;5;6;5;5-);"
\storePredefinedDiagram #guitar-basic-alt-barre
\chordmode {b:7} #guitar-tuning
"x;2-(;4;2;4;2-);"

\markup { \vspace #2 }

\markup { \column {
\fontsize #2.8 "Acordes en primera posición"
\vspace #1.5
} }

\markup { \vspace #1.2 }

\markup \justify  { \hspace #3
A continuación presentamos las posiciones más usuales para los tipos de
acordes más básicos, en primera posición.
}
\markup { \vspace #1 }

\markup { \vspace #1.2 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 c s d s e s f s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 c s d s e s f s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 g s a s b s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 g s a s b s
      }
    }
  >>
}

\markup { \vspace #2.2 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 c:m s d:m s e:m s f:m s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 c:m s d:m s e:m s f:m s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 g:m s a:m s b:m s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 g:m s a:m s b:m s
      }
    }
  >>
}

\markup { \vspace #2.2 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 c:7 s d:7 s e:7 s f:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 c:7 s d:7 s e:7 s f:7 s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic
        s1 g:7 s a:7 s b:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic
        s1 g:7 s a:7 s b:7 s
      }
    }
  >>
}

\pageBreak
\markup { \fill-line { "
Acordes en primera posición
" "
2
" } }
\markup { \draw-hline }
\markup { \vspace #1 }

\markup { \vspace #1.5 }

\markup \justify  { \hspace #3
Las posiciones que siguen son alternativas a las que presentamos en la
página anterior.
En general difieren más o menos sutilmente en el resultado sonoro;
en particular las de C, D, D7 y B7 dan mejor resultado al rasguear,
al aprovechar mejor las cuerdas graves.
}
\markup { \vspace #1 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic-alt
        s1 c s d s e:m s d:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic-alt
        s1 c s d s e:m s d:7 s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic-alt
        s1 e:7 s a:7 s b:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic-alt
        s1 e:7 s a:7 s b:7 s
      }
    }
  >>
}

\markup { \vspace #1.5 }

\markup \justify  { \hspace #3
Las siguientes son también intercambiables con las anteriores;
en este caso hacemos uso abundante de la \italic{ cejilla } para desplazarnos
hacia el registro màs agudo del instrumento.
}
\markup { \vspace #1 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 c s d s g s a s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 c s d s g s a s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 c:m s d:m s a:m s c:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 c:m s d:m s a:m s c:7 s
      }
    }
  >>
}

\markup { \vspace #0.8 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
    ragged-last = ##f
  }
  
  <<
    \new ChordNames { 
      \chordmode {
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 d:7 s g:7 s a:7 s b:7 s
      }
    }

    \new FretBoards \with {
      \textLengthOn
      \override FretBoard
        #'size = #1.6
      \override FretBoard.fret-diagram-details.finger-code = #'below-string
      \override FretBoard.fret-diagram-details.barre-type = #'straight
      \override FretBoard.fret-diagram-details.number-type = #'roman-upper
    } {
      \chordmode {
        % Same as above
        \set predefinedDiagramTable = #guitar-basic-alt-barre
        s1 d:7 s g:7 s a:7 s b:7 s
      }
    }
  >>
}

\markup { \vspace #1.5 }

\markup \justify  { \hspace #3
Esta presentaciòn de posiciones alternativas no pretende ser exhaustiva,
sino simplemente ilustrar la variedad de sonoridades posibles que la
guitarra ofrece para cada acorde.

}
\markup { \vspace #1 }

