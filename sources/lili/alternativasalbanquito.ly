\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Alternativas al uso del banquito"
\vspace #1.5
} }

\markup \justify  { \hspace #3
“La guitarra debe ajustarse al cuerpo, y no el cuerpo a la guitarra”. La frase
es de Abel Carlevaro, y aún hoy nos parece razonable buscar la relación
óptima entre la guitarra y nuestro cuerpo de acuerdo con este principio.
La respuesta a este problema, tradicionalmente, ha consistido en el uso de
ese artilugio tan resistido, y hasta odiado a veces, por tantísimos
estudiantes de guitarra, al menos durante los primeros años de práctica:
el famoso banquito, que viene a cambiar de una vez y para siempre el hábito de
cruzar las piernas, calzar la guitarra ahí donde mejor caiga, colgarle encima
los brazos de manera que no se nos deslice hacia el suelo, y con lo que nos
quede de movilidad en nuestra ya atareada anatomía, arreglárnosla para rascar
el encordado y, si podemos, sujetar algún acorde, y si hay que cambiarlo por
otro en algún momento, pues Dios prooverá, y si no lo hace nos queda el
consuelo de que Spinetta era musicalmente analfabeto y tampoco usaba esa
miseria de sillita en miniatura donde, según el profe que nos tocó en suerte,
debería ir apoyado el pie izquierdo, y a partir de ahí a arreglárnoslas como
podamos.
Cuestión, que después de una hora de penosísima clase, no pudimos tocar nada
de tan incómodos que estábamos, nos duele hasta la ropa que llevamos puesta,
y ya estamos considerando estudiar Veterinaria o Administración de Empresas, o
subirnos a un taxi y armar una banda Punk, o...
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Claramente, esta línea de razonamiento no nos va a llevar muy lejos.
La respuesta a este dilema es bien otra: salvo que la primera clase de
guitarra sea \italic{realmente} nuestra primerísima experiencia con la
guitarra, lo que traemos a esta primera clase es mucho más que una guitarra,
léase: un pesadísimo bagaje de \italic {pésimos hábitos} que es necesario
romper, para luego cambiar por otros, mejores y más productivos, y recién ahí,
empezar a mejorar como guitarristas y músicos. Entre estos hábitos, y esto es
lo que aquí nos ocupa, uno de los más elementales: sentarnos como venga,
agarrar la guitarra como nos salga, y tocar como primero se nos ocurra. Mala
idea, si el propósito es mejorar.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Entonces: ¿por qué el banquito? Pues por tradición, por años y décadas y hasta
siglos de no haber nadie ideado nada mejor, más práctico, ergonómico,
elegante... pero hay buenas, indiscutibles razones para usar el banquito,
ya explicadas y desarrolladas en las lecturas obligatorias de este práctico
( \italic{Eduardo Fernández - Técnica, Mecanismo, Aprendizaje - 2.1. Posición} ).
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Pero estamos en el Siglo XXI del calendario Gregoriano, y existen hoy por hoy
diversas alternativas a este incómodo artilugio. Cada una de ellas tiene sus
pros y contras, y es decisión de cada uno optar por la solución que mejor le
resulte. Paso a enumerar las que conozco, y los invito a probarlas e incluso
a desarrollar las suyas propias, que si contribuyen a mejorar la experiencia de
tocar la guitarra, tanto mejor para todos.

}
\markup { \vspace #1 }

\markup { \column {
\large "Distintos tipos de banquito"
\vspace #1
} }

\markup \justify  { \hspace #3
Los hay de varios tipos, algunos más caros, otros más económicos, e incluso
algunos pueden fabricarse en forma casera, según nuestras habilidades y
expectativas.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
La figura 1 muestra el banquito plegable metálico más usual;
bastante práctico a la hora de transportarlo, viene con varios registros de altura,
pero es un poco caro y con el tiempo tiende a aflojarse y volverse inestable.
(Las figuras se encuentran en las últimas tres páginas de este documento.)
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En figura 2 vemos un diseño similar, de madera, que con un poco de maña podemos fabricarlo
nosotros mismos; definitivamente es más económico e igualmente práctico.
Los banquitos que usamos en la escuela, de madera también pero sin registros de altura,
son aún más fáciles de hacer pero claramente menos versátiles, ya que al no poder ajustar
la altura, a veces nos quedan incómodos, según nuestra estatura, largo de piernas, etc.

}
\markup { \vspace #1 }

\markup { \column {
\large "El ergoplay y sus variantes"
\vspace #1
} }

\markup \justify  { \hspace #3
Una solución más moderna y muy interesante es el ergoplay,
que se muestra en la figura 3 y del que existen varios modelos comerciales.
No son mucho más caros que un banquito plegable, y su principal ventaja
es que nos permite tocar sentados normalmente, en perfecto equilibrio.
La figura 4 muestra el ergoplay colocado en la guitarra,
y en la figura 5 vemos la posición resultante al tocar.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
La variante mostrada en la figura 6, no mucho más económica comercialmente
pero seguramente más factible de ser construida en casa, con un poco de ingenio y/o
algún taller de costura que nos dé una mano; consiste en una silueta de
gomaespuma, telgopor o algún material similar, recubierto con cuerina.
El único inconveniente de este diseño es que, al igual que el banquito no plegable,
no puede ajustarse a diferentes anatomías o instrumentos; además, precisa de algún
método de fijación a la pierna y a la guitarra para evitar que resbale, por ejemplo algún tipo de fieltro.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Por último, les comparto una experiencia que hicimos hace un par de años en clase, con un grupo de estudiantes
que supieron compensar la imposibilidad de comprar un ergoplay comercial con un maravilloso derroche de ingenio,
y realizaron un ergoplay similar al de la figura 6, construído enteramente de cartón y absolutamente funcional y sólido.
Lamentablemente no conservé las fotos que tomé en esa oportunidad, pero el resultado fue
un éxito, y si bien quedaron pendientes ciertas mejoras, quedó demostrado que con ingenio
y trabajo podemos reemplazar el malhadado banquito de guitarrista por algún recurso
alternativo, adecuado a nuestras posibilidades, y que nos permita lograr una posición
óptima para desarrollar nuestra técnica sin esfuerzos innecesarios.

}
\markup { \vspace #1 }

\pageBreak
\markup { \column {
\huge "Resumen"
\vspace #1.2
} }

\markup \justify  { \hspace #3
En resumen, la idea de este texto es compartirles mis opiniones y experiencias en relación a este tema
tan poco conversado pero ciertamente importante. Yo, personalmente, no tengo ningún problema con usar el
banquito, de hecho me resulta muy cómodo, pero el propósito no es el banquito en sí, sino, como bien dice
Alberto Fernández en su libro:
}
\markup { \vspace #1 }

\markup { \hspace #5 \bold {"-"} \hspace #1 "la posición de tocar debe ser estable;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "debe ser posible mantenerla sin un esfuerzo excesivo;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "debe mantener el instrumento estable con respecto al cuerpo;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "debe permitir realizar cómodamente todos los movimientos necesarios para la" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "ejecución." }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Dicho esto, a trabajar, y que cada uno encuentre su camino, con o sin banquito,
pero siempre con buena técnica, y sobre todo con disfrute y sin esforzarnos de más.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Queda dicho, y hasta la próxima clase.

}
\markup { \vspace #1 }

