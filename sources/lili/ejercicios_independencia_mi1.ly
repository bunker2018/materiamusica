\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Mano izquierda: ejercicios"
\vspace #1.5
} }

\markup { \vspace #2 }

\markup { \column {
\large "Independencia: posiciones"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\clef "treble_8"
\boxMark #"1"
\repeat volta 1 {
<<
	\relative c' { \lhUp <c-2 f-4>4 }
\\
	\relative c { \lhDown \snDown <d-1\5 aes'-3>4 }
>>
r4
<<
	\relative c' { \lhUp <b-4 d-3>4 }
\\
	\relative c { \lhDown \snDown <g-1 f'-2>4 }
>>
r4
}
\boxMark #"2"
\repeat volta 1 {
<<
	\relative c' { \lhUp <d-4 f-2>4 }
\\
	\relative c { \lhDown \snDown <e-3\5 gis-1>4 }
>>
r4
<<
	\relative c' { \lhUp <b-4 d-3>4 }
\\
	\relative c { \lhDown \snDown <g!-2 e'-1>4 }
>>
r4
}
\boxMark #"3"
\repeat volta 1 {
<<
	\relative c' { \lhUp <b-1 e-2>4 }
\\
	\relative c { \lhDown \snDown <e-4\5 gis-3>4 }
>>
r4
<<
	\relative c' { \lhUp <cis-4 e-3>4 }
\\
	\relative c { \lhDown \snDown <a-2 fis'-1>4 }
>>
r4
}
\boxMark #"4"
\repeat volta 1 {
<<
	\relative c' { \lhUp <c-1 g'-3>4 }
\\
	\relative c { \lhDown \snDown <e-2\4 b'-4>4 }
>>
r4
<<
	\relative c' { \lhUp <cis-2 g'-4>4 }
\\
	\relative c { \lhDown \snDown <e-1 bes'-3>4 }
>>
r4
}
} }

\markup { \vspace #2 }

\markup { \column {
\large "Independencia: posiciones con ceja"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\spacingWide
\score { {
\clef "treble_8"
\boxMark #"1"
\repeat volta 1 {
<<
	\relative c' { \lhUp <b-1 f'-4 aes-1>4^\fbIV }
\\
	\relative c { \lhDown \snDown <aes-1 d-2 aes'-3>4 }
>>
r4
<<
	\relative c' { \lhUp  <bes-1 ees-2 g-1>4^\fbIII }
\\
	\relative c { \lhDown <g-1 ees'-4 g-3>4 }
>>
r4
}
\boxMark #"2"
\repeat volta 1 {
<<
	\relative c' { \lhUp <cis-4 e-3 gis-1>4^\fbIV }
\\
	\relative c { \lhDown \snDown <a-2 cis-1 fis-1>4 }
>>
r4
<<
	\relative c' { \lhUp  <b-2 e-4 g-1>4^\fbIII }
\\
	\relative c { \lhDown <g-1 d'-3 f-1>4 }
>>
r4
}
} }


