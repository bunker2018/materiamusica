[dotmagic]

[mytitle Criterios de digitación I]

[subsection Consideraciones generales]
[par]
Se dice, y con razón, que no es posible tocar bien una partitura mal digitada.
Si bien cada mano es un caso particular, y cada guitarrista tiene sus propios
hábitos y preferencias de digitación, existen ciertos criterios básicos que
debemos considerar para tomar las mejores decisiones a la hora de digitar un
pasaje.
[par]
Un caso particular es el de obras escritas por compositores guitarristas,
compuestas, por así decir, &grqq;guitarra en mano&erqq;.
Como ejemplo podemos citar las obras de
Máximo Pujol,
Carlos Moscardini,
Juan Falú,
entre muchos otros.
En estos casos, difícilmente haya que cambiar de digitación, salvo que haya
que elaborarla por no estar indicada en la partitura; de hecho, en general
estas partituras tal como están digitadas da la impresión de que
&grqq;suenan solas&erqq;, y se dejan tocar fácilmente.

[suggested-examples]
Preludio tristón@(Máximo Pujol)@
Tristango en vos@(Máximo Pujol)@
Candombe en mi@(Máximo Pujol)@
El enigma@(Carlos Moscardini)@
[close]

[par]
Un caso muy diferente se da cuando el compositor no maneja la guitarra como
instrumento, salvo a nivel teórico y, por lo tanto, apela a lo que es posible
o realizable en el instrumento. Esto nos deja, en el mejor de los casos,
ante la disyuntiva de elaborar una digitación factible técnica y musicalmente,
y en el peor de los casos (que los hay) con pasajes muy difíciles de resolver.

[suggested-examples]
5 Piezas para Guitarra@(Astor Piazzolla)@
Naranjos en flor@(Mario Castelnuovo Tedesco)@
[close]

[par]
Claramente, esta descripción es muy simplista, y existe una gran variedad de
compositores
&grqq;no guitarristas&erqq;
que han dejado un legado de obras exquisitas y perfectamente tocables. Señalar
los casos extremos tiene como propósito contextualizar el problema de digitar
una partitura o un pasaje determinado.

[suggested-examples]
Suite Castellana@(Federico Moreno Torroba)@
Le Tombeau de Debussy@(Manuel de Falla)@
[close]

[par]
El marco estilístico de la obra en cuestión (período histórico,
características del instrumento para el que fue escrita la obra,
tradiciones y usos típicos del género o estilo en cuestión)
es también un dato importante a tener en cuenta al momento de decidir los
criterios de digitación de la música. Un ejemplo, en cierta forma negativo,
son las transcripciones para guitarra de Andrés Segovia, cuyo legado a la
difusión y al desarrollo del repertorio de la guitarra clásica es innegable,
pero cuyas transcripciones, sobre todo de música anterior al Romanticismo,
adolecen de la falta de información histórica disponible en su época, e
incluso de sus propios (y notables) hábitos de digitación.
Comparar las ediciones de Segovia con las reediciones y revisiones de Stanley
Yates nos da una idea de la evolución de los criterios de digitación.

[suggested-examples Comparar las siguientes versiones:]
Asturias@(Isaac Albéniz, transcripción por Stanley Yates)@
Asturias@(Isaac Albéniz, transcripción por Andrés Segovia)@
_Las partituras se hallan disponibles en la biblioteca del sitio.@
[close]

[par]
A continuación, y sin ánimo de ser exhaustivos, enumeramos algunos criterios
básicos para abordar la digitación de un pasaje musical.

[vspace 1]
[subsection Digitación de mano derecha]

[emph  Evitar cuanto sea posible el &grqq;cruce de dedos&erqq;]
[par]
Tanto en pasajes de escalas como en arpegios,
se desaconseja pasar de una cuerda inferior a otra superior con los dedos
invertidos en relación a la presentación normal de la mano.
Por ejemplo, tocar la 1ª cuerda con
\\hspace #0.8 \\small { \\italic {i} } \\hspace #0.8  
y a continuación la 2ª cuerda con
\\hspace #0.8 \\small { \\italic {m} } \\hspace #0.8 
resulta antinatural e incómodo, sobre todo en pasajes rápidos.

[emph Evitar cuanto sea posible la repetición de dedos]
[par]
Tocar dos notas consecutivas con el mismo dedo, sobre todo en la misma cuerda,
entorpece notablemente la ejecución al tensionar innecesariamente la mano, con
efectos no necesariamente deseables sobre la articulación. En pasajes lentos
puede usarse incluso como recurso, para un mejor control tímbrico; es
frecuente también deslizar un dedo de una cuerda a la inmediata superior,
como efecto tímbrico o bien para simplificar la ejecución del pasaje.
Este recurso funciona muy bien si se apoya ligeramente el toque.
[par]
Deslizar el pulgar hacia abajo también resulta un recurso útil; se trata del
mismo concepto del \\italic{barrido} de púa utilizado en la guitarra
eléctrica, popularizado por Frank Gambale y otros.

[emph El uso del pulgar y el anular como recurso para el traslado]
[par]
En ocasiones, en medio de un pasaje es necesario trasladar la mano derecha
a una posición diferente, ya sea hacia las cuerdas agudas o hacia las graves.
Para pasar a una posición más aguda, bajar el pulgar nos coloca naturalmente
la mano en la posición deseada; lo mismo vale para el anular, que tiende a
desplazar la mano hacia arriba. De esta forma, el traslado transversal de la
mano derecha se logra de forma orgánica y sin esfuerzo.

[vspace 1]
[subsection Digitación de mano izquierda]

[emph Conexión de las posiciones entre sí: dedos guía]
[par]
En caso de tener que enlazar dos posiciones disímiles, por ejemplo
mediante un traslado o un cambio importante en la disposición o
presentación de la mano izquierda, conviene encontrar alguna conexión
posible entre ambas posiciones, que nos sirva de referencia para que la mano
no nos quede
&grqq;en el aire&erqq;.
Siempre que podamos, tratemos de que al menos un dedo se deslice sobre la
misma cuerda, en el caso de un traslado. No es necesario que ese dedo tenga
necesariamente que pisar la nota de llegada; alcanza con tener una referencia concreta de la distancia a recorrer en el traslado.
[par]
En el caso de los cambios de presentación
(de longitudinal a transversal o viceversa),
conviene disponer de un dedo \\italic{pivot,} que al permanecer fijo sobre la
misma nota nos sirve de eje para realizar el giro del brazo con precisión
y seguridad.

[emph Minimizar en lo posible la acumulación de operadores técnicos complejos]
[par]
Se denomina \\italic{operadores técnicos} a todo elemento del mecanismo que
actúa físicamente en el pasaje de una manera significativa, agregando algún
tipo de dificultad o esfuerzo en particular. Eduardo Fernández enumera una
serie de operadores que pueden servirnos como punto de partida (agrego
algunos de mi propia cosecha):

[item la cejilla;]
[item las situaciones de distensión y contracción:]
[item los traslados longitudinales;]
[item los traslados transversales (aunque no necesariamente siempre);]
[item los cambios de presentación (no siempre);]
[item el trabajo en posiciones muy elevadas;]
[item el trabajo en posiciones muy bajas;]
[item ciertos tipos de ligados, especialmente los ornamentales (trinos, etc.);]
[item ciertos traslados transversales de un dedo, que no alcanzan a clasificarse como]
[item_ cambios de posición.]

[par]
Resulta evidente que un  pasaje que involucra tres o cuatro de estos operadores
a la vez, resultará más dificultoso de resolver que otro que no involucra
ninguno de ellos, o sólo unos pocos, o si los operadores intervienen de manera
de darnos tiempo de acomodar la mano entre uno y otro. Sin pretender aquí
establecer una regla rígida, resulta conveniente en general dosificar los
operadores técnicos que intervienen en un mismo pasaje, evitando que su
acumulación dificulte innecesariamente la ejecución, siempre que sea
posible.

[emph Preparar la posición de antemano]
[par]
La transición de un pasaje a otro resultará siempre mucho más trabajosa
e imprecisa si el movimiento lo efectuamos
&grqq;a último momento&erqq;, como quien se
&grqq;zambulle&erqq; en la posición. El concepto de \\italic{preparación de la
posición} nos sugiere que, lo antes posible, presentemos la mano en forma
congruente con la posición de llegada;
ni bien los tiempos de la música nos proporcionan esa pausa tan valiosa (por
ejemplo, mientras el acorde anterior está sonando), la mano puede y debe
aprovechar ese tiempo para reacomodar la presentación, elevar o bajar un dedo,
armar una cejilla, realizar un traslado, distender o contraer la posición...
siempre, claro está, que no se resienta la correcta articulación del pasaje.
[par]
Otro recurso, que tiene que ver con el mismo concepto, se basa en un reflejo
que adquirimos desde muy temprana edad: la mano se dirige
instintivamente allí donde posamos la mirada. Este reflejo adquirido nos es
muy útil a la hora de realizar traslados, sobre todo a posiciones lejanas:
si miramos fijo de antemano el casillero de llegada, la mano izquierda
se ocupará por sí sola de realizar el traslado con precisión, mucho más
fácilmente que si lo hacemos a ciegas, acompañando el movimiento de la mano
con la mirada.

[emph Cuerda al aire y traslados]

[par]
En caso de tener que realizar un traslado a posiciones lejanas,
la posibilidad de intercalar una cuerda al aire entre una posición y otra
facilita enormemente la ejecución del traslado, al otorgarnos un tiempo
extra para acomodar convenientemente la mano. Debe tenerse en cuenta,
naturalmente, el efecto tímbrico y de articulación resultante.

[emph Bisagra]

[par]
Un recurso relativamente nuevo en la técnica de mano izquierda, la llamada
\\italic{bisagra} consiste en pisar la primera cuerda (o en ocasiones incluso
la segunda y tercera) con la base del dedo
1
en forma similar a la presentación
de una cejilla,
sin pisar las cuerdas superiores, con cuidado de mantener el dedo relajado,
sin tensiones innecesarias.
Esto puede, según el caso, favorecer la ejecución del pasaje
de diversas maneras: para preparar una cejilla en la siguiente posición,
para relajar la posición de la mano en general (evitando que el dedo
1
deba arquearse excesivamente para tomar la primera cuerda), para facilitar
una distensión de otro modo trabajosa, para tomar a continuación una nota con dedo 1 en una cuerda grave,  y muchos otros casos particulares.
Recomiendo investigar las posibilidades de esta técnica, que utilizada con
criterio puede convertir en relativamente fáciles pasajes de otro modo
complejos y trabajosos.

[idle]
TODO: cejilla oblicua
[vspace 1]
[subsection Criterios musicales]

[emph Registros timbricos; la sonoridad de la cuerda al aire]

[par]
En la guitarra, como sabemos, la misma altura puede generalmente tocarse en
diferentes cuerdas; por ejemplo, la nota \\italic{si} de la 2ª cuerda al
aire puede tocarse también en la 3ª cuerda, IVª posición, y asimismo en
la 4ª cuerda, IXª posición. Claramente, el efecto tímbrico es diferente
en cada caso, así como la duración efectiva del sonido resultante
\\italic{(sustain),} lo que puede repercutir, no necesariamente en nuestro
favor, en el resultado musical del pasaje.
[par]
Por lo tanto, debemos tener esto en cuenta toda vez que la continuidad tímbrica
sea relevante para la interpretación del pasaje en cuestión. Una manera de
subsanar este inconveniente es mediante un trabajo juicioso de la mano
derecha, desplazando el punto de ataque longitudinalmente sobre la cuerda
para igualar lo más posible el timbre; tomar la cuerda con la uña ligeramente
inclinada, o apoyar el toque en el caso de la cuerda al aire, puede también
aminorar la diferencia tímbrica. En cualquier caso, el oído y la sensibilidad
musical tienen la última palabra.

[emph Uso de armonicos como recurso timbrico y de  digitacion]

[par]
Así como la cuerda al aire puede resolvernos un traslado o pasaje dificultoso,
la posibilidad de tocar una nota como armónico natural puede relajar
considerablemente la ejecución de un pasaje. Cabe aquí la misma aclaración
referida en el punto anterior sobre la diferencia tímbrica entre la nota pisada
y el sonido armónico; no obstante, como efecto tímbrico local, el timbre
particular de los armónicos puede otorgar cierto interés adicional al
pasaje, algo así como un ornamento tímbrico o una nota de color,
por así decir, siempre y cuando no suene extraño o fuera de lugar según el
contexto estilístico de que se trate.

[emph Digitaciones apropiadas para diferentes  tempi]

[par]
La ejecución de un pasaje cambia notablemente según se trate de un tempo
rápido o de uno lento. En general, en pasajes rápidos, prima la movilidad y
agilidad de ambas manos, en tanto que las diferencias de timbre y los matices
de articulación tienden a hacerse menos perceptibles. Por el contrario, en
pasajes lentos, mantener la duración de las notas y controlar el timbre y la
articulación cobran mayor importancia, lo cual en posiciones complejas
puede sobrecargar la mano, por ejemplo al mantener durante mucho tiempo una
cejilla o una posición distendida. Por lo tanto, debe tenerse en cuenta el
tempo del pasaje en cuestión al momento de elaborar la estrategia de digitación:
una cuerda al aire en medio de un traslado tenderá a pasar desapercibida en
un pasaje rápido, mientras que en un pasaje lento nos obligará a controlar más
cuidadosamente el efecto tímbrico, o incluso a cambiar de estrategia.

[emph Continuidad de la  digitacion y continuidad  de la frase]

[par]
Cada traslado, cambio de presentación, en fin, cada operador técnico, supone
una posible interrupción del sonido, o en cualquier caso un esfuerzo adicional
por mantener la continuidad. La música, por otro lado, salvo raros casos, no
se organiza como un continuo indivisible: por el contrario, cada obra se halla
articulada en secciones, éstas en períodos, frases, semifrases, motivos...
La articulación de las diferentes partes que componen la obra es tan
importante como la continuidad interna de cada elemento de la forma.
[par]
Este concepto nos propone una alternativa interesante que nos permite
resolver a la vez el problema musical de articular apropiadamente la forma,
y el problema técnico de distribuir y administrar los operadores técnicos que
ponemos en juego. Si, por ejemplo, hacemos coincidir un traslado con un punto
de articulación de la forma (por ejemplo, entre el final de una frase y el comienzo
de la siguiente), no tendremos necesidad de esforzarnos por conectar las
posiciones; todo lo contrario, usaremos para nuestro provecho la interrupción
del sonido generada por el traslado para favorecer la articulación de la forma.
[par]
En resumen: la propuesta consiste en digitar
&grqq;en bloque&erqq;
cada frase, motivo o cualquier elemento indivisible de la forma, cambiando
de posición en los puntos de articulación de la misma, combinando la digitación
de la obra y el plan de interpretación de forma orgánica y fácilmente realizable.

[emph Igual digitacion para igual material]

[par]
En general, en una pieza musical, el mismo material se desarrolla y varía
conservando cierta consistencia interna: contorno melódico, diseños rítmicos,
interválica... si, de ser esto posible, aplicamos a repeticiones o variaciones del
mismo material musical, patrones de digitación de ambas manos que sean congruentes
o incluso idénticos, esto favorecerá no sólo la memorización de la obra, ya que
estaremos básicamente repitiendo
&grqq;paquetes de movimientos&erqq; fáciles de memorizar a nivel neuromotor,
sino también la correspondencia en la articulación de cada pasaje, lo que
contribuirá a su inmediato reconocimiento como
&grqq;otra versión de la misma cosa&erqq;, que es el fin último de la
interpretación de tales pasajes.
[end]
