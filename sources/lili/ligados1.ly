\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Ligados"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Diferentes clases de ligado: ascendente, descendente, y combinaciones de
los mismos.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  c8-1\3(^\markup \small {ascendente} d-3) r4
  d8-3(^\markup \small {descendente} c-1) r4
  c8-1(^\markup \small {Idem, con cuerda al aire} g-0) r4
  g8-0( d'-3) r4
  c16-1( ^\markup \small {Diferentes combinaciones} d-3 c8-1) d16-3( c-1 d8-3) c16-1( d-3 ees-4 d-3) ~ d8 r8
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Diferentes tipos de ligado"
\vspace #1 } }

\markup { \vspace #2 }

\markup { \column {
\huge "Ligados ascendentes"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Golpe seco sobre la nota con la yema del dedo; el dedo golpea y queda apoyado." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Siempre golpear con la punta de la yema (dedo arqueado), nunca con la parte" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "carnosa (dedo estirado)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Como ejercicio, dar vuelta la guitarra de manera que las cuerdas nos queden" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "contra el cuerpo, y golpear repetidamente sobre el mástil, con la mano en" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "posición normal." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La muñeca puede girar un poco, acompañando el movimiento (como si se desenroscara" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "de la mano)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El mismo ejercicio puede hacerse sobre una mesa, colocando el pulgar debajo" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "y golpeando sobre la tabla, o utilizando el brazo derecho como si fuera el" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "mástil de la guitarra." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Ligados descendentes"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "El dedo que realiza el ligado tira de la cuerda e inmediatamente sale hacia" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "afuera; el efecto es similar a lo que hacemos con la mano derecha al pulsar" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "la cuerda." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "No confundir con retirar simplemente el dedo de la cuerda, ya que no estaríamos" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "produciendo ningún sonido. El ligado debe articular la nota con claridad." }
\markup { \vspace #0.2 }
