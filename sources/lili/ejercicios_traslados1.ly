\markup { \column {
\fontsize #2.8 "Traslado: ejercicios"
\vspace #1.5
} }

\markup { \column {
\huge "Traslado longitudinal"
\vspace #1.2
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\clef "treble_8"
\boxMark #"1"
\relative c, {
f16-1\6
	_\markup \small { a) Continuar hasta la XIª posición y volver siguiendo el diseño. }
	_\markup \small { b) Repetir en todas las cuerdas.}
	_\markup \small { c) Aumentar progresivamente la distancia del traslado.}
	fis-2 g-3 gis-4
a-4 aes-3 g-2 fis!-1
g-1 gis-2 a!-3 ais-4
b-4 bes-3 a-2 gis!-1
	s4^\markup {(etc)}
}
} }

\score { {
\clef "treble_8"
\boxMark #"2"
\relative c, {
f16-1\6
	_\markup \small { a, b, c) Proceder igual que el ejercicio 1. }
	_\markup \small { d) Mantener la mano armada en cada traslado (no contraer la mano). }
	fis-2 g-3 gis-4
fis!-1 g-2 gis-3 a-4
g-1 gis-2 a-3 ais-4
gis!-1 a-2 ais-3 b-4
	s4^\markup {(etc)}
}
} }


\markup { \vspace #2 }

\markup { \column {
\large "Traslado con nota pivot"
\vspace #1
} }

\markup \justify  { \hspace #3
En los siguientes ejercicios, proceder igual que en el ejercicio 2 del
punto anterior.
}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\clef "treble_8"
\boxMark #"1"
\relative c, {
f8-1\6
%	_\markup \small { a, b, c) Proceder igual que el ejercicio 2. }
	fis-2
fis!-1 g-2
g-1 gis-2
gis!-1 a-2
	s4^\markup {(etc)}
}
} }

\score { {
\clef "treble_8"
\boxMark #"2"
\relative c, {
f8-1\6
%	_\markup \small { a, b, c) Proceder igual que el ejercicio 2. }
	g-3
g-1 a-3
a-1 b-3
b-1 cis-3
	s4^\markup {(etc)}
}
} }


\markup { \vspace #2 }

\markup { \column {
\large "Traslado de posiciones fijas"
\vspace #1
} }

\markup \italic  {
TODO: armar ejercicion

}
\markup { \vspace #1 }

\pageBreak
\markup { \vspace #2 }

\markup { \column {
\large "Traslado mediante la cuerda al aire"
\vspace #1
} }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\time 3/4
\boxMark #"1"
\repeat volta 1 { \relative c'' {
a16-2^\markup {I}
	_\markup \small { Repetir varias veces. }
	b-0 c-1 e-0
a-1^\markup {V} b-3 c-4 b-3
a-1 e-0 c-1^\markup {I} b-0
} }
} }

\score { {
\clef "treble_8"
\boxMark #"2"
\time 4/4
\repeat volta 1 { \relative c' {
a16-2^\markup {I}
	_\markup \small { Repetir varias veces. }
	b-0 c-1 e-0
a-1\2^\markup {X} b-3 c-4 d-1
e-3 d-1 c-4 b-3
a-1 e-0 c-1^\markup {I} b-0
} }
} }

\score { {
\clef "treble_8"
\boxMark #"3"
\time 3/4
\relative c {
\snDown
\grace {
	s16 \tweak extra-offset #'(0 . 1.5) ^\markup \teeny \bold { "1)" }
	    \tweak extra-offset #'(0 . -2.9) _\markup \teeny \bold { "2)" }
	s
}
g16-3 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
	b-2 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	d-0 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	f-4 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
g-0 \rhiOffset #'(0 . 0)
		\rhmoffset #'(0 . 0)
	b-0 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	d-2\3 \rhpOffset #'(0 . 0)
				\rhioffset #'(0 . 0)
	f-1\2 \rhiOffset #'(0 . 0)
				\rhmoffset #'(0 . 0)
g-3 \rhmOffset #'(0 . 0)
		\rhioffset #'(0 . 0)
	b-2 \rhaOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhiOffset #'(0 . 0)
				\rhaoffset #'(0 . 0)
	d-4 \rhmOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
d4 \rhaOffset #'(0 . 0)
	 \rhioffset #'(0 . 0)
s4_\markup \small \italic { "H. Villa-Lobos - Preludio Nº3" }
}
} }

\score { {
\clef "treble_8"
\boxMark #"4"
\time 3/4
\relative c, {
\snDown
\grace {
	s16 \tweak extra-offset #'(0 . 1.5) ^\markup \teeny \bold { "1)" }
	    \tweak extra-offset #'(0 . 0) _\markup \teeny \bold { "2)" }
	s
}
fis16-2 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
	ais-1 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	e-2 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
fis-4 \rhiOffset #'(0 . 0)
		\rhmoffset #'(0 . 0)
	ais-3 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	cis-2 \rhpOffset #'(0 . 0)
				\rhioffset #'(0 . 0)
	e-0 \rhiOffset #'(0 . 0)
				\rhmoffset #'(0 . 0)
fis-2\2 \rhmOffset #'(0 . 0)
		\rhioffset #'(0 . 0)
	ais-1 \rhaOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhiOffset #'(0 . 0)
				\rhaoffset #'(0 . 0)
	e-4 \rhmOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
e4 \rhaOffset #'(0 . 0)
	 \rhioffset #'(0 . 0)
s4_\markup \small \italic { "H. Villa-Lobos - Preludio Nº3" }
}
} }


