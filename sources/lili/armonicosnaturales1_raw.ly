\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Armónicos naturales"
\vspace #1.5
} }

\markup \justify  { \hspace #3
La producción de sonidos armónicos es uno de los recursos más habituales en la música para guitarra;
técnicamente son relativamente fáciles de ejecutar,
y constituyen un recurso tímbrico y expresivo muy atractivo.
No obstante, con frecuencia nos encontramos con un armónico en una partitura
y no sabemos cómo resolver su ejecución,
o no estamos seguros de cuál sería la altura correcta del sonido resultante.

}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En parte, como veremos más adelante, esto puede tener que ver con algunas inconsistencias en la escritura
que desgraciadamente abundan en el repertorio;
puede darse el caso de que el pasaje no esté adecuadamente digitado,
y de hecho es habitual que diferentes autores o editores
difieran en el criterio adoptado para indicar la digitación y hasta la altura de los armónicos.

}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Este apunte se propone responder, lo más breve y claramente posible, las siguientes preguntas:

}
\markup { \vspace #1 }

\markup { \hspace #5 \bold {"-"} \hspace #1 "¿Qué son los sonidos armónicos y cómo se producen?" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "¿Dónde se encuentran y qué alturas producen los armónicos naturales en la" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "guitarra?" }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup { \column {
\huge "La serie armónica"
\vspace #1.2
} }

\markup \justify  { \hspace #3
Cuando pulsamos una cuerda, el sonido resultante consiste en una onda de \italic{presión de aire.}
Esta onda está compuesta por una serie de vibraciones parciales superpuestas,
que llamamos \italic{armónicos.}
La onda más grave (es decir, la de menor frecuencia) es la que produce la altura característica del
sonido, o \italic{fundamental.}
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Por ejemplo, si tocamos un \italic{do,} el sonido resultante está compuesto por una onda cuya frecuencia
corresponde a la nota \italic{do,} en este caso la fundamental,
más una serie de vibraciones más agudas (es decir, de menor frecuencia) superpuestas a ésta.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Estas vibraciones parciales se ordenan del grave al agudo y se les asigna un índice;
de esta forma, obtenemos lo que llamamos la \italic{serie armónica:}
a la vibración más grave la llamamos \italic{primer armónico} o \italic{fundamental;} a la siguiente
\italic{segundo armónico,} y así sucesivamente.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
El segundo armónico tiene una frecuencia doble de la del primero, resultando en un sonido una octava por
encima de la fundamental.
El tercer armónico tiene una frecuencia triple de la del primero (dos octavas por encima);
el cuarto armónico, una frecuencia cuatro veces mayor (dos octavas por encima).
Esta relación entre las frecuencias de los armónicos y de la fundamental
se mantiene a medida que avanzamos en la serie.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Por otro lado, cada armónico suena con menor intensidad que el que le
precede en la serie; a partir del quinto armónico, se vuelven prácticamente inaudibles.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Los armónicos son, a su vez, uno de los parámetros que nos permiten reconocer el timbre característico de una fuente sonora
(diferenciar un instrumento de otro, o reconocer el timbre de la voz de una persona),
junto con los \italic{formantes} y los \italic{parciales.}

}
\markup { \vspace #1 }

\markup { \column {
\huge "Los armónicos de una cuerda"
\vspace #1.2
} }

\markup \justify  { \hspace #3
Si pulsamos una cuerda afinada en una determinada altura (nuestra fundamental)
e interrumpimos la vibración exactamente en el punto medio de la cuerda,
se producirá un sonido con una frecuencia doble de la fundamental,
es decir, estaremos produciendo el primer armónico.
Si interrumpimos la vibración en el punto exacto que divide la longitud de la cuerda en tres segmentos iguales,
produciremos el tercer armónico, y así sucesivamente.
La figura 1 muestra gráficamente la ubicación de los sucesivos armónicos en una cuerda.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En todos los instrumentos de cuerda pulsada o frotada, los sonidos armónicos se obtienen siguiendo este principio;
el hecho de que en la guitarra el segundo armónico se ubique justo a la altura del traste XII
no tiene que ver con la ubicación del traste,
sino con que ese es exactamente el punto medio de la cuerda.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Si observamos en la figura la ubicación del tercer armónico,
veremos que hay dos puntos en la cuerda que nos permiten obtener ese armónico
(los dos puntos que dividen la cuerda en tres partes iguales).
Esa es la razón por la que en la guitarra los armónicos del traste VII y XIX
producen exactamente la misma altura:
corresponden a los puntos que dividen la cuerda en tres,
es decir, en los dos casos estamos produciendo el tercer armónico.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En resumen: los armónicos naturales en la guitarra, al igual que en todos los instrumentos de cuerda pulsada o frotada,
se obtienen interrumpiendo la vibración de la cuerda
\italic{exactamente en los puntos que dividen la cuerda en N partes iguales,}
donde \italic{N} corresponde al número de orden del armónico resultante.
En consecuencia, ciertos armónicos pueden obtenerse en diferentes puntos de la cuerda.

}
\markup { \vspace #1 }

\pageBreak
\markup { \column {
\huge "Alturas de los armónicos de la serie"
\vspace #1.2
} }

\markup \justify  { \hspace #3
La figura 2 muestra
los primeros 12 sonidos de
la serie armónica, con la nota \italic{do} como fundamental:

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
  X = \markup \bold {*}
  upper = \relative c' {
  \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/2)
  s1 s s s e^\X g bes^\X c d^\X e fis^\X g
  }
  lower = \relative c, {
  c1
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"1º"}
  c'
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"2º"}
  g'
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"3º"}
  c
  \tweak extra-offset #'(0 . 0.4)
  ^\markup \italic {"4º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"5º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"6º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"7º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"8º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"9º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"10º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"11º"}
  s
  \tweak extra-offset #'(0 . 1.8)
  ^\markup \italic {"12º"}
  }
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
 \new PianoStaff <<
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\override Staff.BarLine.allow-span-bar = ##f
\upper
\revert Score.BarLine.stencil
\revert Staff.BarLine.allow-span-bar
\bar "|"
}
\new Staff {
\clef "bass"
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\override Staff.BarLine.allow-span-bar = ##f
\lower
\revert Score.BarLine.stencil
\revert Staff.BarLine.allow-span-bar
\bar "|"
}
>> } } }

\markup \fill-line { \center-column { \vspace #1
\small "Figura 2: Serie armónica a partir de la nota do."
\vspace #1 } }

\markup \justify  { \hspace #3
(Los armónicos marcados con \bold{*} suenan ligeramente desafinados
en relación al sonido temperado correspondiente.)
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En la guitarra, nos son útiles a los efectos de este apunte los primeros 5 armónicos, ya que los siguientes son
poco audibles y se usan sólo raramente.
Podemos, entonces, deducir de la figura 2 la siguiente relación interválica
entre la fundamental y cada armónico:
}
\markup { \vspace #1 }

\markup { \hspace #5 \bold {"-"} \hspace #1 "1º armónico: nota fundamental;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "2º armónico: una octava por encima de la fundamental;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "3º armónico: una octava y una quinta justa por encima de la fundamental;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "4º armónico: dos octavas por encima de la fundamental;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "5º armónico: dos octavas y una tercera mayor por encima de la fundamental." }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Sabemos, además, que los armónicos se obtienen identificando los puntos que dividen la cuerda
en \italic{N} partes iguales, donde \italic{N} corresponde al número de orden del armónico en la serie.
Tomando como referencia el diapasón de la guitarra, estos puntos son:
}
\markup { \vspace #1 }

\markup { \hspace #5 \bold {"-"} \hspace #1 "1º armónico: la cuerda al aire;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "2º armónico: traste XII;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "3º armónico: trastes VII y XIX;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "4º armónico: traste V;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "5º armónico: trastes IV, IX y XVI." }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup \justify  { \hspace #3
El 5º armónico, en realidad, no se ubica exactamente a la altura de los trastes indicados, sino que se halla
ligeramente desplazado; es necesario encontrar el punto exacto en el que el armónico
suena con la nitidez suficiente.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Con esta información, estamos en condiciones de ubicar con precisión los armónicos naturales
en cada cuerda de la guitarra (figura 3).

}
\markup { \vspace #1 }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . -0.8)
  ^\markup \bold {1ª cuerda}
  }
  \relative c' {
  e1^\flageolet
  e'
  _\markup \teeny {XII}
  b
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  e
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
  gis
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 0.4)
  ^\markup \bold {2ª cuerda}
  }
  \relative c' {
  b1^\flageolet
  b'
  _\markup \teeny {XII}
  fis'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  b,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
  dis
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 1.4)
  ^\markup \bold {3ª cuerda}
  }
  \relative c' {
  g1_\flageolet
  g'
  _\markup \teeny {XII}
  d'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  g,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
  b
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {4ª cuerda}
  }
  \relative c {
  d1_\flageolet
  d'
  _\markup \teeny {XII}
  a'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  d
  _\markup \teeny {V}
  fis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {5ª cuerda}
  }
  \relative c {
  a1_\flageolet
  a'
  _\markup \teeny {XII}
  e'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  a
  _\markup \teeny {V}
  cis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {6ª cuerda}
  }
  \relative c, {
  e1_\flageolet
  e'
  _\markup \teeny {XII}
  b'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  e
  _\markup \teeny {V}
  gis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Figura 3: Los primeros 5 armónicos en cada una de las cuerdas."
\vspace #1 } }

\pageBreak
\markup { \draw-hline }
\markup { \vspace #0.4 }
\markup { \hspace #3 \bold { Ejemplos de armónicos naturales en el repertorio: } }
\markup { \vspace #0.4 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Reginald Smith Brindle:" }
\italic { "Natural harmonics" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Antonio Lauro:" }
\italic { "Vals Nº2" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Atahualpa Yupanqui:" }
\italic { "Duérmete Changuito" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 
\upright { "Las partituras se hallan disponibles en la biblioteca del sitio." }
}
\markup { \vspace #0.2 }
\markup { \draw-hline }
\markup { \vspace #1 }
\markup { \vspace #1 }

\markup { \column {
\large "Armónicos naturales con otras afinaciones"
\vspace #1
} }

\markup \justify  { \hspace #3
La ubicación de los armónicos naturales que presentamos en la figura 3
presupone la afinación tradicional de la guitarra: \italic{E B G D A E}
de la 1ª a la 6ª cuerdas.
En caso de que la afinación sea otra, necesitaremos recalcular las alturas de los armónicos
para aquellas cuerdas cuya afinación sea diferente.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Tomemos como ejemplo la 6ª cuerda afinada en Re:
conociendo la relación interválica entre la fundamental (cuerda al aire)
y los primeros 4 armónicos (figura 2),
podemos deducir las alturas de los armónicos con la nueva
afinación (figura 4).

}
\markup { \vspace #1 }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {6ª cuerda en re}
  }
  \relative c, {
  d1_\flageolet
  d'
  _\markup \teeny {XII}
  a'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  d
  _\markup \teeny {V}
  fis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Figura 4: Los armónicos de la 6ª cuerda en re."
\vspace #1 } }

\markup \justify  { \hspace #3
Otra afinación muy común,
sobre todo en transcripciones de música para laúd,
consiste en bajar la 3ª cuerda a \concat{fa \super \sharp :}

}
\markup { \vspace #1 }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
notes = {
  \clef "treble_8"
  \grace { s16
  \tweak extra-offset #'(-4 . 1.4)
  ^\markup \bold { \concat {"3ª cuerda en fa" \super \sharp } }
  }
  \relative c {
  fis1_\flageolet
  fis'
  _\markup \teeny {XII}
  cis'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
  fis
  _\markup \teeny {V}
  ais,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
  }
  
}
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\notes
\revert Score.BarLine.stencil
\bar "|"
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Figura 5: Los armónicos de la 3ª cuerda en fa sostenido."
\vspace #1 } }

\pageBreak
\markup { \column {
\large "Armónicos artificiales"
\vspace #1
} }

\markup \justify  { \hspace #3
La técnica de
\concat { \char ##x00AB armónicos }
\concat { artificiales \char ##x00BB }
consiste en producir los armónicos con la mano derecha,
generalmente posando la yema del dedo índice
sobre el punto nodal de la cuerda (el punto exacto
donde se ubica el armónico),
y pulsando la cuerda con alguno de los dedos libres,
usualmente el pulgar para las cuerdas graves (bordonas)
y el anular para las agudas (primas).
Como esta técnica nos deja libre la mano izquierda,
podemos entonces pisar una nota y generar con la mano derecha
cualquiera de los sonidos armónicos a partir de la nota
pisada (fundamental). Por ejemplo:

}
\markup { \vspace #1 }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \override TimeSignature.style = #'numbered
  << {
  \override TextSpanner #'direction = #up
  \override TextSpanner #'(bound-details left text) = \markup \small \italic { arm. \concat { 8 \super {va.}}}
  \override TextSpanner.style = #'line
  \override TextSpanner.bound-details.right.text
  = \markup { \draw-line #'(0 . -1) }
  \override TextSpanner.bound-details.left.padding = #-1
  \override TextSpanner.bound-details.right.padding = #-1
  
  s2 \startTextSpan s4 \stopTextSpan
  }{
  \relative c' {
  e4-0
  _\markup \teeny {XII}
  fis-2
  _\markup \teeny {XIV}
  g2-3
  _\markup \teeny {XV}
  }
  } >>
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Figura 6: Ejemplo de armónicos artificiales."
\vspace #1 } }

\markup \justify  { \hspace #3
En el ejemplo de la figura 6, las notas escritas nos indican
qué notas debemos pisar con la mano izquierda; los
casilleros indicados con números romanos indican dónde
se ubica el armónico para cada nota. Los armónicos suenan
una octava arriba, tal como está indicado.

}
\markup { \vspace #1 }

\markup { \draw-hline }
\markup { \vspace #0.4 }
\markup { \hspace #3 \bold { Ejemplos de armónicos artificiales en el repertorio: } }
\markup { \vspace #0.4 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Reginald Smith Brindle:" }
\italic { "Artificial harmonics" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Máximo Pujol:" }
\italic { "Preludio tristón (compases finales)" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Agustín Barrios:" }
\italic { "El sueño de la muñeca (parte B)" }
}
\markup { \vspace #0.2 }
\markup { \draw-hline }
\markup { \vspace #1 }
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Observamos que, para la cuerda al aire, el armónico de octava
se ubica en el traste XII; si pisamos la cuerda en el
IIº casillero, el armónico se desplaza 2 casilleros hacia
el puente, al traste XIV; si pisamos el IIIº casillero,
el armónico se desplaza al traste XV...
Podemos ver que, cuando pisamos una nota, las posiciones
relativas de los armónicos naturales a partir de esa nota
guardan la misma relación que encontramos para las
cuerdas al aire:
el armónico de octava (2º armónico)
se ubica a 12 casilleros de distancia
de la nota pisada, y podemos comprobar que a 7 casilleros
de distancia encontraremos el 3º armónico,
a 5 casilleros de distancia el 4º armónico,
y a 4 casilleros de distancia el 5º armónico.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Podemos, entonces, utilizar la técnica de armónicos
artificiales para producir no sólo el armónico de octava,
sino también todos los demás armónicos superiores
a partir de una nota cualquiera, simplemente memorizando
esta relación interválica.

}
\markup { \vspace #1 }

\markup { \draw-hline }
\markup { \vspace #0.4 }
\markup { \hspace #3 \bold { Ejemplos sugeridos: } }
\markup { \vspace #0.4 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Roland Dyens:" }
\italic { "Syracuse" }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Roland Dyens:" }
\italic { "L'hymne a l'amour" }
}
\markup { \vspace #0.2 }
\markup { \draw-hline }
\markup { \vspace #1 }
\markup { \vspace #1 }

\markup { \column {
\huge "Ejercicios sugeridos"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Escribir todos los armónicos naturales disponibles en la guitarra, del más grave" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "al más agudo; indicar para cada nota la cuerda y el casillero correspondientes;" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "practicar la línea resultante hasta lograr una interpretación fluida." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Componer una melodía utilizando exclusivamente armónicos naturales; escribirla," }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "digitarla y practicarla hasta estar conformes con el resultado." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Ubicar y escribir todos los armónicos naturales disponibles en una o varias de" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "las siguientes afinaciones alternativas (los ejemplos indican las notas de la" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "1ª cuerda a la 6ª):" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "D A F"\super \sharp" D A D" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "D B G D G D" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "F D A D A D" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "E B G D G E"\super \flat"" }
\markup { \vspace #0.2 }
