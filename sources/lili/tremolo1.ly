\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Ejercicios preliminares para el estudio del trémolo"
\vspace #1.5
} }

\markup { \column {
\huge "Generalidades"
\vspace #1.2
} }

\markup \justify  { \hspace #3
El trémolo consiste en un patrón de digitación de mano derecha
que generalmente se mantiene constante durante un largo período
de tiempo, incluso desde el comienzo hasta el final de la obra.
Por lo tanto, es indispensable lograr el nivel de relajación
suficiente para poder sostener la ejecución el tiempo que sea necesario.

}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Para la secuenciación del trabajo, entonces, tendremos en cuenta dos
aspectos:

}
\markup { \vspace #1 }

\markup \justify  {
1) trabajar el mecanismo priorizando el control, la precisión y la
economía de movimientos;

}
\markup { \vspace #1 }

\markup \justify  {
2) incrementar paulatinamente tanto la velocidad como la duración de
los ejercicios, siempre cuidando de no cansarnos ni tensionar las manos
en exceso.

}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
El trabajo de la mano izquierda, si bien puede parecer secundario,
no debe desdeñarse, ya que generalmente implica tomar cada posición
con rapidez y mantenerla hasta el siguiente cambio de posición.
Una adecuada digitación de mano izquierda para los pasajes críticos
de la obra en cuestión (anticipación de posiciones, relevos de dedos,
sustitución de la cejilla por otros recursos, etc.)
nos puede facilitar considerablemente la ejecución.

}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Finalmente, tener en cuenta que contrariamente a lo que a primera vista
pudiera parecer, la velocidad no es tan importante;
el efecto deseado (que el trémolo suene como una nota sostenida
en lugar de una sucesión de ataques)
se logra mediante la regularidad y la suavidad del toque:
no necesariamente rápido, sino suave, regular y controlado.

}
\markup { \vspace #1 }

\markup { \column {
\huge "Aspectos prioritarios del mecanismo"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Equilibrio dinámico y tímbrico de la acción de los dedos "\italic{i m a.}"" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Regularidad métrica (idéntica duración para todas las subdivisiones)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Control de la microdinámica en el crescendo/decrescendo." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La velocidad no es tan importante como la regularidad y la suavidad." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Economía de movimientos (no demasiado amplios, y nunca bruscos ni forzados)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Independencia del pulgar respecto de los dedos "\italic{i m a.}"" }
\markup { \vspace #0.2 }
\markup \justify  { \hspace #3
En relación a este último punto:
mientras los dedos \italic{i m a} realizan el trémolo,
el pulgar se mueve hacia la cuerda que debe tocar a continuación,
en un gesto suave y continuo, sin brusquedades.
Este posiblemente sea el recurso más importante de dominar
para la ejecución correcta del trémolo.

}
\markup { \vspace #1 }

\markup { \column {
\huge "Ejercicios"
\vspace #1.2
} }

\markup \justify  { \hspace #3
A los efectos de dominar la técnica, realizar el trémolo preferentemente
en la 2da o 3ra cuerdas.
Trabajar lentamente al principio, descansando luego de unas cuantas repeticiones,
e incrementar paulatinamente la velocidad y la duración del ejercicio.

}
\markup { \vspace #1 }

\markup { \column {
\large "Trémolo en una cuerda"
\vspace #1
} }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 1 }
  \time 2/4
  \repeat volta 1 { <<
  \relative c' {
  e32\2[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  } \\ \relative c' {
  e8_\rhp
  _\markup \small { Procurar el máximo equilibrio tímbrico y dinámico. }
  e8_\rhp
  e8_\rhp
  e8_\rhp
  }
  >> }
  
} }

\markup { \column {
\large "Trabajo del pulgar"
\vspace #1
} }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 2 }
  \time 2/4
  \repeat volta 1 { <<
  \relative c' {
  r32
  e-4\2\rha e\rhm e\rhi
  r e e e
  r e e e
  r e e e
  } \\ \relative c {
  a8-0_\rhp
  e'8-1_\rhp
  c'8-3_\rhp
  e,8_\rhp
  }
  >> }
  
} }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 3 }
  \time 3/4
  \repeat volta 1 { <<
  \relative c'' {
  r32
  g-4\1\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  r g g g
  r g g g
  r g g g
  r g g g
  r g g g
  r g g g
  r g g g
  r g g g
  } \\ \relative c, {
  e8-0_\rhp[
  e'8-2_\rhp]
  b8-1_\rhp[
  g'8-0_\rhp]
  e8[
  b'8-0]
  g8[
  b8]
  e,8[
  g8]
  b,8[
  e8]
  }
  >> }
  
} }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 4 }
  \time 2/4
  \repeat volta 1 { <<
  \relative c'' {
  r32
  g-4\1\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  r g\rha g\rhm g\rhi
  } \\ \relative c, {
  e8-0_\rhp
  b''8-0_\rhp
  b,8-2_\rhp
  g'8-0_\rhp
  }
  >> }
  
} }

\markup { \vspace #1 }

\markup { \column {
\large "Dinámica"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 5 }
  \time 2/4
  \repeat volta 1 {
  
  <<
  { s2 \p \< s2 s2 s2 \f \> s2 s4. s16. s32 \p }
  { <<
  \relative c' {
  e32\2[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  e[ e_\rha e_\rhm e_\rhi]
  } \\ \relative c' {
  e8_\rhp
  e8_\rhp
  e8_\rhp
  e8_\rhp
  }
  >>
  \repeat unfold 5 {
  <<
  \relative c' {
  e32[ e e e]
  e[ e e e]
  e[ e e e]
  e[ e e e]
  } \\ \relative c' {
  e8
  e8
  e8
  e8
  }
  >>
  }
  } >>
  }
  
} }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 6 }
  \time 4/4
  \repeat volta 1 { <<
  \relative c' {
  r32
  e-4\2\rha e\rhm e\rhi
  r e\rha e\rhm e\rhi
  r e\rha e\rhm e\rhi
  r e\rha e\rhm e\rhi
  r32
  e-4 e e
  r e e e
  r e e e
  r e e e
  } \\ \relative c {
  a8-0_\rhp
  _\markup { \dynamic {f} \italic { (sub.) } }
  e'8-1_\rhp
  c'8-3_\rhp
  e,8_\rhp
  a,8
  _\markup { \dynamic {p} \italic { sub. } }
  e'8
  c'8
  e,8
  }
  >> }
  
} }

\markup { \vspace #1 }

\markup { \column {
\large "Control y relajación"
\vspace #1
} }

\markup \justify  { \hspace #3
A modo de preparación para realizar el trémolo a velocidad,
trabajar el siguiente ejercicio manteniendo el segundo compás
unas pocas repeticiones al principio, aprovechando las
repeticiones del primer compás para recuperar la relajación
y el control.
Paulatinamente, aumentar la cantidad de repeticiones
del segundo compás, hasta lograr mantener el mecanismo bajo
control de forma relajada y natural.

}
\markup { \vspace #1 }

\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 7 }
  \time 4/4
  \repeat volta 1 { <<
  \relative c' {
  r16 e\2-4_\rha[ e_\rhm e_\rhi]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  } \\ \relative c {
  e4-1\4_\rhp
  e4
  e4
  e4
  }
  >> }
  \repeat volta 1 { <<
  \relative c' {
  r32 e[ e e]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  r e[ e e]
  } \\ \relative c {
  e8[
  e8]
  e8[
  e8]
  e8[
  e8]
  e8[
  e8]
  }
  >> }
  
} }

\markup { \draw-hline }
\markup { \vspace #0.4 }
\markup { \hspace #3 \bold { Obras y estudios sugeridos } }
\markup { \vspace #0.4 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Leo Brouwer:" }
\italic { "Estudio Sencillo Nº3." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "B. Terzi:" }
\italic { "Trillo-Tremolo." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "Laurindo Almeida:" }
\italic { "Lamento en forma de trémolo." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "F. Tárrega:" }
\italic { "Recuerdos de la Alhambra; Sueño." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "A. Barrios:" }
\italic { "Canción de la hilandera; El último canto; Contemplación." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "G. Regondi:" }
\italic { "Nocturno; Iº Air Varié." }
}
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1
\upright { "M. M. Ponce:" }
\italic { "Sonata III (3º movimiento)." }
}
\markup { \vspace #0.2 }
\markup { \draw-hline }
\markup { \vspace #1 }
