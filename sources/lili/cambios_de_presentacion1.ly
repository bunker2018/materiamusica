\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Cambios de presentación"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Tipos de presentación:
\italic {transversal,}
\italic {longitudinal,}
\italic {transversal invertida.}

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c {
  <b\5-1 g'\4-4>1
  ^\markup \small {Longitudinal}
  <fis\6-1 gis''\1-1>
  ^\markup \small {Transversal}
  <fis''\1-1 a,,\6-4>
  ^\markup \small {Transversal invertida}
  }
  
} } } }

\markup { \vspace #2 }

\markup { \column {
\huge "Presentación longitudinal"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Dedos paralelos a la cuerda; codo relajado, detrás de la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Brazo perpendicular al mástil." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Presentación transversal"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Dedos aproximadamente paralelos al traste." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Codo arriba, sin tensionar el hombro." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "No quebrar la muñeca, mantener la linealidad del brazo. Pulgar bien situado" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "detrás de la mano." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Presentación transversal invertida"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Dedo 1 más alto que el dedo 4 (similar a la presentación transversal con" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "el ángulo opuesto)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Codo contra el cuerpo, manteniendo la linealidad de la muñeca y el brazo." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Ejercicios"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Manteniendo la mano armada, balancear el codo. No quebrar la muñeca." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "Rotar desde el pulgar, como un péndulo (el pulgar funciona como pivot)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Realizar el siguiente ejercicio:" }
\markup { \vspace #0.2 }
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c {
  b4\6-1
  cis''\1-4 bes-1 cis,,\6-4 bes-1
  c''\1-4 a-1 c,,\6-4 a-1
  b''\1-4 gis-1 b,,\6-4 gis-1
  bes''\1-4 g-1 bes,,\6-4 g-1
  ^ \markup \small {(etc...)}
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Ejercicio para los cambios de presentación"
\vspace #1 } }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Modo de realizar el ejercicio:" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Tocar la primera nota con la mano en posición normal." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Dejando el dedo 1 puesto, rotar el codo hasta que el dedo 4 llega a la siguiente" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"  "} \hspace #1 "nota." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Soltar el dedo 1, dejando puesto el dedo 4, y rotar el codo hasta que el dedo" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"  "} \hspace #1 "1 alcanza la siguiente nota." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Continuar de la misma forma; no quebrar la muñeca ni buscar la nota con el" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"  "} \hspace #1 "dedo. El trabajo debe hacerse exclusivamente rotando el brazo." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "El dedo que dejamos pisando la cuerda mientras rotamos el brazo funciona" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"  "} \hspace #1 "como pivot; el pulgar se desliza acompañando la posición." }
\markup { \vspace #0.2 }
