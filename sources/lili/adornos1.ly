\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Adornos"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Los adornos, u ornamentaciones, son, como su nombre lo indica,
recursos que no son esenciales o estructurales para la línea o la armonía,
sino que sirven para adornar, decorar o embellecer la música.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En general, el uso de determinados adornos varía según el género del que se
trate; hay ocasiones en que los adornos se suelen improvisar con cierta
libertad, sin necesidad de escribirlos (como en el barroco, el jazz y los
géneros populares y folklóricos en general), mientras que en otros casos
los adornos se escriben con exactitud, como en el clasicismo.
Ocurre también que el mismo adorno se deba interpretar de diferente forma
según el género o el período histórico, como los trinos del barroco y del
clacisismo, que si bien pueden aparecer escritos igual, varían en su
correcta interpretación.

}
\markup { \vspace #1 }

\markup { \column {
\huge "Adornos más usuales"
\vspace #1.2
} }

\markup { \column {
\large "Mordente"
\vspace #1
} }

\markup \justify  { \hspace #3
Consiste en ligar hacia la nota vecina diatónica y volver rápidamente a la
nota principal.
En el caso del \italic{mordente superior,} se liga hacia la nota vecina por encima
de la nota principal;
el \italic{mordente inferior} se indica con el signo de mordente atravesado por
una línea vertical, en cuyo caso se liga hacia la nota vecina por debajo
de la nota principal.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  c4\prall s
  c32(^\markup \small {Se toca:} d c8.)
  s4
  c4\mordent s
  c32(^\markup \small {Se toca:} b c8.)
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Mordentes superior e inferior"
\vspace #1 } }

\markup { \column {
\large "Grupeto"
\vspace #1
} }

\markup \justify  { \hspace #3
Puede pensarse como dos mordentes yuxtapuestos;
según la dirección del signo, se parte de la nota principal
hacia la nota superior, de vuelta a la nota principal, luego la nota inferior
y finalmente volvemos a la nota principal \italic{(grupeto ascendente);}
o bien en sentido inverso \italic{(grupeto descendente)}.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWide
  \relative c' {
  c4\turn s
  c32(^\markup \small {Se toca:} d c b c8)
  s4
  c4\reverseturn s
  c32(^\markup \small {Se toca:} b c d c8)
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Grupetos ascendente y descendente"
\vspace #1 } }

\markup { \column {
\large "Trino"
\vspace #1
} }

\markup \justify  { \hspace #3
Consiste en alternar rápidamente entre la nota principal y la nota
vecina superior.
La cantidad de repeticiones (o más propiamente \italic{repercusiones} del
trino) pueden variar según el caso, y de hecho a lo largo de la historia la
interpretación del trino ha variado considerablemente.
Un caso particular es el trino barroco, que consiste en la
\italic{preparación} del trino,
el trino propiamente dicho y la \italic{resolución} del trino,
dando lugar a un ornamento complejo y con muchas variantes posibles.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
A los efectos de este resumen, basta mencionar la diferencia entre
el trino barroco más elemental, que comienza con la nota auxiliar y que
usamos en la interpretación de piezas del barroco y del clasicismo temprano,
y el trino clásico, que comienza con la nota principal, que es el que
usaremos en las obras más actuales (del clasicismo en adelante).

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWide
  \relative c' {
  c4\trill _\markup \small {Barroco} s
  d32(^\markup \small {Se toca:} c d c) ~ c8
  s4
  c4\trill _\markup \small {Clásico} s
  c32(^\markup \small {Se toca:} d c d c8)
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Trinos barroco y clásico"
\vspace #1 } }

\markup { \column {
\large "Apoyatura y acciaccatura"
\vspace #1
} }

\markup \justify  { \hspace #3
Consiste en hacer preceder la nota principal con la nota vecina
superior o inferior.
En el caso de la apoyatura, la nota auxiliar "roba" su valor rítmico a la nota
principal, como se muestra en el ejemplo; la acciaccatura, por el contrario,
se toca como una nota muy breve, dirigiéndose inmediatamente a la nota
principal. Tanto la apoyatura como la acciaccatura pueden ser
superiores o inferiores, y asimismo pueden consistir en más de una
nota (apoyatura doble o triple, por ejemplo).
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
La acciaccatura se distingue de la apoyatura tachando la plica de la nota
de adorno con una pequeña línea inclinada.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  \grace {d8(} c4)^\markup \small {apoyatura}
  \grace {c8(} b4)
  d8([^\markup \small {Se toca:} c8)]
  c( b)
  \grace {a16( b} c4)^\markup \small {doble} ^\markup \small {apoyatura}
  \grace {d16 c(} b4)
  a16(^\markup \small {Se toca:} b c8)
  d16( c b8)
  \slashedGrace {d8(} c4)^\markup \small {acciaccatura}
  \slashedGrace {c8(} b4)
  d64([^\markup \small {Se toca:} c) ~ c8..]
  c64( b) ~ b8..
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Apoyatura y acciaccatura"
\vspace #1 } }

\markup { \column {
\large "Bordadura"
\vspace #1
} }

\markup \justify  { \hspace #3
La bordadura se ejecuta articulando la nota principal,
a continuación la nota auxiliar (superior o inferior según el caso)
para volver a la nota principal, en rápida sucesión:

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  \grace {c16( d} c4)
  ^\markup \small {superior}
  ^\markup \small {bordadura}
  s4
  c32([^\markup \small {Se toca:} d c8.)]
  s4
  \grace {c16( b} c4)
  ^\markup \small {inferior}
  ^\markup \small {bordadura}
  s4
  c32([^\markup \small {Se toca:} b c8.)]
  s4
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Bordadura"
\vspace #1 } }

\markup { \column {
\large "Arpeggiato"
\vspace #1
} }

\markup \justify  { \hspace #3
El \italic {arpeggiato} consiste en tocar las notas
de un acorde en sucesión, a diferencia del acorde \italic {plaqué}
en el que las notas del acorde se atacan simultáneamente.
Generalmente el acorde se arpegia de la nota más grave a la más aguda,
salvo indicación contraria, y de forma más o menos rápida según
el efecto deseado.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En la guitarra tenemos dos formas de ejecutar un \italic {arpeggiato}:
deslizando el pulgar a la manera de un rasgueo más controlado,
o bien incorporando los cuatro dedos de la mano derecha
\italic {(p, i, m, a)}
en un solo gesto suave y fluido, lo que nos permite un mejor control
del sonido y nos deja la mano mejor preparada para seguir tocando.
Si el acorde tuviera más de 4 sonidos, deslizamos el pulgar sobre las
cuerdas más graves y tomamos las 3 cuerdas más agudas del acorde
con los dedos \italic {i m a} como se muestra en el ejemplo.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c, {
  <e b' e gis b e>4\arpeggio
  s8.
  s16^\markup \small {Se toca:}
  \set tieWaitForNote = ##t
  \grace { e32\rhp ~ b'\rhp ~ e\rhp ~ gis\rhi ~ b\rhm ~ e\rha ~ }
  <e,, b' e gis b e>4
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Arpeggiato"
\vspace #1 } }

\markup \justify  { \hspace #3
También podemos aplicar el recurso del \italic {arpeggiato}
en un pasaje a dos o más voces, donde notas de diferentes voces suenan
simultáneamente, por ejemplo bajos y melodía;
en este caso tenemos la opción de arpegiarlas brevemente,
ya sea para realzar la expresividad del pasaje,
o bien para facilitar algún recurso técnico, como es el caso cuando
queremos tocar la nota de la melodía con toque apoyado.
Vale aclarar que no conviene abusar de este recurso,
ya que al igual que cualquier otro recurso expresivo,
tiende a desgastarse su efecto si lo utilizamos sobremanera.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  <<
  \relative c'' {
  g4 s4^\markup \small {Puede tocarse:} g4
  } \\ \relative c, {
  \set tieWaitForNote = ##t
  e4 s4 \grace {e16 ~ s8 } e4
  }
  >>
  }
  
} } } }

\markup { \vspace #2 }

\markup { \column {
\large "Glissando y portamento"
\vspace #1
} }

\markup \justify  { \hspace #3
Tanto el glissando como el portamento consisten en deslizarnos de una nota
hacia otra, haciendo que se escuchen todos los sonidos intermedios.
En la guitarra esto se logra simplemente deslizando el dedo sobre la cuerda
hasta llegar a la nota de destino.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En el glissando, tocamos la nota de partida y deslizamos el dedo hasta alcanzar
la nota de destino;
en el caso del portamento, la nota de destino debe articularse también.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En resumen: En el glissando, toco y deslizo;
en el portamento, toco, deslizo y toco.
Es importante reconocer la diferencia entre un caso y otro,
así como habituarnos a las diferentes maneras de escribirlos.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  c4-1(
  \glissando
  g'-1)
  c,4-1
  \glissando
  \once \override TextScript #'extra-offset = #'(-4.8 . 6.4)
  _\markup \italic \teeny {gliss.}
  g'-1
  s8
  \once \hide NoteHead
  \once \hide Stem
  \once \override NoteHead.no-ledgers = ##t
  \once \override Glissando.bound-details.left.padding = #0.3
  \once \override TextScript #'extra-offset = #'(-1.8 . 8.4)
  \grace { <b, d f >8_\markup \italic \teeny {gliss.} \glissando }
  <e-3 g-2 b-1>2
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Glissando"
\vspace #1 } }

\markup \justify  { \hspace #3
En el segundo compás, la nota (o acorde en este caso) de comienzo no está
definida en la escritura;
lo que se busca es simplemente lograr el efecto sonoro del glissando.
A continuación vemos el mismo recurso aplicado al portamento.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  c4-1(
  \glissando
  \slashedGrace {g'8)}
  g4
  c,4-1
  \glissando
  \once \override TextScript #'extra-offset = #'(-4.8 . 6.4)
  _\markup \italic \teeny {port.}
  g'-1
  \once \hide NoteHead
  \once \hide Stem
  % \once \override NoteHead.no-ledgers = ##t
  % \once \override Glissando.bound-details.left.padding = #0.3
  b,8 \glissando
  \slashedGrace {g'8} g4
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Portamento"
\vspace #1 } }

\markup \justify  { \hspace #3
Podemos encontrarnos con diferentes maneras de escribir
estos adornos;
lo importante es conocer los tipos básicos y su aplicación,
e ir con el tiempo desarrollando el criterio
que nos permita utilizarlos apropiadamente según el caso.

}
\markup { \vspace #1 }

\markup { \vspace #2 }

\markup { \column {
\large "Vibrato"
\vspace #1
} }

\markup \justify  { \hspace #3
El vibrato consiste en la alteración más o menos periódica de la altura del
sonido; se caracteriza en función de la mayor o menor velocidad (o más
propiamente frecuencia) de la variación periódica de altura, y de la mayor o
menor variación de altura en sí misma (extensión o amplitud del vibrato).
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
El uso de este efecto confiere una mayor expresividad al sonido, y al mismo
tiempo, en un instrumento de sonidos breves como la guitarra, contribuye en
cierta medida a prolongar la duración de la nota, o \italic {sustain}.
Como decimos siempre, es un recurso del que no conviene abusar, para no
desgastar su efecto, y debe usarse con criterio y siempre en forma adecuada
al contexto estilístico de la música que se esté interpretando.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
Un vibrato rápido y de gran amplitud puede sonar nervioso o tenso,
mientras que un vibrato lento y sutil sonará calmo, introspectivo o hipnótico.
Todo dependerá del efecto que queramos lograr en el pasaje en cuestión.
}
\markup { \vspace #1 }

\markup \justify  { \hspace #3
En la partitura puede indicarse específicamente el tipo de vibrato, por
ejemplo:
\italic {poco vib., vib. molto} o \italic {molto vib., vib. lento,}
etc. Las indicaciones \italic {non vib., liscio} (liso, en italiano)
o \italic {freddo} (frío),
indican expresamente que no debe vibrarse en absoluto.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c' {
  c8 d e f g2^\markup \italic \small {vib.}
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Vibrato"
\vspace #1 } }

\markup \justify  { \hspace #3
Contrariamente a la técnica de vibrato en la guitarra eléctrica, que se realiza
haciendo oscilar la yema del dedo de la mano izquierda en forma perpendicular
a la cuerda, en la cuerda de nylon el movimiento se realiza en forma paralela
a la cuerda, ejerciendo cierta presión sobre la misma; el efecto es el de
estirar y aflojar la tensión de la cuerda, como si tiráramos de ella en ambos
sentidos, en forma muy sutil. Puede experimentarse también con un movimiento
análogo al vibrato en los instrumentos de cuerda frotada, en el que la yema
del dedo realiza un movimiento circular; hay videos de Steve Vai mostrando
en primer plano esta técnica en la guitarra eléctrica, con muy buen efecto.
En cualquier caso es cuestión de experimentar hasta dar con la técnica que
mejor nos resulte, según los hábitos y preferencias de cada guitarrista.
}
\markup { \vspace #1 }

