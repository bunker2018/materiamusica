\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Arpegios"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Variantes: arpegio ascendente, descendente,
combinaciones y diferentes diseños,
variaciones rítmicas en el pulgar,
diferentes ritmos en el diseño,
incorporación de dobles cuerdas,
arpegios con separación de dedos.

}
\markup { \vspace #1 }

\markup { \hspace #5 \bold {"-"} \hspace #1 "La mano debería trabajar quieta, salvo traslados hacia diferentes cuerdas." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Practicar las variantes más simples e incorporar las variantes más complejas" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "en forma secuenciada." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Tomar los ejercicios propuestos como punto de partida para el diseño de ejercicios" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "más complejos y desafiantes." }
\markup { \vspace #0.2 }
