[version 2.20.0]

[include ly/paper/a4paper.ly]
[include ly/paper/defaults.ly]
[paper]
bottom-margin = 0
[include ly/fretboards/guitar-basic.li]

[vspace 2]
[section Acordes en primera posición]
[vspace 1.2]

[markup-indented justify]
A continuación presentamos las posiciones más usuales para los tipos de
acordes más básicos, en primera posición.
[vspace 1.2]

[fretboards-small guitar-basic c d e f]
[vspace 0.8]
[fretboards-small guitar-basic g a b]
[vspace 2.2]
[fretboards-small guitar-basic c:m d:m e:m f:m]
[vspace 0.8]
[fretboards-small guitar-basic g:m a:m b:m]
[vspace 2.2]
[fretboards-small guitar-basic c:7 d:7 e:7 f:7]
[vspace 0.8]
[fretboards-small guitar-basic g:7 a:7 b:7]

[pagebreak]

[header-fancy]
left: Acordes en primera posición
right: 2
[vspace 1.5]

[markup-indented justify]
Las posiciones que siguen son alternativas a las que presentamos en la
página anterior.
En general difieren más o menos sutilmente en el resultado sonoro;
en particular las de C, D, D7 y B7 dan mejor resultado al rasguear,
al aprovechar mejor las cuerdas graves.
[fretboards-small guitar-basic-alt c d e:m d:7]
[vspace 0.8]
[fretboards-small guitar-basic-alt e:7 a:7 b:7]

[vspace 1.5]
[markup-indented justify]
Las siguientes son también intercambiables con las anteriores;
en este caso hacemos uso abundante de la \\italic{ cejilla } para desplazarnos
hacia el registro màs agudo del instrumento.
[fretboards-small guitar-basic-alt-barre c d g a]
[vspace 0.8]
[fretboards-small guitar-basic-alt-barre c:m d:m a:m c:7]
[vspace 0.8]
[fretboards-small guitar-basic-alt-barre d:7 g:7 a:7 b:7]

[vspace 1.5]
[markup-indented justify]
Esta presentaciòn de posiciones alternativas no pretende ser exhaustiva,
sino simplemente ilustrar la variedad de sonoridades posibles que la
guitarra ofrece para cada acorde.

[end]
