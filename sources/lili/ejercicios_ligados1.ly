\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Ligados: ejercicios"
\vspace #1.5
} }

\markup { \column {
\large "Ligados ascendentes"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 1 }
  \time 3/4
  \repeat volta 1 { \relative c' {
  b8\3-1(
  _\markup \small { Reproducir en todas las cuerdas y en varias posiciones. }
  c-2) b-1( cis-3) b-1( d-4)
  cis-3( d-4) c-2( d-4) c-2( des-3)
  } }
  
} }

\markup { \vspace #2 }

\markup { \column {
\large "Ligados descendentes"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 2 }
  \time 3/4
  \repeat volta 1 { \relative c' {
  c8\3-2(
  _\markup \small { Reproducir en todas las cuerdas y en varias posiciones. }
  b-1) cis-3( b-1) d-4( b-1)
  d-4( cis-3) cis-3( c-2) d-4( c-2)
  } }
  
} }

\markup { \vspace #2 }

\markup { \column {
\large "Ligados con cuerda al aire"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 3 }
  \time 3/4
  \repeat volta 1 { \relative c'' {
  a8\1-1( c-4) c-4( a-1) a-1( e-0)
  e\2-1( fis-3) fis-3( e-1) e-1( b-0)
  cis\3-2([ d-3) d-3( g,-0)]
  g\4-1([ gis-2) gis-2( d-0)]
  dis\5-2([ e-3) e-3( a,-0)]
  } }
  
} }

\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { { \clef "treble_8"
\mark \markup { \box \bold 3 }
  \time 2/4
  \repeat volta 1 { \relative c'' {
  b8\1-3([ c-4) c-4( e,-0)]
  fis\2-3([ g-4) g-4( b,-0)]
  cis\3-2([ d-3) d-3( g,-0)]
  gis\4-2([ a-3) a-3( d,-0)]
  dis\5-2([ e-3) e-3( a,-0)]
  } }
  
} }

