\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Distensión y contracción"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Posiciones distendidas (mano abierta) y contraídas (mano cerrada):

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c {
  <e-2 b'-4 c-1 g'-3>1
  ^\markup \small {Distensión}
  <e-1 bes'-3 cis-2 g'-4>1
  ^\markup \small {Contracción}
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Posiciones distendidas y contraídas"
\vspace #1 } }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Sensación localizada en el centro de la palma." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Distensión: sujetar el cuerpo de la guitarra con los dedos de la mano" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "izquierda, con el pulgar detrás de los demás dedos." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Contracción: juntar las yemas de los 5 dedos, sin estirarlos." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Localizar el esfuerzo en la mano, no en los dedos." }
\markup { \vspace #0.2 }
