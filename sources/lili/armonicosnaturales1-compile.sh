#!/bin/bash

while true; do case $1 in
  -show) show=1; shift;;
  *) break;
esac; done

# Split the file and re-merge with tex/overtone.pdf
rawfile=armonicosnaturales1_raw.li
infile=armonicosnaturales1_raw.pdf
outfile=armonicosnaturales1.pdf
img1=tex/overtone.pdf

lili -pdf $rawfile
pdfjam --outfile ./.arm1.pdf -- $infile 1-2
pdfjam --outfile ./.arm2.pdf -- $infile 3-
pdfjam --outfile $outfile -- .arm1.pdf $img1 .arm2.pdf
rm -v .arm1.pdf .arm2.pdf
[ "$show" ] && mupdf $outfile
echo "All done."
