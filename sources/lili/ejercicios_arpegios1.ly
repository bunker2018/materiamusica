\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Arpegios: ejercicios"
\vspace #1.5
} }

\markup { \vspace #2 }

\markup { \column {
\large "Arpegios circulares"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\time 6/8
\clef "treble_8"

\mark \markup \box \bold { 1 }
\repeat volta 1 {
<<
	\hide NoteHead
	\relative c' {
		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhiOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\relative c { d4. }
>>
}
} }
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\time 6/8
\clef "treble_8"
\mark \markup \box \bold { 2 }
\repeat volta 1 {
<<
	\hide NoteHead
	\relative c' {
		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhaOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\relative c { d4. }
>>
}
} }
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\time 6/8
\clef "treble_8"

\mark \markup \box \bold { 3 }
\repeat volta 1 {
<<
	\hide NoteHead
	\relative c' {
		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhiOffset #'(0.8 . 0.0)

		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhaOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\relative c { d4. s d s }
>>
}

} }

\markup { \vspace #2 }

\markup { \column {
\large "Para ordenar"
\vspace #1
} }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\score { {
\time 6/8
\clef "treble_8"

\boxMark #"1"
\repeat volta 1 {
<<
	\hide NoteHead
	\snDown
	\relative c' {
		r8   \rhpOffset #'(0.0 . 0.2)
		b\3  \rhiOffset #'(0.8 . 1.2)
		b\2  \rhmOffset #'(0.5 . 0.0)
		b\1  \rhaOffset #'(0.8 . 0.2)
		b\2  \rhmOffset #'(0.8 . 0.0)
		b\3  \rhiOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\snDown
	\relative c { d4.\5 }
>>
}

\boxMark #"2"
\repeat volta 1 {
<<
	\hide NoteHead
	\relative c' {
		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhaOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\relative c { d4. }
>>
}

\boxMark #"3"
\repeat volta 1 {
<<
	\hide NoteHead
	\relative c' {
		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhiOffset #'(0.8 . 0.0)

		r8 \rhpOffset #'(0.0 . 0.2)
		b  \rhaOffset #'(0.8 . 0.2)
		b  \rhmOffset #'(0.5 . 0.0)
		b  \rhiOffset #'(0.8 . 1.2)
		b  \rhmOffset #'(0.8 . 0.0)
		b  \rhaOffset #'(0.8 . 0.0)
	}
\\
	\hide NoteHead
	\relative c { d4. s d s }
>>
}

} }



