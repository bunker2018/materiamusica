\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Toques de mano derecha"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Toques: libre, apoyado, de pulgar, plaqué.

}
\markup { \vspace #1 }

\markup { \vspace #2 }

\markup { \column {
\huge "Toque libre"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Articulación desde la segunda falange." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Preparación: dedo frente a la cuerda, separado apenas (pocos mm)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El dedo toma la cuerda en velocidad, no apoyarse en la cuerda y luego tirar" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "de ella." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La mano trabaja en el aire; no apoyar ningún dedo en otras cuerdas ni en la" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "tapa del instrumento." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El movimiento debería ser sólo del dedo, no de toda la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La acción del dedo al tocar y la preparación del siguiente deben hacerse en" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "un solo movimiento, simultáneamente." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Toque apoyado"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "El dedo trabaja recto, articulando desde la primera falange." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El dedo pulsa la cuerda y queda apoyado en la cuerda siguiente." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Para lograr un sonido más lleno y potente, la articulación puede hacerse desde" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "la muñeca, interviniendo toda la mano." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Toque de pulgar"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "El pulgar se ubica más o menos paralelo a la cuerda, no perpendicular a la misma." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El movimiento se articula desde la unión del pulgar con la mano, tomando la" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "cuerda hacia abajo, y continúa el movimiento describiendo un círculo hasta" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "volver a la posición de partida." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El movimiento debe hacerse desde el dedo exclusivamente, sin mover la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El toque de pulgar puede hacerse apoyado, descansando en la cuerda vecina" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "inferior, para lograr un sonido con más cuerpo y volumen." }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup { \column {
\large "Toque de pulgar con yema"
\vspace #1
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Sirve para lograr un sonido más opaco y lleno, o para destacar tímbricamente" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "la línea." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Puede ser de buen efecto hacerlo apoyando." }
\markup { \vspace #0.2 }
\markup { \vspace #1 }

\markup { \column {
\large "Pizzicato"
\vspace #1
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Se efectúa asordinando la cuerda con el filo de la palma." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "El efecto consiste en un sonido apagado y breve, casi staccato, que recuerda" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "el sonido de un contrabajo o cello tocado pizzicato." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Puede mejorarse el efecto realizando el pizzicato con yema." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Una variante es el quasi-pizzicato, que consiste en tocar la nota pizzicato" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "e inmediatamente retirar la mano de la cuerda, quitando la sordina, lo que" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "confiere una resonancia más larga al sonido." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Ejecución de acordes plaqué"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Los dedos trabajan igual que en el toque libre." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Los dedos que realizan el acorde deben estar en contacto, trabajando como un" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "bloque." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "En caso de que intervenga el pulgar, las yemas del pulgar no debe tocarse con" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "la yema del índice." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La mano trabaja inmóvil, sin rebotar." }
\markup { \vspace #0.2 }
