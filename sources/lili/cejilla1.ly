\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Cejilla"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Posiciones con cejilla (o ceja) y media cejilla (o media ceja):

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  \relative c {
  <g-1 d'-3 f-1 b-2 f'-4 g-1>1 ^\fbIII
  ^\markup \small { Cejilla }
  <g'-3 bes-1 d-1 g-1>1 ^\bbIII
  ^\markup \small { Media cejilla }
  }
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Ceja y media ceja"
\vspace #1 } }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Dedo 1 recto pero sin tensión" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Pulgar bien colocado detrás de la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Distribuir el esfuerzo en toda la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "En la media ceja, no quebrar el dedo 1, mantenerlo recto aunque moleste." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Si alguna cuerda no suena, corregir la posición arriba/abajo hasta encontrar" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "la ubicación óptima; dependerá de la contextura de la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Siempre mantener arqueados los dedos 2, 3 y 4 (apuntando hacia las cuerdas)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Ejercicio: armar la ceja en el aire y presionar con el pulgar en diferentes" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "zonas del dedo 1." }
\markup { \vspace #0.2 }
