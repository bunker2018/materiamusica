\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Construcción de la posición"
\vspace #1.5
} }

\markup { \column {
\huge "El cuerpo y el instrumento"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Equilibrio. Distribución del peso en posición de parado." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Distribución de peso en posición de sentado. Isquiones." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Tensiones en estado de reposo. Hombros, columna, piernas, cuello." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La función del banquito. Movilidad, acceso a todos los registros del" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "instrumento." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Distribución del peso en posición asimétrica (banquito)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Colocación de la guitarra. Libertad de movimientos sin sujeción." }
\markup { \vspace #0.2 }
\markup { \column {
\huge "Construcción de la posición"
\vspace #1.2
} }

\markup { \column {
\large "Construcción del ejercicio"
\vspace #1
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Posición de parado. Registro del equilibrio. Distribución del peso." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Posición de sentado. Pasar el peso de un isquión al otro. Trabajo desde los" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "pies." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Colocación del banquito. Registro de equilibrio en posición asimétrica." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Colocación del instrumento. Registro de apoyos y sujeción pasiva." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Registro de movilidad y tensiones. Naturalización de los movimientos:" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Traslados longitudinal y transversal." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Cambios de presentación." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Ceja en primera posición. Presentación transversal en primera posición." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Traslado de primera posición al registro agudo. Trabajo desde los pies" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"  "} \hspace #1 "y los hombros. Corrección del equilibrio." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Trabajo progresivo de la relajación en diversas configuraciones." }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "Rasgueos, pizzicato, técnicas extendidas." }
\markup { \vspace #0.2 }
