\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Técnica básica de mano derecha"
\vspace #1.5
} }

\markup { \column {
\huge "Construcción de la posición"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Armado de la posición: sujetar el antebrazo, sacar el pulgar hacia el codo." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Movimientos básicos: articular con cada dedo, describir círculos con la yema" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "del pulgar." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\huge "Presentación de la mano derecha"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Para tocar una línea melódica en una cuerda: los dedos i-m-a perpendiculares" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "a las cuerdas, el pulgar queda paralelo a las cuerdas." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Para acordes plaqué o arpegios: girar la muñeca de manera que las yemas de los" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "dedos i-m-a queden enfrentados a las cuerdas correspondientes." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "En ningún caso el pulgar debe quedar escondido dentro de la mano." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "La mano debe estar en el aire, el contacto es sólo del antebrazo contra el" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"  "} \hspace #1 "cuerpo de la guitarra." }
\markup { \vspace #0.2 }
