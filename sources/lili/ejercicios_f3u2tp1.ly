\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Unidad 2 - Trabajo práctico Nº1"
\vspace #1.5
} }

\markup { \column {
\huge "Ejercicios"
\vspace #1.2
} }

\markup { \column {
\large "Traslados; líneas melódicas; toque libre"
\vspace #1
} }

\markup \justify  { \hspace #3
Aplicar toque apoyado al final de las frases.

}
\markup { \vspace #1 }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\time 3/4
\mark \markup \box \bold { 1 }
\repeat volta 1 { \relative c'' {
\snDown
<a-2^\rhi>16 ^\markup {I}
	_\markup \small { Repetir varias veces. }
	b-0^\rhm c-1^\rhi e-0^\rhm
a-1^\rhi ^\markup {V} b-3^\rhm c-4^\rhi b-3^\rhm
a-1^\rhi e-0^\rhm c-1^\rhi ^\markup {I} b-0^\rhm
} }
} }

\markup { \vspace #0.5 }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\clef "treble_8"
\mark \markup \box \bold { 2 }
\time 4/4
\repeat volta 1 { \relative c' {
\snDown
<a-2^\rhi>16 ^\markup {I}
	_\markup \small { Repetir varias veces. }
	b-0^\rhm c-1^\rhi e-0^\rhm
a-1\2^\rhi^\markup {X} b-3^\rhm c-4^\rhi d-1^\rhm
e-3^\rhi d-1^\rhm c-4^\rhi b-3^\rhm
a-1^\rhi e-0^\rhm c-1^\rhi^\markup {I} b-0^\rhm
} }
} }

\markup { \vspace #0.5 }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\clef "treble_8"
\mark \markup \box \bold { 3 }
\time 3/4
\relative c, {
\snDown
\grace {
	s16 \tweak extra-offset #'(-0.4 . 1.5) ^\markup \teeny \bold { "1)" }
	    \tweak extra-offset #'(-0.4 . 1) _\markup \teeny \bold { "2)" }
	s
}
fis16-2 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
	ais-1 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
	e-2 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
fis-4 \rhpOffset #'(0 . 0)
		\rhioffset #'(0 . 0)
	ais-3 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-2 \rhmOffset #'(0 . 0)
				\rhioffset #'(0 . 0)
	e-0 \rhaOffset #'(0 . 0)
				\rhmoffset #'(0 . 0)
fis-2\2 \rhpOffset #'(0 . 0)
		\rhioffset #'(0 . 0)
	ais-1 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhmOffset #'(0 . 0)
				\rhioffset #'(0 . 0)
	e-4 \rhaOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
e4 \rhmOffset #'(0 . 0)
	 \rhioffset #'(0 . 0)
s4_\markup \small \italic { "H. Villa-Lobos - Preludio Nº3" }
}
} }

\markup { \vspace #0.5 }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\clef "treble_8"
\mark \markup \box \bold { 4 }
\time 3/4
\relative c {
\snDown
\grace {
	s16 \tweak extra-offset #'(0 . 1.5) ^\markup \teeny \bold { "1)" }
	    \tweak extra-offset #'(0 . -2.9) _\markup \teeny \bold { "2)" }
	s
}
g16-3 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
	b-2 \rhiOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	d-0 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	f-4 \rhpOffset #'(0 . 0)
			\rhioffset #'(0 . 0)
g-0 \rhiOffset #'(0 . 0)
		\rhmoffset #'(0 . 0)
	b-0 \rhmOffset #'(0 . 0)
			\rhaoffset #'(0 . 0)
	d-2\3 \rhpOffset #'(0 . 0)
				\rhioffset #'(0 . 0)
	f-1\2 \rhiOffset #'(0 . 0)
				\rhmoffset #'(0 . 0)
g-3 \rhmOffset #'(0 . 0)
		\rhioffset #'(0 . 0)
	b-2 \rhaOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
	cis-4 \rhiOffset #'(0 . 0)
				\rhaoffset #'(0 . 0)
	d-4 \rhmOffset #'(0 . 0)
			\rhmoffset #'(0 . 0)
d4 \rhaOffset #'(0 . 0)
	 \rhioffset #'(0 . 0)
s4_\markup \small \italic { "H. Villa-Lobos - Preludio Nº3" }
}
} }

\markup { \vspace #0.5 }

\version "2.20.0"
\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\score { {
\clef "treble_8"
\mark \markup \box \bold { 5 }
\time 4/4
\key d \major
\relative c'' {
r8
	<g>16 fis e d cis b
a
	g' fis e d cis b a
g
	fis' e d cis b a g
fis
	e' d cis b a g fis
e
	d' cis b
cis8
s4
_\markup \small \italic { "J. S. Bach - Preludio de" }
_\markup \small \italic { "  la Suite Nº1 para cello" }
}
} }

