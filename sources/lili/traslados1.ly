\version "2.20.0"


%%% Inserting file: /usr/lib/lili/ly/common/analysis.ly
\layout {
\context {
\Voice
\consists "Horizontal_bracket_engraver"
\override HorizontalBracket #'direction = #UP
}
}
%%% /usr/lib/lili/ly/common/analysis.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/a4paper.ly
\paper {
indent = 0
ragged-last = ##t
ragged-bottom = ##t
ragged-last-bottom = ##t
top-margin = 28
bottom-margin = 36
left-margin = 28
right-margin = 28
}

%%% /usr/lib/lili/ly/paper/a4paper.ly: END OF FILE


%%% Inserting file: /usr/lib/lili/ly/paper/defaults.ly
\paper {
  tagline = ##f
  print-page-number = ##f
	#(include-special-characters)
}

%%% /usr/lib/lili/ly/paper/defaults.ly: END OF FILE

\markup { \column {
\fontsize #2.8 "Traslados"
\vspace #1.5
} }

\markup \justify  { \hspace #3
Tipos de traslado:
\italic{longitudinal} y
\italic{transversal}.

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  c'2-1\2^\markup \small {Longitudinal} e'2-1\2
  g2-1\4^\markup \small {Transversal} e'2-1\2
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "Tipos de traslado"
\vspace #1 } }

\markup { \column {
\huge "Traslado longitudinal"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Movimiento del brazo, el codo se muevo sólo lo necesario." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Secuencia de movimientos:" }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "la mano izquierda suelta la posición," }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "movimiento del brazo seco y preciso justo hasta la posición de destino," }
\markup { \vspace #0.2 }
\markup { \hspace #8 \normal-text {"-"} \hspace #1 "mano izquierda toma la posición." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Evitar desplazar la mano antes de soltar la posición." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Evitar tomar la posición de destino antes de llegar con el traslado." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Ejercicio: registrar como sensación las distancias en el mástil." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Mantener la mano armada durante el traslado; controlar la posición del pulgar." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Llegar al traslado con la mano ya preparada (anticipar la posición)." }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Pongo el ojo en el punto de llegada." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\large "Traslado con nota pivot"
\vspace #1
} }

\markup \justify  { \hspace #3
Tipo especial de traslado en que las
posiciones de partida y de destino comparten una nota en la misma cuerda:

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  a2-1\3 c'2-4
  c'-1 r
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "La nota Do funciona como pivot"
\vspace #1 } }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Registrar la referencia como sensación." }
\markup { \vspace #0.2 }
\markup { \vspace #2 }

\markup { \column {
\large "Traslado de posiciones fijas"
\vspace #1
} }

\markup \italic  {
TODO: armar ejercicios

}
\markup { \vspace #1 }

\markup { \vspace #2 }

\markup { \column {
\large "Traslado mediante la cuerda al aire"
\vspace #1
} }

\markup \justify  { \hspace #3
La cuerda al aire puede facilitar el traslado al darnos tiempo para desplazarnos:

}
\markup { \vspace #1 }

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"
\markup { \fill-line {
\score {
\layout { ragged-right = ##t indent = 0 }
{ \clef "treble_8"
  \spacingWider
  a8-2 b-0 c'-1 e'-0^\markup \bold {*} a'-1 b'-3 c''4-4
} } } }

\markup \fill-line { \center-column { \vspace #1
\small "La cuerda al aire (*) nos da tiempo"
\vspace #1 } }

\markup { \vspace #2 }

\markup { \column {
\huge "Traslado transversal"
\vspace #1.2
} }

\markup { \hspace #5 \bold {"-"} \hspace #1 "Movimiento: codo y muñeca adelante/atrás, pulgar desliza arriba/abajo" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "No contraer la mano ni agarrar el mástil; mantener la mano ahuecada" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Mantener relajado el hombro" }
\markup { \vspace #0.2 }
\markup { \hspace #5 \bold {"-"} \hspace #1 "Cuidar que el pulgar no sobresalga por arriba ni pierda contacto por debajo" }
\markup { \vspace #0.2 }
