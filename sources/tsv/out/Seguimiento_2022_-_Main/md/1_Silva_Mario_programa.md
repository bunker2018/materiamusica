# Material del curso - Silva, Mario

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 1º Prof./Tec.

**Horario:** Jueves 20:30 -- 21:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 31/3/2022

- _H. Villa-Lobos_:  Preludio No.1
- _C. Moscardini_:  Doña Carmen
- _M. Llobet_:  Canco del lladre
- _A. Barrios_:  La Catedral (mov. I)
- _A. Lauro_:  Vals No.2
- _F. Tárrega_:  Preludio No.5
- _H. Ayala_:  Guarania
- _M. Castelnuovo Tedesco_:  Appunti X
- _Dusan Bogdanovic_:  Lament (Six Balkan Miniatures, No. 2)
- _F. Hand_:  Estudio No.3
- _Cadícamo/Pereyra_:  Madame Ivonne
