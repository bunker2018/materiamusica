# Seguimiento - Ruiz, Laura Romina

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Martes 18:30 -- 20:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (29/3)


_(Encuentro virtual)_ Presentación de la materia. Recapitulación del material de obras acordado durante el ciclo lectivo 2021.

Tarea asignada: retomar la lectura del programa de obras; considerar la posibilidad de modificar el repertorio. Repasar las actividades complementarias correspondientes a la Unidad 1 del curso (ya abordadas durante el 2021).

### Clase Nº2 (5/4)


Preludio de la Suite No.4 (J. S. Bach): digitación en función de la frase. Criterios básicos de digitación de ambas manos. Contexto estilístico. Material musical de la exposición; consistencia de digitación y articulación en sucesivas elaboraciones del mismo material.

Propuestas de alternativas de obras: trémolos; Waltz for Debby.

### Clase Nº3 (19/4)


Trabajo sobre el Preludio de la Suite No.4 para laúd (J. S. Bach). Primeros 50 compases. Alternativas de digitación. Terceras en doble cuerda.

Presentación de la Unidad 1 del curso: ornamentación en el Barroco. Textos: Donington, La ornamentación libre o italiana. Ejemplos de ornamentación. Trabajo preliminar sobre el Louré y la Gavotte en Rondeau de la Suite.

Propuesta: elaborar un cronograma anual detallado para una mejor organización del trabajo sobre las obras y los contenidos.

## Registro de asistencias

**1º Cuatrimestre:** 3 de 4 (75.0%)

