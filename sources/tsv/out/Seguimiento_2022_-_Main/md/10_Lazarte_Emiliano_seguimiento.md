# Seguimiento - Lazarte, Emiliano

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º FOBA

**Horario:** Lunes 19:30 -- 20:30

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (11/4)


Trabajo sobre Zamba del grillo (A. Yupanqui). Primeros sistemas. Lectura, indicaciones técnicas, métrica.

### Clase Nº2 (18/4)


Trabajo sobre Zamba del grillo (Yupanqui). Lectura de la introducción y las estrofas. Indicaciones rítmicas. Soluciones de digitación de mano derecha. Interpretación de adornos (mordente). Ejecución de ligados ascendentes.

Presentación de Lágrima (F. Tárrega): indicaciones generales; interpretación de arrastres. Se sugiere trabajar Four Comets (R. Towner) y el Estudio Nº9 de L. Brouwer como estudios de ligados.

### Clase Nº3 (25/4)


Trabajo sobre Zamba del grillo (A. Yupanqui). Lectura completa de la partitura. Indicaciones para la digitación de pasajes: dedos guía o pivot. Indicaciones rítmicas; sugerencias para la decodificación y resolución de figuraciones rítmicas complejas. Apagadores. Dinámica, timbre, carácter expresivo de la frase: interpretación de repeticiones de material idéntico o similar.

Lágrima (F. Tárrega): indicaciones técnicas; propuestas de digitación.

## Registro de asistencias

**1º Cuatrimestre:** 3 de 3 (100.0%)

