# Material del curso - Lazarte, Emiliano

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º FOBA

**Horario:** Lunes 19:30 -- 20:30

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 11/4/2022

- _A. Yupanqui_:  Zamba del grillo
- _F. Tárrega_:  Lágrima
- _P. López Minnucci_:  El trébol
- _R. Towner_:  Four comets - I
