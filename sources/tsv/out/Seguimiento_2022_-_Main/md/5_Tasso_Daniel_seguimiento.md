# Seguimiento - Tasso, Daniel

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Jueves 9:00 -- 11:00 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (31/3)


Revisión del programa pautado para el curso durante el ciclo lectivo 2021.

Queda abierta la posibilidad de reemplazar _Que lo diga el río_ (J. Falú) por otra obra popular o folklórica, así como el recorte de la Suite de J.S. Bach (Preludio, Louré, Gavotte en Rondeau).

Planificación del recorrido del curso en función de la demanda técnica e interpretativa del material.

- Primer cuatrimestre: _Sonidos de aquel día_ (Sinesi) y material de aula (Yupanqui); _Fantasía X_ (Mudarra); J.S.Bach (Louré - Gavotte - Preludio).

- Segundo cuatrimestre: _Que lo diga el río_; _Homenaje a Debussy (Falla); _Introducción y Rondó_ (Aguado).

- _Un sueño en la floresta_ (trémolo - A. Barrios): trabajar a lo largo del año, eventualmente reemplazar por otra obra similar (ej. _El último canto_).

Actividad propuesta para el material de aula: determinar el nivel apropiado para cada obra; recorte de contenidos; preparación del material según el caso (digitaciones, indicaciones de interpretación, actividades complementarias).

### Clase Nº2 (28/4)


Trabajo sobre Sonidos de aquel día de Q. Sinesi. Adaptación de la digitación al carácter y contexto estilístico de la obra. Preparación del texto para el trabajo técnico e interpretativo. Planificación y secuenciación de la continuidad del trabajo.

## Registro de asistencias

**1º Cuatrimestre:** 2 de 4 (50.0%)

