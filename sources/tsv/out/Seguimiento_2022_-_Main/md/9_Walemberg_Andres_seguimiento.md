# Seguimiento - Walemberg, Andrés

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º FOBA

**Horario:** Viernes 19:30 -- 21:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (8/4)


Trabajo sobre Un día de noviembre (Brouwer): lectura completa de la obra. Indicaciones técnicas e interpretativas: continuidad del tempo (parte B); diferenciación de planos en sucesión (alternancia de canto y contracanto); resolución de pasajes técnicamente demandantes: ejercicios.

Presentación de Sons de carrilhoes (Guimaraes). Indicaciones generales.

### Clase Nº2 (22/4)


Trabajo sobre Un día de Noviembre de Leo Brouwer. Lectura completa de la obra. Trabajo sobre el rango dinámico y tímbrico; ejercicios y actividades complementarias para la progresiva adquisición y refuerzo de los mecanismos involucrados. Técnica de mano izquierda: presentación de la mano; optimización del trabajo técnico.

Demostración de las obras del programa a trabajar durante las próximas semanas: Estudio Sencillo Nº11 de Leo Brouwer; Lágrima de F. Tárrega.

## Registro de asistencias

**1º Cuatrimestre:** 2 de 2 (100.0%)

