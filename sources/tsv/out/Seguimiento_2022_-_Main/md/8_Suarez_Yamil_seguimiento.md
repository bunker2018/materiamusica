# Seguimiento - Suárez, Yamil

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º Prof./Tec.

**Horario:** Viernes 10:30 -- 12:30 hs -> Martes 10:30 -- 12:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (1/4)


Presentación de la materia. Propuesta de obras: Valses No.3 y 4 (A. Barrios), Confesión (A. Barrios), 10 Canciones Populares Catalanas (M. Llobet), entre otras.

Queda abierta la posibilidad de elaborar una adaptación o arreglo para guitarra, conjuntamente con el espacio de Técnicas de Improvisación.

### Clase Nº2 (8/4)


Propuesta de obras para el programa: Contra marea (Sinesi); Missing Her (F. Hand); Waltz for Debby (B. evans -- arr. R. Towner); 5 Piezas para Guitarra (A. Piazzolla).

Trabajo sobre Agua e vinho (Gismonti). Discusión de alternativas de digitación. Tempo rubato: importancia de la regularidad métrica en el plano de acompañamiento como soporte de las desviaciones rítmicas en la línea. Diseño de ejercicios al respecto. Interpretación del plano de acompañamiento como línea: apagadores.

## Registro de asistencias

**1º Cuatrimestre:** 2 de 3 (66.66666667%)

