# Material del curso - Curayán Tello,  Juan

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Miércoles 10:00 -- 12:00 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 20/4/2022

- _Quique Sinesi_:  Cielo abierto (arr. V. Villadangos)
- _Alonso Mudarra_:  Fantasía X
- _J.S. Bach_:  (a definir -- cf. Unidad 1)
- _Siglo XX_:  (a definir -- cf. Unidad 1)
- _A. Barrios_:  La Catedral (completa)
- _Juan Falú_:  Taxco por siempre _(guarania - trémolo)_
