# Material del curso - Suárez, Yamil

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º Prof./Tec.

**Horario:** Viernes 10:30 -- 12:30 hs -> Martes 10:30 -- 12:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 8/4/2022

- _E. Gismonti_:  Agua e vinho (arr. Pablo Marfil)
