# Seguimiento - Oñate, Cristian

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º FOBA

**Horario:** Jueves 11:00 -- 12:00

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (21/4)


Trabajo de lectura a primera vista sobre el Estudio Nº7 de M. Carcassi. Indicaciones sobre técnicas de estudio (lectura de partituras); construcción de la posición; técnica básica de mano izquierda.

### Clase Nº2 (28/4)


Trabajo sobre el estudio Nº7 de M. Carcassi. Indicaciones técnicas; estrategias para la lectura y memorización del texto.

Construcción de la posición. El cuerpo y el instrumento. Presentación longitudinal de mano izquierda.

Continuidad del trabajo: estudios Nº 1, 4 y 16 de M. Carcassi.

## Registro de asistencias

**1º Cuatrimestre:** 2 de 2 (100.0%)

