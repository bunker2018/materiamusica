# Seguimiento - Morales, Mayra Belén

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º Prof./Tec.

**Horario:** Jueves 18:30 -- 20:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (31/3)


Presentación de la materia. Propuesta de obras: Capricho Árabe (F. Tárrega); Estudio No.2 (F. Hand); Choro de Juliana (M. Pereira).

Planificación del trabajo de preparación del programa de examen de 2º año. Revisión de la obra De aquella luz (W. Heinze): trabajo sobre la definición rítmica de las frases; indicaciones técnicas e interpretativas.

### Clase Nº2 (7/4)


Capricho Árabe (F. Tárrega): trabajo sobre la primera carilla de partitura. Propuestas de digitación; aplicación de recursos dinámicos, tímbricos y temporales en el contexto estilístico de la obra. Ornamentaciones.

### Clase Nº3 (28/4)


Trabajo sobre las obras propuestas para Música de Cámara. Sugerencias de digitación; técnicas de estudio. Adaptación de la técnica de mano izquierda para pasajes rápidos.

Trabajo sobre Capricho Árabe de F. Tárrega. Indicaciones técnicas. Diferenciación de planos sonoros. Técnicas de estudio (trabajo sobre pasajes rápidos _legato_). Ornamentaciones. Trabajo tímbrico.

Gnossienne Nº4 (E. Satie): primeros compases. Sugerencias de digitación. Apoyaturas en doble cuerda. Apagadores.

## Registro de asistencias

**1º Cuatrimestre:** 3 de 4 (75.0%)

