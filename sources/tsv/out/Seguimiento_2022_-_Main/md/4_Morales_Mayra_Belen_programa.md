# Material del curso - Morales, Mayra Belén

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º Prof./Tec.

**Horario:** Jueves 18:30 -- 20:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 31/3/2022

- _M. Pereira_:  Choro de Juliana
- _F. Tárrega_:  Capricho Árabe
- _F. Hand_:  Estudio No.2
- _E. Satie_:  Gnossienne No.4
