# Material del curso - Ruiz, Laura Romina

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Martes 18:30 -- 20:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** Ciclo lectivo 2021

- _J.S. Bach_:  Suite No.4 para laúd (completa o en parte)
- _F. Tárrega_:  Capricho Árabe
- _D. Scarlatti_:  Sonata L366
- _Dusan Bogdanovic_:  Mysterious Habitats
- Trémolo - a definir (Campanas del Alba -- Sainz de la Maza?)
- _Bill Evans_:  Waltz for Debby (arr. Ralph Towner)
