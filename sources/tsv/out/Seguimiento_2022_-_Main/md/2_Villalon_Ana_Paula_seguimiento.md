# Seguimiento - Villalón, Ana Paula

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 1º Prof./Tec.

**Horario:** Miércoles 19:00 -- 20:00 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (30/3)


Presentación de la materia. Indicaciones sobre técnica básica; repaso de estudios y obras breves.

Se sugiere la preparación para el abordaje de obras del nivel mediante la lectura de estudios y obras más accesibles.

### Clase Nº2 (20/4)


La alumna decide discontinuar la cursada por razones laborales. Queda abierto el espacio para continuar la actividad en forma virtual si las circunstancias lo permiten.

## Registro de asistencias

**1º Cuatrimestre:** 2 de 4 (50.0%)

