# Seguimiento - Curayán Tello,  Juan

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Miércoles 10:00 -- 12:00 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (30/3)


Presentación de la materia. Introducción a los contenidos correspondientes a la Unidad 1 del curso. Preselección de las obras del programa.

Tarea asignada para la semana: leer 3er mov. de La Catedral (A. Barrios) y Fantasía X (A. Mudarra).

### Clase Nº2 (6/4)


Trabajo sobre el 3er movimiento de La Catedral (A. Barrios). Lectura de la primera carilla de la partitura. Indicaciones técnicas. Análisis del material en función de la interpretación: textura; microdinámicas; dinámica por registro.

Técnicas de estudio: indicaciones para la lectura a primera vista en función del abordaje de obras de grandes dimensiones.

Continuidad del trabajo: Cielo abierto (Sinesi); avanzar con la lectura de La Catedral (3er movimiento),

### Clase Nº3 (20/4)


Trabajo sobre La Catedral de A. Barrios (mov. III). Lectura completa de la obra. Alternativas  de digitación. Articulación consistente del material. Continuidad  del discurso.

Presentación de la Unidad 1 del curso. Esbozo preliminar del programa de obras.

Continuidad del trabajo: Cielo abierto (Sinesi); Fantasía X (Mudarra).

### Clase Nº4 (27/4)


Trabajo sobre Cielo abierto de Q. Sinesi. Técnicas de estudio (lectura del texto y abordaje de obra). Indicaciones técnicas; sugerencias de digitación. Interpretación de los signos presentes en la partitura. Análisis del texto: forma; texturas; planos sonoros. Se sugiere prestar atención a la figuración rítmica en la lectura del texto.

## Registro de asistencias

**1º Cuatrimestre:** 4 de 5 (80.0%)

