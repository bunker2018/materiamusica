# Material del curso - Tasso, Daniel

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 4º Prof./Tec.

**Horario:** Jueves 9:00 -- 11:00 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** Ciclo lectivo 2021

- _D. Aguado_:  Introducción y Rondó
- _A. Barrios_:  Un sueño en la floresta
- _M de Falla_:  Homenaje a Debussy
- _J.S. Bach_:  Suite No.4 para laúd
- _Quique Sinesi_:  Sonidos de aquel día
- _Juan Falú_:  Que lo diga el río
- _A. Mudarra_:  Fantasía X
