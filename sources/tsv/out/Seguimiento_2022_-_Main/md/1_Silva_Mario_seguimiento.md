# Seguimiento - Silva, Mario

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 1º Prof./Tec.

**Horario:** Jueves 20:30 -- 21:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (31/3)


Presentación de la materia. Propuesta de obras basada en el repertorio trabajado durante el ciclo lectivo 2021 y anteriores. Presentación general de los aspectos técnicos a trabajar durante el año (Unidad 2).

### Clase Nº2 (7/4)


Appunti X (M. Castelnuovo Tedesco): trabajo sobre notas repetidas. Igualación del toque de dedos _i m a_; _crescendo_ y _diminuendo_ graduales. Aplicación a la textura propuesta en la partitura. Planos sonoros.

### Clase Nº3 (28/4)


Trabajo sobre La Catedral de A. Barrios (1er movimiento) y Appunti X de M. Castelnuovo Tedesco. Interpretación en función de la forma: secciones, períodos, frases, semifrases; jerarquización de los puntos de articulación de la forma. Recursos dinámicos y temporales: _crescendo_, _diminuendo_, _rallentando_, _accelerando_, _ritenuto_. Ruptura de la simetría formal y de la regularidad de la frase.

## Registro de asistencias

**1º Cuatrimestre:** 3 de 4 (75.0%)

