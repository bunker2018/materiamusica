# Seguimiento - Sandoval, Marcos

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 1º Prof./Tec.

**Horario:** Miércoles 19:30 -- 20:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (27/4)


Trabajo sobre el Estudio No.1 de M. Carcassi. Digitación de ambas manos; criterios de digitación de mano derecha. Indicaciones técnicas.

Trabajo para la próxima clase: lectura de estudios No. 4, 7 y 16 de M. Carcassi; Estudio Sencillo No.6 de Leo Brouwer.

## Registro de asistencias

**1º Cuatrimestre:** 1 de 1 (100.0%)

