# Material del curso - Walemberg, Andrés

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 3º FOBA

**Horario:** Viernes 19:30 -- 21:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** 18/3/2022

- _J. Guimaraes_:  Sons de carrilhoes
- _F. Tárrega_:  Lágrima
- _L. Brouwer_:  Un día de noviembre
- _L. Brouwer_:  Estudio Sencillo No.11
- _A. Barrios_:  El sueño de la muñeca
