# Seguimiento - Tejerina, Emanuel

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 2º Prof./Tec.

**Horario:** Lunes 18:30 -- 19:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## 1º Cuatrimestre

### Clase Nº1 (28/3)


_(Encuentro virtual)_ Presentación de la materia. Recapitulación del material de obras acordado durante el ciclo lectivo 2021.

Tarea asignada: retomar la lectura del programa de obras; considerar la posibilidad de modificar el repertorio.

### Clase Nº2 (4/4)


Propuesta de obras para un trabajo preliminar de lectura y abordaje de obra: estilos de A. Yupanqui y A. Fleury. Indicaciones sobre técnicas de estudio y lectura a primera vista.

### Clase Nº3 (11/4)


Trabajo sobre Lloran las ramas del viento (A. Yupanqui). Indicaciones de digitación; técnica de estudio (lectura a 1ra. vista). Se sugiere trabajar sobre el Estudio No.16 (M. Carcassi), particularmente la lectura de notas en posiciones elevadas del mástil y la cejilla.

### Clase Nº4 (18/4)


Trabajo sobre El Tostao (estilo, A. Fleury). Indicaciones para la lectura a primera vista: lectura preliminar del ritmo; textura; variaciones del material. Atender a todas las indicaciones de la partitura desde la primera lectura: dinámica, acentos, carácter, tempo. Mantener la regularidad del pulso durante la lectura.

Se sugiere trabajar el material complementario de lectura a primera vista y los 25 estudios de M. Carcassi (lectura de notas en posiciones elevadas).

Continuidad del trabajo: abordar las obras ya leídas (El tostao - A. Fleury; Lloran las ramas del viento - A. Yupanqui) en función de la interpretación musical; continuar el trabajo de lectura con el material complementario sugerido.

## Registro de asistencias

**1º Cuatrimestre:** 4 de 5 (80.0%)

