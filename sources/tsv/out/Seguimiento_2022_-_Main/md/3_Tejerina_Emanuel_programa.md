# Material del curso - Tejerina, Emanuel

**Materia:** Instrumento (Guitarra)

**Carrera/Nivel:** 2º Prof./Tec.

**Horario:** Lunes 18:30 -- 19:30 hs

**Docente:** Marcelo López Minnucci

**Ciclo lectivo:** 2022

## Programa de obras

**Última actualización:** Ciclo lectivo 2021

- _Pablo López Minnucci_:  Río abajo
- _C. Moscardini_:  Despuntando el vicio
- _H. Ayala_:  Choro
- _Damas/Manzi_:  Fuimos (arr. P. López Minnucci)
- _A. Yupanqui_:  Canciones del abuelo No. 1 o 2
- _A. Fleury_:  Real de guitarreros _(milonga)_
