# Default configuration

plain_prompt="${bold}${cyan} :: "
msg0_prompt="${bold}${green} [[=== "
msg1_prompt="${bold}${green} => ${bold}${yellow}"
msg1b_prompt="${bold}${cyan} => ${bold}${magenta}"
msg2_prompt="${green}  -> ${bold}${white}"
msg2b_prompt="${blue}  -> ${bold}${magenta}"
msg3_prompt="${bold}${blue}   -> ${white}"
info_prompt="${blue}${bold} * ${reset}${blue}"
info1_prompt="${blue}${bold}    * ${reset}${blue}"
info2_prompt="${blue}${bold}      * ${reset}${blue}"
info3_prompt="${blue}${bold}        * ${reset}${blue}"
warn_prompt="${red}${bold} WARNING: ${white}"
err_prompt="${red}${bold} ERROR: ${white}"
plain_postprompt=
msg0_postprompt="${bold}${green} ===]]${reset}"
msg1_postprompt=
msg1b_postprompt=
msg2_postprompt=
msg2b_postprompt=
msg3_postprompt=
info_postprompt=
info1_postprompt=
info2_postprompt=
info3_postprompt=
warn_postprompt=
err_postprompt=

YEAR=2022
TEACHER="Marcelo López Minnucci"
HTMLDIR="../../public/private"
ROOTURL="https://bunker2018.gitlab.io/materiamusica/private"
HTMLTITLE="Ciclo Lectivo 2022"
HTMLHOME="main.html"
BULK_PREFIX="2022_instrumento_guitarra_marcelo_lopez_m"
ASSIST="private/Weekly Schedule 2022 - Asistencia.pdf"
