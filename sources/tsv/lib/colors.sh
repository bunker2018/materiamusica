#!/bin/bash

# Ansi color code variables
black="\e[0;90m"
red="\e[0;91m"
green="\e[0;92m"
yellow="\e[0;93m"
blue="\e[0;94m"
magenta="\e[0;95m"
cyan="\e[0;96m"
white="\e[0;97m"
expand_bg="\e[K"
black_bg="\e[0;100m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
yellow_bg="\e[0;103m${expand_bg}"
blue_bg="\e[0;104m${expand_bg}"
magenta_bg="\e[0;105m${expand_bg}"
cyan_bg="\e[0;106m${expand_bg}"
white_bg="\e[0;107m${expand_bg}"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"

