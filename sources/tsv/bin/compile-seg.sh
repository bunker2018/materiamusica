#!/bin/bash

# Load common functions
. ./lib/common.sh

# Load config if exists
if [ -f ./config.sh ]
  then . ./config.sh; fi;

USAGE="Usage: $0 [OPTIONS] [--] FILE"
_day=$(date +%d | sed -e 's/^0//')
_month=$(date +%m | sed -e 's/^0//')
_year=$(date +%Y)
CURRENT_DATE="${_day}/${_month}/${_year}"

show_help() {
  echo "$USAGE"
  echo
  echo "Options:"
  echo "  -d, --date DATE         Set current date to DATE (string)"
  echo "  -y, --year YEAR         Set current year to YEAR"
  echo "  -t, --teacher TEACHER   Set teacher name"
  echo "  -h, --help              Show this help message"
  echo
  echo "Check 'lib/template.tsv' for syntax"
}

make_class() {
  anum=$1
  class_date=$2
  txt="$3"
  period=$4
  classes_outfile=build/clases_${period}_${anum}
  echo "__CLASS:$class_date" >>$classes_outfile
  echo >>$classes_outfile
  echo "$txt" | sed -e "s/ __N__/\n\n/g" >>$classes_outfile
  echo >>$classes_outfile
}

sanitize_string() {
  echo $@ | sed -e '
s/,//;
s/ /_/g;
s/á/a/g;
s/é/e/g;
s/í/i/g;
s/ó/o/g;
s/ú/u/g;
s/Á/A/g;
s/É/E/g;
s/Í/i/g;
s/Ó/O/g;
s/Ú/U/g;
  '
}

cute() {
  case $1 in
    diagnostico) echo -n "Diagnóstico";;
    programa) echo -n "Programa";;
    seguimiento) echo -n "Seguimiento";;
    evaluacion) echo -n "Evaluación";;
  esac
}

while true; do case $1 in
  -d|--current-date) CURRENT_DATE="$2"; shift 2;;
  -y|--year) YEAR=$2; shift 2;;
  -t|--teacher) TEACHER=$2; shift 2;;
  -h|--help) show_help; exit 0;;
  --) shift; break;;
  *) break;;
esac; done

infile="$1"

[ "$infile" ] || abort 1 "$USAGE"
[ -f "$infile" ] || abort 1 "'$infile': file not found."

msg0 "Compiling: '$infile'"

# Clean build directory
rm ./build/* >/dev/null 2>&1

# Initialize
lnum=0
mode='ignore'
prog_ini=0
diag_ini=0
prop_ini=0
eval_1c_ini=0
eval_2c_ini=0
eval_FINAL_ini=0

cat "$infile" | while read line; do
  lnum=$(($lnum+1))
  comment=0
  LABEL="$(echo "$line" | cut -f1)"
  case "$LABEL" in
    COMMENT)
      # Safely and silently ignore this line
      comment=1;;
    ALUMNO)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ALUMNO';;
    NIVEL)
      msg1 "Line $lnum: found label '$LABEL'"; mode='NIVEL';;
    HORARIO)
      msg1 "Line $lnum: found label '$LABEL'"; mode='HORARIO';;
    ASIST_1c_T)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_1c_T';;
    ASIST_1c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_1c';;
    ASIST_1c_PERC)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_1c_PERC';;
    ASIST_2c_T)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_2c_T';;
    ASIST_2c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_2c';;
    ASIST_2c_PERC)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_2c_PERC';;
    ASIST_ANN_T)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_ANN_T';;
    ASIST_ANN)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_ANN';;
    ASIST_ANN_PERC)
      msg1 "Line $lnum: found label '$LABEL'"; mode='ASIST_ANN_PERC';;
    CLASES_1c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='CLASES_1c';;
    CLASES_2c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='CLASES_2c';;
    PROGRAMA)
      msg1 "Line $lnum: found label '$LABEL'"; mode='PROGRAMA';;
    DIAGNOSTICO)
      msg1 "Line $lnum: found label '$LABEL'"; mode='DIAGNOSTICO';;
    PROPUESTA)
      msg1 "Line $lnum: found label '$LABEL'"; mode='PROPUESTA';;
    EVALUACION_1c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='EVALUACION_1c';;
    EVALUACION_2c)
      msg1 "Line $lnum: found label '$LABEL'"; mode='EVALUACION_2c';;
    EVALUACION_FINAL)
      msg1 "Line $lnum: found label '$LABEL'"; mode='EVALUACION_FINAL';;
    -)
      : msg2 "Line $lnum: no label, parsing continues";;
    *)
      warnmsg "Line $lnum: unknown label"; mode='IGNORE' ;;
  esac
  
  # Skip parsing if comment
  [[ $comment -eq 1 ]] && continue

  # Parse current line according to $mode
  case $mode in
    ALUMNO)
      msg2 "Initializing students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        ANUM=$(($f-1))
        msg3 "$anum: '$str'"
        echo "$str" >>build/alumnos.lst
      done
    ;;
    NIVEL)
      msg2 "Parsing academic levels for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/niveles.lst
      done
    ;;
    HORARIO)
      msg2 "Parsing class schedule for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/horarios.lst
      done
    ;;
    ASIST_1c_T)
      msg2 "Parsing assistance (1c_t) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_1c_t.int
      done
    ;;
    ASIST_1c)
      msg2 "Parsing assistance (1c) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_1c.int
      done
    ;;
    ASIST_1c_PERC)
      msg2 "Parsing assistance (1c_perc) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_1c_perc.int
      done
    ;;
    ASIST_2c_T)
      msg2 "Parsing assistance (2c_t) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_2c_t.int
      done
    ;;
    ASIST_2c)
      msg2 "Parsing assistance (2c) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_2c.int
      done
    ;;
    ASIST_2c_PERC)
      msg2 "Parsing assistance (2c_perc) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_2c_perc.int
      done
    ;;
    ASIST_ANN_T)
      msg2 "Parsing assistance (ANN_t) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_ann_t.int
      done
    ;;
    ASIST_ANN)
      msg2 "Parsing assistance (ANN) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_ann.int
      done
    ;;
    ASIST_ANN_PERC)
      msg2 "Parsing assistance (ANN_perc) for $ANUM students"
      f=1
      while true; do
        f=$(($f+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$f)"
        case $str in -*) break;; esac
        [[ $str == '' ]] && break
        anum=$(($f-1))
        echo "$str" >>build/assist_ann_perc.int
      done
    ;;
    CLASES_*)
      [[ $LABEL != '-' ]] && {
        msg2 "Parsing classes (${classes_period}) for $ANUM students"
        classes_period=$(echo $LABEL | cut -d_ -f2)
      }
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"
        case $str in
          _DATE_*)
            class_date="$(echo "$str" | cut -d'_' -f3)"
            txt="$(echo "$str" | cut -d'_' -f4-)"
          ;;
          -) continue;;
          *)
            class_date="EMPTY"
            txt="$str"
          ;;
        esac
        make_class $anum $class_date "$txt" $classes_period
      done
    ;;
    PROGRAMA)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing repertoire for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $prog_ini -eq 0 ]]
        then
          echo "_DATE:$str" >build/programa_$anum
        else
          case $str in
            -) continue;;
            *) echo "_WRK:$str" >>build/programa_$anum;;
          esac
        fi
      done
      prog_ini=1
    ;;
    DIAGNOSTICO)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing diagnostics for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $diag_ini -eq 0 ]]
        then
          case $str in
            -) continue;;
            *) echo "_$str" >build/diagnostico_$anum;;
          esac
        else
          case $str in
            -) continue;;
            *) echo -e "${str}\n" >>build/diagnostico_$anum;;
          esac
        fi
      done
      diag_ini=1
    ;;
    PROPUESTA)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing proposal for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $prop_ini -eq 0 ]]
        then
          case $str in
            -) continue;;
            *) echo "_$str" >build/propuesta_$anum;;
          esac
        else
          case $str in
            -) continue;;
            *) echo -e "${str}\n" >>build/propuesta_$anum;;
          esac
        fi
      done
      prop_ini=1
    ;;
    EVALUACION_1c)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing eval (1c) for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $eval_1c_ini -eq 0 ]]
        then
          case $str in
            -) continue;;
            *) echo "_$str" >build/evaluacion_1c_$anum;;
          esac
        else
          case $str in
            -) continue;;
            *) echo -e "${str}\n" >>build/evaluacion_1c_$anum;;
          esac
        fi
      done
      eval_1c_ini=1
    ;;
    EVALUACION_2c)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing eval (2c) for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $eval_2c_ini -eq 0 ]]
        then
          case $str in
            -) continue;;
            *) echo "_$str" >build/evaluacion_2c_$anum;;
          esac
        else
          case $str in
            -) continue;;
            *) echo -e "${str}\n" >>build/evaluacion_2c_$anum;;
          esac
        fi
      done
      eval_2c_ini=1
    ;;
    EVALUACION_FINAL)
      [[ $LABEL != '-' ]] && \
        msg2 "Parsing eval (FINAL) for $ANUM students"
      for anum in $(seq 1 $ANUM); do
        sf=$(($anum+1)) # start with 2nd field
        str="$(echo -en "$line" | cut -f$sf)"

        if [[ $eval_FINAL_ini -eq 0 ]]
        then
          case $str in
            -) continue;;
            *) echo "_$str" >build/evaluacion_FINAL_$anum;;
          esac
        else
          case $str in
            -) continue;;
            *) echo -e "${str}\n" >>build/evaluacion_FINAL_$anum;;
          esac
        fi
      done
      eval_FINAL_ini=1
    ;;
    IGNORE)
      : # Ignoring this line (comment or unknown label)
    ;;
    *)
      warnmsg "mode set to unknown value $mode"
    ;;
  esac
done

msg1b "Parsing done"

# Clean (or create) output directory tree
main="$(echo "$infile" | sed -e 's/ /_/g; s/.tsv//')"
msg1b "Cleaning output directories for: '$main'"
rm out/${main}/{md,html,pdf}/* 2>/dev/null
rm out/${main}/* 2>/dev/null
mkdir -p out/${main}/{md,html,pdf}
outdir=out/$main

msg0 "Generating data buffer"

NUM=$(cat ./build/alumnos.lst | wc -l)

# Generate markup for each student
for i in $(seq 1 $NUM); do
  
  # Gather information
  name="$(cat build/alumnos.lst | sed -n ${i}p)"
  schedule="$(cat build/horarios.lst | sed -n ${i}p)"
  lvl="$(cat build/niveles.lst | sed -n ${i}p)"

  case $lvl in
    CS*)
      level="$(echo $lvl | sed -e 's/CS//; s@$@º Prof./Tec.@')"
    ;;
    FOBA*)
      level="$(echo $lvl | sed -e 's/FOBA//; s@$@º FOBA@')"
    ;;
  esac

  ass1c="$(cat build/assist_1c.int | sed -n ${i}p| sed -e 's/,/./')"
  ass1c_perc="$(cat build/assist_1c_perc.int | sed -n ${i}p| sed -e 's/,/./')"
  ass1c_t="$(cat build/assist_1c_t.int | sed -n ${i}p| sed -e 's/,/./')"
  ass2c="$(cat build/assist_2c.int | sed -n ${i}p| sed -e 's/,/./')"
  ass2c_perc="$(cat build/assist_2c_perc.int | sed -n ${i}p| sed -e 's/,/./')"
  ass2c_t="$(cat build/assist_2c_t.int | sed -n ${i}p| sed -e 's/,/./')"
  assann="$(cat build/assist_ann.int | sed -n ${i}p| sed -e 's/,/./')"
  assann_perc="$(cat build/assist_ann_perc.int | sed -n ${i}p| sed -e 's/,/./')"
  assann_t="$(cat build/assist_ann_t.int | sed -n ${i}p| sed -e 's/,/./')"

  msg1 "Student: '$name' ($level) - $schedule"
  
  #######################################################
  # Generate buffer

  {
    cat <<!
name="$name"
level="$level"
schedule="$schedule"
!
  } >build/${i}.buf

  # Assistance report
  {
    # No classes yet
    if [[ $assann_t == '0' ]]; then cat <<!
ALL_ASS="-"
!
    # Just 1st period
    elif [[ $ass2c_t == '0' ]]; then
      _this_perc=$(echo "scale=2; $ass1c_perc*1.0" | bc -l)
      cat <<!
ALL_ASS="${ass1c} de ${ass1c_t} (${_this_perc}%)"
!
    # Both periods and full year
    else
      # Round percents to 2 decimal points
      _this_perc1=$(echo "scale=2; $ass1c_perc/1.0" | bc -l)
      _this_perc2=$(echo "scale=2; $ass2c_perc/1.0" | bc -l)
      _this_percann=$(echo "scale=2; $assann_perc/1.0" | bc -l)
      cat <<!
ASS_1="${ass1c} de ${ass1c_t} (${_this_perc1}%)"
ASS_2="${ass2c} de ${ass2c_t} (${_this_perc2}%)"
ALL_ASS="${assann} de ${assann_t} (${_this_percann}%)"
!
    fi
  } >build/${i}_assist.buf

  # bc(1) developers should die slowly and painfully.
done

msg0 "Generating markup source files"

for i in $(seq 1 $NUM); do
  
  # Reset buffer variables
  ASS_1=-
  ASS_2=-
  ALL_ASS=-

  # Source stuff for this guy
  source build/${i}.buf
  safe_name="$(sanitize_string "$name")"
  source build/${i}_assist.buf

  msg1 "Looking for data about '$name' in buffer"
  # Append to main
  : ...

  # Diagnóstico
  if [ -f build/diagnostico_$i ]; then
    msg2 "Generating diagnostics"
    {
      echo "# Diagnóstico inicial - $name"
      echo
      echo "**Materia:** Instrumento (Guitarra)"; echo
      echo "**Carrera/Nivel:** $level"; echo
      echo "**Docente:** $TEACHER"; echo
      cat build/diagnostico_$i | while read _l; do case $_l in
        _DATE*)
          echo "**Fecha:** $(echo $_l | cut -d: -f2-)"; echo
        ;;
        *)
          echo $_l
        ;;
      esac; done
    } >"${outdir}/md/${i}_${safe_name}_diagnostico.md"
  fi

  # Programa de obras
  if [ -f build/programa_$i ]; then
    pwc=$(cat build/programa_$i | wc -l)
    if [ $pwc == "1" ]; then
      msg2b "Repertoire empty, skipping"
    else
      msg2 "Generating repertoire"
      {
        echo "# Material del curso - $name"; echo
        echo "**Materia:** Instrumento (Guitarra)"; echo
        echo "**Carrera/Nivel:** $level"; echo
        echo "**Horario:** $schedule"; echo
        echo "**Docente:** $TEACHER"; echo
        echo "**Ciclo lectivo:** $YEAR"; echo
        echo "## Programa de obras"; echo
        cat build/programa_$i | while read l; do
          case $l in
            _DATE*)
              str="$(echo $l | cut -d: -f2-)"
              echo "**Última actualización:** ${str}"; echo
            ;;
            _WRK*)
              str="$(echo $l | cut -d: -f2-)"
              case "$str" in
                *:*)
                  auth="$(echo $str | cut -d: -f1)"
                  wrk="$(echo $str | cut -d: -f2-)"
                  echo "- _${auth}_: ${wrk}"
                ;;
                *)
                  echo "- $str"
                ;;
              esac
            ;;
            *)
              echo "$l"; echo
            ;;
          esac
        done

        if [ -f build/propuesta_$i ]; then
          #TODO: elaborar este titulo
          echo "## Propuesta"; echo
          cat build/propuesta_$i | while read pl; do
            case $pl in
              _DATE*)
                str="$(echo $pl | cut -d: -f2)"
                echo "**Última actualización:** ${str}"
                echo
              ;;
              *)
                echo "$pl";
              ;;
            esac
          done
        fi
      } >>"${outdir}/md/${i}_${safe_name}_programa.md"
    fi
  fi

  # Seguimiento
  if [ -f build/clases_1c_$i ]; then
    msg2 "Generating tracing (1c)"
    c=0
    {
      echo "# Seguimiento - $name"; echo
      echo "**Materia:** Instrumento (Guitarra)"; echo
      echo "**Carrera/Nivel:** $level"; echo
      echo "**Horario:** $schedule"; echo
      echo "**Docente:** $TEACHER"; echo
      echo "**Ciclo lectivo:** $YEAR"; echo
      echo "## 1º Cuatrimestre"; echo
      cat build/clases_1c_$i | while read _l; do
        case $_l in
          __CLASS*)
            c=$(($c+1))
#            echo "### Clase Nº$c"; echo
#            echo "**Fecha:** $(echo "$_l" | cut -d: -f2)"; echo
            echo "### Clase Nº$c ($(echo "$_l" | cut -d: -f2))"; echo
          ;;
          *)
            echo "$_l"
          ;;
        esac
      done
    } >>"${outdir}/md/${i}_${safe_name}_seguimiento.md"
  fi

  if [ -f build/clases_2c_$i ]; then
    msg2 "Generating tracing (2c)"
    c=0
    {
      echo
      echo "## 2º Cuatrimestre"; echo
      cat build/clases_2c_$i | while read _l; do
        case $_l in
          __CLASS*)
            c=$(($c+1))
#            echo "### Clase Nº$c"; echo
#            echo "**Fecha:** $(echo "$_l" | cut -d: -f2)"; echo
            echo "### Clase Nº$c ($(echo "$_l" | cut -d: -f2))"; echo
          ;;
          *)
            echo "$_l"
          ;;
        esac
      done
    } >>"${outdir}/md/${i}_${safe_name}_seguimiento.md"
  fi

  if [ -f "${outdir}/md/${i}_${safe_name}_seguimiento.md" ]
  then
    {
      echo "## Registro de asistencias"; echo
      if [[ "$ASS_1" != '-' ]]; then
        echo "**1º Cuatrimestre:** $ASS_1"; echo
      fi
      if [[ "$ASS_2" != '-' ]]; then
        echo "**2º Cuatrimestre:** $ASS_2"; echo
      fi
      if [[ "$ALL_ASS" != '-' ]]; then
        if [[ "$ASS_1" != '-' ]]; then
          echo "**Anual:** $ALL_ASS"; echo
        else
          echo "**1º Cuatrimestre:** $ALL_ASS"; echo
        fi
      fi
    } >>"${outdir}/md/${i}_${safe_name}_seguimiento.md"
  fi

  if [ -f build/evaluacion_1c_$i ]; then
    msg2 "Generating eval (1c)"
    {
      echo "# Evaluación - $name"
      echo "**Materia:** Instrumento (Guitarra)"; echo
      echo "**Carrera/Nivel:** $level"; echo
      echo "**Horario:** $schedule"; echo
      echo "**Docente:** $TEACHER"; echo
      echo "**Ciclo lectivo:** $YEAR"; echo
      echo "## 1º Cuatrimestre"; echo
      
      cat build/evaluacion_1c_$i | while read l; do
        case $l in
          _DATE*)
            echo "**Fecha:** $(echo $l | cut -d: -f2-)"; echo
          ;;
          *)
            echo $l
          ;;
        esac
      done
      
      if [ -f build/evaluacion_2c_$i ]; then
        echo "## 2º Cuatrimestre"; echo
        cat build/evaluacion_2c_$i | while read l; do
          case $l in
            _DATE*)
              echo "**Fecha:** $(echo $l | cut -d: -f2-)"; echo
            ;;
            *)
              echo $l
            ;;
          esac
        done
      fi

      if [ -f build/evaluacion_FINAL_$i ]; then
        echo "## Evaluación final"; echo
        cat build/evaluacion_FINAL_$i | while read l; do
          case $l in
            _DATE*)
              echo "**Fecha:** $(echo $l | cut -d: -f2-)"; echo
            ;;
            *)
              echo $l
            ;;
          esac
        done
      fi
    } >>"${outdir}/md/${i}_${safe_name}_evaluacion.md"
  fi
done

msg0 "Generating pdfs"

for i in $(seq 1 $NUM); do
  # Source stuff for this guy
  source build/${i}.buf
  safe_name="$(sanitize_string "$name")"
  # Store safe name for later
  echo $safe_name >>build/all_names.lst
done

for i in $(seq 1 $NUM); do

  # Source stuff for this guy
  source build/${i}.buf
  safe_name="$(sanitize_string "$name")"
  
  msg1 "Generating pdf files for '$safe_name'"
  
  for l in diagnostico programa seguimiento evaluacion full
  do
    if [ -f ${outdir}/md/${i}_${safe_name}_${l}.md ]
    then
      msg2 "Processing: '$l'"
      pandoc -t latex \
        -o ${outdir}/pdf/${safe_name}_${l}.pdf \
        ${outdir}/md/${i}_${safe_name}_${l}.md
    fi
  done

done

msg0 "Generating bulk pdfs"

# Initialize bulk lists
for i in "init" "eval" "full"; do echo -n >"build/bulk_${i}"; done

cat build/all_names.lst | sort -h | while read safe_name; do
  msg1 "Generating bulk pdf files for '$safe_name'"
  if ls ${outdir}/pdf/${safe_name}_diagnostico.pdf >>/dev/null 2>&1; then
    echo -n " ${outdir}/pdf/${safe_name}_diagnostico.pdf" >>build/bulk_init
    echo -n " ${outdir}/pdf/${safe_name}_diagnostico.pdf" >>build/bulk_rec
    echo -n " ${outdir}/pdf/${safe_name}_diagnostico.pdf" >>build/bulk_full
  fi
  if ls ${outdir}/pdf/${safe_name}_programa.pdf >>/dev/null 2>&1; then
    echo -n " ${outdir}/pdf/${safe_name}_programa.pdf" >>build/bulk_init
    echo -n " ${outdir}/pdf/${safe_name}_programa.pdf" >>build/bulk_rec
    echo -n " ${outdir}/pdf/${safe_name}_programa.pdf" >>build/bulk_full
  fi
  if ls ${outdir}/pdf/${safe_name}_seguimiento.pdf >>/dev/null 2>&1; then
    echo -n " ${outdir}/pdf/${safe_name}_seguimiento.pdf" >>build/bulk_rec
    echo -n " ${outdir}/pdf/${safe_name}_seguimiento.pdf" >>build/bulk_full
  fi
  if ls ${outdir}/pdf/${safe_name}_evaluacion.pdf >>/dev/null 2>&1; then
    echo -n " ${outdir}/pdf/${safe_name}_evaluacion.pdf" >>build/bulk_eval
    echo -n " ${outdir}/pdf/${safe_name}_evaluacion.pdf" >>build/bulk_full
  fi
done

if [ -n "$(cat build/bulk_init)" ]; then
  pdfjam --outfile ${outdir}/pdf/${BULK_PREFIX}_inicio.pdf -- $(cat build/bulk_init)
fi
if [ -n "$(cat build/bulk_eval)" ]; then
  pdfjam --outfile ${outdir}/pdf/${BULK_PREFIX}_evaluacion.pdf -- $(cat build/bulk_eval)
fi
if [ -n "$(cat build/bulk_rec)" ]; then
  pdfjam --outfile ${outdir}/pdf/${BULK_PREFIX}_recorrido.pdf -- $(cat build/bulk_rec)
fi
if [ -n "$(cat build/bulk_full)" ]; then
  pdfjam --outfile ${outdir}/pdf/${BULK_PREFIX}_full.pdf -- $(cat build/bulk_full)
fi

msg0 "Generating webpages"

# Initialize main page
msg1 "Initializing main page"
html_homepage="${ROOTURL}/${HTMLHOME}"

{ cat <<!
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<title>${HTMLTITLE} | Inicio</title>
</head>
<body>
<h1>${HTMLTITLE}</h1>
<table cellspacing="3" cellpadding="3" border="1">
<tr>
<th>Alumno</th>
<th>Carrera/Nivel</th>
<th>Horario</th>
<th>Diagnóstico</th>
<th>Programa</th>
<th>Seguimiento</th>
<th>Evaluación</th>
</tr>
!
} >${outdir}/html/${HTMLHOME}

for i in $(seq 1 $NUM); do

  # Source stuff for this guy
  source build/${i}.buf
  safe_name="$(sanitize_string "$name")"
  
  msg1 "Generating html files for '$safe_name'"

  # Initialize this row in main page buffer
  echo -n "<tr><td>${name}</td><td>${level}</td><td>${schedule}</td>" \
    >>build/table-rows.html
  
  for l in diagnostico programa seguimiento evaluacion
  do
    if [ -f ${outdir}/md/${i}_${safe_name}_${l}.md ]
    then
      # Generate this page for this guy
      htmlfile="${safe_name}_${l}.html"
      msg2 "Processing: '$l'"
      {
        cat <<!
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<title>${name}: $(cute $l)</title>
</head>
<body>
<p><a href="${html_homepage}">Volver</a></p>
<hr/>
!
        pandoc ${outdir}/md/${i}_${safe_name}_${l}.md
        cat <<!
<br/>
</body>
</html>
!
      } >${outdir}/html/${htmlfile}

      # Add table row to main page
      echo -n "<td align="center"><a href="${ROOTURL}/${htmlfile}">HTML</a>&nbsp;<a href="${ROOTURL}/${safe_name}_${l}.pdf" target="_blank">PDF</a></td>" \
        >>build/table-rows.html

    else
      # No files for this guy, leave table cell empty
        echo -n "<td align="center">-</td>" \
          >>build/table-rows.html
    fi
  done
  
  # Close table row in main page
  echo "</tr>" \
    >>build/table-rows.html

done

# Sort rows alphabetically and close table
cat build/table-rows.html | sort -h \
  >>${outdir}/html/${HTMLHOME}
echo "</table>" >>${outdir}/html/${HTMLHOME}

if [ -f ${outdir}/pdf/${BULK_PREFIX}_inicio.pdf ]; then
  echo "<p><a href=\"${ROOTURL}/${BULK_PREFIX}_inicio.pdf\" target=\"_blank\">Descargar diagnósticos de entrada y programas</a></p>" \
    >>${outdir}/html/${HTMLHOME}
fi
if [ -f ${outdir}/pdf/${BULK_PREFIX}_recorrido.pdf ]; then
  echo "<p><a href=\"${ROOTURL}/${BULK_PREFIX}_recorrido.pdf\" target=\"_blank\">Descargar recorridos</a></p>" \
    >>${outdir}/html/${HTMLHOME}
fi
if [ -f ${outdir}/pdf/${BULK_PREFIX}_evaluacion.pdf ]; then
  echo "<p><a href=\"${ROOTURL}/${BULK_PREFIX}_evaluacion.pdf\" target=\"_blank\">Descargar evaluaciones</a></p>" \
    >>${outdir}/html/${HTMLHOME}
fi
if [ -f ${outdir}/pdf/${BULK_PREFIX}_full.pdf ]; then
  echo "<p><a href=\"${ROOTURL}/${BULK_PREFIX}_full.pdf\" target=\"_blank\">Descargar todo</a></p>" \
    >>${outdir}/html/${HTMLHOME}
fi

# Assist pdf
#if [ "$ASSIST" ]; then
#  echo "<p><a href=\"${ROOTURL}/${ASSIST}\" target=\"_blank\">Descargar planilla de asistencias</a></p>" \
#    >>${outdir}/html/${HTMLHOME}
#fi

# Close main page
msg1 "Closing main page"
{ cat <<!
<br/>
</body>
</html>
!
} >>${outdir}/html/${HTMLHOME}

msg0 "Exporting files"
cp ${outdir}/html/*.html ${outdir}/pdf/*.pdf $HTMLDIR

msg0 "All done"
