\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \key d \minor \time 3/2
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"escritura:"}
  }
  \relative c'' { d8[ c d bes] c1 }
  \bar "||"
  \grace { s16
    \tweak extra-offset #'(2 . 1)
    ^\markup \small {"ejecución aprox.:"}
    \tweak extra-offset #'(0 . 1)
    ^\markup \small {"(b)"}
  }
  \relative c'' { d16[ c ^~ c8 d16 bes ^~ bes8] c1 }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}
\score { \new Staff {
  \key d \minor \time 4/2
  \relative c'' { c8[ d bes c] a[ bes a bes] c1 }
  \bar "||"
  \grace { s16
    \tweak extra-offset #'(0 . 1)
    ^\markup \small {"(a)"}
  }
  \relative c'' { c8.[ d16 bes8. c16] a8.[ bes16 a8. bes16] c1 }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}
\score { \new Staff {
  \key d \minor \time 3/2
  \relative c'' { bes8[ a bes c] a1 }
  \bar "||"
  \grace { s16
    \tweak extra-offset #'(0 . 1)
    ^\markup \small {"(c)"}
  }
  \relative c'' { bes8[ a8. bes16 c8] c1 }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}
\score { \new Staff {
  \key d \minor \time 3/2
  \relative c'' { c8[ bes a g] a1 }
  \bar "||"
  \relative c'' {
    \grace { s16
      \tweak extra-offset #'(0 . 1)
      ^\markup \small {"(a)"}
    }
    c8 ~
    c16
    \set stemLeftBeamCount = #2
    \set stemRightBeamCount = #1
    bes
    \grace { s16
      \tweak extra-offset #'(0 . 1)
      ^\markup \small {"(b)"}
    }
    \set stemLeftBeamCount = #1
    \set stemRightBeamCount = #2
    a
    g ~
    g8  a1  }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}
\score { \new Staff {
  \key d \minor \time 8/4
  \relative c'' {
    \set suggestAccidentals = ##t
    g16 a g a bes c bes c d8
    \once
    \override Staff.AccidentalSuggestion.parenthesized = ##t
    ees!16 c
    d c d bes
    c1
  }
  \bar "||"
  \relative c'' {
    \set suggestAccidentals = ##t
  \grace { s16
    \tweak extra-offset #'(0 . 1)
    ^\markup \small {"(a)"}
  }
    g16.
    a32 g16. a32 bes16. c32 bes16. c32 d8
    \once
    \override Staff.AccidentalSuggestion.parenthesized = ##t
  \grace { s16
    \tweak extra-offset #'(-2 . -2)
    ^\markup \small {"(b)"}
  }
    ees!32 c ~ c16
  \grace { s16
    \tweak extra-offset #'(0 . 1)
    ^\markup \small {"(c)"}
  }
    d32 c d bes ~ bes8
    c1
  }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}
