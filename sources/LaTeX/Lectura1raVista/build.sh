#!/bin/bash

pdflatex lectura_1ra_vista_obras.tex || exit 1
pdflatex caratula.tex || exit 1
pdfjam --outfile lectura_1ra_vista.pdf -- caratula.pdf lectura_1ra_vista_obras.pdf
