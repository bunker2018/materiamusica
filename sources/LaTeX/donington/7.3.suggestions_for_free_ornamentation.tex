
\noindent
Ya que no hay dos intérpretes modernos
que aborden la ornamentación libre
con el mismo talento o desde los mismos supuestos tácitos,
sería una impertinencia imponer reglas;
no obstante,
las sugerencias que siguen
pueden tal vez verse confirmadas por la evidencia
recopilada más arriba
y por los ejemplos musicales expuestos más adelante:

\vspace{1em}
(i)
Ornamentar las partes de una polifonía vocal
requiere de los intérpretes
tacto y moderación mutuos
\textit{(198, 200)},
dejando particularmente las primeras entradas
idénticas, o casi idénticas, al original.

\vspace{1em}
(ii)
En el estilo recitativo
hoy comúnmente conocido como monodía,
los pasajes ornamentales
son un medio de intensificación, %are an enhancement,
pero deben mostrar la mayor consideración por las palabras ---
siempre es ésta la preocupación primordial en el estilo recitativo.
Debería ornamentarse las sílabas largas y no las breves \textit{(202)};
la sílaba final de la frase no debería ornamentarse \textit{(199, 207)};
la anteúltima nota es particularmente apropiada para una ornamentación extensa;
los pasajes apasionados deberían cantarse de manera más simple y expresiva
que los pasajes más desapasionados \textit{(202)}.

Los pioneros del estilo recitativo
fueron cantantes virtuosos, como Caccini y Peri,
que favorecieron el despliegue vocal en los pasajes apropiados.
Su música contiene una gran cantidad de ejemplos notables en partitura,
mientras que otros eran improvisados al momento de la ejecución
de acuerdo al gusto del intérprete.
Así, el \textit{Nuove Musiche} de Caccini,
publicado en Florencia en 1602,
incluye un aria
(tan libre, no obstante, que es prácticamente un arioso)
de su ópera \textit{Il Rapimento di Cefalo},
con la indicación
`cantada, con la ornamentación escrita'
por el renombrado bajo Palantrotti,
bastante florida pero sólo en las palabras especialmente sugerentes,
como
\textit{suave}, dulce;
\textit{mortale}, mortal;
\textit{ale}, alas;
\textit{risvegliaro Amore}, Amor despierto.
A continuación encontramos un aria para tenor
escrita con ornamentaciones parecidas,
pero en este caso se indica que fue cantada
`con diferente ornamentación, de acuerdo con su propio estilo, por Jacopo Peri'.

\vspace{1em}
(iii)
La improvisación de partes ornamentales floridas
por parte de instrumentos melódicos
de la orquesta de comienzos del barroco
estuvo sujeta a las msmas consideraciones que hemos descrito para las voces,
incluyendo
no estorbar el desarrollo de otras voces;
no mostrar exhibicionismo ni un contrapunto excesivamente ambicioso;
ornamentar especialmente la anteúltima nota, y nunca la última;
ornamentar más para la ópera que para la iglesia o la música de cámara;
y en general
aspirar a la belleza expresiva más bien que al exhibicionismo personal
\textit{(203, 204, 207, 208, 209)}.

\vspace{1em}
En qué medida se propagó
este extraño arte de improvisar
una textura orquestal completa sobre una línea de bajo
(por cierto, Praetorius \textit{(220)} lo introdujo en su volumen de 1619 en Alemania),
o por cuánto tiempo perduró,
son preguntas para las que no tenemos respuesta;
pero bien puede haber dado cuenta
de los frecuentes casos,
en las partituras de las óperas del barroco temprano,
de instrucciones para interludios y acompañamientos orquestales
en los que no hay música escrita,
o bien únicamente
una nota pedal de bajo,
o una línea de bajo,
o una línea de bajo con pentagramas vacíos por encima
dejando abierta la ejecución de melodías superiores nunca indicadas expresamente,
sino que presumiblemente
se resolvían en el ensayo siguiendo la inspiración del momento.
Para una interpretación moderna de estos pasajes,
probablemente sea necesario escribirlos.

El estilo recitativo Italiano
se conoció en Francia
a partir de la visita de Caccini en 1605;
y comenzó a ser aún mejor conocido
después de la visita de De Nyert a Roma en 1638.
Debido a estos contactos, entre otros,
se desarrolló en Francia
un floreciente estilo nacional de bel canto
en la composición y la interpretación vocal,
si bien no apareció un estilo nacional de ópera
hasta Lully.
Los dos líderes principales
del bel canto francés
fueron Bénigny de Bacilly,
quien tituló su importante tratado \textit{(221)}
\textit{Sur l'art de bien chanter}\ndelt{\enquote{Sobre el arte de cantar bien}.},
que es una traducción literal de la expresión italiana;
y Michel Lambert,
cuya influencia creció considerablemente
cuando Lully se casó con su hija en 1694,
y se convirtió en (o siguió siendo)
un habitual entrenador de los cantantes de Lully.
La ornamentación de los \textit{airs} franceses
por los cantantes entrenados en esta línea
persistió a lo largo del siglo XVII e incluso después:
predominantemente se trataba de ornamentación libre;
sin embargo, incluía varios ornamentos específicos definidos parcialmente \textit{(222)}.
En el siglo XVII
apareció otra corriente de ornamentación
con los laudistas franceses,
que posteriormente heredaron los grandes clavecinistas
como François Couperin,
sus predecesores y sus sucesores;
también se extendió a Alemania,
incluido J. S. Bach.
Gambistas como Marais
y flautistas como Hotteterre
hicieron uso de estas maneras de ornamentar,
pero sobre todo, probablemente, de los modismos propios de la interpretación vocal.
Hacia comienzos del siglo XVIII,
varios ornamentos específicos
habían ya adoptado formas un tanto estrictas,
en tanto que
la ornamentación libre
se había reducido radicalmente en cantidad
en comparación con los estilos italianos.

\vspace{1em}
(iv)
Para la ornamentación en la música francesa del siglo XVII,
especialmente para la voz y para los instrumentos melódicos
como la gamba y la flauta,
el alcance es muy amplio \textit{(210)}.
Para cuando Lully había desarrollado
un estilo de recitativo genuinamente francés,
más lírico y melodioso que el recitativo italiano de la época,
él mismo se encargó de prescribir la ornamentación libre
que anteriormente había admitido para los \textit{airs};
incluso los ornamentos más insignificantes
debían aplicarse muy esporádicamente \textit{(211)}.

Mientras tanto,
en el bel canto italiano
el aria da capo
evolucionaba bajo varias categorías
como melancólico, brillante, furioso, entre otras;
cada una de ellas con su particular estilo de ornamentación.
Las cadenzas eran de uso común
(especialmente al final de la sección central),
y se esperaba que,
mediante la juiciosa invención de variaciones improvisadas,
la recapitulación de la primera sección
--- la repetición da capo ---
no resultara excesivamente obvia,
como lo explica Dr. Burney \textit{(223)}:
`adornos tales como los que, en Italia,
se esperaría de cualquier cantante hábil
en cada presentación'.

\vspace{1em}
(v)
Para la primera sección de las arias da capo,
muy pocos ornamentos específicos son necesarios,
y casi ninguna ornamentación libre,
si es que se la necesita en absoluto;
para la sección central,
no mucho más;
para la repetición da capo,
definitivamente es necesaria en mayor medida,
siempre que no distorsione o destruya la melodía original
\textit{(216, 216)};
las cadenzas, si se las introduce,
deben ser de proporciones moderadas.

Esto mismo aplica
para la composición de melodías,
más o menos improvisadas,
del tipo tan común de los adagios
que Dr Burney \textit{(224)} describió como
`poco más que  un contorno
librado a las habilidades del ejecutante'.
Cualesquiera sean las proporciones de preparación, memorización o improvisación,
tal melodía debería sonar como si resultara de la invención espontánea \textit{(213, 214, 218, 219)}.
Las notas estructurales
(es decir, aquellas expresamente escritas)
generalmente pueden ser ejecutadas con mayor énfasis
que las notas ornamentales que las conectan entre sí.
No obstante, ocasionalmente
una nota de paso acentuada puede caer en el pulso,
y en consiguiente demorar y alivianar una nota estructural.
Las notas de paso no acentuadas, ya sean muchas o pocas,
acordes arpegiados yasean simples o parcialmente melódicos,
y notas de adorno en todas sus variantes
son siempre válidos como recurso.


\vspace{1em}
(vi)
El material musical utilizado para crear una melodía ornamental
a partir de un simple esqueleto estructural
debe mostrar la misma consistencia y relevancia
que esperaríamos de cualquier composición
\textit{(212, 213, 214, 217)};
en la ejecución,
el resultado debe ser sutil, no demasiado enfático y rítmicamente liviano
\textit{(218, 219)}.

