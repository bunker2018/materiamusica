
El principio que debemos entender aquí es que
en la notación barroca,
el puntillo ordinario
(que para nosotros incrementa la duración de la figura
en la mitad de su valor)
se utilizaba para incrementar la duración de la figura
en cualquier proporción que fuese conveniente.
Para nosotros la duración del puntillo es estándar,
si bien nunca es tan rígida en la práctica como lo es en teoría.
En la notación barroca el puntillo es variable.
Puede incrementar el valor de la nota punteada
en menos de la mitad (\textit{under-dotting}),
en la mitad de su duración (\textit{standard dotting}),
o en más de la mitad (\textit{over-dotting}),
incluso en algunos casos resultando equivalente a nuestro doble puntillo %%REVISAR
(\textit{double-dotting}).
Si bien ocasionalmente pueden encontrarse casos
de figuras con doble puntillo
expresamente escrito
a partir de la segunda mitad del período barroco,
y mediante ligaduras de prolongación en obras del barroco temprano,
en general no parece haber intención de indicar una diferencia sustancial. %%REVISAR
Como es usual,
el hecho fundamental
es que la notación barroca
era habitualmente vaga e inconsistente a simple vista,
sin que esto implique en modo alguno
que fuera aceptable una interpretación vaga o inconsistente
en la ejecución.
El puntillo variable en la notación barroca
es simplemente un caso particular de esta actitud general.

La evidencia de que el puntillo barroco es variable
proviene, en parte,
de innumerables pasajes
en los que el ritmo tal como está escrito
no corresponde a la duración real del compás
a menos que se interprete el puntillo como variable.
Las situaciones que se muestran en la figura \ref{table_variable_baroque_dot} son ejemplos típicos de esta utilización del puntillo variable.

\begin{center}
\begin{figure}
\includegraphics{lilypond/TableVariableBaroqueDot.png}
\caption{El puntillo variable en el barroco.\label{table_variable_baroque_dot}}
\end{figure}
\end{center}

Cuando se interprete deliberadamente un puntillo como variable con fines expresivos,
debe considerarse siempre el contexto estilístico del barroco.

\dquotation{(169) Nicolas Gigault, 1683:}{
`Cuando hay una semicorchea sobre una corchea
es necesario tomarlas simultáneamente'
[agregando el doble puntillo para sincronizar las figuras,
como se ve en su música].
}

\dquotation{(170) Michel L'Affilard, 1694:}{
`Para ejecutar los puntillos en su valor
[es decir, su duración prevista en lugar de la duración escrita],
es necesario retener la negra con puntillo,
y pasar rápidamente por la corchea siguiente.`
}

\dquotation{(171) Étienne Loulié, 1696:}{
`Cuando el puntillo está en el mismo pulso
que la negra que lo precede,
debemos retener el canto de esta negra un poco más,
y pasar rápidamente por la corchea que le sigue.'
}

\dquotation{(172) Jacques Martin Hotteterre, 1737:}{
`A veces escribimos puntillos a continuación de una figura,
lo que aumenta su duración en la mitad de su valor'; pero
`en movimientos en que las negras sean desiguales
[en la ejecución, si bien no en la escritura],
el puntillo a continuación de la negra
equivale a una corchea con puntillo
[es decir que el puntillo equivale al doble puntillo];
de manera que la corchea que sigue a una negra con puntillo
es siempre breve' [es decir, equivalente a una semicorchea].
}

\dquotation{(173) Johann Mattheson, 1737:}{
Para \textit{entries} y ciertas danzas es necesario
`exagerar el puntillo'. \ndelt{en el original en inglés: «\ldots{}it is necessary to use ``the very dotted manner''».}
}

\dquotation{(174) Joachim Quantz, 1752:}{
`En las corcheas con puntillo, semicorcheas y fusas,
nos apartamos de la regla estándar,
para lograr la vitalidad que estas figuras deben expresar.'
Por lo tanto
`ya sea el tempo lento o rápido
[la expresión es tan variable que]
no es posible determinar exactamente la duración de las notas breves a continuación del puntillo.'
Y
`cuando el puntillo aparece después de la segunda nota'
se lo trata de igual manera
`en cuanto a la duración de la nota larga y la breve,
sólo que el orden se invierte';
y
`cuanto más breve toquemos la primera' nota, `más viva y enérgica será la expresión;
y por el contrario,
cuanto más alarguemos el puntillo, más dulce y agradable la expresión'.
\par
[Sobre la Obertura Francesa]:
`En el estilo majestuoso, la introducción procede
tanto mediante notas largas,
entre las cuales las otras partes realizan movimientos rápidos,
como mediante notas con puntillo.
[\ldots] % The dotted notes must be pushed on sharply by the performer, and executed with vigour.
El puntillo debe alargarse, y las notas subsiguientes tocarse mucho más breves.'
Cuando
`tres o más fusas aparecen después de un silencio o una figura con puntillo',
dichas notas,
`especialmente en movimientos lentos,
no se ejecutan siempre de acuerdo a su valor [escrito],
sino muy cerca del final de la duración asignada,
y lo más rápidamente posible;
como frecuentemente sucede, por ejemplo, en oberturas, \textit{entries} y \textit{furies}.
De todos modos,
estas notas rápidas deben tocarse 
«sueltas», ligando muy poco.' \ndelt{en el original en inglés:
«\ldots{}must be separately bowed»; hace referencia a separar los movimientos de arco entre nota y nota
en los instrumentos de cuerda frotada. La articulación resultante es \textit{non legato}.}
Y de nuevo:
`a continuación de una nota larga y un silencio breve'
las fusas
`deben siempre tocarse muy rápidamente;
esto es así tanto en \textit{adagio} como en \textit{allegro}.
Por lo tanto uno debe esperar hasta casi el final de la duración, para las notas rápidas,
para no alterar la medida del compás.'
}

%\dquotation{(175) C. P. E. Bach, 1753:}{
%}

%\dquotation{(176) Leopold Mozart, 1756:}{
%}

%\dquotation{(177) J. A. P. Schultz, 1771:}{
%}

Al igual que la \textit{inegalité},
la aplicación del puntillo variable no puede reducirse a un sistema.
Pero en general:

\vspace{1em}
\noindent{}(i) \textit{Under-dotting (acortar la nota con puntillo y alargar la nota breve)
puede ser deseable:}\\
\indent{}Si hay pares de notas conformados cada uno por una nota con puntillo y una nota breve,
en un movimiento expresivo;
dichos pares, si no son muy largos o muy breves en duración,
podrían sonar demasiado pesados si se tocan literalmente,
pero sonarán más suaves y agradables si se aliviana el puntillo.
El efecto en estos casos
consiste en \textit{relajar} las notas con puntillo
aproximándolo a la figuración atresillada
con la que \textit{intensificamos} las notas iguales
al aplicarles la \textit{inegalité}.\\
\indent{}Si hay figuras de tresillo en otra parte,
en cuyo caso las figuras con puntillo deben sincronizarse
mediante el \textit{under-dotting}
(la alternativa, mucho más rara,
es aplicar \textit{over-dotting},
tocando la nota breve no al mismo tiempo,
sino marcadamente después de la nota breve del tresillo).

\vspace{1em}
\noindent{}(ii) \textit{El puntillo estándar
puede ser deseable:}\\
\indent{}En aquellas situaciones,
que se dan en la mayoría de los casos,
en que no surge ningún motivo particular
para alargar o acortar el puntillo;
entendiendo que esta situación normal, si bien estándar,
no es rígida, y que una pequeña desviación en un sentido u otro
puede ciertamente surgir como parte del desarrollo natural de la línea.\\
\indent{}En aquellos casos, muy frecuentes,
en que hay figuras regulares en otra línea
con las que las notas con puntillo simplemente deben sincronizarse.

\vspace{1em}
\noindent{}(iii) \textit{Over-dotting (alargar la nota con puntillo y acortar la nota breve)
puede ser deseable:}\\
\indent{}Si hay pares de notas conformados cada uno por una nota con puntillo y una nota breve,
en un movimiento enérgico.
La proporción en la que alteramos la duración del puntillo
dependerá de la velocidad:
en movimientos no tan rápidos
podemos aproximarnos cómodamente a la duración del doble puntillo
o incluso más,
mientras que en movimientos más rápidos
sólo habrá tiempo para alargar muy poco el puntillo
o no alargarlo en absoluto
(es decir, las figuras con puntillo en movimientos rápidos
probablemente ya suenen lo suficientemente enérgicas,
y cualquier intento de aplicar \textit{over-dotting}
simplemente sonará desagradable y brusco).\\
\indent{}Si hay figuras con puntillo en otra parte que se mueven al doble de velocidad,
con las que las notas con puntillo pueden sincronizarse
mediante el doble puntillo
(si bien no necesariamente deba hacerse así):
ver \textit{(172)} más arriba.

\vspace{1em}
Al igual que en el caso de la \textit{inegalité},
entonces,
el puntillo también puede escribirse en dos direcciones,
y puede dar lugar a una flexibilidad rítmica incluso mayor.\\
\indent{}En el \textit{puntillo simple}\ndelt{En el original: \lqq{}\textit{straightforward dotting}\rqq{}.}
la duración más larga corresponde a la primera nota del par.
En ritmos cantables que suenan aproximadamente como un tresillo,
la situación es la misma que en el caso de la \textit{inegalité} para frases \textit{cantabile},
si bien se llega a este resultado,
por así decir,
en sentido opuesto
(es decir relajando el ritmo escrito en lugar de intensificarlo).
Existe  la misma disposición a la articulación \textit{legato},
en caso de que no se halle escrito expresamente,
y la misma tendencia a una caída suave.
Igualmente,
es posible escribir el efecto rítmico tal como se desea que suene,
reemplazando  las figuras con puntillo con tresillos,
o sustituyendo el compás simple por un compás compuesto,
como sugiere C. P. E. Bach en \textit{168)}.
En pasajes vigorosos casi nunca es deseable articular \textit{legato};
generalmente es preferible
articular el par de notas
interpretando el puntillo, o parte de éste,
casi como un silencio de articulación.\\
\indent{}En el \textit{puntillo invertido}\ndelt{En el original: \lqq{}\textit{reversed dotting}\rqq{}.}
la duración más larga corresponde a la segunda nota del par.
En ritmos \textit{cantabile}
el efecto se aproxima a la aplicación de la desigualdad inversa,
y de la misma forma invita a la articulación \textit{legato}
y a una caída suave.
En ritmos vigorosos ligar no es necesario,
pero sí probable:
cuanto más veloz sea el pasaje,
mayor la probabilidad.
El puntillo invertido en todas sus formas
es más bien un recurso de efecto,
a diferencia del puntillo simple,
que en su forma estándar es naturalmente muy usual
en todo tipo de géneros musicales.

