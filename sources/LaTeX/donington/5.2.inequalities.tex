
Durante el barroco e incluso antes,
y a través de todas las tradiciones nacionales,
existe evidencia de una práctica expresiva
hoy comúnmente descrita como desigualdad (\textit{inegalité}):
la ejecución desigual de notas escritas con valores iguales.

\dquotation{(148) Loys Bourgeois, Génova, 1550:}{
Ejecutar las negras `de dos en dos, permaneciendo
más tiempo en la primera, que en la segunda'.
}

\dquotation{(149) Fray Tomás de Santa María, Valladolid, 1565:}{
Para las negras, el método consiste en
`demorarse en la primera, apurarse en la cuarta',
no obstante
`no apurarse demasiado, sino sólo levemente';
y para las corcheas o bien
`demorarse en la primer corchea y apurarse en la segunda',
o
`apurarse en la primer corchea, demorarse en la segunda',
etc.
}

\dquotation{(150) Girolamo Frescobaldi, Roma, 1615/16:}{
Ejecutar la segunda de cada par de semicorcheas
`un poco punteadas'.
[Nótese que \enquote{punteadas} debe leerse aquí como
\enquote{más o menos desiguales},
es decir, no exactamente la figura con puntillo
sino aproximadamente.]
}

\dquotation{(151) Giovanni Domenico Puliaschi, Roma, 1618:}{
Ejecutar los pares de notas escritas con figuras iguales
`aquí punteando la primera nota, allá la segunda,
según lo requiera el pasaje'.
}

\dquotation{(152) Autor inglés anónimo, alrededor de 1660:}{
Producir \textit{inegalité}
`robando la mitad del valor a una nota
y agregándolo a la nota siguiente'.
}

\dquotation{(153) Bénigne de Bacilly, París, 1668:}{
`De cada dos notas, una generalmente se puntea'
pero
`se ha considerado apropiado no indicarlo por temor
a que los imbéciles
(\textit{sic})
se acostumbren a tocarlas así';
ya que en la mayoría de los casos deben ser
`punteadas con la moderación necesaria para que no
resulte obvio';
y en efecto
`en algunos pasajes es incluso necesario evitar
el puntillo completamente'.
[Nuevamente nótese el grado variable de desigualdad
que implica \enquote{puntillo} en este contexto.]
}

\dquotation{(154) Roger North, Inglaterra, alrededor de 1690:}{
`En notas breves' la desigualdad
`confiere vida y espíritu a los toques,
y una buena mano la usará a menudo con tal fin,
aunque no esté expresado'
[en la notación].
}

\dquotation{(155) Alessandro Scarlatti, Nápoles, 1694:}{
`tocamos en tiempo igual'
las corcheas escritas como iguales en un pasaje que
por lo demás podría pensarse que invita a la ejecución
desigual.
}

\dquotation{(156) Georg Muffat, Augsburgo, 1693:}{
En un compás de dos tiempos lento
`varias corcheas en sucesión' no deben tocarse desiguales
`para mayor elegancia en la ejecución',
ya que normalmente deberían tocarse `en compás simple'.
}

\dquotation{(157) Étienne Loulié, París, 1696:}{
`En cada compás, pero especialmente en compases de tres tiempos,
la divisiones débiles se tocan de dos diferentes maneras,
si bien se escriben de la misma forma.
1. En ciertos casos se tocan iguales'
como por ejemplo
`en las melodías en que los sonidos se mueven por salto'.
[La edición de Amsterdam de 1698 agrega, pero equivocadamente,
como puede verse en las citas anteriores:
`y en todos los tipos de música extranjera
en los que nunca se tocará con puntillo excepto cuando
esté así escrito'.]\break
Y `2. A veces la primera división se toca un poco más larga'
como por ejemplo
`en las melodías en que los sonidos se mueven por grado'.
Alternativamente, `la primera división se toca mucho más larga
que la segunda, pero la primera división debería'
en tal caso estar escrita `con puntillo'.
}

\dquotation{(158) Sébastien de Brossard, París, 1701/3, s.v.}{
Andante [es decir la variante italiana con bajo continuo en
corcheas]
`significa, sobre todo para el Bajo Continuo,
que todas las notas deben tocarse iguales,
y los sonidos bien separados'.
[Esto es cierto también para muchas partes similares de Bajo
Continuo en Allegro.]
}

\dquotation{(159) Michel de Saint-Lambert, París, 1702:}{
Ciertas notas se tocan desiguales
`porque la desigualdad les confiere más gracia
[pero ninguna regla es definitiva, porque]
el gusto tiene la última palabra en esto
igual que en el tempo'.
}

\dquotation{(160) Jacques M. Hotteterre, París, 1707:}{
`Las corcheas no siempre deben tocarse iguales',
ya que
`en algunos compases debería tocarse una larga y otra corta'
-- especialmente en
`compases de dos tiempos,
\musMeter{3}{4} y
\musMeter{6}{4}'.
}

\dquotation{(161) Michel de Montéclair, París, 1709:}{
Las notas tienden a ser
`iguales en \meterC,
\musMeter{2}{4} y
\musMeter{3}{8}'
pero
`desiguales en el compás de \textbf{3} simple'.
De todas formas, es muy difícil indicar principios generales
sobre la igualdad o desigualdad de las figuras,
`porque es el estilo de las piezas a ser cantadas
lo que decide esto';
pero en general
`las notas agrupadas de a cuatro en un pulso
deben tocarse desiguales, la primera un poco más larga
que la segunda';
o dos notas agrupadas en un pulso en compases de tres tiempos
en movimientos lentos.
}

\dquotation{(162) François Couperin, París, 1716/17:}{
`Escribimos distinto de como interpretamos',
mientras que [pero la evidencia de que disponemos nos muestra
que esto constituye, una vez más, una exageración]
`los Italianos por el contrario escriben su música en los valores
reales en que fue imaginada.
Por ejemplo, nosotros punteamos [en la ejecución]
varias corcheas [que se siguen]
en sucesión por grado; y sin embargo las escribimos iguales'.
}

\dquotation{(163) Pier Francesco Tosi, Bologna, 1723:}{
`Cuando sobre el movimiento igual de un Bajo,
que procede lentamente de una corchea a la otra,
un Cantante' se mueve
`casi siempre por grado en valores desiguales'.
}

\dquotation{(164) Michel de Montéclair, París, alrededor de 1730:}{
En compás simple de 2 tiempos,
`la primera corchea dura casi lo mismo que si estuviera escrita
con puntillo, y la segunda [se toca] casi tan rápido como una
semicorchea'.
En
\musMeter{3}{2},
\textbf{3},
\musMeter{3}{4} o
\musMeter{6}{4},
`las corcheas son desiguales'.
En
\musMeter{2}{4} o
\musMeter{3}{8},
`las corcheas son iguales. Las semicorcheas son desiguales.'
[Pero instrucciones tan definitivas como éstas, si bien son frecuentes,
no son consistentes con las de otros autores franceses.]
}

\dquotation{(165) Michel Corrette, París y Lyons, alrededor de 1740:}{
`El compás de cuatro tiempos \meterC{} o \meterCutC{} es muy utilizado
en la música italiana',
en la que
`es necesario puntear las semicorcheas de dos en dos.'
Del compás de \textbf{2} francés,
`los italianos nunca lo usan. Las corcheas deben tocarse desiguales
de dos en dos, es decir tocar la primera larga y la segunda breve.'
[Puede decirse que]
`el compás de
\musMeter{2}{4} o
\musMeter{2}{8} es el compás de 2 tiempos de los italianos',
en el que
`las corcheas deben tocarse iguales, y las semicorcheas desiguales';
mientras que
`el compás de
\musMeter{12}{8}
se encuentra en la música italiana, alemana, francesa e inglesa',
donde
`las corcheas deben tocarse iguales y las semicorcheas desiguales'.
}

\dquotation{(166) Michel Corrette, París, 1741:}{
`Las corcheas se tocan iguales en
[algunos tipos de]
música italiana como puede verse en el
Courante de la Sonata Op. V No. 7 de Corelli'
[un movimiento notable por proceder por salto más bien que por grado%
%--ver ejemplo \ref{} más abajo
].
`Pero en la música francesa la segunda corchea de cada pulso
se toca más rápidamente.'
}

{\vspace{0.5em}\noindent\textit{(167) Joachim Quantz, Berlín, 1752 (también en francés, con ligeras diferencias verbales usadas aquí en parte):}

[\textit{a}]
`Es necesario en piezas de velocidad moderada e incluso en adagio
que la nota más breve sea tocada con una cierta desigualdad,
aunque a simple vista parezcan del mismo valor;
de modo que en cada figura la nota acentuada,
es decir la primera, tercera, quinta y séptima
[etc.],
suenen un poco más apoyadas que las notas de paso,
es decir la segunda, cuarta, séptima y octava
[etc.],
si bien no deben sostenerse tanto como si llevaran puntillo,'

[\textit{b}]
`Esto no ocurre, sin embargo, tan pronto como dichas notas
se hallen mezcladas con figuras de notas aún más breves
o de la mitad del valor; porque entonces estas últimas'
son las que deben tocarse
`la primera y la cuarta notas un poco apoyadas,
y su tono un poco más elevado que el de la segunda y cuarta notas'.

[\textit{c}]
Pero la desigualdad queda excluída para las las notas
`en un movimiento muy rápido, donde el tiempo no permite tocarlas
desiguales, y donde por lo tanto sólo podemos aplicar duración e
intensidad a la primera de las cuatro';
también en notas \textit{staccato} no ligadas;
también en notas escritas \textit{non legato} o \textit{staccato};
también en
`varias notas con la misma altura';
también
`donde un ligado abarca más de dos notas';
también en
`corcheas en gigues'.
Porque
`todas estas notas deben ser tocadas iguales,
la una no más larga que la otra'.

[\textit{c}]
`Si en un allabreve lento o en compás simple
hay un silencio de corchea en el tiempo fuerte,
seguido de figuras con puntillo,
hay que leer el silencio como si tuviera escrito el puntillo,
o bien [otro] silencio de la mitad de su valor,
y como si la nota que le sigue
[aunque esté escrita como semicorchea]
fuera una fusa'
[es decir, la desigualdad se aplica a los silencios
al igual que a las notas].

\vspace{0.5em}
No existe un sistema fidedigno de \textit{inegalité}
que pueda construirse a partir de esta o cualquier otra
evidencia disponible.
Pero las siguientes sugerencias, apoyadas por la evidencia aportada
más arriba, pueden ayudarnos a mantener nuestro uso de la
\textit{inegalité} dentro de los límites apropiados del estilo.

\vspace{0.5em}\noindent(i) \textit{No se admite desigualdad en absoluto:}

Si existen indicaciones verbales en contrario
(%
\textit{tempo eguale},
\textit{note eguale},
\textit{egualmente},
\textit{notes égales},
\textit{coups égaux},
etc.).

Si existen signos de staccato (rayas, cuñas, puntos, etc.)
encima o debajo de las notas,
o si existen instrucciones verbales a tal efecto --
\textit{staccato},
\textit{marcato},
\textit{détachez},
\textit{notes martelées},
\textit{mouvement décidé} o
\textit{marqué},
etc. --
o si, sin que hubiera una indicación particular en la partitura,
el pasaje se toma staccato.

Si las notas que pudieran tocarse desiguales
son demasiado rápidas para lograr un resultado aceptable
sin sacudidas indebidas,
o tan lentas que no se lleguen a distinguir las figuras.

Si las notas que pudieran tocarse desiguales
se hallan ligadas o agrupadas en cualquier combinación
excepto de a pares,
o si el sentido mismo de la música
impide que se agrupen naturalmente de a pares.

\vspace{0.5em}\noindent(ii) \textit{La desigualdad probablemente no sea deseable:}

Si el carácter de la música no es ni \textit{\textit{cantabile}} ni vigoroso,
sino más bien contenido y regular
(algunos autores barrocos ubican la allemande en esta categoría).

Si los intervalos de la melodía proceden principalmente por salto
(como en el Courante de la Sonata Op. V No. 7 de Corelli,
descrita más arriba por Corrette
%166
como inapropiada para la ejecución desigual);
o si el sentido de la música es del tipo regular y decidido,
generalmente procediendo por saltos,
que es común en la línea de bajo en andantes y en ciertos tipos
de allegro, especialmente de carácter italiano
(tal como indica más arriba Brossard%
%158
).

Si la velocidad de las notas que de otro modo pudieran tocarse desiguales,
si bien no tan rápido o lento que llegue a ser inaceptable,
es, sin embargo, no del todo cómoda para tal propósito
(en este caso queda abierto un amplio margen para el juicio individual).

\vspace{0.5em}\noindent(iii) \textit{Cierto grado de desigualdad probablemente sea deseable:}

Si el carácter de la música es o bien \textit{\textit{cantabile}}
(en cuyo caso la desigualdad puede ser leve, a menudo similar a un ritmo
de tresillo),
o vigoroso
(en cuyo caso la desigualdad puede ser más pronunciada, a menudo similar
a un ritmo de figura con puntillo o incluso
--más bien excepcionalmente-- con doble puntillo.

Si los intervalos se hallan ligados de a pares o tienden a agruparse
naturalmente de a pares.

Si los intervalos se hallan emparejados principalmente por grado
(ya sea cantando como en las variantes tardías y más tranquilas de la
sarabanda, en la chaconne y el passacaille, y en muchas otras
de las formas de danza más elegantes, por un lado; ya sea vigorosamente
como en algunas de las más enérgicas, como el bourrée, el rigaudon
o la gavotte por otro lado).
Los saltos \textit{entre} un par de notas y otro no desalientan el uso
de la \textit{inegalité}, ya que ésta sólo puede ocurrir \textit{entre}
las notas que componen el par.
Más aún, unos \textit{pocos} saltos dentro de los pares de notas
puede que sea aceptable tocarlos en forma desigual; pero no demasiados.

Cuando la desigualdad parece ser aceptable y deseable,
las notas candidatas a ser tocadas desiguales son aquellas con la figuración
más rápida que aparece en número sustancial durante el pasaje.
Un \textit{pequeño} número de notas rápidas puede ignorarse a los efectos
de este análisis, en cuyo caso éstas pueden, o bien ejecutarse iguales,
o bien (si no son demasiado rápidas) tocarse desiguales también.
Un \textit{gran} número de notas rápidas (si bien lo bastante rápidas
como para ser en sí mismas candidatas a ser tocadas desiguales
en lugar de las figuras inmediatamente más largas)
excluye la \textit{inegalité} como posibilidad.
Y por otro lado, si las notas más rápidas presentes en número sustancial
en el pasaje son más lentas que dos notas por pulso,
la desigualdad probablemente deba descartarse.
De manera que, en efecto, las notas que, siendo favorables otras condiciones,
pudieran ser tocadas desiguales, no deberían ser para esto
ni demasiado rápidas ni demasiado lentas.

\vspace{0.5em}\noindent(iv) \textit{La desigualdad puede ser no sólo deseable sino necesaria:}

Si existen indicaciones verbales al respecto --
\textit{note diseguale},
\textit{note puntate},
\textit{note lunghette},
\textit{inégales},
\textit{notes inégales},
\textit{pointer} (literalmente `puntillo'),
\textit{lourer} (literalmente `ligadura',
\textit{couler} (literalmente `ligado', etc.

Si la desigualdad se halla indicada en un pasaje relacionado,
o en una o más partes si bien no en todas ellas,
o bien al principio pero no a continuación,
o de alguna otra forma que sugiera continuar de manera consistente.

\vspace{0.5em}
La desigualdad puede darse en dos direcciones:

La desigualdad \textit{simple} alarga la primera nota del par
y abrevia la segunda nota.
En un ritmo \textit{cantabile} similar a la medida de tresillo,
puede agregarse un ligado si no está ya expresamente escrito;
y a continuación una caída suave, quedando cada par de notas un poco
separado del siguiente.
Es muy posible anotar este efecto reescribiendo el compás de
\musMeter{3}{4} en
\musMeter{9}{8}, o escribiendo tresillos%
%(ver \ref{} más abajo)
.
En ritmos vigorosos próximos a la figura con puntillo,
no es necesario escribir el ligado: al contrario, es a menudo una buena práctica
interpretar el alargamiento del valor como un silencio de articulación,
insertando mentalmente no un puntillo
sino un silencio equivalente;
y en lugar de hacer una caída suave,
atacar (\textit{stoccare}) la figura con energía.
Es, nuevamente, muy posible anotar este efecto en la partitura
y las partes mediante puntillos, o bien silencios en lugar de puntillos.
Pero deberíamos dejar claro que,
ya sean improvisados o preconcebidos o agregados al texto preparado,
ninguno de estos ritmos implica una exactitud matemática, sino por el contrario
una flexibilidad natural.

La desigualdad \textit{inversa} alarga la segunda nota del par luego de
abreviar la primera. Esto puede hacerse en pasajes \textit{cantabile}, asimilándolo
a un ritmo de tresillo (probablemente agregando un ligado si no se halla
escrito ya en la partitura) lo que a un tempo razonablemente lento
proporcionará una caída aún más suave.
La desigualdad inversa puede también ocurrir en ritmos vigorosos
a un tempo bastante rápido, dando como resultado un ritmo similar a
la figura con puntillo invertida.
Las notas pueden tocarse ligadas o sueltas,
pero en ambos casos el acento ocurre en la primera nota del par
y cada par de notas sigue estando un poco separado del siguiente.

Al momento de decidir, en caso de hacerlo en absoluto,
con qué nivel de exactitud hemos de escribir en el texto preparado
una aproximación a la \textit{inegalité} que buscamos para la ejecución,
tal vez valga la pena recordar
que anotar las partes es, en general, considerablemente provechoso,
incluso tratándose de grupos que manejan el estilo en profundidad.
Una indicación oportuna puede prevenir aquellas vacilaciones momentáneas
bajo condiciones de \textit{stress} que pueden afectar hasta a los
mejores intérpretes.

\dquotation{(168) C. P. E. Bach, 1753:}{
`Ahora que los tresillos son cada vez más usados en compás simple
o de \musMeter{4}{4}, así como en \musMeter{2}{4} y \musMeter{3}{4},
han aparecido muchas piezas que podrían muy convenientemente
escribirse en \musMeter{12}{8}, \musMeter{9}{8} o \musMeter{6}{8}'
[una sugerencia tardía, pero útil].
}

\vspace{0.5em}
De las dos direcciones en que puede producirse la desigualdad,
el alargamiento simple de la primera nota y el acortamiento de la segunda
es un recurso mucho más habitual y versátil que el caso inverso
(alargamiento de la segunda nota y acortamiento de la primera).
Y la desigualdad sostenida (del tipo que no encontramos escrito literalmente)
es un recurso mucho más sutil que la desigualdad vigorosa
(que bien podría escribirse y de hecho frecuentemente se ha escrito
mediante figuras con puntillo).
Cualquier desigualdad usada fuera de su contexto apropiado,
o usada en exceso,
puede sonar amanerada o incluso grotesca.
Pero la desigualdad introducida por elección del ejecutante
puede ser un recurso maravilloso si se usa en el momento apropiado
--sobre todo, la desigualdad simple en un ritmo \textit{cantabile}.

