\version "2.20.0"

#(set-global-staff-size 14)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

upper = {
  \key bes \major
  \grace { s16
    \tweak extra-offset #'(-5 . 1.8)
    ^\markup \small { "(a)" }
  }
  \new Voice = upp { \relative c'' {
    \time 65/64
    \scaleDurations 65/64 { bes8
      \tweak extra-offset #'(-2 . -2)
      ^\markup \small { "escritura:" }
      bes4 bes8 d16 c bes4 bes8 }
    \time 7/8
    \scaleDurations 7/8 {
      f'32[^( ees d8.]) ees32[^( d c8.])
      bes4 ^\trill ^~ bes8. d16 }
    \time 4/4
    { bes4 ^~ bes8. ees16 bes4 ^~ bes8. f'16 }
  } }
}

lower = {
  \key bes \major
  \grace { s16
    \tweak extra-offset #'(-5 . 1.8)
    ^\markup \small { "(b)" }
  }
  \new Voice = low { \relative c'' {
    \time 4/4
    bes8
      \tweak extra-offset #'(-2 . -2)
      ^\markup \small { "ejecución por Farinelli:" }
    bes4 bes8
    \scaleDurations 2/3 { d16[ c bes] }
    bes4 bes8
    \time 4/4
    f'16[ d d c32^( ^\prall d])
    ees16[ c c bes32^( ^\prall c])
    \scaleDurations 2/3 { d16[ c bes] ^~ }
    bes4 d8
    \time 57/64
    \scaleDurations 57/64 {
      bes4.
      \scaleDurations 2/3 { ees16[ d c] }
      bes4. f'32[ ees d c]
    }
    \break

    \time 4/4
%    \once \override TrillSpanner.font-size = #-2
    \once \override TrillSpanner.font-shape = #'upright
    \once \override TrillSpanner.staff-padding = #3
    \once \override TrillSpanner.style = #'trill
    \once \override TrillSpanner.to-barline = ##t
    \once \override TrillSpanner.bound-details.left.text =
      \markup \concat { \musicglyph "scripts.trill" " " \bold "[" }
    \once \override TrillSpanner.bound-details.right.text =
      \markup \bold { "]" }
    \once \override TrillSpanner.bound-details.right.stencil-align-dir-y = #0.2
    bes1 \startTrillSpan ^~ bes1 ^~ bes1 ^~ bes1
    f'16
    \stopTrillSpan
    d d ^\prall c32 d
    ees16 c c ^\prall bes32 c
    d16 bes bes ^\prall a32 bes
    c16 a a ^\prall g32 a
    bes2. ^\trill ^~ bes16 c d ees
    \break

    f
      \tweak extra-offset #'(-1 . 0.4)
      ^\markup \small { "[cadenza incorporada por Farinelli]" }
      ees d c bes a g f bes a g f ees d c bes
    bes'4 ^\trill ^~ bes16 c d ees f ees d c bes a g f
    \break

    bes a g f ees d c bes
      bes'8[ ^\prall
      f ^\prall
      d ^\prall
      f] ^\prall
    bes4 ^\trill ^~ bes16 d c d
      bes4 ^\trill ^~ bes16 d c d
    \scaleDurations 4/3 { bes4. ^\trill a8
        bes4
        \tweak extra-offset #'(0 . -2)
        ^\fermata
        \tweak extra-offset #'(-4 . -10.5)
        ^\markup \small { "[textual]" }
    }
  } }
}

lyricsa = \lyricmode {
  Quel l'u -- si -- gno " " -- lo chein -- na -- mo -- ra --
  - - -- - - -- -
}

lyricsb = \lyricmode {
  Quel l'u -- si -- gno " " - lo chein --
    na " " " " -
    mo " " " " -
    ra - - -- -  -  - - -  -  - - - -
    -   --   --   --
      -  -  -  - -
      -  -  -  - -
      -  -  -  - -
      -  -  -  - -
      -   -- - - -
    - - - -   - - - -   - - - -  - - - -
      -   -- - - -   - - - -   - - - -
    - - - -  - - - -  - - - -
      -   --  - - -   -   -- - - -   -  - to
}

\markup { \vspace #1 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
      \remove "Timing_translator"
      \remove "Default_bar_line_engraver"
    }
    \context {
      \Staff
      \consists "Timing_translator"
      \consists "Default_bar_line_engraver"
    }
  }
  <<
    \new Staff \with { \remove "Time_signature_engraver" } { \upper }
    \new Lyrics \lyricsto upp { \lyricsa }
    \new Staff \with { \remove "Time_signature_engraver" } { \lower }
    \new Lyrics \lyricsto low { \lyricsb }
 >>
}

\markup { \vspace #1 }

