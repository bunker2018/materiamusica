\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \relative c' {
    \override Staff.Accidental.parenthesized = ##t
    \time 5/2
    r2 d1 \grace {cis2 s s} d1
    \bar "||"
    r2
    \scaleDurations 4/5 {
      d2.*2/6
      \grace {s2 s}
      cis8[ d cis]
    }
    \scaleDurations 4/6 {
      d16[
      \set stemLeftBeamCount = #2
      \set stemRightBeamCount = #1
      cis
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #2
      d
      \set stemLeftBeamCount = #2
      \set stemRightBeamCount = #1
      cis
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #2
      b
      cis]
    }
    d1
  }
  \bar "||"
  } \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit BarNumber }
  }
}

