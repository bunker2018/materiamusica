\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \override Staff.Clef.stencil = #(lambda (grob)
        (bracketify-stencil (ly:clef::print grob) Y 0.1 0.2 0.1))
    \time 2/4
    \relative c'' {
      c2
      \tweak extra-offset #'(1 . 0)
      ^\markup { \musicglyph "scripts.prall" }
      _\markup { \small "Cadence" }
      d16[ c d c d c d c]
      \bar "||"
      c2
      ^\markup { \musicglyph "scripts.downprall" }
      _\markup { \small "Cadence appuyée" }
      \scaleDurations 8/9 { d8[ c16 d c d c d c] }
      \bar "||"
      \time 3/4
      d2
      \tweak extra-offset #'(1 . 0)
      ^\markup { \rotate #180 \musicglyph "scripts.upprall" }
      _\markup { \small "Double Cadence" }
      e4
      e16[ d e d e d c d] e4
      \bar "||"
    }
  } \layout { \context { \Staff \omit TimeSignature } }
}

