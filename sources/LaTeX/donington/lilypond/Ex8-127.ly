\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff {
      \clef alto
      \key f \major
      \time 3/4
      \relative c' {
        g4 e f _~ f f4.
          ^\markup \concat {
            \large { "[ = " }
            \musicglyph "scripts.trill"
            \large { " ]" }
          }
        e8 f2.
        \bar "||"
      }
    }
    \new Staff {
      \clef bass
      \key f \major
      \time 3/4
      \relative c {
        e8[ f g e] a4 ^~ a
        << {
          g4.
          ^\markup \concat {
            \large { "[ = " }
            \musicglyph "scripts.trill"
            \large { " ]" }
          }
          f8
        } \\ {
          c2
        } >>
        <f, a c f>2.
        \bar "||"
      }
    }
    \new Staff {
      \clef bass
      \key f \major
      \time 3/4
      << \relative c {
        \stemDown e4 \stemUp c a bes c2 f,2.
      } \\ {
        s2 s4
          \tweak extra-offset #'(0 . 0)
          ^\markup \teeny { \musicglyph "six" }
        s4
          \tweak extra-offset #'(0 . 0)
          ^\markup \teeny { \musicglyph "seven" }
        s4.
          \tweak extra-offset #'(0 . 0)
          ^\markup \teeny { \musicglyph "four" }
          \tweak extra-offset #'(0 . 0)
          ^\markup \teeny { \musicglyph "five" }
        s8
          \tweak extra-offset #'(0 . 0)
          ^\markup \teeny { \musicglyph "three" }
      } >>
      \bar "||"
    }
  >>
  \layout {
    \context {
      \Score
      \omit BarNumber
    }
  }
}

