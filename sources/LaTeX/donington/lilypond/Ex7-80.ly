\version "2.20.0"

#(set-global-staff-size 10)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key f \major
      \relative c'' {
        \time 4/4
        c8[ \scaleDurations 2/1 { c'32. bes64] }
          a8[ bes16. c32]
          d16.[
            \set stemLeftBeamCount = #3
            \set stemRightBeamCount = #2
            c32
            \set stemLeftBeamCount = #2
            \set stemRightBeamCount = #3
            bes c bes c]
          \scaleDurations 8/12 {
            d32 c bes
              \set stemLeftBeamCount = #3
              \set stemRightBeamCount = #1
              a
            \set stemLeftBeamCount = #1
            \set stemRightBeamCount = #3
            g
              a g
              \set stemLeftBeamCount = #3
              \set stemRightBeamCount = #1
              a
            \set stemLeftBeamCount = #1
            \set stemRightBeamCount = #3
            bes
              a g f
          }

        e8[ ^~ e32 c d e]
          f[ g f g]
          a[ bes a bes]
          c4 r

        \break

        \scaleDurations 4/3 {
          g32 f
          \set stemLeftBeamCount = #3
          \set stemRightBeamCount = #1
          e
        }
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #2
          f16. bes32
          g8.
            \tweak extra-offset #'(0 . 0)
            ^\markup \tiny { \bold "+" }
            \tweak extra-offset #'(-2 . 0)
            ^\markup \small { \concat { "[ = " \musicglyph "scripts.trill" " ]" } }
          f16 f2
          \bar "||"
          \hideNotes
          \stopStaff
          \override Staff.StaffSymbol.transparent = ##t
          \startStaff
          s2
          \omit Score.BarLine
          s2
      }
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass \key f \major
      \relative c {
        \time 4/4
        a8 \scaleDurations 2/1 { \scaleDurations 4/5 { a'32. g32 } }
          f8 f, bes4
          <<
            {
              s8  \tweak extra-offset #'(0 . 0)
                  ^\markup \teeny { \musicglyph "six" }
              s8  \tweak extra-offset #'(0 . 0)
                  ^\markup \teeny { \musicglyph "five" }
              s2  \tweak extra-offset #'(0 . 0)
                  ^\markup \teeny { \musicglyph "four" }
                  \tweak extra-offset #'(0 . 0)
                  ^\markup \teeny { \musicglyph "two" }
            }
          \\
            { bes'4 ^~ bes2 }
          >>
        r8 bes
        a32.[
          \set stemLeftBeamCount = #4
          \set stemRightBeamCount = #1
          bes64
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #4
          a g f
          \set stemLeftBeamCount = #4
          \set stemRightBeamCount = #1
          e
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #3
          d32 c d bes]

        c8[ a16. bes32] c8 c, f2
      }
    }
  >>
}

