\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \relative c'' {
    \key d \minor
    \time 4/4
    \scaleDurations 4/5 { fis8 g8. }
    \scaleDurations 4/3 { fis16 g fis }
    g fis g fis
    g fis e fis
  }
  \bar "||"
  } \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit BarNumber }
  }
}

