\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key d \major \time 2/2
      \relative c'' {
        \autoBeamOff
        cis8.
          \tweak extra-offset #'(-8 . 1.6 )
          ^\markup \small {
            "folio" \concat { "110" \super "v" }
          }
        b16 b4 s2
      }
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass \key d \major \time 2/2
      <<
        \relative c {
          \stemUp a4
          \tweak extra-offset #'(-5.5 . 0 )
          ^\markup \teeny {
            "[bac] - cia  -  te - lo"
          }
          e''8 b cis[ b cis dis]
        }
      \\
        \relative c {
          \stemDown s4 e a8[ gis fis b]
        }
      >>
    }
  >>
}

