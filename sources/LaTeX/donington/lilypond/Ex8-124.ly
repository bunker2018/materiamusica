\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff {
      \clef bass \time 4/4
      \relative c {
        \partial 2 {
          f4
          \tweak extra-offset #'(-2 . 0.5)
          ^\markup \small { "(a)" }
          g
        }
        a4. d,8 fis2
        \once \omit Score.TimeSignature
        \time 3/8
        g4.
        \bar "||"
      }
    }
    \new Staff {
      \clef bass \time 4/4
      \relative c {
        \partial 2 {
          \autoBeamOff
          f8.
          \tweak extra-offset #'(-2 . -0.5)
          ^\markup \small { "(b)" }
          ^( g16 )
          \autoBeamOn
          g8 ^\markup \tiny { \bold "+" }
          ^( f16 g)
        }
        a4 ^~ a16 d,[ e f]
        \scaleDurations 8/12 {
          g16[ fis g fis!
          g fis! g fis!
          g fis! e fis!]
        }
        \once \omit Score.TimeSignature
        \time 3/8
        g4.
        \bar "||"
      }
    }
    \new Staff {
      \clef bass \time 4/4
      \relative c {
        \partial 2 {
          \autoBeamOn
          f8.
          \tweak extra-offset #'(-2 . -0.5)
          ^\markup \small { "(c)" }
          ^( g16 )
          a32^>^( g a
          \set stemLeftBeamCount = #3
          \set stemRightBeamCount = #1
          g
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #3
          a g f g)
        }
        s1
          \tweak extra-offset #'(-1 . 0)
          ^\markup \tiny { "etc." }
        \once \omit Score.TimeSignature
        \time 3/8
        s4.
        \bar "||"
      }
    }
  >>
  \layout { \context { \Score \omit BarNumber } }
}

