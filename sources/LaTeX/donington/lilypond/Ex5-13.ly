\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \override Stem.neutral-direction = #up
    \key e \major \time 3/4
    \relative c'' {
      \partial 4 {
        gis4
        \tweak extra-offset #'(-12 . 2)
        ^\markup \small {"(a)"}
      }
      fis2^\prall
        \tweak extra-offset #'(-8.2 . 0)
        ^\markup \small { \bold "Tendrement" }
      b4
      dis,2^\prall e4
      fis8[^\prall e fis gis a fis]
      gis4 e gis
      fis2^\prall b4
      dis,2^\prall e4
      fis8[^\prall e fis gis a fis]
      e2
      \bar ":|."
    }
  }
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
  }
}

\score {
  \new Staff {
    \override Stem.neutral-direction = #up
    \key e \major \time 3/4
    \relative c'' {
      \partial 4 {
        gis4
        \tweak extra-offset #'(-12 . 2)
        ^\markup \small {"(b)"}
      }
      gis4
      ^\prall _( fis)
      b4
      \grace {e,8_(} dis2
        \tweak extra-offset #'(-1 . 0)
        ^\markup \concat {
          \large {"["}
          \raise #0.2 \teeny { \musicglyph "scripts.trill" }
          " "
          \raise #0.8 \teeny { \musicglyph "scripts.turn" }
          \large {"]"}
        }
      ) \startTrillSpan
      e4 \stopTrillSpan
      fis8.[^\prall e16 fis8. gis16 a8. fis16]
      \grace {fis8_(} gis4
          \tweak extra-offset #'(-2 . 1)
          ^\markup \concat {
            \large {"["}
            \score {
              \new Staff \with { \magnifyStaff #5/7 }
              {
                \relative c'' {
                  a16 a8. a16 a8.
                }
              }
              \layout {
                \omit Score.Clef
                \omit Staff.TimeSignature
                \override Voice.NoteHead.transparent = ##t
                \omit Staff.StaffSymbol
                ragged-right = ##t
              }
            }
            \large {" ]"}
          }
      ) \grace {fis8_(} e4) gis
          \tweak extra-offset #'(6 . -6)
          ^\markup \small { "etc." }
    }
  }
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
    \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16)
  }
}

