\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new StaffGroup <<
  \new Staff {
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(-2 . -0.2)
      ^\markup \small { "escritura" }
    }
    \relative c'' {
      g'2 f4.^\trill e8
      s2. s64 s s s s32 s16
    }
  }
  \new Staff {
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(-2 . -0.2)
      ^\markup \small { "ejecución" }
    }
    \relative c'' {
      g'2.*7/6 f16^\mordent e
    }
    \bar "||"
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      ^\markup \small { "[interpretación]" }
    }
    \relative c'' {
      g'2. ^~ g64[ f g f ^~ f32 e16]
    }
  }
>>
\layout { \context { \Staff \omit TimeSignature } }
}
