\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 2/2
  \grace { s16
    \tweak extra-offset #'(0 . 0)
    _\markup \tiny {"a beat"}
    \tweak extra-offset #'(3.6 . -3.8)
    ^\markup { \rotate #45 \musicglyph "ties.lyric.short" }
    \tweak extra-offset #'(4.5 . -4.1)
    ^\markup { \rotate #-35 \musicglyph "flags.mensuralu03" }
  }
  \relative c'' {
    b2 c
    b2 \magnifyMusic 0.88 { b8 ^( } c4. )
  }
  \bar "||"
  \grace { s16
    \tweak extra-offset #'(0 . -1)
    _\markup \tiny {"a backfall"}
    \tweak extra-offset #'(4.8 . -2.2)
    ^\markup { \musicglyph "flags.mensuralu13" }
  }
  \relative c'' {
    g2 f
    g2 \magnifyMusic 0.88 { g8 _( } f4. )
  }
  \bar "|"
  } \layout { \context { \Staff \omit TimeSignature } }
}
