\paper{
  indent=0\mm
  line-width=120\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\version "2.20.0"

{
<<
    {g'2 a' a' b' b' c'' c'' d''}
    {c'1 d' e' f' }
>>
}
