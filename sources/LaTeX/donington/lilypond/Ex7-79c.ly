\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff
    {
      \key d \major \time 3/2
      \relative c' {
        r2 r fis
        cis'1 cis2
        fis1 fis4 gis
        a2 gis4^( a) b^( gis!)
        \break

        a2 fis d
        d2^( cis) cis
        fis2 b,2. cis4
        cis1.
        \bar "||"
      }
    }
    \new Staff
    {
      \key a \major \time 3/2
      \relative c' {
        r2
          \tweak extra-offset #'(-4 . 1)
          ^\markup \small { \bold { "Adagio" } }
          r2 fis4. d'8
        \grace { d4^( } cis1 cis8) e^( d cis)
        fis2. fis4 gis gis8.^(
          \tweak extra-offset #'(0 . 0)
          ^\markup \concat { "[" \musicglyph "scripts.trill" "]" }
          fis32 gis)
        a2
          \tuplet 3/2 { gis4^( a b) }
          \tuplet 3/2 { a4^( b gis) }
        \grace { gis4 } a2
          \grace { gis4 } fis2
          e8^( d cis d)
        gis4 d cis2 ^~ cis8 e^( d cis)
        fis4 d \grace { a8 b } b2. ^\trill cis4
        cis1.
        \bar "||"
      }
    }
  >>
  \layout { \context { \Score \omit BarNumber } }
}

