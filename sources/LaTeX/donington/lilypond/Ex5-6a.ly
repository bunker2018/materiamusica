\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef soprano \key d \major s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\score {
  <<
    \new Staff \with {
      \remove "Time_signature_engraver"
%      \override VerticalAxisGroup.default-staff-staff-spacing =
%        #'((basic-distance . 3.5)
%           (padding . 4))
    }
    {
      \set Staff.instrumentName = \incipit
      \key d \major \time 2/2
      \relative c'' {
        \autoBeamOff
        cis8.
          \tweak extra-offset #'(-18 . 1)
          ^\markup \small {
            "folio" \concat { "110" \super "r" }
          }
        b16 a4 s2
        \tweak extra-offset #'(8 . 0)
        ^\markup \small \italic { "e sim." }
      }
    }
    \new Staff \with {
      \remove "Time_signature_engraver"
    }
    {
      \clef bass \key d \major \time 2/2
      <<
        \relative c {
          \stemUp a4
          \tweak extra-offset #'(-5.5 . 0.5 )
          ^\markup \teeny {
            "[culla]     -     te - lo"
          }
          a'8 e fis8.[ e16 fis8 gis]
        }
      \\
        \relative c {
          \stemDown s4 a d8.[ cis16 b8 e]
        }
      >>
    }
  >>
  \layout {
    \override Staff.InstrumentName.Y-offset = #-4.225
  }
}

