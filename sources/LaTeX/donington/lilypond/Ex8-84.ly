\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { <<
  \new Staff {
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(9.6 . -3.3)
      ^\markup { \rotate #12 \musicglyph "scripts.lcomma" }
    }
    \relative c'' {
      b8 s8 s4 c4 s
  \bar "||"
    }
    \grace { s16
      \tweak extra-offset #'(9.6 . -2.3)
      ^\markup { \rotate #12 \musicglyph "scripts.lcomma" }
    }
    \relative c'' {
      c8 s8 s4 b4 s
  \bar "||"
    }
  }

  \new Staff {
    \time 2/2
    \relative c'' {
      b8 s8 s4 b8[ ^( s c] ) s
    }
    \relative c'' {
      c8 s8 s4 c8[ ^( s b] ) s
    }
  }
>>
\layout { \context { \Staff \omit TimeSignature } }
}
