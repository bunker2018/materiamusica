\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 3/4
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 11"}
  }
  \relative c'' {
    r4 r c
    \grace { \stemUp c8 _( \stemNeutral } d2 ) e4
    \grace { \stemUp e8 _( \stemNeutral } f2 ) g4
  }
  \bar "||"
  \omit Staff.TimeSignature \time 3/8
  \relative c''' {
    \partial 8 {
      \grace { s16
        \tweak extra-offset #'(-1.5 . -0.8)
        ^\markup \small {"Fig. 13"}
      }
      g8
    }
    \grace { \stemUp f8 _( \stemNeutral } e4 ) d8
    \grace { \stemUp d8 _( \stemNeutral } c4 ) d8
  }
  \bar "||"
  \undo \omit Staff.TimeSignature \time 6/8
  \relative c'' {
    \grace { s16
      \tweak extra-offset #'(-2 . 1)
      ^\markup \small {"Fig. 15"}
    }
    c8. d16 c8 c4 f8
    \grace { \stemUp f8 _( \stemNeutral } e4. ) ^~ e4 f8
  }
  \bar "||"
}
\layout {
\context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16) }
}}

\score { \new Staff {
  \time 3/4
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 12"}
  }
  \relative c'' {
    r4 r c c4 ^( d ) e e ^( f ) g
  }
  \bar "||"
  \omit Staff.TimeSignature \time 2/4
  \relative c''' {
    \partial 8 {
      \grace { s16
        \tweak extra-offset #'(-1.5 . -0.8)
        ^\markup \small {"Fig. 14"}
      }
      g8
    }
    f4 ^( e8 ) d d4 ^( c8 ) d
  }
  \bar "||"
  \undo \omit Staff.TimeSignature \time 6/8
  \relative c'' {
    \grace { s16
      \tweak extra-offset #'(-2 . 1)
      ^\markup \small {"Fig. 16"}
    }
    c8. d16 c8 c4 f8 f4. ^( e4 ) f8
  }
  \bar "||"
}
\layout {
\context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16) }
}}

\score { \new Staff {
  \time 6/4
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 17"}
  }
  \override Stem.neutral-direction = #up
  \relative c''' {
    g2 d4 b4. a8 g4 \grace { g8 _( } a2. ) _~ a2 c4
  }
  \bar "||"
  \omit Staff.TimeSignature \time 2/2
  \override Stem.neutral-direction = #down
  \stemUp
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 23"}
    \relative c'' { c8 _( }
  }
  \stemNeutral
  \relative c'' {
    b2 )
    r2
    \time 2/4
    \grace { \stemUp c8 _( \stemNeutral } b4 ) r4
    \grace { \stemUp c8 _( \stemNeutral } b4 ) r8 g
  }
  \bar "||"
}
\layout {
\context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/24) }
}}

\score { \new Staff {
  \time 6/4
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 18"}
  }
  \override Stem.neutral-direction = #up
  \relative c''' {
    g2 d4 b4. a8 g4 g2. _( a2 ) c4
  }
  \bar "||"
  \omit Staff.TimeSignature \time 2/2
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"Fig. 24"}
  }
  \override Stem.neutral-direction = #down
  \relative c'' {
    c2 ^( b )
    \time 2/4
    c4 ^( b )
    \override Stem.neutral-direction = #up
    c4 ^( b8 ) g
  }
  \bar "||"
}
\layout {
\context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 4/110) }
}}
