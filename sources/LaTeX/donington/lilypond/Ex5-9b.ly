\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "G_8"
    \time 3/1
    \relative c {
      c8.[
      \tweak extra-offset #'(-5 . 2)
      ^\markup \small { "(b)" }
      d16
      \tweak extra-offset #'(-5 . 1)
      ^\markup \small { "ejecución:" }
      e8. f16 g8. a16]
    }
  }
  \layout {
    \omit Staff.TimeSignature
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32) }
  }
}

\markup { \vspace #0.2 }
