\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef soprano s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\score {
  \new PianoStaff <<
    \new Staff \with {
      \remove "Time_signature_engraver"
    }
    {
      \set Staff.instrumentName = \incipit
      \relative c'' {
        \grace { s16
          \tweak extra-offset #'(0 . 1.3)
          ^\markup \small {"(a)"}
          \tweak extra-offset #'(3.6 . -1)
          ^\markup { \concat {
            \small "[  "
            \teeny \note-by-number #2 #0 #0.8
            "   "
            \teeny \note-by-number #2 #0 #0.8
            \small "   ]"
          } }
        }
        \time 2/2 r4 d \grace { \stemUp c \stemNeutral } b2
        \time 1/4 c4
        \bar "||"
        \grace { s16
          \tweak extra-offset #'(0 . 1.3)
          ^\markup \small {"(b)"}
        }
        \time 2/2 r4 d c b
        \time 1/4 c4
        \bar "||"
        \grace { s16
          \tweak extra-offset #'(0 . 1.3)
          ^\markup \small {"(c)"}
        }
        \time 2/2 d2 b
        \time 1/4 c4
        \bar "||"
      }
    }
    \new FiguredBass {
      \figuremode {
        r2 <2>2 <6>4
        r4 <5> <5> <4\\> <6>
        <5>2 <4\\> <6>4
      }
    }
    \new Staff \with {
      \remove "Time_signature_engraver"
      \clef bass
    }
    {
      \relative c' {
        g2 f e4
        g2 f e4
        g2 f e4
      }
    }
  >>
  \layout {
    \override Staff.InstrumentName.X-offset = #-5.775
    \override Staff.InstrumentName.Y-offset = #-6.85
  }
}

