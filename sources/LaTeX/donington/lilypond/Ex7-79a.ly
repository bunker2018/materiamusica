\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff
    {
      \key d \major \time 4/4
      \relative c'' {
        \partial 8 {
          e8
          \tweak extra-offset #'(-8 . 1.6 )
          ^\markup \small { "(a)" }
        }
        cis8 a cis d16^( cis) b8 e,4 \stemUp b'8 \stemNeutral
        a8_( b16 cis) b8. a16 gis8 a4 e'8
        \break

        e8 a, d8. d16 d8^( c4) a'16^( gis)
        fis16^( e) d^( cis) b4 a8 cis
          fis4 ^~
          \grace { \bar "" \hideNotes fis \unHideNotes \bar "|" }
        \break
      }
    }
    \new Staff
    {
      \key a \major \time 4/4
      \relative c'' {
        \partial 8 {
          e8
          \tweak extra-offset #'(-8 . 1.6 )
          ^\markup \small { "(b)" }
        }
        \grace { d8 } cis8
        \grace { b8 } a8
        cis16^( a32 cis e16) cis
          b16^( dis32 e) e,4 \stemUp b'8 \stemNeutral
        a16_( gis fis e) dis32_( fis b dis) fis16._( a,32)
          \grace { a8 } gis8
          \grace { fis8 } e8 r e'

        e16^( cis b a) d16^( cis32 d) b'16^( d,)
          \grace { e8 } d8 cis ^~ cis16 e a e
        fis16 a, d b b8. ^\trill a16 a8 cis16 eis
          fis4 ^~
          \grace { \bar "" \hideNotes fis \unHideNotes \bar "|" }
      }
    }
  >>
  \layout { \context { \Score \omit BarNumber } }
}

