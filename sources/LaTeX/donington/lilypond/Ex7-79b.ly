\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff
    {
      \key d \major \time 12/8
      \relative c'' {
        \partial 8 { e8 }
        cis4 e8 b4 e8 a,4. r8 r e
        \stemUp fis8_( a d) gis,( b e) cis_( b a) \stemNeutral
      }
    }
    \new Staff
    {
      \key a \major \time 12/8
      \relative c'' {
        \partial 8 {
          e8
          \tweak extra-offset #'(-4 . 0)
          ^\markup \small { \bold { "Giga" } "Allegro" }
        }
        cis8^( a e') b_( a gis) a^( cis e) a^( e a,)
        fis'8^( d a) \stemUp gis_( b e) cis_( b) a
      }
    }
  >>
  \layout { \context { \Score \omit BarNumber } }
}

