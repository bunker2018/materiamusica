\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef soprano s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\score {
  <<
    \new Staff {
      \override Staff.TimeSignature.style = #'single-digit
      \set Staff.instrumentName = \incipit
      \time 3/4
      \new Voice = uno { \relative c'' {
        e4
          \tweak extra-offset #'(0 . 1)
          ^\markup \small { "Sarabande" }
        d8 c b a gis2 e4 c'4. d8[ c b] c4 ^( b ) a
        \tweak extra-offset #'(3 . 0)
        ^\markup \small { "etc." }
      }
    } }
    \new Lyrics \lyricsto uno {
      \lyricmode {
        Al -- lez " " Ber - gers, des -- sus l'her - - -- bet -- te
      }
    }
    \new Staff {
      \override Staff.TimeSignature.style = #'single-digit
      \clef bass \time 3/4
      \new Voice = dos { \relative c {
        a4
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "Basse à chanter" }
        c d e2 c'8^( b
        \scaleDurations 3/4 {
          a2)
          \scaleDurations 4/2 { \autoBeamOff d,8 d \autoBeamOn }
        }
        e2 a,4
        \bar "||"
      }
    } }
    \new Lyrics \lyricsto dos {
      \lyricmode {
        Al -- lez Ber -- gers, des -- sus l'her -- bet -- te
      }
    }
  >>
  \layout {
    \override Staff.InstrumentName.Y-offset = #-4.925
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 4/116) }
  }
}

\score {
  <<
    \new Staff \with {
      \remove "Time_signature_engraver"
    } {
      \new Voice = tres { \relative c'' {
        \time 3/4
        \scaleDurations 4/3 { e16[
          \tweak extra-offset #'(0 . 1)
          ^\markup \small { "2. Couplet" }
          f e] }
        d8[ c] b a
        \bar "!"
        gis2
          \tweak extra-offset #'(0 . 0.35)
          _\markup \small { \bold "+" }
        \autoBeamOff fis16 _( e8. ) \autoBeamOn
        \bar "!"
        c'16[ b c8 d c] d b
        \bar "!"
        c4^( b) a
        \tweak extra-offset #'(2.3 . 0)
        ^\markup \small { "etc." }
        \bar "||"
      }
    } }
    \new Lyrics \lyricsto tres {
      \lyricmode {
        Chan " " - tant " " le "___" soir sur la - - - - fon - ge -- re
      }
    }
    \new Staff \with {
      \remove "Time_signature_engraver"
    } {
      \relative c {
        \clef bass \time 3/4
        a4
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "accidentals.flat" }
        c
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "six" }
        \once \override Stem.neutral-direction = #up
        d
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "accidentals.flat" }
        \bar "!"
        e4
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "accidentals.sharp" }
        \autoBeamOff e'8 d c8. b16 \autoBeamOn
        \bar "!"
        a4
          << {
            s4 s
              \tweak extra-offset #'(0 . 0)
              ^\markup \tiny { \musicglyph "six" }
          } \\ {
            d,2
              \tweak extra-offset #'(0 . 0)
              ^\markup \tiny { \musicglyph "seven" }
          } >>
        \bar "!"
        e4
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "four" }
        e,
          \tweak extra-offset #'(0 . 0)
          ^\markup \tiny { \musicglyph "accidentals.sharp" }
        a
        \bar "||"
      }
    }
  >>
}
