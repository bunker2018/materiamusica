\version "2.20.0"

#(set-global-staff-size 14)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

upper = {
  \key d \minor \time 4/2
  \grace { s16
    \tweak extra-offset #'(-5 . 1.8)
    ^\markup \small { "(a)" }
  }
  \relative c'' {
    \set melismaBusyProperties = #'()
    bes1 a2 r4 d
    g,4 fis g2 a r4 d
    a4. a8 a4 bes c2 2
    d2. \autoBeamOff c8 bes \autoBeamOn a bes c2 bes4
    a1 \bar "" \break g1
    bes1 a2 r4 d
    g,4 fis g2 a
  }
}

lower = {
  \key d \minor \time 4/2
  \grace { s16
    \tweak extra-offset #'(-5 . 1.8)
    ^\markup \small { "(b)" }
  }
  \relative c'' {
    \stemUp
    \set suggestAccidentals = ##t
    \override Staff.AccidentalSuggestion.parenthesized = ##t
    bes2. \tuplet 3/2 { a8.[ bes16 c8] } a2 r4 \stemDown d \stemNeutral
    g,4 fis \tuplet 6/4 { g8.[ a16 bes8] a8.[ bes16 c8] } a2 r4 d
    a4. a8 a4 bes16 g a bes c4 c d2 ~
    d4 \autoBeamOff 8. bes16 \autoBeamOn a8. bes16 c2 bes16 a bes c a2
    s1  g1
    \tuplet 12/4 {
      d'16[ c bes c]   bes8[ c16 a]   bes[ a g a]   g8[ a16 fis!]
      g[ fis! e fis]
        \set suggestAccidentals = ##f
        g[ a g a]   bes[ c d bes]   c[ d ees c]
        \bar "" \break
        d16[ c d ees!]   d[ ees! d c]   bes[ c d bes]   c[ a bes g]
    } a2 r4 d
    g,4 fis g8 a bes16 a bes g a2
  }
}

lyricsa = \lyricmode {
  Ar -- di, ar 
  di,  cor mi -- o, Che
  non fu vi -- sta ma -- i
  Fiam -- ma di più "__" "__" bei
  ra -- í;
  Ar -- di, ar --
  di cor mi -- o,
}

lyricsb = \lyricmode {
  Ar - - - di, ar
  di, cor mi - - - - - o, Che
  non fu vi -- sta "__" "__" "__" ma - i
  Fiam - ma di pií bei "__" "__" "__" ra -- í;
  Ar - - -   - - -   - - - -   - - -   - - - -   - - - -   - - - -   - - - -
  - - - -   - - - -   - - - -   - - - -   di, ar --
  di cor mi - - - - - o,
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff \with { \remove "Time_signature_engraver" } {
      \override Staff.TupletBracket.stencil = ##f
      \override Staff.TupletNumber.stencil = ##f
      \new Voice = "upp" \with { \remove "Forbid_line_break_engraver" } { \upper }
    }
    \new Lyrics \lyricsto "upp" { \lyricsa }
    \new Staff \with { \remove "Time_signature_engraver" } {
      \override Staff.TupletBracket.stencil = ##f
      \override Staff.TupletNumber.stencil = ##f
      \new Voice = "low" \with { \remove "Forbid_line_break_engraver" } { \lower }
    }
    \new Lyrics \lyricsto "low" { \lyricsb }
 >>
 \layout { \context { \Score
  \override SystemStartDelimiter.style = #'dashed-line } }
}

\markup { \vspace #1 }

