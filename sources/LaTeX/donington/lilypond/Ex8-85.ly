\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \set Staff.instrumentName = \markup { "(a)" }
    \override Staff.TimeSignature.style = #'single-digit
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(-1 . 1)
      ^\markup { "Ports de voix" }
    }
    \relative c'' {
      c4. b8
      \grace { \stemUp b4 _( \stemNeutral }
      c4. ) d8
      
      \grace { \stemUp d4 _( \stemNeutral }
      e4. ) d8
      \grace { \stemUp d4 _( \stemNeutral }
      e4. ) f8
      
      \grace { \stemUp f4 _( \stemNeutral }
      g2 )
      \bar "||"
    }
  }
  \layout {
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 16/168) }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup { "(b)" }
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(1 . 1)
      ^\markup { "Coulements" }
    }
    \relative c''' {
      \partial 2 {
        g4
        \grace { \stemUp f4 _( \stemNeutral }
        e4 )
      }
      \grace { \stemUp d4 _( \stemNeutral }
      c4 )
      \grace { \stemUp b4 _( \stemNeutral }
      a4 )
      f'
      \grace { \stemUp e4 _( \stemNeutral }
      d4 )
      \grace { \stemUp c4 _( \stemNeutral }
      b4 )
      \grace { \stemUp a4 _( \stemNeutral }
      g4 )
      \bar "||"
    }
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16) }
  }
}
