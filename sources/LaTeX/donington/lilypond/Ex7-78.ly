\version "2.20.0"

#(set-global-staff-size 14)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\layout {
  \context {
    \Staff
    \consists "Mark_engraver"
    \override RehearsalMark.self-alignment-X = #LEFT
%    \override Clef #'break-visibility = #begin-of-line-invisible
  }
  \context {
    \StaffGroup
    systemStartDelimiterHierarchy = 
      #'(SystemStartBrace (SystemStartBracket a b))
  }
  \context {
    \Score
    \override SystemStartBrace.style = #'bar-line
    \omit SystemStartBar
    \omit SystemStartBracket
    \override SystemStartBrace.padding = #-0.1
    \override SystemStartBrace.thickness = #1.6
    \remove "Mark_engraver"
    \override StaffGrouper.staffgroup-staff-spacing.basic-distance = #15
  }
}

upper = {
  \key d \major \time 2/2
  \relative c' {
    d2
      \tweak extra-offset #'(-5 . 1.8)
      ^\markup \small { "(a)" }
      \tweak extra-offset #'(0 . -2)
      ^\markup \small { "Primera edición" }
    _~ d8[ fis] e[ d] | b'4 a8. g16 fis2 \bar "||"
  }
}

lower = {
  \clef bass \key d \major \time 2/2
  \relative c {
    d1
      \tweak extra-offset #'(0 . 0)
      ^\markup \small { \italic "Grave" }
    _~ | \stemUp
    d4
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { \musicglyph "two" }
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { \musicglyph "four" }
    cis
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { \musicglyph "six" }
    d2
    \bar "||"
  }
}

corelli = {
  \key d \major \time 2/2
  \relative c' {
    d2
      \tweak extra-offset #'(-1 . 0)
      ^\markup \small { "(b)" }
      \tweak extra-offset #'(0 . 0)
      ^\markup \small { "Ed. de 1715, ornamentos tal como los ejecutaba Corelli" }
    _~ d8[ fis16_( g16])
    e8.
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { \bold "+" }
    d16
      \tweak extra-offset #'(-1 . 0)
      ^\markup \tiny { \concat { "[- " \musicglyph "scripts.trill" "]" } }
    |
    \scaleDurations 8/21 {
      b'8[ _~ b32 cis_( b cis d e d cis
      b a g fis e fis g a b)]
    }
    a8.[
      \tweak extra-offset #'(0 . 1)
      ^\markup \tiny { \bold "+" }
    g16]
    fis8 _~
    \scaleDurations 4/13 {
      fis16 g32_( fis g fis g fis g a fis g a)
    }
    d,4
    \bar "||"
  }
}

\markup { \vspace #1 }

\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \remove "Time_signature_engraver" } {
        \upper
      }
      \new Staff \with { \remove "Time_signature_engraver" } {
        \clef "bass"
        \lower
      }
    >>
    \new Staff \with { \remove "Time_signature_engraver" } {
      \corelli
    }
 >>
}

\markup { \vspace #1 }

