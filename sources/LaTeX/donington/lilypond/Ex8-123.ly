\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 2/4
  \grace { s16
    \tweak extra-offset #'(0 . -1)
    _\markup \tiny {"A Backfall"}
    \tweak extra-offset #'(2.7 . -1)
    _\markup \tiny {"shaked"}
    \tweak extra-offset #'(4.8 . -3.2)
    ^\markup { \musicglyph "flags.mensuralu13" }
  }
  \relative c'' {
    g4 f
    \bar "|"
    g4
    g32 f g f g f g f
    \bar "||"
  }
  } \layout { \context { \Staff \omit TimeSignature } }
}

\markup { \vspace #0.2 }

