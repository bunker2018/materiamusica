\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

apPath = #'(
  (moveto 0 0)
  (lineto 0.1 -3)
  (lineto 0.22 0)
  (closepath)
)

\markup { \vspace #1 }

\score { \new Staff {
  \time 2/4
  \grace { s16
    \tweak extra-offset #'(0 . 0)
    _\markup \tiny {"a forefall"}
    \tweak extra-offset #'(3.2 . -1.2)
    ^\markup {
      \rotate #-55
      \override #'(filled . #t)
      \path #0.15 #apPath
    }
  }
  \relative c'' {
    s4 g
    s8 f16[ g8.] s8
  }
  \bar "||"
  \grace { s16
    \tweak extra-offset #'(0 . 0)
    _\markup \tiny {"a backfall"}
    \tweak extra-offset #'(3.2 . -1)
    ^\markup {
      \rotate #-125
      \override #'(filled . #t)
      \path #0.15 #apPath
    }
  }
  \relative c'' {
    s4 g
    s8 a16[ g8.] s8
  }
  \bar "|"
  } \layout { \context { \Staff \omit TimeSignature } }
}
