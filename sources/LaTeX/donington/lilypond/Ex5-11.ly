\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
  ragged-right = ##f
}

doubleLine = #'(
  (moveto 0 0)
  (lineto 1.7 0)
  (moveto 0 0.4)
  (lineto 1.7 0.4)
)

doubleLineWPrep = #'(
  (moveto 0 0)
  (lineto 1.7 0)
  (moveto 0 0.4)
  (lineto 1.7 0.4)
  (moveto -0.6 0.2)
  (lineto -0.9 2.2)
)

semiquaver = \markup \concat {
  \lower #2.2 \large {"[ "}
  \tiny { \musicglyph "flags.u4" }
  \lower #2.2 \large {" ]"}
}
          
\markup { \vspace #1 }

\score {
  <<
    \new PianoStaff <<
      \new Staff {
        \key g \major \time 2/2
        \relative c'' {
          r8
            \tweak extra-offset #'(-4 . 1.5)
            ^\markup \small { "(a)" }
            g8 fis4
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            e16[ g a b] c d e fis
        }
        <<
          \relative c''' {
            g2 ^~ g16[ fis g e] fis g fis g
              \tweak extra-offset #'(1.5 . -3.4)
              ^\markup { \rotate #8 \path #0.2 #'(
                (moveto 0 0)
                (lineto 1.5 0)
              )}
          }
        \\
          \relative c'' {
            r8 e d4
              \tweak extra-offset #'(0 . 0)
              _\markup { \rotate #30 \path #0.2 \doubleLine }
              c2
          }
        >>
        \relative c'' {
          a'4. a8
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            \tweak extra-offset #'(0.5 . 0)
            ^\markup { \rotate #210 \musicglyph "ties.lyric.default" }
            b16[ a g fis] e d cis b
            \tweak extra-offset #'(0 . 0)
            ^\markup \small { "etc."}
        }
      }
      \new Staff {
        \clef bass \key g \major \time 2/2
        <<
          \relative c {
            r8 d g4 g2
            s1
            r8 a d4. d8
            \scaleDurations 8/9 {
              g4 ^~ \hideNotes g32 \unHideNotes
            }
          }
        \\
          \relative c {
            b4. g8 c2 _~
            c8 c' b4
              \tweak extra-offset #'(0 . 0)
              ^\markup { \rotate #30 \path #0.2 \doubleLine }
              a4. g8
            fis4. d8 g4. g8
          }
        >>
      }
    >>
  >>
  \layout {
  }
}

\score {
  <<
    \new PianoStaff <<
      \new Staff {
        \key g \major \time 2/2
        \relative c'' {
          r8
            \tweak extra-offset #'(-4 . 1.5)
            ^\markup \small { "(b)" }
            g8 fis4
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            e16.[ g32 a16. b32] c16. d32 e16. fis32
        }
        <<
          \relative c''' {
            g2 ^~ g16.[ fis32 g16. e32]
              \stemDown fis16.[
              \set stemLeftBeamCount = #3
              \set stemRightBeamCount = #1
              g32
              \set stemLeftBeamCount = #1
              \set stemRightBeamCount = #2
              g16
              \tweak extra-offset #'(0 . 0)
              ^\markup { \rotate #30 \path #0.2 \doubleLine }
              fis32 g32] \stemNeutral
          }
        \\
          \relative c'' {
            r8 e d4
              \tweak extra-offset #'(0 . 0)
              _\markup { \rotate #30 \path #0.2 \doubleLine }
              c2
          }
        >>
        \relative c'' {
          a'4. a8
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            \tweak extra-offset #'(0.5 . 0)
            ^\markup { \rotate #210 \musicglyph "ties.lyric.default" }
            b32[ a16. g32 fis16.] e32 d16. cis32 b16.
            \tweak extra-offset #'(0 . 0)
            ^\markup \small { "etc."}
        }
      }
      \new Staff {
        \clef bass \key g \major \time 2/2
        <<
          \relative c {
            r8 d g4 ^~ g8 g c4 ^~
            c8 c b4
              \tweak extra-offset #'(0 . 0)
              ^\markup { \rotate #30 \path #0.2 \doubleLine }
              r8 c4. ^~
            c8 a d4. d8 g4
          }
        \\
          \relative c {
            b2 c2 _~
            c4*4/2 a'4. g8
            fis4. d8 g4. g8
          }
        >>
      }
    >>
  >>
  \layout {
  }
}

\score {
  <<
    \new PianoStaff <<
      \new Staff {
        \time 3/4
        \relative c'' {
          \partial 8 {
            a8
            \tweak extra-offset #'(-4  . 0)
            ^\markup \small { "(c)" }
            \tweak extra-offset #'(-1 . -2)
            ^\semiquaver
          }
          <c, e a^~>4 a'8. a16 g8.
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            f16
          e4._~ e16 a gis8. b16
          a32_( b c8) a16 a4.
            \tweak extra-offset #'(1  . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLineWPrep }
            _~ a16 gis
        }
      }
      \new Staff {
        \clef bass \time 3/4
        \partial 8 { s8 }
        <<
          \relative c' {
            r8. a16^~ a4 b
            r8. c16^~ c4 b
            r8. a16 e2
          }
        \\
          \relative c {
            r4 a s
            r4 c s
            s4 r e,
          }
        >>
      }
    >>
    \new Staff \with { \omit TimeSignature } {
      \time 3/4
      \relative c'' {
        \partial 8 {
          a8
          \tweak extra-offset #'(-1 . 0.5)
          ^\semiquaver
          \tweak extra-offset #'(-4  . -3)
          ^\markup \small { "(d)" }
        }
        <c, e a>4. a'8
          \tweak extra-offset #'(-1 . 0.5)
          ^\semiquaver
          g8. f16
        e4. a8
          \tweak extra-offset #'(-1 . 0.5)
          ^\semiquaver
          gis8. b16
        \stemUp a16
          \tweak extra-offset #'(0 . 0)
          ^\markup \concat {
            \large {"["}
            \score {
              \new Staff \with { \magnifyStaff #5/7 }
              {
                \relative c'' {
                  a32 a32 a8.
                }
              }
              \layout {
                \omit Score.Clef
                \omit Staff.TimeSignature
                \omit Voice.NoteHead
                \omit Staff.StaffSymbol
                ragged-right = ##t
              }
            }
            \large {" ]"}
          }
          b16 c8 a4.*7/6
            \tweak extra-offset #'(1  . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLineWPrep }
          gis8*2/4
          \tweak extra-offset #'(-1 . 0.5)
          ^\semiquaver
          \stemNeutral
      }
    }
    \new Staff \with { \omit TimeSignature } {
      \clef bass \time 3/4
      \partial 8 {
        s8
        \tweak extra-offset #'(-4  . 0.5)
        ^\markup \concat {
          \large {"["}
          \teeny { \raise #1 \compound-meter #'(3 . 4) }
          \large {"]"}
        }
      }
      \relative c' {
        a4 a, b'
        r8. c16 c,4 b'
        r8. a16 e4 e,
      }
    }
    \new PianoStaff <<
      \new Staff {
        \time 3/4
        \set Staff.timeSignatureFraction = 3/2
        \relative c'' {
          \partial 8 {
            a8
            \tweak extra-offset #'(-4  . 0)
            ^\markup \small { "(e)" }
          }
          <c, e a^~>4. a'16 a16 g8.
            \tweak extra-offset #'(0 . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLine }
            f16
          e4._~ e16 a
              \tweak extra-offset #'(1.2 . 0.2)
              _\markup { \rotate #-8 \path #0.2 #'(
                (moveto 0 0)
                (lineto 2.7 0)
              )}
            gis8. b16
          a32 b c8 a16 a4.*7/6
            \tweak extra-offset #'(1  . 0)
            ^\markup { \rotate #30 \path #0.2 \doubleLineWPrep }
            gis8*2/4
            \tweak extra-offset #'(-1 . 0.5)
            ^\semiquaver
        }
      }
      \new Staff {
        \clef bass \time 3/4
        \set Staff.timeSignatureFraction = 3/2
        \partial 8 {
          s8
          \tweak extra-offset #'(-4  . 1)
          ^\markup \concat {
            \large {"[ = "}
            \teeny { \raise #1 \compound-meter #'(3 . 4) }
            \large {"]"}
          }
        }
        <<
          \relative c' {
            a2^\prall b4
            r8. c16 ^~ c4 b
            r8. a16 e2
          }
        \\
          \relative c {
            r4 a s
            r4 c s
            s4 r e,
          }
        >>
      }
    >>
  >>
}

