\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \relative c'' {
    \time 2/4
    \stemUp
    <g b d>2
    \tweak extra-offset #'(-4.5 . 0)
    ^\markup \small { "(a)" }
    \time 4/4
    \stemNeutral
    <c, c'>1
    \bar "||"

    \time 2/4
    << \voiceTwo {
%    \stemDown
      <g' d'>2
      \tweak extra-offset #'(-3 . 1)
      ^\markup \small { "(b)" }
    } \\ \voiceOne {
%    \stemUp
      \once \override NoteColumn.force-hshift = #0.8
      b4. a16 b
    } >>
    \stemNeutral
    \time 4/4
    <c, c'>1
    \bar "||"

    \time 2/4
    \stemUp
    <g' b d>2
    \tweak extra-offset #'(-3.5 . -1)
    ^\markup \small { "(c)" }
    \bar ""
    \grace { a8 b }
    \bar "|"
    \time 4/4
    <c, c'>1
    \bar "||"
%    \break

    \time 2/4
    << {
      \once \override NoteColumn.force-hshift = #0.5
      c'8
      \tweak extra-offset #'(-3.5 . -2.5)
      ^\markup \small { "(d)" }
      [^~^( c32 b c b] c[ b c
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      b
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      c b a b)]
    } \\ {
      <g d'>2
    } >>
    \time 4/4
    <c, c'>1
    \bar "||"
    \hideNotes \stopStaff \once \override Staff.StaffSymbol.transparent = ##t \startStaff s2 \omit Staff.BarLine s4. s s \undo \omit Staff.BarLine
  }
  } \layout {
    \context { \Staff \omit TimeSignature }
    \omit Score.BarNumber
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8) }
  }
}
