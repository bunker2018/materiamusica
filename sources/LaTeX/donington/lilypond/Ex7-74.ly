\version "2.20.0"

#(set-global-staff-size 14)

%TODO: polymetric stuff everywhere

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

drawBarline = \markup {
  \override #'(thickness . 2)
  \draw-line #'(0 . 4)
}

noBarline = \once \override Staff.BarLine.stencil = ##f

upper = {
  \key f \major
  \set melismaBusyProperties = #'()
  \autoBeamOff
  \override Stem.neutral-direction = #up
  \grace { s16
    \tweak extra-offset #'(-2 . 0.5)
    ^\markup \small { "1" }
  }
  \relative c'' {
    \scaleDurations 4/4 {
      \time 2/2
      a2. a8 a8
      a2 r4 c4
\tweak extra-offset #'(2 . 8)
^\markup \center-column { \small {
"\"Care-charming Sleep\""
"(Robert Johnson)"
} }
      bes4 a bes4. a8
    }
%    \time 15/8
%    \scaleDurations 15/8 {
%      a2 r8 cis d e
%    }
%    \time 5/8
%    \scaleDurations 5/8 {
%      e1
%    }
    
    \time 6/8
    \scaleDurations 6/8 {
      \scaleDurations 4/5 {
        a2
\tweak extra-offset #'(19 . 5)
^\markup \column { \left-align { \small {
"1. por Ms Don, c. 57"
"2. por Ms Fitzwilliam 52.D.15"
"3. por BM Ms Add. II,608"
} } }
        r8 }
      \scaleDurations 4/3 { cis8 d e }
    }
    \time 3/8
    \scaleDurations 3/8 {
      e1
      \tweak extra-offset #'(-1 . 1)
      ^\markup \tiny { "[mediados del siglo XVII]" }
    }

    \break

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 2/2
    \scaleDurations 4/2 {
      e4
    \tweak extra-offset #'(-3.6 . 0.2)
    ^\markup \small { "1" }
      d8 c
    }
    b4. a8 a2 _~
    a4 a f d
    bes'2. a4
    \break

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 5/4
    \scaleDurations 5/4 { g4
    \tweak extra-offset #'(-3.6 . 0.2)
    ^\markup \small { "1" }
    c, c'4. bes8 }
    \time 3/4
    \scaleDurations 3/4 { a2. g4 }
    \time 7/4
    \scaleDurations 7/4 {
      \scaleDurations 9/8 { g4. f8 }
      \scaleDurations 7/8 { f2 }
    }
    \break

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 2/2
    a4
    \tweak extra-offset #'(-3.6 . 0)
    ^\markup \small { "1" }
    a8 a cis4. d8
    \time 7/8
    \scaleDurations 7/8 { e4. a,8 d2 ^~ }
    \time 11/8
    \scaleDurations 11/8 { d4 g, bes4. a8 }
    \time 3/4
    \scaleDurations 3/4 { a4 a2. }
  }
}

middle = {
  \key f \major \time 2/2
  \set melismaBusyProperties = #'()
  \autoBeamOff
  \override Stem.neutral-direction = #up
  \grace { s16
    \tweak extra-offset #'(-2 . 0.5)
    ^\markup \small { "2" }
  }
  \relative c'' {
    \scaleDurations 4/4 {
      \time 2/2
      a2. a8 a8
      a2.
        \autoBeamOn \slashedGrace { a16 ^( bes } c4) \autoBeamOff
    }
    \scaleDurations 11/6 {
      \scaleDurations 3/4 { bes8 }
      \scaleDurations 5/4 { g }
%      << {
%        bes8
%        \once \override NoteColumn #'force-hshift = #-0.8
%        g
%      } \\ {
%        \hideNotes f8 f \unHideNotes
%      } >>
    }
    \scaleDurations 13/18 {
      \autoBeamOn \slashedGrace { g16 _( a } bes2) _~ \autoBeamOff
      bes8 bes
    }

    \time 3/8
    \scaleDurations 3/8 { a1 }
    \time 6/8
    \scaleDurations 6/8 { cis4 cis8 d
        \grace { cis16[^(^\markup \tiny {
          \musicglyph "accidentals.natural"
        } d] } e2)
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 2/2
    \scaleDurations 4/6 {
      r2
%  \grace { s16
    \tweak extra-offset #'(-3.6 . 0.2)
    ^\markup \small { "2" }
%  }
      r8 e
    }
    \scaleDurations 8/9 {
      d8 c16[^( d e b] c[ d c])
    }
    b4. a8 a2
    r4 a8 f d[_( e f g])
    \scaleDurations 4/2 {
      a8 _( bes4 ) a16[ g]
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 11/32
    \scaleDurations 11/16 {
      a2
%  \grace { s16
    \tweak extra-offset #'(-3.6 . -0.2)
    ^\markup \small { "2" }
%  }
    }
    \time 14/16
    \scaleDurations 14/16 {
      c,16[_( d e f] g[ a bes8]) c2 ^~
    }
    \time 25/32
    \scaleDurations 25/32 {
      c4 bes8[_( a]) a4. a8
    }
    \time 6/4
    \scaleDurations 12/11 {
      g16[_( a bes c] a[ bes c f,] g[ a g a] bes[ a] g4.) f8
    }
    \time 1/4
    \scaleDurations 2/8 {
      f1
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 2/2
    \scaleDurations 3/4 { r2
    \tweak extra-offset #'(-3.6 . 0.2)
    ^\markup \small { "2" }
    }
    \scaleDurations 5/4 { a4 a8 a }
    \time 2/2
    cis4. \grace { d16[^( e] } f8) e2
    r4 a, \grace { b16[^( c] } d4.) g,8
    bes4 a16[_( bes c bes]) a8 a4.
  }
}

lower = {
  \key f \major \time 2/2
  \set melismaBusyProperties = #'()
  \autoBeamOff
  \override Stem.neutral-direction = #up
  \grace { s16
    \tweak extra-offset #'(-2 . 0.5)
    ^\markup \small { "3" }
  }
  \relative c'' {
    \scaleDurations 4/4 {
      \time 2/2
      r4 a2 a8 a      % c1
      a2 r4 c4        % c2
    }
    \scaleDurations 11/6 {  % c3
        bes8
        g
    }
    \scaleDurations 13/18 {
%      << {
%        \once \override NoteColumn #'force-hshift = #-1.5
        bes2
%        \once \override NoteColumn #'force-hshift = #-0.5
        a4
%      } \\ { \hideNotes a2 g4 \unHideNotes } >>
    }

    \time 3/8
    \scaleDurations 3/8 { a1 }
    \time 6/8
    \scaleDurations 6/8 { cis8 cis4 cis8
      \tweak extra-offset #'(3.5 . -4.5)
      ^\markup { \rotate #125
        \override #'(filled . #t)
        \path #0.2 #'(
          (moveto 0 0)
          (lineto -0.1 3.8)
          (lineto 0.1 0)
          (closepath)
        )
      }
      e2 }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \grace { s16
    \tweak extra-offset #'(-2 . 0.5)
    ^\markup \small { "3" }
  }
    \time 2/2
    \scaleDurations 8/13 {
      r4 e2 d16[^(
    }
    \scaleDurations 8/7 {
      c16 d e] f32[ e d16]) c8
    }
    b4. a8 a2
    r4 a f8 d bes'4 _~
    \scaleDurations 4/2 {
      bes4 a8[_( g])
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \time 19/16
    \scaleDurations 5/4 {
      g4
    }
%  \grace { s16
    \tweak extra-offset #'(-3.6 . 0)
    ^\markup \small { "3" }
%  }
    \scaleDurations 14/7 { c,16[_(
      d e f] g[ a bes])
    }
    \time 33/16
    \scaleDurations 2/3 { c4. }
    \scaleDurations 4/3 { bes8 a4 }
    \scaleDurations 21/9 {
      g16[^( f g a bes] c[ d32 c bes16 a])
    }
    \time 2/4
    \scaleDurations 2/4 {
      g4. f8 f2
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \grace { s16
    \tweak extra-offset #'(-2 . 0.5)
    ^\markup \small { "3" }
  }
    \time 2/2
    a4 a8 a cis4. d8
    \time 7/8
    \scaleDurations 7/8 { e4. a,8 _~ a4. d8 ^~ }
    \time 27/16
    \scaleDurations 27/24 {
      d4 c16[_( bes a g]) g[^( a g a] bes[ c bes c]
        d[ c d ees d c] \stemDown bes8) \stemUp
    }
    \time 7/16
    \scaleDurations 7/8 { a8 a4. }
  }
}

lyricsa = \lyricmode {
  Care -- char ming
    Sleep, thou
    ea -- ser of all
    woes bro ther of
    Death,

  sweet -- ly thy
    self dis -- pose
    " " on this af --
    flic -- ted

  wight, fall like a
    cloud in
    gen -- tle showers;

  give no -- thing that is
    loud or pain -- - --
    ful to his
    slum -- bers,
}

lyricsb = \lyricmode {
  Care -- char ming
    Sleep, thou
    ea -- ser of " " all
    woes
    bro ther of
    Death,

  sweet -- ly thy - - - - - -
    self dis -- pose
    " " on this af - - -
    flic -- - ted

  man
    fall - - - - - - like " "
    a " " cloud in
    gen - - - - - - - - - - - - - - -- tle
    showers;

  give no -- thing
    that is loud
    or pain -- ful
    to his - - - slum -- bers,
}

lyricsc = \lyricmode {
  Care -- char ming
    Sleep, thou
    ea -- ser of all
    woes
    bro ther of
    Death,

  sweet -- ly - - - - - - thy
    self dis -- pose
    on this af -- flic -- - --  ted " "

  wight, fall - - - - - -
    like a cloud in - - - - - - - - -
    gen -- tle showers;

  give no -- thing that is
    loud or " " pain - --
    ful " " " " " " to - - - - - - - - - - - - - his
    slum -- bers,
}

\markup { \vspace #4 }

\score {
  \layout {
    \context {
      \Score
      \omit BarNumber
      \remove "Timing_translator"
      \remove "Default_bar_line_engraver"
    }
    \context {
      \Staff
      \consists "Timing_translator"
      \consists "Default_bar_line_engraver"
    }
  }
  <<
    \new Staff \with { \remove "Time_signature_engraver" } {
      \override Staff.TupletBracket.stencil = ##f
      \override Staff.TupletNumber.stencil = ##f
      \new Voice = "upp" \with { \remove "Forbid_line_break_engraver" } { \upper }
    }
    \new Lyrics \lyricsto "upp" { \lyricsa }

    \new Staff \with { \remove "Time_signature_engraver" } {
      \override Staff.TupletBracket.stencil = ##f
      \override Staff.TupletNumber.stencil = ##f
      \new Voice = "mid" \with { \remove "Forbid_line_break_engraver" } { \middle }
    }
    \new Lyrics \lyricsto "mid" { \lyricsb }

    \new Staff \with { \remove "Time_signature_engraver" } {
      \override Staff.TupletBracket.stencil = ##f
      \override Staff.TupletNumber.stencil = ##f
      \new Voice = "low" \with { \remove "Forbid_line_break_engraver" } { \lower }
    }
    \new Lyrics \lyricsto "low" { \lyricsc }
  >>
}

\markup { \vspace #1 }

