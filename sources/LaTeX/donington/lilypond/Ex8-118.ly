\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 8/8
  \relative c'' {
    gis8
    _\markup \small { "Groppo di sopra" }
    a16[ gis a gis
    a gis a gis
    a gis a gis
    \once \override Staff.Accidental.parenthesized = ##t
    fis
    gis]
  }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}

