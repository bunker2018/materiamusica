\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { <<
  \new Staff {
    \time 3/4
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      _\markup \teeny { "Accent" }
      \tweak extra-offset #'(0 . 0)
      _\markup \teeny { "steigend" }
      \tweak extra-offset #'(0.8 . -3.4)
      ^\markup { \rotate #-18 \musicglyph "ties.lyric.default" }
    }
    \relative c'' {
      s8 c4 s8 s4
      \bar "||"
    }
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      _\markup \teeny { "Accent" }
      \tweak extra-offset #'(0 . 0)
      _\markup \teeny { "fallend" }
      \tweak extra-offset #'(0.8 . -2.4)
      ^\markup { \rotate #-18 \musicglyph "ties.lyric.default" }
    }
    \relative c'' {
      s8 c4 s8 s4
      \bar "||"
    }
  }

  \new Staff {
    \time 3/4
    \relative c'' {
      s8 b[ ^( s c] ) s4
    }
    \relative c'' {
      s8 d[ ^( s c] ) s4
    }
  }
>>
\layout { \context { \Staff \omit TimeSignature } }
}
