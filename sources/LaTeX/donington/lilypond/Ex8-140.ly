\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  <<
    \relative c'' { \voiceOne {
      <b d>2
      \tweak extra-offset #'(-4.5 . 0)
      ^\markup \small { "(a)" }
      c1

      <d>2
      \tweak extra-offset #'(-3 . -1)
      ^\markup \small { "(b)" }
      c1

      e8
      \tweak extra-offset #'(-3 . -2.5)
      ^\markup \small { "(c)" }
      ^~ e32 d e d
      e[ d e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d c d]
      c1

      <c e>8
      \tweak extra-offset #'(-4.5 . -1)
      ^\markup \small { "(d)" }
      ^~ <c e>32 <b d> <e c> <b d>
      <c e>[ <b d> <c e>
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      <b d>
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      <c e> <b d> <a c> <b d>]
      c1
    } }
  \\
    \relative c'' { \voiceTwo {
      g2 c,1
      g'2 c,1
      <g' b>2 c,1
      g'2 c,1
    } }
  \\
    \relative c'' { \voiceThree {
      \time 2/4
      s2
      \bar "||"
      \time 4/4
      s1
      \bar "||"

      \time 2/4
      c8 ^~ c32 b c b
      c[ b c
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      b
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      c b a b]
      %\bar "||"
      \time 4/4
      s1
      \bar "||"

      \time 2/4
      s2
      \bar "||"
      \time 4/4
      s1
      \bar "||"
      \break

      \time 2/4
      s2
      %\bar "||"
      \time 4/4
      s1
      \bar "||"
      \hideNotes \stopStaff \once \override Staff.StaffSymbol.transparent = ##t \startStaff s2 \omit Staff.BarLine s4. s s s s \undo \omit Staff.BarLine
    } }
  \\
    {
      s2
      _\markup \teeny { \musicglyph "five" }
      _\markup \teeny { \musicglyph "three" }
      s1
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "three" }

      s4
      _\markup \teeny { \musicglyph "five" }
      _\markup \teeny { \musicglyph "four" }
      s
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "three" }
      s1
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "three" }

      s4
      _\markup \teeny { \musicglyph "six" }
      _\markup \teeny { \musicglyph "three" }
      s
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "three" }
      s1
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "three" }

      s4
      _\markup \teeny { \musicglyph "six" }
      _\markup \teeny { \musicglyph "four" }
      s
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -2)
      _\markup \teeny { \musicglyph "three" }
      s1
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "five" }
      \tweak extra-offset #'(0 . -0.6)
      _\markup \teeny { \musicglyph "three" }
    }
  >>
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \omit Score.BarNumber
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8) }
  }
}
