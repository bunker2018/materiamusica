\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new StaffGroup <<
  \new Staff {
    \time 3/4
    \relative c'' {
      \grace { \stemUp c4
        \tweak extra-offset #'(-2 . 1)
        ^\markup \tiny { "escritura" }
      } \stemNeutral
      b2 c4
    }
    \bar "||"
    \time 2/4
    \relative c'' {
      \grace { \stemUp c4
        \tweak extra-offset #'(-2 . 1)
        ^\markup \tiny { "y" }
      } \stemNeutral
      b2
    }
    \bar "||"
  } 
  \new Staff {
    \time 3/4
    \relative c'' {
      \grace { s4
        \tweak extra-offset #'(-2 . 1)
        ^\markup \tiny { "ejecución" }
      }
      c4. ^( b8 ) c4
    }
    \bar "||"
    \time 2/4
    \relative c'' {
      \grace { s4 }
      c4. ^( b8 )
    }
    \bar "||"
  } 
>>
  \layout {
    \context {
      \StaffGroup
      \omit SystemStartBracket
    }
  }
}
