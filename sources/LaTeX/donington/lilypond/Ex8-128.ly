\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "(a)   " }
    \once \override Staff.TimeSignature.style = #'single-digit
    \time 2/2
    \relative c'' {
      e2
        _\markup \tiny { "Tremblement appuyé et lié" }
      ^( d4. )
        \tweak extra-offset #'(3 . -1)
        ^\markup { \musicglyph "scripts.prallprall" }
      c8 c1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/64)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "      " }
    \time 2/2
    \relative c'' {
      e2 ^\( ^~
        _\markup \tiny { \italic "Interpretación" }
      e16 d32
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      e
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      d e d e d8[\) r16 c] c1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/64)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "(b)   " }
    \time 2/2
    \relative c'' {
      c4.
        _\markup \tiny { "Tremblement ouvert" }
      d8 d4.
        \tweak extra-offset #'(0 . 0)
        ^\markup { \musicglyph "scripts.prallprall" }
      ^( c16 d ) e1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "      " }
    \time 2/2
    \relative c'' {
      c4.
        _\markup \tiny { \italic { "Interpretación" } "  (o con el ritmo de" \italic {"(c)"} ")" }
      d8[ e32^> d^( e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d c d)]
      e1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "(c)   " }
    \time 2/2
    \relative c'' {
      g'4.
        _\markup \tiny { "Tremblement fermé" }
      d8 d4.
        \tweak extra-offset #'(0 . 0)
        ^\markup { \musicglyph "scripts.prall" }
      ^( c16 d ) c1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "      " }
    \time 2/2
    \relative c'' {
      g'4.
        _\markup \tiny { \italic { "Interpretación" } "  (o con el ritmo de" \italic {"(b)"} ")" }
      d8 e8^>[ ^\( ^~ e32 d e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d e
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      d
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
      e d c d\)]
      c1
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "(d)   " }
    \time 3/4
    \relative c'' {
      e4
        _\markup \tiny { "Tremblement lié sans etre appuyé" }
      ^( d4
        \tweak extra-offset #'(0 . 0)
        ^\markup { \musicglyph "scripts.prallprall" }
      ) c4
      \bar "||"
      e4 ^\( ^~ \grace { \stemDown e32 d e d e } d4 \) c
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "      " }
    \time 3/4
    \relative c'' {
      e4
      ^\( ^~
      \once \override TupletBracket.bracket-visibility = ##f
      \tuplet 5/4 {
        e32 d e d
        \set stemLeftBeamCount = #3
        \set stemRightBeamCount = #1
        e
      }
      d8 \) c4
        \tweak extra-offset #'(14 . -3.6)
        ^\markup \tiny { \italic { "(Interpretación.)" } }
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "(e)   " }
    \time 3/4
    \relative c'' {
      e4
        _\markup \tiny { "Tremblement detaché" }
      d4
        \tweak extra-offset #'(0 . 0)
        ^\markup { \musicglyph "scripts.prallprall" }
      c4
      \bar "||"
      e4 \grace { \stemDown e32 ^\( d e d e } d4 \) c
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

\score {
  \new Staff {
    \set Staff.instrumentName = \markup \italic { "      " }
    \time 3/4
    \relative c'' {
      e4
      \once \override TupletBracket.bracket-visibility = ##f
      \tuplet 5/4 {
        e32 ^\( d e d
        \set stemLeftBeamCount = #3
        \set stemRightBeamCount = #1
        e
      }
      d8 \) c4
        \tweak extra-offset #'(14 . -3.6)
        ^\markup \tiny { \italic { "(Interpretación.)" } }
      \bar "||"
    }
  }
  \layout {
    \context { \Score
      \omit BarNumber
      \omit TimeSignature
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/8)
    }
  }
}

