\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \time 2/4
    \relative c'' {
      \grace { s8
        \tweak extra-offset #'(-1.6 . 1.2)
        ^\markup \small {"(a)"}
      }
      g4 _( f
        \tweak extra-offset #'(0 . 0)
        ^\markup { \musicglyph "scripts.trill_element" }
        \tweak extra-offset #'(1.4 . -1.3)
        ^\markup \small { \musicglyph "scripts.trilelement" }
      )
      \bar "||"
      g4 _( _~ g64 f g f _~ f8. )
      \bar "||"
      \grace { s8
        \tweak extra-offset #'(-1.5 . 0)
        ^\markup \small {"(b)"}
      }
      a16 f_( e^\prall d) b' g_( f^\prall e)
      \bar "||"
      \grace { s8
        \tweak extra-offset #'(-1 . -2)
        ^\markup \small {"(c)"}
      }
      b'16 f_( e64 f e32 d16)
        \tweak extra-offset #'(-0.5 . 0.5)
        ^\markup \tiny {"etc."}
      \bar "||"
      \grace { s8
        \tweak extra-offset #'(-1 . -3)
        ^\markup \small {"(d)"}
      }
      b'16 f_.
      \scaleDurations 8/5 { f128_( e f e _~ e ) }
      d16
        \tweak extra-offset #'(-0.5 . 0.5)
        ^\markup \tiny {"etc."}
      \bar "||"
    }
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

