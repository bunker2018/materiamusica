\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new PianoStaff <<
    \new Staff {
      \clef G
      \time 2/4
      \relative c'' {
        e4 d^\trill c1*2/4
        \tweak extra-offset #'(-1 . 0)
        _\markup \italic { \small "[sic]" }
        \bar "||"
      }
    }
    \new Staff {
      \clef G
      \time 2/4
      \relative c'' {
        e4
        \scaleDurations 8/12 {
          d32[ e d e d e d e]
          d16.[
          \tweak extra-offset #'(-1 . 0)
          _\markup \italic { \small "[sic - - - - - - - ]" }
          c32]
        }
        c2
        \bar "||"
      }
    }
  >>
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

