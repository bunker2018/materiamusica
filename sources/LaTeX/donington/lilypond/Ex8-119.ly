\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \key d \minor
  \relative c'' {
    \time 1/2
    bes8
    \tweak extra-offset #'(-7 . 2)
    ^\markup \italic {"(a)"}
    a bes c
    \bar "|"
    \time 6/4
    a8.[ bes16 g8. a16]
    fis16[ e d e]
    fis[ g a bes]
    c8.[ d16 bes8. c16]
    \once \omit Staff.BarLine
    \break
    \time 2/4
    a16[ bes c a]
    bes[ g a bes]
    \bar "|"
    \time 4/4
    a16[ d, e fis]
    g[ fis g e]
    \scaleDurations 8/7 {
      fis[ g fis g fis e fis]
    }
    \once \override Staff.BarLine.transparent = ##t
    \undo \omit Staff.TimeSignature
    \once \override Staff.TimeSignature.style = #'single-digit
    \time 3/2
    g1.
  }
  \bar "||"
  } \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit BarNumber }
  }
}

\score { \new Staff {
  \key d \minor
  \clef alto
  \relative c' {
    \time 5/4
    d16
    \tweak extra-offset #'(-7 . -1)
    ^\markup \italic {"(b)"}
    c bes a
    g f e f
    g a bes a
    c a bes c
    d c bes c
    \once \omit Staff.BarLine
    \break
    \time 3/4
    a bes c a
    bes a bes c
    bes8.
    ^\markup \concat {
      \large "["
      \musicglyph "scripts.trill"
      \large "]"
    }
    a16
    \bar "|"
    \time 4/4
    a1
  }
  \bar "||"
  \hideNotes
  \stopStaff
  \override Staff.StaffSymbol.transparent = ##t
  \startStaff
  s2
  } \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit BarNumber }
  }
}

