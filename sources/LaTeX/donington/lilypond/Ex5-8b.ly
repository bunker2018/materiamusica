\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new PianoStaff <<
    \new Staff 
    {
      \time 3/2
      <<
        \relative c'' {
          c1
          \tweak extra-offset #'(-5 . 2)
          ^\markup \small { "(b)" }
          g2
          \tweak extra-offset #'(9 . 0)
          ^\markup \small \italic { "e sim." }
        }
      \\
        \relative c' {
          d1
          \tweak extra-offset #'(-0.8 . 2)
          _\markup \small { "(" }
          e4
          \tweak extra-offset #'(-0.8 . 4.5)
          _\markup \small { "(" }
          _~ e16 f e d
        }
      >>
    }
    \new Staff
    {
      \clef bass \time 3/2
      \relative c' {
        c2 b4.^\prall a8 bes2
          \tweak extra-offset #'(-0.5 . 5.8)
          _\markup \small { "(" }
      }
    }
  >>
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
    \omit Staff.TimeSignature
  }
}

\markup { \vspace #0.2 }
