\version "2.20.0"

#(set-global-staff-size 12)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\markup { \fill-line { \center-column { \line {
\score { <<
  \new Staff \relative c'' {
    \time 3/4
    a4. a8 s4
    s2. \tweak extra-offset #'(-1.5 . -2.5)
    ^\markup {"puede equivaler a:"} s2.
    \tuplet 3/2 { a2 a4 }
  }
  \new Staff \relative c'' {
    a4. a16[ a a] s8.
    s2. \tweak extra-offset #'(-1.5 . -2.5)
    ^\markup {"puede equivaler a:"} s2.
    a4 _~ a16[ a a a]
  }
  \new Staff \relative c'' {
    a4. a32[ a a] s32 s4
    s2. \tweak extra-offset #'(-1.5 . -2.5)
    ^\markup {"puede equivaler a:"} s2.
    a4 _~ a32[ a a a]
  }
  \new Staff \relative c'' {
    a4. a16 s s4
    s2. \tweak extra-offset #'(-1.5 . -2.5)
    ^\markup {"puede equivaler a:"} s2.
    a4.. a16
  }
>>
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Staff \omit Clef }
    \context { \Staff \omit BarLine}
    \context { \Staff \omit StaffSymbol}
    \context { \Score \omit SystemStartBar}
  }
}
}}}}
