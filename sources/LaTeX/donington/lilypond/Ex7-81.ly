\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a4
          \tweak extra-offset #'(-6 . -0.5)
          ^\markup \small { \italic "(a)" }
          \tweak extra-offset #'(-3 . 0)
          ^\markup \small { "[" \italic { "no demasiado lento" } "]" }
          \tweak extra-offset #'(-3 . 0)
          ^\markup \small { \bold "Sarabanda; largo" }
          f d
        bes'2 a4

        g f4. g8
        e2 d4

        a' f d
        e a a,
        d4. cis8[ d e]
        cis2.
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "1." }
        cis
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "2." }
      }
    }
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a4
          \tweak extra-offset #'(-6 . -1)
          ^\markup \small { \italic "(b)" }
          g8.^( f16) e8.^( d16)
        bes'4 a8.^( g16) a4 ^~
        a g8.^( f16) e8.^( g16)
        \grace { f8 ^\trill ^( } e4.. d32 e) d4

        f8.^( a16) g8.^( f16) e8.^( d16)
        e8.^( g16) f8.^( e16) d8.^( cis16)
        d8.^( f16) e8.^( cis16) d8.^( e16)
        cis2.
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "1." }
        s2.
      }
    }
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a8.^(
          \tweak extra-offset #'(-6 . -2.5)
          ^\markup \small { \italic "(c)" }
          bes32 a) g8.^( f16) e8.^( f32 d)
        c'8^(^~ c32 bes a bes)
          a8^(^~ a32 g f e)
          g^( f e
            \set stemLeftBeamCount = #3
            \set stemRightBeamCount = #1
            f
            \set stemLeftBeamCount = #1
            \set stemRightBeamCount = #3
            e d cis d)

        a'8.^( e16) g8.^( f16)
          c'32^( bes a
            \set stemLeftBeamCount = #3
            \set stemRightBeamCount = #1
            bes
            \set stemLeftBeamCount = #1
            \set stemRightBeamCount = #3
            a g f e)
        \tuplet 9/8 {
          f32^( e d e f d e f g)
        }
          \grace f8^\trill ^( e8.. d32) d4

        f16 a g f g f e^( f32 d)
          \tuplet 9/8 {
            bes32 c bes a bes c d e f
          }
        g16 bes a g a e a, e'
          \tuplet 9/8 {
            a32 g bes a g f e f g
          }

        d'16 c bes c bes a g^( a32 bes)
          \tuplet 11/8 {
            a32^( g bes a g f e f g f e)
          }
        s2.
        d8^(
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "2." }
          cis) r4 r
      }
    }
    \new Staff {
      \key f \major
      \clef bass \time 3/4
      \relative c' {
        d4 d,2 g2 f4
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "six" }
        \break

        cis4
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "six" }
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "five" }
          d g,
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "seven" }
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "five" }
        a2
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "accidentals.sharp" }
          d4
        \break

        d'4 bes,2
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "six" }
          c'4
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "seven" }
          c,2
          \tweak extra-offset #'(0 . 0)
          _\markup \teeny { \musicglyph "six" }
        \break

        bes'4 bes,2
        << \relative c' {
          \magnifyMusic 0.63 {
            \override Score.SpacingSpanner.spacing-increment
              = #(* 1.2 0.63)
          r8.
            \tweak extra-offset #'(0 . -5)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto -0.5 0)
                (lineto -0.5 5)
                (lineto 0 5)
              )}
    \once \override TrillSpanner.font-size = #-3
%    \once \override TextSpanner.font-shape = #'upright
    \once \override TextSpanner.staff-padding = #3
    \once \override TextSpanner.style = #'dashed-line
    \once \override TextSpanner.to-barline = ##t
    \once \override TextSpanner.bound-details.left.text =
      \markup \italic { "8ve" }
%    \once \override TextSpanner.bound-details.right.text =
%      \markup \bold { "]" }
    \once \override TextSpanner.bound-details.right.stencil-align-dir-y = #0.2
            bes16 \startTextSpan a8. <g bes>16 <f a>8. <e g>16 \stopTextSpan
            \tweak extra-offset #'(1 . -7.2)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto 0.5 0)
                (lineto 0.5 5)
                (lineto 0 5)
              )}
          d'16\rest
            \tweak extra-offset #'(0 . -4)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto -0.5 0)
                (lineto -0.5 5)
                (lineto 0 5)
              )}
            <f a> <f a> <e g>
            <e g> <d f bes> <d f bes> <c e a>
            <bes d g>
              \tweak extra-offset #'(-0.5 . 2)
              _\markup \concat{
                \huge { "[" }
                \normal-size-super { \musicglyph "rests.2" }
                \huge { "]" }
              }
              <a c f> <a c f> <g bes e>
          }
       } \\ \relative c {
          \revert Score.SpacingSpanner.spacing-increment
          \stemUp a2.
          \tweak extra-offset #'(-1 . -4)
          ^\markup \small { "1." }
          \stemDown a2.
          \tweak extra-offset #'(-1 . -3.5)
          ^\markup \small { "2." }
          \stemNeutral
        } >>
      }
    }
  >>
  \layout {
    \context {
      \Staff
      \override TimeSignature.style = #'single-digit
    }
    \context {
      \Score
      \omit BarNumber
    }
  }
}

