\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \relative c'' {
    \time 4/4
    b8 g a b
    c16
    ^\markup \small {"groppo [ = trino]"}
    b c b c b a b
    c2 r
  }
  \bar "|"
  } \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit BarNumber }
  }
}

