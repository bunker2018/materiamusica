\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new PianoStaff <<
    \new Staff 
    {
      \time 3/2
      <<
        \relative c'' {
          c1
          \tweak extra-offset #'(-7 . 2)
          ^\markup \small { "(a)" }
          \scaleDurations 16/17 {
            g2
            \tweak extra-offset #'(14 . 0)
            ^\markup \small \italic { "e sim." }
            ^( \hideNotes a32) \unHideNotes
          }
        }
      \\
        \relative c' {
          s1 e8 f e d
        }
      >>
    }
    \new Staff
    {
      \clef bass \time 3/2
      \relative c' {
        c2 b4^\prall a bes2
      }
    }
  >>
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
  }
}

\markup { \vspace #0.3 }
