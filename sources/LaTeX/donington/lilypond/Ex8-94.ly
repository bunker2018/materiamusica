\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { <<
  \new Staff {
    \time 2/8
    \relative c'' {
      \partial 8 { c8 }
      \grace { c8 } a'8.[ c,16]
    }
    \bar "||"
  } 
  \new Staff {
    \time 2/8
    \relative c'' {
      \partial 8 { c8 }
      \grace { s16
        \tweak extra-offset #'(-9 . -1)
        ^\markup \tiny { \italic "Ejecución:" }
      }
      c8[ ^( a'16. ) c,32]
    }
    \bar "||"
  } 
>>
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \omit SystemStartBar }
  }
}
