\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \override Staff.Clef.stencil = #(lambda (grob)
        (bracketify-stencil (ly:clef::print grob) Y 0.1 0.2 0.1))
    \time 3/4
    \relative c'' {
      \grace { s16
        \tweak extra-offset #'(0 . 0)
        ^\markup { \small "Ejemplo" }
      }
      << {
        \hideNotes f4 ^( g2 )
      } \\ {
        e4 d2 ^\prall
      } >>
      b4 ^( c2 )
      \bar ""
      \grace { s1 s }
      \clef G
      \scaleDurations 2/3 { s8 e4 }
        \tweak extra-offset #'(0 . 0)
        ^\markup { \small "Expresión [según Rameau]" }
      \scaleDurations 8/7 { d16[ e d e d e d] }
      b4 c16[ b c b c b c8]
      \bar ""
    }
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

