#!/bin/bash
: ${1:?No filename provided, I refuse to proceed any further.}
lilypond \
  -dbackend=eps \
  -dno-gs-load-fonts \
  -dinclude-eps-fonts \
  -dresolution=300 \
  --png \
  $1
