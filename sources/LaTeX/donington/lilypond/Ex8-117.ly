\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 8/4
  \relative c'' { c1 d }
  \bar "||"
  \relative c'' {
    c8 b16 c
    d c d c
    d c d c
    d c b c
    d1
  }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}

