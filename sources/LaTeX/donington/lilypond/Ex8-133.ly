\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%\markup { \vspace #1 }

semiquaver = \markup \concat {
  \lower #2.2 \large {"[ "}
  \tiny { \musicglyph "flags.u4" }
  \lower #2.2 \large {" ]"}
}
          
equalstrill = \markup \concat {
  \large { "[ = " }
  \musicglyph "scripts.trill"
  \large { " ]" }
}

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef soprano s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\score { <<
  \new Staff \with {
    \omit TimeSignature
    \omit Clef
  } {
    \stopStaff
    s2.*4
    \break
    s4
    \startStaff
    \undo \omit Staff.Clef
    \override Score.ClefModifier.font-size = #0
    \clef G
    <<
      \relative c' {
        \grace { s8
          \tweak extra-offset #'(-11 . -7)
          ^\markup \center-column {
            \small {"es decir"}
            \small {"aprox."}
          }
        }
        <c e'>8_([
        <b_~ d'^~>
        \tweak extra-offset #'(1 . 0)
        \startTrillSpan
        <b_~ d'^~>
        \stopTrillSpan
        <b d'>16
        <c c'>)]
        <c c'>2.
      }
    \\
      {
        s8 s
        \tweak extra-offset #'(1 . 0)
        _\startTrillSpan
        s
        \stopTrillSpan
      }
    >>
    \bar "||"
    \stopStaff
  }
  \new Staff {
    \clef alto
    \time 3/4
    \relative c' {
      R2.
      \grace { s8
        \tweak extra-offset #'(-1 . 0)
        ^\markup \small {"(a)"}
      }
      R2.
      c8. g16 c8 d16^( e) d8 e16^( c)
      \grace { s8
        \tweak extra-offset #'(-1.5 . -3.5)
        ^\markup \small {"(b)"}
      }
      b8.^\trill a16 g2

      \time 3/4
      \break

      \grace { s8
        \tweak extra-offset #'(-3 . 1)
        ^\markup \small {"(c)"}
      }
      e'8. c16 b4.^\trill c8
      c2.
      \bar "||"
      \hideNotes \stopStaff \once \override Staff.StaffSymbol.transparent = ##t \startStaff s2 \omit Staff.BarLine s4. s s \undo \omit Staff.BarLine
    }
  }
  \new PianoStaff <<
    \new Staff {
      \set Staff.instrumentName = \incipit
      \clef G
      \time 3/4
      \relative c'' {
        c8. g16 c8 d16 e d8 e16 c
        \grace { c8
          ^\equalstrill
        } b8. a16 g2
        e'8. d16 e8 f16 g f8 g16 e
        \grace { e8
          \tweak extra-offset #'(1 . 0)
          ^\equalstrill
        } d8. c16 b2
        \break
        c8. a'16 \grace { e8 } d4. ^\trill c8
        c2.
        \bar "||"
      }
    }
    \new Staff {
      \clef bass
      \time 3/4
      \relative c {
        c4 e f
        g4. f8[
          \tweak extra-offset #'(-1 . 0)
          ^\semiquaver
          e8. d16]
        c2 f4
        g4. f8[
          \tweak extra-offset #'(-1 . 0)
          ^\semiquaver
          e8. d16]
        \break
        e8. f16 g4 g,
        c2.
        \bar "||"
      }
      \hideNotes \stopStaff \once \override Staff.StaffSymbol.transparent = ##t \startStaff s2 \omit Staff.BarLine s4. s s \undo \omit Staff.BarLine
    }
  >>
>>
  \layout {
    \override Staff.InstrumentName.X-offset = #-5.775
    \override Staff.InstrumentName.Y-offset = #-18.85
    \omit Score.BarNumber
    \override Staff.TimeSignature.break-visibility = ##(#f #f #t)
  }
}

