\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new ChoirStaff <<
  \new Staff {
    \relative c'' {
      \time 1/4
      c4
      ^\markup { \musicglyph "scripts.prall" }
      \bar "|"
      \time 3/8
      c4.
      ^\markup { \musicglyph "scripts.lineprall" }
      \bar "||"
    }
  }
  \new Staff {
    \relative c'' {
      \time 1/4
      d32
        ^\markup \tiny { "simple" }
        ^\markup \tiny { "Tremblement" }
      c d
          \set stemLeftBeamCount = #3
          \set stemRightBeamCount = #1
      c
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #3
      d c d c
      \bar "|"
      \time 3/8
      d8
        ^\markup \tiny { "appoyé" }
        ^\markup \tiny { "Tremblement" }
      ^~ d32[ c d
          \set stemLeftBeamCount = #3
          \set stemRightBeamCount = #1
      c
          \set stemLeftBeamCount = #1
          \set stemRightBeamCount = #3
      d c d c]
      \bar "||"
    }
  } >>
  \layout {
    \context { \Staff
      \omit TimeSignature
    }
    \context { \Score
      \omit SystemStartBar
      \omit SystemStartBracket
    }
  }
}
