\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 2/8
  \relative c'' {
    g4
    \tweak extra-offset #'(0 . 0)
    ^\markup { \path #0.1 #'(
      (moveto 0 0)
      (lineto 2 -1)
      (moveto 0.2 0.4)
      (lineto 2.2 -0.6)
    )}
    a32[ g a g] a[ g a g]
    \bar "||"
    g4
    \tweak extra-offset #'(-1 . 0)
    ^\markup { \path #0.1 #'(
      (moveto 0 0)
      (lineto 2 -1)
      (moveto 2.5 -0.75)
      (lineto 4.5 0.25)
      (moveto 2.7 -1.05)
      (lineto 4.7 -0.05)
    )}
    a8_( a32[ g a g])
  }
  \bar "|"
  } \layout { \context { \Staff \omit TimeSignature } }
}
