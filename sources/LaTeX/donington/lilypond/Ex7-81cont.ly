\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a4 c,4. c8
        c2 b4
        g'4 bes,!4. bes8

        \stemUp bes2 \stemNeutral a4
        f'4 ees g
        cis,4. cis8 d4 ^~

        d8 e e4. d8
        d2.
          \tweak extra-offset #'(-3 . 1)
          ^\markup \small { "1." }
          d2.
          \tweak extra-offset #'(-3 . 1)
          ^\markup \small { "2." }
      }
    }
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a8.^( fis16) c8.^( ees16) d8.^( a16)
        c8._( d,16) g,8._( c'16) \stemUp b4 \stemNeutral
        g'8.^( e16) bes8.^( d16) c8._( g16)
        bes8._( f16) c8._( bes'16) a4
        f'8.^( d16) ees8.^( d16) e8._( g16)
        cis,8._( a16) e8._( e'16) d8.^( a16)
        c!8.^( bes16) a8._( e16) cis8._( e16)
        d4 r r s2.
      }
    }
    \new Staff {
      \key f \major
      \time 3/4
      \relative c''' {
        a8.^( fis16) c8.^( d32 ees) d8.^( a16)
        c8._( d,16) g,8_( _~ g32 c' b a) g4
        g'8.^( e16) bes8.^( c32 d) c8._( g16)

        bes8._( f16) c8_( _~ c32 bes' a g) a4
        f'16 a g f ees[^( f32 g]) f16[^( g32 aes])
          \tuplet 9/8 {
            g32 aes g f g f ees f g
          }
        cis,16 g e f' e cis g bes' a d, a d'
        c!16 g d bes' a e d a' g e cis g'
        s2.
        fis16 a, e' g, <fis d'>2
      }
    }
    \new Staff {
      \key f \major
      \clef bass \time 3/4
      \relative c {
        << \relative c' {
          \magnifyMusic 0.63 {
            \override Score.SpacingSpanner.spacing-increment
              = #(* 1.2 0.63)
            <a d>4
            \tweak extra-offset #'(2 . -4)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto 0.5 0)
                (lineto 0.5 4)
                (lineto 0 4)
              )}
          }
        } \\ \relative c {
          fis4
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
        } >>
          d
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "seven" }
          fis
        g
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "five" }
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "four" }
        << \relative c' {
          \magnifyMusic 0.63 {
            \override Score.SpacingSpanner.spacing-increment
              = #(* 1.2 0.63)
            r8.
            \tweak extra-offset #'(-1 . -2.5)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto -0.5 0)
                (lineto -0.5 4)
                (lineto 0 4)
              )}
            a16 g8. f16
            \tweak extra-offset #'(2 . -3)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto 0.5 0)
                (lineto 0.5 4)
                (lineto 0 4)
              )}
          }
        } \\ \relative c {
          s4 s
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "accidentals.natural" }
        } \\ \relative c {
          \stemDown g2 \stemNeutral
        } >>
        e4
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
          c
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "seven" }
          e
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "five" }
        \break

        f
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "four" }
          << { f,2 } \\ { s4 s
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "three" }
          } >>
        f'4
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
          g2
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \concat {
              \musicglyph "six"
              \musicglyph "accidentals.flat"
            } }
        a4.
            \tweak extra-offset #'(-1.5 . -0.4)
            ^\markup { \concat {
              \huge { "[ " }
              \tiny { \note-by-number #2 #2 #1 }
            } }
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "accidentals.sharp" }
          g8 f4
            \tweak extra-offset #'(-2.8 . 0.2)
            ^\markup { \concat {
              \tiny { \note-by-number #4 #0 #1 }
              \huge { " ]" }
            } }
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
        \break

        g4
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "six" }
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "five" }
          a
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "four" }
          a,
            \tweak extra-offset #'(0 . 0)
            _\markup \teeny { \musicglyph "accidentals.sharp" }
        << \relative c'' {
          \magnifyMusic 0.63 {
            \override Score.SpacingSpanner.spacing-increment
              = #(* 1.2 0.63)
            r8
            \tweak extra-offset #'(-1 . -1.5)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto -0.5 0)
                (lineto -0.5 6)
                (lineto 0 6)
              )}
            r16 <g bes>16 <f a>8. <bes, e g>16
              <a d f>8. <g bes cis e>16
            \tweak extra-offset #'(2 . -5.1)
            ^\markup { \path #0.2 #'(
                (moveto 0 0)
                (lineto 0.5 0)
                (lineto 0.5 6)
                (lineto 0 6)
              )}
          }
        } \\ \relative c {
          d2.
          \tweak extra-offset #'(-2 . -1)
          ^\markup \small { "1." }
        } >>
        d2.
          \tweak extra-offset #'(0 . 0)
          ^\markup \small { "2." }
              \tweak extra-offset #'(-1 . 0)
              _\markup \concat{
                \huge { "[" }
                \super { \musicglyph "accidentals.sharp" }
                \huge { "]" }
              }
        \bar "||"
      }
    }
  >>
  \layout {
    \context {
      \Staff
      \omit TimeSignature
    }
    \context {
      \Score
      \omit BarNumber
    }
  }
}

\markup { \vspace #0.2 }

