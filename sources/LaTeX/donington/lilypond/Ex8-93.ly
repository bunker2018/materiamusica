\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \time 4/4
    \grace { s16
      \tweak extra-offset #'(-6 . 0)
      ^\markup \small {"Fig. II"}
    }
    \once \omit Staff.TimeSignature \time 3/4
    << \relative c''' {
      \partial 8 { g8 }
      \grace { f4 } e4. d8 c[ g]
    } \\ \relative c' {
      \partial 8 { r8 }
      r8 c g' f e4
    } >>
    \bar "||"

    \time 2/4
    << \relative c''' {
      g8. a16 \grace { g8 } f4
    } \\ \relative c' {
      c8[ c b g]
    } >>
    \bar "||"

    \time 3/4
    << \relative c''' {
      g4 a c, \grace { c } b2 c4
    } \\ \relative c' {
      c4 f fis g fis e
    } >>
    \bar "|"
    \break

    \override
      Staff.TimeSignature.break-visibility = ##(#f #t #t)
    \time 4/4
    \grace { s16
      \tweak extra-offset #'(-6 . 2)
      ^\markup \small {"IV"}
    }
    << \relative c'' {
      \partial 8 { c8 }
      \grace { s16
        \tweak extra-offset #'(-6.4 . 0)
        ^\markup \small {"(a)"}
      }
      b4 \grace { b8 } a4 ^\trill g2
    } \\ \relative c' {
      \partial 8 { c8 }
      d4 d g,2
    } >>
    \bar "||"

    \time 3/8
    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-3.2 . -2.3)
        ^\markup \small {"(b)"}
      }
      e8.*2/4 [ f32*2/4 g32*2/4 ]
        f8 e \grace { e4 } d4.
    } \\ \relative c' {
      c8 b c g g' f
    } >>
    \bar "||"

    \time 3/4
    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-3.6 . 0.5)
        ^\markup \small {"(c)"}
      }
      g4 c c \grace { c2 } b2.
    } \\ \relative c' {
      c4 c8[ d e f] g4 g,2
    } >>
    \bar "||"

    \time 4/4
    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-3.6 . -1.6)
        ^\markup \small {"(d)"}
      }
      e4. fis8 \grace { fis4 } g2 ^\fermata
    } \\ \relative c'' {
      c8 b a4 g2 _\fermata
    } >>
    \bar "|"
    \break

    \time 4/4
    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-2 . 0)
        ^\markup \small {"(e)"}
      }
      a8. d16 \grace { c8 } b4 ^\trill
        \grace {
          b4
        } c2
    } \\ \relative c' {
      f8[ d] g
      \once \override NoteColumn #'force-hshift = #-0.8
      g,
      \once \override NoteColumn #'force-hshift = #-1.2
      c2
    } >>
    \bar "||"

    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-0.5 . 1.2)
        ^\markup \small {"(f)"}
      }
      e8. a16
        g16*2/3 [ f16*2/3 e16*2/3 ]
        f16*2/3 [ e16*2/3 d16*2/3 ]
        \grace { d4 } c2
    } \\ \relative c' {
      c8[ f,] g g
      \once \override NoteColumn #'force-hshift = #-1.2
      c2
    } >>
    \bar "||"

    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-1 . 0)
        ^\markup \small {"(g)"}
      }
      c4. d8 d2 ^\trill \grace { d4 } c1
    } \\ \relative c'' {
      a4 f g g, c1
    } >>
    \bar "||"

    << \relative c'' {
      \grace { s16
        \tweak extra-offset #'(-1 . 0.8)
        ^\markup \small {"(h)"}
      }
      g4. c8 \grace { c4 } b4.
        \grace { s16
          \tweak extra-offset #'(1 . 0)
          ^\markup \tiny {"etc."}
        }
        c8
    } \\ \relative c' {
      r8 c e c r g' g, a
    } >>
    \bar "|"
  }
  \layout { \context { \Score \omit BarNumber } }
}
