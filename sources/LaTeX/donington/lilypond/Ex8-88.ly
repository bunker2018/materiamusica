\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \time 3/4
    \partial 8 \relative c'' { g8 }
    \grace { s16
      \tweak extra-offset #'(-7 . 1.5)
      ^\markup \small { "Escritura:" }
    }
    \relative c'' {
      c4 g r8 g d'4 g,
      \bar "||"
      g8
      \grace { s16
        \tweak extra-offset #'(-2 . 0.5)
        ^\markup \small { "ejecución aproximada:" }
      }
       s8 g8 _( c ) g4 r8 g \stemUp g _( d' ) g,4
      \bar "||"
    }
  }
}

\score {
  \new Staff {
    \clef "bass"
    \time 2/2
    \partial 4 \relative c' { a4 }
    \grace { s16
      \tweak extra-offset #'(-7 . 1.5)
      ^\markup \small { "Escritura:" }
    }
    \relative c {
      e2 r4 a c,2 r4 e a,1
      \bar "||"
      \omit Staff.TimeSignature
      \time 1/4
      a'4
      \time 2/2
      \grace { s16
        \tweak extra-offset #'(-4 . 0.5)
        ^\markup \small { "ejecución:" }
      }
      a4 ^( e ) r a a ^( c, ) r e e ^( a,2. )
      \bar "||"
    }
  }
}
