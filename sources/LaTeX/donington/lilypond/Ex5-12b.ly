\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \key bes \major \time 3/4
    \new Voice = melody { \relative c'' {
    \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
      g'8.
      \tweak extra-offset #'(-7.5 . 0.5)
      ^\markup \small { "(b)" }
      \tweak extra-offset #'(-1 . 0)
      _\markup \small {
        "[ isles___________"
      }
      [ f16 g8. aes16] g4
      \tweak extra-offset #'(0 . 0)
      _\markup \small {
        "ex     -     celling ]"
      }
    \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/4)
      s8
    } }
  }
  \layout {
    \omit Staff.TimeSignature
  }
}

