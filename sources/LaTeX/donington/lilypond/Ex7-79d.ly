\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key a \major \time 4/4
      \relative c'' {
        e4
          \tweak extra-offset #'(-6 . 0.5)
          ^\markup \small { \italic { "(a)" } "Versión original" }
          a b, gis'
        a, fis' gis, b'
        cis, a' b, e
        fis, dis' e,2
      }
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key a \major \time 4/4
      \relative c'' {
        e4
          \tweak extra-offset #'(-6 . 0.2)
          ^\markup \small { \italic { "(b)" } "Interpretación de Corelli" }
          \grace { e8^( } a4)
          b,4 \grace { b8^( } gis'4)
        a,4 \grace { a8^( } fis'4)
          gis,4 \grace { gis8^( } b'4)
        cis,4 \grace { cis8^( } a'4)
          b,4 \grace { b8^( } e4)
        fis,4 \grace { fis8_( } dis'4)
          e,2
      }
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key a \major \time 4/4
      \relative c'' {
        e4
          \tweak extra-offset #'(-6 . 0.2)
          ^\markup \small { \italic { "(c)" } "Interpretación de Dubourg" }
          a4
          \scaleDurations 2/3 { b,8[ cis dis] }
          \scaleDurations 2/3 { e8[ fis gis] }
        a,4 fis' gis, b'
        \scaleDurations 2/3 { cis,8[ dis e] }
          \scaleDurations 2/3 { fis8[ gis a] }
          b,4 e
        \scaleDurations 2/3 { fis,8[ gis a] }
          \scaleDurations 2/3 { b8[ cis dis] }
          e,2
      }
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key a \major \time 4/4
      \relative c'' {
        \tuplet 3/2 { e8
          \tweak extra-offset #'(-6 . 0.2)
          ^\markup \small { \italic { "(d)" } "Interpretación de Geminiani" }
          fis e }
          a8 cis, b a' gis4
        \tuplet 3/2 { a,8 b a } fis'8 cis gis4 b'8 b,
        cis4 a'8 cis,
          \mergeDifferentlyHeadedOn
          << {
            b4 e a, dis e
          } \\ {
            s4 gis, fis fis e'
          } >>
          e,
      }
    }
  >>
}

