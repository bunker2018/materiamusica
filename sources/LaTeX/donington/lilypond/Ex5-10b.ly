\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \override Stem.neutral-direction = #up
    \key f \major \time 2/2
    \relative c' {
      f4
      \tweak extra-offset #'(-7 . 1.5)
      ^\markup \small { "(b)" }
      g a bes c2
    }
  }
  \layout {
    \omit Staff.TimeSignature
%    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32) }
  }
}

