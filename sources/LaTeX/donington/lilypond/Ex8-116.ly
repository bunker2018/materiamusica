\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \time 9/8
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"(a)"}
  }
  \relative c' {
    a8
    \grace {
      b8 a b c d c d e f e f g
      a g a g a g a g a g f g
    }
    a1
  }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}

\score { \new Staff {
  \time 3/2
  \grace { s16
    \tweak extra-offset #'(-2 . 1)
    ^\markup \small {"(b)"}
  }
  \relative c' { d2 e1 }
  \bar "||"
  \relative c' { e16[ d e d e d c e] e1 }
  \bar "||"
  } \layout { \context { \Staff \omit TimeSignature } }
}

