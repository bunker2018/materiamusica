\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef varbaritone s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\score {
  \new PianoStaff <<
    \new Staff {
      \time 2/2
      <<
        \relative c' {
          fis2 \stemDown d'8.[ c16] \stemNeutral b8. a16

        }
      \\
        \relative c' {
          d2 s2
        }
      >>
      \relative c'' {
        gis2^\prall e'8.[ d16] e8. b16
        \stemUp
        c8.[ gis16] a8.[^\mordent b16] b4.^\prall a8
        <cis, e a>2
      }
    }
    \new Staff {
      \set Staff.instrumentName = \incipit
      \clef bass \time 2/2
      <<
        \relative c' {
          a2 g8. a16 b4
          s2 a4 gis
          \mergeDifferentlyHeadedOn
          a4 f
            \mergeDifferentlyHeadedOff
            e2
          a,2
        }
      \\
        \relative c {
          d8[ e d c] b2
          e8. f16 e8. d16 c4 b
          a8[ e'] f d e4 e,
          r4 a,
        }
      >>
    }
  >>
  \layout {
    \override Staff.InstrumentName.X-offset = #-5.725
    \override Staff.InstrumentName.Y-offset = #-14.175
  }
}

