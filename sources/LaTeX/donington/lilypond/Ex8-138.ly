\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

#(define-markup-command (long-curly-bracket layout props arg-height)
   (number?)
  "Draw a curly bracket with a variable length."
     (interpret-markup layout props
       (markup
        #:translate (cons 0 (* arg-height -1))
        (#:with-dimensions (cons -0.001 0) (cons 0.001 0)
         (#:override (cons (quote filled) #t)
          (#:path 0.01 `(
           (moveto   -0.8   0.0)
           (curveto  -0.2   0.4  -0.3  0.7 -0.3  1.5)
           (lineto   -0.3   ,arg-height)
           (curveto  -0.4   ,(+ arg-height 1.3) 0.5 ,(+ arg-height 2.8) 0.7 ,(+ arg-height 2.8))
           (curveto   0.6   ,(+ arg-height 2.5) 0.1 ,(+ arg-height 2)   0.1 ,arg-height)
           (lineto    0.1   1.5)
           (curveto   0.1   0.7   0.1   0.0  -0.8   0.0)
           (closepath)
           (curveto  -0.2  -0.4  -0.3  -0.7  -0.3  -1.5)
           (lineto   -0.3   ,(* arg-height -1))
           (curveto  -0.4   ,(* (+ arg-height 1.3) -1) 0.5 ,(* (+ arg-height 2.8) -1) 0.7 ,(* (+ arg-height 2.8) -1))
           (curveto   0.6   ,(* (+ arg-height 2.5) -1) 0.1 ,(* (+ arg-height 2) -1)   0.1 ,(* arg-height -1))
           (lineto    0.1  -1.5)
           (curveto   0.1  -0.7   0.1   0.0  -0.8   0.0)
           (closepath))))))))

\markup { \vspace #1 }

\score { \new Staff {
  \relative c'' {
    \time 2/4
    \stemUp
    <g b d>2
    \tweak extra-offset #'(-4.5 . 0)
    ^\markup \small { "(a)" }
    \time 4/4
    \stemNeutral
    <c, c'>1
    \bar "||"

    \time 2/4
    << {
      <g' d'>2
      \tweak extra-offset #'(-3 . -1)
      ^\markup \small { "(b)" }
    } \\ {
      \once \override NoteColumn.force-hshift = #1.3
      b4. c8
    } >>
    \time 4/4
    <c, c'>1
    \bar "||"

    \time 2/4
    << {
      \once \override NoteColumn.force-hshift = #0.5
      c'8
      \tweak extra-offset #'(-4 . -2)
      ^\markup \small { "(c)" }
      [^~^( c32
      b
      \tweak extra-offset #'(6.2 . 0.7)
      ^\markup {
        \rotate #-90 \long-curly-bracket #3
      }
      c b] c[ b c b]^~
      b16[) b32\rest c]
    } \\ {
      <g d'>2
    } >>
    \time 4/4
    <c, c'>1
    \bar "||"
    \break

    \time 2/4
    << {
      \once \override NoteColumn.force-hshift = #0.5
      c'8
      \tweak extra-offset #'(-5.8 . -1)
      ^\markup \small { "(d)" }
      [^~^( c32 b c b] c[ b c
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      b^~
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #2
      b16.  c32)]
    } \\ {
      <g d'>2
    } >>
    \time 4/4
    <c, c'>1
    \bar "||"
    \hideNotes \stopStaff \once \override Staff.StaffSymbol.transparent = ##t \startStaff s2 \omit Staff.BarLine s4. s s \undo \omit Staff.BarLine
  }
  } \layout {
    \context { \Staff \omit TimeSignature }
    \omit Score.BarNumber
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8) }
  }
}
