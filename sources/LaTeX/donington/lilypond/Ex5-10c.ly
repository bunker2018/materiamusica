\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \override Stem.neutral-direction = #up
    \time 3/2
    \relative c' {
      f2
      \tweak extra-offset #'(-7.5 . 1.5)
      ^\markup \small { "(c)" }
      g4 a2 b4
      \tweak extra-offset #'(0 . 0)
      ^\markup \concat {
        \large "["
        \raise #0.3
        \tiny { \musicglyph "accidentals.flat" }
        \large "]"
      }
      c2.
    }
  }
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
%    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32) }
  }
}

