\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \key b \minor
  \time 12/8
  \relative c' {
    \partial 8 {
      fis8
      \tweak extra-offset #'(0 . 1)
      ^\markup \small {"escritura"}
    }
    \grace { s2 }
    d'8.
      \tweak extra-offset #'(-1 . -4.1)
      ^\markup \teeny { \musicglyph "scripts.prall" }
      cis16 b8 \grace {cis8} \stemDown b8. ais16 b8 \stemNeutral b4. ^~ b4 \bar "||"
      fis8
      \tweak extra-offset #'(0 . 0)
      ^\markup \small {"ejecución aprox."}
    \grace { b16 cis } d8. cis16 b8
      cis16^>[ b64 cis b cis b16 r32 ais b8] b4. ^~ b4 \bar "||"
  }
  } \layout {
    %\context { \Staff \omit TimeSignature }
  }
}
