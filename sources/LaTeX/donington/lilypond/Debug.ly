\version "2.20.0"

\score {
  <<
    \new Staff
    {
      \key d \major \time 2/2
      \relative c'' {
        \autoBeamOff cis8.
          \tweak extra-offset #'(-15 . 1)
          ^\markup \small {
            "folio" \concat { "110" \super "v" }
          }
        b16 a4 s2
        \tweak extra-offset #'(8 . 0)
        ^\markup \small \italic { "e sim." }
      }
    }
    \new Staff
    {
      \clef bass \key d \major \time 2/2
      <<
        \relative c {
          \stemUp a4 a'8 e fis8.[ e16 fis8 gis]
        }
      \\
        \relative c {
          \stemDown s4 a d8.[ cis16 b8 e]
        }
      >>
    }
  >>
  \layout {
    \override Staff.InstrumentName.Y-offset = #-4.18
  }
}

