\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \key bes \major \time 3/4
    \set Staff.timeSignatureFraction = 31/4
    \new Voice = melody { \relative c'' {
    \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
      g'8
      \tweak extra-offset #'(-11 . -0.5)
      ^\markup \small { "(a)" }
      \tweak extra-offset #'(-5 . 0)
      _\markup \small {
        "[all]  isles___________"
      }
      ^([ f g a
      \tweak extra-offset #'(0 . 0)
      ^\markup \concat {
        \large {"["}
        \teeny { \raise #0.5 \musicglyph "accidentals.flat" }
        \large {"]"}
      }
      )] g4
      \tweak extra-offset #'(0 . 1.8)
      _\markup \small {
        "ex     -     [celling]"
      }
    \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/4)
      s8
    } }
  }
  \layout {
    \override Staff.TimeSignature.style = #'single-digit
  }
}

