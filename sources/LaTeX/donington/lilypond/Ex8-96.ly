\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \time 2/2
    \relative c'' {
      \partial 4 { g4 }
      c2 \grace { b8 } c2
      \bar "||"
      c8
      \grace { b16 } c4
      \grace { b16 } c4
      c8
      \bar "||"
    }
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 4/69) }
  }
}

\score {
  \new Staff {
    \time 2/4
    <<
      \relative c'' {
        \partial 8 { g8 }
        g8
        \grace { bes16 } a4
        \grace { c16 } bes8
        \bar "||"
      }
    \\
      \relative c' {
        \partial 8 { e8 }
        f4 d
        \bar "||"
      }
    >>
    \time 3/4
    \relative c'' {
      \partial 8 { c8 }
      \grace { d16 } \stemDown c8[ b
      \grace { c16 } b8 a
      \grace { b16 } a8 g]
      \bar "||"
    }
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8) }
  }
}
