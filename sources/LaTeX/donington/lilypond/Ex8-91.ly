\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\markup {

\score {
  \new PianoStaff <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \time 3/8
      \relative c' {
        \grace { s16
          \tweak extra-offset #'(-5 . 1)
          ^\markup \small {"(a)"}
        }
        f4 ^\prall e8 d \grace { c_( } b)
        \grace { s16
          \tweak extra-offset #'(3 . 0)
          ^\markup \small {"etc."}
        }
        g'8
      }
      \bar "||"
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass
      \mergeDifferentlyHeadedOn
      \mergeDifferentlyDottedOn
      \relative c' {
        <<
          { \grace { \hideNotes c8 ^~ } \unHideNotes c8 b c g4. }
        \\
          { \grace { s16 s } d4 c8 g' f e }
        >>
      }
      \bar "||"
    }
  >>
}
"       "
\score {
  \new PianoStaff <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \time 3/8
      \relative c'' {
        \grace { s16
          \tweak extra-offset #'(-5 . 1)
          ^\markup \small {"(b)"}
        }
        g32 f g f _~ f8 e e32 d e
        \set stemLeftBeamCount = #3
        \set stemRightBeamCount = #1
        d
        \set stemLeftBeamCount = #1
        \set stemRightBeamCount = #2
        c16 _( b )
        \grace { s16
          \tweak extra-offset #'(5 . 0)
          ^\markup \small {"etc."}
        }
        g'8
      }
      \bar "||"
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass
      \mergeDifferentlyHeadedOn
      \mergeDifferentlyDottedOn
      \relative c' {
        <<
          { \grace { \hideNotes c8 ^~ } \unHideNotes c8 b c g4. }
        \\
          { \grace { s16 s } d4 c8 g' f e }
        >>
      }
      \bar "||"
    }
  >>
}
"   "
}
