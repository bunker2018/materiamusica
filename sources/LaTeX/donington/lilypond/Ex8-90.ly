\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new StaffGroup <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key b \major \time 4/4
      \set doubleSlurs = ##t
      \override Stem.neutral-direction = #up
      \relative c'' {
        r8 <gis b> \grace { <gis b>8( } <ais cis>4)
        r8 <gis b> \grace { <gis b>8( } <fisis ais>4)
      }
      \bar "||"
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass \key b \major \time 4/4
      \relative c' {
        gis16 dis' gis, dis'
        fisis, dis' fisis, dis'
        gis, dis' gis, dis'
        dis, dis' dis, dis'
      }
      \bar "||"
    }
  >>
  \layout { \context {
    \StaffGroup \override SystemStartBracket.stencil = ##f
  } }
}

\score {
  \new StaffGroup <<
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \key b \major \time 4/4
      \override Stem.neutral-direction = #up
      \grace { s16
        \tweak extra-offset #'(3 . 1)
        ^\markup \small { "Ejecución:" }
      }
      \relative c'' {
        r8 <gis b> <gis_( b>8[ <ais) cis>8]
        r8 <gis b> <gis_( b>8[ <fisis) ais>8]
      }
      \bar "||"
    }
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \clef bass \key b \major \time 4/4
      \relative c' {
        gis16 dis' gis, dis'
        fisis, dis' fisis, dis'
        gis, dis' gis, dis'
        dis, dis' dis, dis'
      }
      \bar "||"
    }
  >>
  \layout {
    \context {
      \StaffGroup \override SystemStartBracket.stencil = ##f
    }
    \context {
      \Score
      \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 4/77)
    }
  }
}

