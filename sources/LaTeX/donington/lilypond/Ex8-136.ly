\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

incipit = \markup { \score {
    \new Staff \with { \remove "Time_signature_engraver" }
    {
      \override Score.SpacingSpanner.base-shortest-duration =
        #(ly:make-moment 1/2)
      \clef soprano s16 \bar "|"
    }
    \layout { ragged-right = ##t }
  }
  \hspace #-.6
}

\markup { \vspace #1 }

\score { \new Staff {
  \set Staff.instrumentName = \incipit
  \clef G
  \time 4/4
  \relative c'' {
    e4.
      \tweak extra-offset #'(-1 . 1)
      ^\markup \small { "(a)" }
      d8
      \tweak extra-offset #'(4 . -1.4)
      ^\markup \tiny { \musicglyph "scripts.stopped" }
      d4. c8
    c2*4/2
    \bar "||"
    e4.
      \tweak extra-offset #'(-1 . 1)
      ^\markup \small { "(b)" }
      d8
    \scaleDurations 8/7 {
      \autoBeamOff
      e8
      \tweak extra-offset #'(4 . -1.8)
      ^\markup \tiny { \musicglyph "scripts.stopped" }
      \tweak extra-offset #'(0 . 0)
      _\markup \tiny { \italic "[sic]" }
      ^( d8.) c8
      \autoBeamOn
    }
    \bar "|"
    \noBreak
    c2*4/2
    \bar "||"
    e4.
      \tweak extra-offset #'(-1 . 1)
      ^\markup \small { "(c)" }
      d8 e4^( d32[ e d
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      e
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #2
      d16 c)]
    \bar "||"
    e4.
      \tweak extra-offset #'(-1 . 1)
      ^\markup \small { "o posiblemente:" }
      d8 e8.^([ d32 e] d[ e d
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      e
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #2
      d16 c)]
    \bar "||"
  }
  } \layout {
    %\context { \Staff \omit TimeSignature }
    \override Staff.InstrumentName.X-offset = #-5.775
    \override Staff.InstrumentName.Y-offset = #-4.285
    \omit Score.BarNumber
  }
}
