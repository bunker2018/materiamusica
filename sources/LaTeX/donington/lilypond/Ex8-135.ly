\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { \new Staff {
  \key bes \major
  \time 4/4
  \relative c'' {
    c2^\trill bes
    \bar "||"
    \scaleDurations 16/15 { d32[^( c d c d c d c d c d c16 c32 bes]) }
    bes2
    \bar "||"
  }
  } \layout {
    %\context { \Staff \omit TimeSignature }
  }
}
