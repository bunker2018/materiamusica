\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score { <<
  \new Staff {
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      _\markup \tiny { "Port de voix" }
      \tweak extra-offset #'(9.6 . -1.9)
      ^\markup { \rotate #12 \musicglyph "scripts.rcomma" }
    }
    \relative c'' {
      b8 s8 s4 c4 s
      \bar "||"
    }
    \grace { s16
      \tweak extra-offset #'(0 . -1)
      _\markup \tiny { "Cheute" }
      \tweak extra-offset #'(9.6 . -2.3)
      ^\markup { \rotate #12 \musicglyph "scripts.rcomma" }
    }
    \relative c'' {
      d8 s8 s4 b4 s
      \bar "||"
    }
    \time 2/4
    \grace { s16
      \tweak extra-offset #'(2.2 . -3.6)
      ^\markup { \rotate #12 \musicglyph "scripts.lcomma" }
    }
    \relative c'' {
      s8 << { c4 } \\ { g } >> s8
      \bar "||"
    }
    \grace { s16
      \tweak extra-offset #'(2.2 . -3.6)
      ^\markup { \rotate #12 \musicglyph "scripts.lcomma" }
    }
    \relative c'' {
      s8 << { b4 } \\ { g } >> s8
      \bar "||"
    }
  }

  \new Staff {
    \time 2/2
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { "Forefall up" }
    }
    \relative c'' {
      b8 s8 s4 b8[ ^( s c] ) s
    }
    \grace { s16
      \tweak extra-offset #'(0 . 0)
      ^\markup \tiny { "Backfall" }
    }
    \relative c'' {
      d8 s8 s4 c8[ ^( s b] ) s
    }
    \relative c'' {
      s8 << { b8[ ^( c] ) } \\ { g4 } >> s8
    }
    \relative c'' {
      s8 << { c8[ ^( b] ) } \\ { g4 } >> s8
    }
  }
>>
\layout { \context { \Staff \omit TimeSignature } }
}
