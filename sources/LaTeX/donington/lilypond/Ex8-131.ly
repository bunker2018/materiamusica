\version "2.20.0"

#(set-global-staff-size 15)

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \time 4/4
    << \relative c'' {
      b4 \grace { b8 ^\trill } a4 g2
      \bar "||"
      b8
      \tweak extra-offset #'(0 . 0)
      ^\markup \small { "[interpretación (a)]" }
      r8 b16^>^( a32
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      b
      \once \override TupletBracket.bracket-visibility = ##f
      \tuplet 5/4 {
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
        a b a b a
      } g2)
      \bar "||"
    } \\ \relative c' {
      d4 d g,2
      d'8 r d4 g,2
    } >>
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

\score {
  \new Staff {
    \time 4/4
    << \relative c'' {
      b4
      \tweak extra-offset #'(0 . 0)
      ^\markup \small { "[interpretación (b)]" }
      ^>^~
      b16^( a32
      \set stemLeftBeamCount = #3
      \set stemRightBeamCount = #1
      b
      \once \override TupletBracket.bracket-visibility = ##f
      \tuplet 5/4 {
      \set stemLeftBeamCount = #1
      \set stemRightBeamCount = #3
        a b a b a
      } g2)
      \bar "||"
    } \\ \relative c' {
      d4 d g,2
    } >>
  }
  \layout {
    \context { \Staff \omit TimeSignature }
    \context { \Score
      \override SpacingSpanner.base-shortest-duration
        = #(ly:make-moment 1/32)
    }
  }
}

