
\noindent
El término proviene del italiano \textit{appoggiare},
``apoyar'',
e implica una nota auxiliar,
usualmente disonante,
siempre acentuada,
sobre el pulso,
y ligada a la nota principal en la que resuelve.
La nota auxiliar puede tomarse en sentido ascendente o descendente: %%%
por grado conjunto,
ya sea por tono o semitono
de acuerdo al contexto tonal
(salvo que se la altere cromáticamente con fines expresivos);
o por salto
(en la práctica común, únicamente en recitativo, por salto de cuarta descendente).
La duración varía entre un término medio en el barroco temprano,
y la posibilidad de optar entre decididamente larga o decididamente breve en el barroco tardío.
Cuanto más breve la apoyatura,
más brillante la expresión y menos significativa su incidencia en la armonía;
cuanto más larga,
más expresivo el efecto y más influyente sobre la armonía.
Un breve silencio de articulación antes de la apoyatura
puede realzar su brillantez o su efecto expresivo, según el caso.

En las apoyaturas consonantes con el contexto armónico
en que haya que optar entre una apoyatura larga y una breve,
esta elección queda a criterio del intérprete,
ya que la notación, si la hay,
muy raramente hace referencia a la duración en forma explícita.
En el barroco tardío,
con la posible excepción del barroco Francés,
se presupone en general una preferencia por la apoyatura larga,
con la consiguiente intensificación de la progresión armónica que le es propia;
no obstante,
si el efecto tiende a oscurecer o debilitar la armonía, %%%CHECK
puede ser deseable una apoyatura breve.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-82.png}
\caption{Ejemplo 82. Playford, \textit{Introducción}, Londres, 1654 (eds. 1660 en adelante), apoyaturas.\label{ex8-82}}
\end{center}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-83.png}
\caption{Ejemplo 83. Apoyaturas en las \textit{Lecciones} póstumas de H. Purcell, Londres, 1696.\label{ex8-83}}
\end{center}
\end{figure}
\end{center}

La apoyatura de duración moderada (\ref{ex8-83})
no desapareció en el barroco tardío,
y en algunos contextos es la solución más obvia,
como las consideraciones melódicas o, más probablemente, armónicas nos sugieren.
Pero desde finales del siglo XVII en adelante
la evidencia sugiere que
la apoyatura estándar pasó a valer la mitad de la duración de la nota principal sin puntillo;
dos tercios de la duración de la nota principal con puntillo;
la duración total de la primera nota, en el caso de dos notas ligadas en compás compuesto;
y la duración total de la nota antes de un silencio.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-84.png}
\caption{Ejemplo 84. D'Anglebert, \textit{Piezas de clavecín}, París, 1689, apoyaturas (cheute ou port de voix).\label{ex8-84}}
\end{center}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-85.png}
\caption{Ejemplo 85.
J. M. Hotteterre, \textit{Principes}, París, 1707, p. 28, apoyaturas
en (a) probablemente sobre el pulso, en (b) probablemente no.
\label{ex8-85}}
\end{center}
\end{figure}
\end{center}

Las duraciones de las notas de adorno en (b) (\ref{ex8-85}) son engañosas, como es habitual.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-86.png}
\caption{Ejemplo 86.
Dieupart, \textit{Suites de clavecin}, París, c. 1720, apoyaturas.
\label{ex8-86}}
\end{center}
\end{figure}
\end{center}


\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-87.png}
\caption{Ejemplo 87.
J. S. Bach, \textit{Klavier-Büchlein}, comenzada en Cöthen, 1720.
\label{ex8-87}}
\end{center}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-88.png}
\caption{Ejemplo 88.
Johann Mattheson, \textit{Volkommene Capellmeister}, Hamburgo, 1739, 11, iii, 24 y 25, apoyaturas por salto.
\label{ex8-88}}
\end{center}
\end{figure}
\end{center}

\dquotation{(233) Joachim Quantz, Berlin, 1752:}{
`Sostener la apoyatura durante la mitad de la duración de la nota principal';
pero si se trata de una figura con puntillo
`dividir la duración en tres partes, de las cuales la apoyatura toma dos,
y la nota principal sólo una: es decir, la duración del puntillo.'
En el caso de dos notas ligadas en compás compuesto,
`la apoyatura debe mantenerse la duración de la primera nota incluyendo el puntillo';
en el caso de una apoyatura antes de un silencio
`se le dará la duración de la nota, y a la nota principal la duración del silencio'.
}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-89.png}
\caption{Ejemplo 89.
Joachim Quantz, \textit{Essay}, Berlín, 1752, últimas páginas del libro,
Tabla VI,
figuras 11, 13, 15, 17, 23
se ejecutan según las figuras 12, 14, 16, 18, 24.
\label{ex8-89}}
\end{center}
\end{figure}
\end{center}

En ocasiones, las apoyaturas pueden tomar una duración mayor que la usual con fines expresivos.

\dquotation{(234) Francesco Geminiani, Londres, 1751:}{
La apoyatura expresiva
`debe hacerse bastante larga,
dándole más de la mitad de la duración de la nota a la que pertenece';
la apoyatura breve carece de tal expresividad,
`pero tendrá siempre un efecto agradable,
y puede aplicarse a cualquier nota a voluntad'.
}

\dquotation{(235) J. E. Galliard, Londres, 1742:}{
`Te detienes más tiempo'
en la apoyatura que en la nota principal.
}

\dquotation{(236) C. P. E. Bach, Berlín, 1753:}{
`La regla general para la duración de la apoyatura es
tomar de la nota siguiente,
si es igual [duple], la mitad de su duración;
y si es desigual [triple], dos tercios de su duración';
pero en ocasiones
`debe prolongarse más allá de su duración normal
en aras de la expresividad deseada.
A veces la duración la determina la armonía.'
}

\dquotation{(237) F. W. Marpurg, Berlín, 1765:}{
`No se deben producir progresiones armónicas defectuosas
mediante una apoyatura
más de lo que se puede recurrir a la apoyatura
para evitar quintas consecutivas.'
[Contrastar con
\textit{197},
\textit{199},
\textit{227},
pero el hecho mismo de que se trate de una apoyatura larga
implica que no se podrá disimular una armonía defectuosa
por medio de la velocidad,
si bien podría hacerse así en el caso de una apoyatura breve.]
}

\vspace{0.5em}
En el ejemplo 90 (figura \ref{ex8-90}),
el primer par de apoyaturas normalmente debería durar una negra,
desplazando las notas principales hacia el silencio que les sigue;
pero esto produce una armonía imposible en el tercer tiempo.
El segundo par, no habiendo un silencio a continuación,
por regla general y por necesidad se ejecutará igualmente en corcheas.
Más adelante (compases 44--5)
se pide el mismo efecto, escrito esta vez en corcheas.
La solución obvia es, por lo tanto,
utilizar corcheas consistentemente en todo el pasaje.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-90.png}
\caption{Ejemplo 90.
J. S. Bach, Preludio XVIII del Libro II del \textit{Clave bien temperado}, apoyaturas.
\label{ex8-90}}
\end{center}
\end{figure}
\end{center}

En el ejemplo 91 (figura \ref{ex8-91}),
la duración normal de la apoyatura en el segundo compás es de una negra,
resultando, en apariencia, quintas consecutivas entre el primer tiempo y el segundo.
Esto, no obstante, es correcto,
ya que el C natural suena al oído como una nota de paso acentuada que resuelve en el B natural,
y es ésta una progresión aceptable y de hecho excelente,
como lo confirma el ejemplo 92 (figura \ref{ex8-92}),
donde la misma progresión se presenta como aceptable.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-91.png}
\caption{Ejemplo 91.
François Couperin, \textit{Pièces de Clavecin}, Libro I, París, 1713,
Cinquième Ordre, compases 9--10, (a) notación, (b) la ejecución más apropiada de la apoyatura.
\label{ex8-91}}
\end{center}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-92.png}
\caption{Ejemplo 92.
Georg Philipp Telemann, \textit{Musikalisches Lob Gottes}, Nüremberg, 1744;
quintas consecutivas aceptables (a);
éstas pueden escribirse como en (b);
las cuales se oyen como si se tratase de la progresión gramaticalmente correcta (c).
\label{ex8-92}}
\end{center}
\end{figure}
\end{center}

\dquotation{(238) Joachim Quantz, Berlín, 1752:}{
Las apoyaturas breves
`toman su valor de las notas a las que preceden';
dichas apoyaturas
`se ejecutan muy brevemente',
pero igualmente
`a tiempo con la nota principal'.
}

\dquotation{(239) François Couperin, París, 1716:}{
Las apoyaturas se tocan
`con la armonía, es decir sobre el tiempo que correspondería a la siguiente nota
[esto es, la nota principal]'.
}

\dquotation{(240) J. E. Galliard, Londres, 1742:}{
`Descansas sobre' la apoyatura
`para alcanzar la nota [principal] de destino'.
}

\dquotation{(241) F. W. Marpurg, Berlín, 1765:}{
`Todas las apoyaturas \ldots deben ocurrir exactamente a tiempo'
junto con
`las partes de acompañamiento \ldots sólo la nota principal,
de la cual la apoyatura es auxiliar,
debe demorarse';
la apoyatura
`debe sonar siempre un poco más fuerte que la nota principal o esencial,
y debe ser ligada suavemente hacia ella'.
}

\dquotation{(242) C. P. E. Bach, Berlín, 1753:}{
`Todas las apoyaturas se tocan más fuerte que la nota siguiente',
y
`se tocan unidas, ya sea que el ligado esté escrito o no'.
}

\dquotation{(243) Joaquim Quantz, Berlín, 1752:}{
`Por regla general se debe efectuar una breve pausa entre la apoyatura y la nota que la precede,
sobre todo si ambas notas tienen la misma altura;
de manera tal que la apoyatura pueda distinguirse con claridad.'
Las apoyaturas
`pueden contribuir en grado sumo' a la armonía;
`ya que resultan en disonancias, como cuartas y séptimas',
las cuales
`resuelven en la nota siguiente [es decir, en la nota principal]'.
Por lo tanto
`no alcanza con saber cómo ejecutar las apoyaturas de acuerdo a su naturaleza y sus variantes,
cuando están escritas;
también es necesario saber cuándo incorporarlas apropiadamente cuando no están escritas'.
}

\dquotation{(244) C. P. E. Bach, Berlín, 1753:}{
`Las apoyaturas están entre los ornamentos más necesarios.
Éstas enriquecen la armonía así como la melodía \ldots
Las apoyaturas modifican acordes que en su ausencia sonarían demasiado simples.
Todas las síncopas y disonancias pueden atribuírseles.
¿Qué sería del arte de la armonía sin estos ingredientes?'
}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-93.png}
\caption{Ejemplo 93.
C. P. E. Bach, \textit{Essay}, Berlín, 1753, ilustraciones para II, ii, 8.
\label{ex8-93}}
\end{center}
\end{figure}
\end{center}

Podemos ver que en la figura \ref{ex8-93}
las notas de adorno representan las apoyaturas que C. P. E. Bach
sugiere deberían ser incorporadas por el intérprete.

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-94.png}
\caption{Ejemplo 94.
C. P. E. Bach, \textit{Essay}, Berlín, 1753, II, ii, II.
extraño efecto rítmico causado por la apoyatura larga prolongada como es usual.
\label{ex8-94}}
\end{center}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-95.png}
\caption{Ejemplo 95.
Leopold Mozart, \textit{Violinschule}. Augsburgo, 1756, IX, 4,
mostrando la creciente tendencia a prolongar las apoyaturas.
\label{ex8-95}}
\end{center}
\end{figure}
\end{center}

C. P. E. Bach \textit{(245)}
también sugiere\ndelt{véase figura \ref{ex8-96}.} ciertos contextos en los cuales la apoyatura breve
(que no produce un efecto apreciable en la armonía)
puede considerarse apropiada:
por ejemplo
`antes de una nota rápida'
o `antes de una nota larga en notas repetidas'
o `en una síncopa'
o `cuando las apoyaturas completan saltos de tercera'.
[Pero C. P. E. Bach no excluye aquí las apoyaturas largas,
de las que proporciona numerosos ejemplos en tales situaciones.]
`Pero en un Adagio el sentimiento es más expresivo
si se toman como las primeras corcheas de un tresillo en lugar de una semicorchea';
mientras que las apoyaturas
`antes de un tresillo [escrito] se tocan breves de manera de evitar confundir el ritmo';
y
`cuando la apoyatura forma una octava con el bajo
se la ejecuta breve
[debido al empobrecimiento de la armonía que resultaría
si se la prolongase a tal punto de afectar la progresión armónica].
Si una nota asciende una segunda e inmediatamente vuelve \ldots
una apoyatura breve bien puede ocurrir sobre la nota intermedia.'

\begin{center}
\begin{figure}
\begin{center}
\includegraphics{lilypond/Ex8-96.png}
\caption{Ejemplo 96.
C. P. E. Bach, ilustraciones (hay otras) de lo indicado en \textit{(245)}.
\label{ex8-96}}
\end{center}
\end{figure}
\end{center}

