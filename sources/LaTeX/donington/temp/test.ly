\version "2.20.0"

music = {
  \relative c' { c d e f }
}

lyricsuno = \lyricmode {
  Un do __ cua
}

\score { <<
  \new Staff {
    \new Voice = "uno" { \music }
  }
  %\new Lyrics \lyricsto "uno" { \lyricsuno }
  \addlyrics {
  Un do "___" cua
  }
%  \lyricsuno }
  >>
}
