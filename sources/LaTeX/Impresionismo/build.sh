#!/bin/bash

pdflatex obras_del_impresionismo_para_guitarra_obras.tex || exit 1
pdflatex caratula.tex || exit 1
pdfjam --outfile obras_del_impresionismo_para_guitarra.pdf -- caratula.pdf obras_del_impresionismo_para_guitarra_obras.pdf
