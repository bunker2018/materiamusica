#!/bin/bash

pdflatex texturas_en_guitarra_obras.tex || exit 1
pdflatex caratula.tex || exit 1
pdfjam --outfile texturas_en_guitarra.pdf -- caratula.pdf texturas_en_guitarra_obras.pdf
