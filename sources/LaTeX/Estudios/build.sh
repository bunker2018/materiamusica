#!/bin/bash

pdflatex seleccion_de_estudios_para_guitarra_obras.tex || exit 1
pdflatex caratula.tex || exit 1
pdfjam --outfile seleccion_de_estudios_para_guitarra.pdf -- caratula.pdf seleccion_de_estudios_para_guitarra_obras.pdf
