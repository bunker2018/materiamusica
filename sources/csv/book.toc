\select@language {spanish}
\contentsline {chapter}{\numberline {1}El cuerpo y el instrumento}{11}
\contentsline {section}{\numberline {1.1}Construcci\IeC {\'o}n de la posici\IeC {\'o}n}{11}
\contentsline {subsection}{\numberline {1.1.1}La posici\IeC {\'o}n de sentado}{12}
\contentsline {subsection}{\numberline {1.1.2}Construcci\IeC {\'o}n de la posici\IeC {\'o}n definitiva}{14}
\contentsline {subsubsection}{Lecturas recomendadas}{16}
\contentsline {section}{\numberline {1.2}Grupos musculares}{16}
\contentsline {subsection}{\numberline {1.2.1}Concepto de fijaci\IeC {\'o}n}{18}
\contentsline {subsubsection}{Lecturas recomendadas}{19}
\contentsline {subparagraph}{El concepto de fijaci\IeC {\'o}n}{19}
\contentsline {section}{\numberline {1.3}Relajaci\IeC {\'o}n; movilidad; estabilidad}{19}
\contentsline {chapter}{\numberline {2}Mano derecha}{21}
\contentsline {section}{\numberline {2.1}Construcci\IeC {\'o}n de la posici\IeC {\'o}n}{21}
\contentsline {subsubsection}{Lecturas recomendadas}{23}
\contentsline {section}{\numberline {2.2}Presentaciones de mano derecha}{23}
\contentsline {subsection}{\numberline {2.2.1}Presentaci\IeC {\'o}n longitudinal}{23}
\contentsline {subsection}{\numberline {2.2.2}Presentaci\IeC {\'o}n transversal}{24}
\contentsline {section}{\numberline {2.3}Traslados de mano derecha}{25}
\contentsline {subsection}{\numberline {2.3.1}Traslado de la boca al puente}{25}
\contentsline {subsection}{\numberline {2.3.2}Traslado transversal}{26}
\contentsline {subsubsection}{Estudios seleccionados}{27}
\contentsline {subsubsection}{Fragmentos seleccionados}{28}
\contentsline {section}{\numberline {2.4}Toques}{28}
\contentsline {subsection}{\numberline {2.4.1}Toque libre}{28}
\contentsline {subsubsection}{Toque libre de pulgar}{29}
\contentsline {subsection}{\numberline {2.4.2}Toque apoyado}{30}
\contentsline {subsubsection}{Toque apoyado de pulgar}{30}
\contentsline {subsubsection}{Estudios seleccionados}{31}
\contentsline {subsection}{\numberline {2.4.3}Pizzicato}{31}
\contentsline {section}{\numberline {2.5}Acordes plaqu\IeC {\'e}}{33}
\contentsline {subsection}{\numberline {2.5.1}Configuraciones de mano derecha}{33}
\contentsline {subsubsection}{Estudios seleccionados}{35}
\contentsline {subsubsection}{Fragmentos seleccionados}{35}
\contentsline {subsubsection}{Obras seleccionadas}{36}
\contentsline {section}{\numberline {2.6}Notas repetidas}{36}
\contentsline {subsubsection}{Estudios seleccionados}{39}
\contentsline {section}{\numberline {2.7}Apagadores}{39}
\contentsline {chapter}{\numberline {3}Mano izquierda}{41}
\contentsline {section}{\numberline {3.1}Construcci\IeC {\'o}n de la posici\IeC {\'o}n}{41}
\contentsline {section}{\numberline {3.2}Traslados de mano izquierda}{42}
\contentsline {subsection}{\numberline {3.2.1}Traslado longitudinal}{42}
\contentsline {subsection}{\numberline {3.2.2}Traslado transversal}{42}
\contentsline {subsection}{\numberline {3.2.3}Traslado mediante la cuerda al aire}{42}
\contentsline {subsubsection}{Fragmentos seleccionados}{43}
\contentsline {section}{\numberline {3.3}Presentaciones de mano izquierda}{43}
\contentsline {subsection}{\numberline {3.3.1}Presentaci\IeC {\'o}n longitudinal}{43}
\contentsline {subsection}{\numberline {3.3.2}Presentaci\IeC {\'o}n transversal}{43}
\contentsline {subsection}{\numberline {3.3.3}Cambios de presentaci\IeC {\'o}n}{44}
\contentsline {section}{\numberline {3.4}Distensi\IeC {\'o}n y contracci\IeC {\'o}n}{44}
\contentsline {subsection}{\numberline {3.4.1}Distensi\IeC {\'o}n}{44}
\contentsline {subsection}{\numberline {3.4.2}Contracci\IeC {\'o}n}{44}
\contentsline {section}{\numberline {3.5}Cejilla}{45}
\contentsline {chapter}{\numberline {4}Arpegios}{47}
\contentsline {subsubsection}{Estudios seleccionados}{47}
\contentsline {chapter}{\numberline {5}L\IeC {\'\i }nea}{49}
\contentsline {section}{\numberline {5.1}L\IeC {\'\i }neas diat\IeC {\'o}nicas}{49}
\contentsline {subsubsection}{Estudios seleccionados}{49}
\contentsline {section}{\numberline {5.2}L\IeC {\'\i }neas en forma de arpegios}{49}
\contentsline {chapter}{\numberline {6}Din\IeC {\'a}micas}{51}
\contentsline {section}{\numberline {6.1}Gradualidad}{51}
\contentsline {section}{\numberline {6.2}Contraste}{51}
\contentsline {section}{\numberline {6.3}Tipos de toque en funci\IeC {\'o}n de la din\IeC {\'a}mica}{51}
\contentsline {chapter}{\numberline {7}Articulaci\IeC {\'o}n y acentos}{53}
\contentsline {section}{\numberline {7.1}Legato}{53}
\contentsline {section}{\numberline {7.2}Staccato}{53}
\contentsline {section}{\numberline {7.3}Tenuto}{53}
\contentsline {section}{\numberline {7.4}Acentos}{54}
\contentsline {section}{\numberline {7.5}Fermata}{54}
\contentsline {chapter}{\numberline {8}Ligados}{55}
\contentsline {section}{\numberline {8.1}Ligados ascendentes}{55}
\contentsline {subsubsection}{Estudios seleccionados}{55}
\contentsline {section}{\numberline {8.2}Ligados descendentes}{55}
\contentsline {subsubsection}{Estudios seleccionados}{56}
\contentsline {section}{\numberline {8.3}Ligados con cuerda al aire}{56}
\contentsline {subsubsection}{Estudios seleccionados}{56}
\contentsline {chapter}{\numberline {9}Adornos}{57}
\contentsline {section}{\numberline {9.1}Apoyatura; mordente; trino}{57}
\contentsline {section}{\numberline {9.2}Arrastres}{57}
\contentsline {section}{\numberline {9.3}Arpeggiato}{58}
\contentsline {section}{\numberline {9.4}Vibrato}{58}
\contentsline {chapter}{\numberline {10}Arm\IeC {\'o}nicos}{59}
\contentsline {section}{\numberline {10.1}Arm\IeC {\'o}nicos naturales}{59}
\contentsline {section}{\numberline {10.2}Arm\IeC {\'o}nicos artificiales}{59}
\contentsline {subsubsection}{Obras seleccionadas}{60}
\contentsline {chapter}{\numberline {A}Adornos}{61}
\contentsline {section}{\numberline {A.1}Mordente}{61}
\contentsline {section}{\numberline {A.2}Grupeto}{62}
\contentsline {section}{\numberline {A.3}Trino}{62}
\contentsline {section}{\numberline {A.4}Apoyatura y acciaccatura}{63}
\contentsline {section}{\numberline {A.5}Bordadura}{63}
\contentsline {section}{\numberline {A.6}Arpeggiato}{64}
\contentsline {section}{\numberline {A.7}Glissando y portamento}{65}
\contentsline {section}{\numberline {A.8}Vibrato}{66}
\contentsline {chapter}{\numberline {B}Arm\IeC {\'o}nicos}{69}
\contentsline {section}{\numberline {B.1}Arm\IeC {\'o}nicos naturales}{69}
\contentsline {subsection}{\numberline {B.1.1}La serie arm\IeC {\'o}nica}{69}
\contentsline {subsection}{\numberline {B.1.2}Los arm\IeC {\'o}nicos de una cuerda}{70}
\contentsline {subsection}{\numberline {B.1.3}Alturas de los arm\IeC {\'o}nicos de la serie}{71}
\contentsline {subsubsection}{Obras seleccionadas}{72}
\contentsline {subsection}{\numberline {B.1.4}Arm\IeC {\'o}nicos naturales con otras afinaciones}{72}
\contentsline {section}{\numberline {B.2}Arm\IeC {\'o}nicos artificiales}{73}
\contentsline {subsubsection}{Obras seleccionadas}{74}
