\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

sL = \set stemLeftBeamCount = #2
cL = \set stemLeftBeamCount = #1
zL = \set stemLeftBeamCount = #0
sR = \set stemRightBeamCount = #2
cR = \set stemRightBeamCount = #1
zR = \set stemRightBeamCount = #0

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
%   \spacingWider
    \partial 8
    << {
      << {
        \relative c {
          \stemUp
          \zL \sR c16[
            \tweak extra-offset #'(1 . 0) ^\rhm
            \sL \cR b
            \tweak extra-offset #'(0.5 . 3) ^\rhi
          \cL \sR f'
            \tweak extra-offset #'(1 . 0) ^\rhm
            e
            \tweak extra-offset #'(0.5 . 3) ^\rhi
            bes'
            \tweak extra-offset #'(0 . 0)
            ^\markup \italic \teeny { "(sim.)" }
            \sL \cR a
          \cL \sR d
            cis g' \sL \zR f]
        }
      } \\ {
        \relative c, {
          \stemDown
          e8-0[
            \tweak extra-offset #'(0 . 0) _\rhp
            a-0
            \tweak extra-offset #'(0 . 0) _\rhp
            d-0
            \tweak extra-offset #'(0 . 0)
            _\markup \italic \teeny { "(sim.)" }
            g-0 b!-0]
        }
      } >>
    } {
      \override Hairpin.rotation = #'(7 -1 0)
      s8 _\mf _\< s4 s16 s32 \!
      s16 \tweak extra-offset #'(1 . -1)
        _\markup \italic { "rall." }
    } >>
    \bar "||"
    \relative c {
      \arpeggioArrowUp
      \lhRight
      < c-3
        \tweak extra-offset #'(-1.8 . 0.4)
        _\rhp
        e-2 g-0 b-0 e-0 >2
      \arpeggio
    }
    \bar "|"
  }
}

