\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \key d \major
    %\spacingWider
		\override BreathingSign #'text = \markup {
			\musicglyph #"scripts.caesura.curved"
		}
	  \relative c, {
      d8\mp-._\pizz
      \tweak extra-offset #'(-4 . 1)
      ^\markup \bold { "Moderato" }
    }
  	\relative c {
      a8-.-0 fis'-.-3 e-.-1 fis-.-3 fis-.-3 a,4-.-0 }
	  \relative c {
      d8-.-1\6^\armXII a'-.-1\5
        \tweak extra-offset #'(1 . -2)
        ^\armXII fis'-.-3\4 e-.-1
      fis-.-4 fis-.-4 a,-.-1\5^\armXII d-.-1\4
        \tweak extra-offset #'(1 . -2)
        ^\armXII }
  	\relative c, {
      d8-. a'-.-0 fis'-.-3 e-.-1 fis-.-3 fis-.-3 a,4-.-0 }
  	\relative c {
      d8-.-1\6^\armXII a'-.-1\5
        \tweak extra-offset #'(1 . -2)
        ^\armXII fis'-.-3\4 e-.-1
      fis4-.-4 }
    \breathe
	  \relative c' {
			a4-4
      _\markup \italic { "nat." }
    }
  }
}

