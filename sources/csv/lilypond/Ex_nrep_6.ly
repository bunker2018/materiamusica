\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
    \spacingWider
    \relative c' {
      \rhUp
      <e\rhi>16_> <e\rha> <e\rhm> <e\rha>_>
      <e\rhi> <e\rha> <e\rhm>_> <e\rha>
      <e\rhi> <e\rha> <e\rhm>_> <e\rha>
      <e\rhi> <e\rha>_> <e\rhm> <e\rha>
    }
    \bar ":|."
  }
}

