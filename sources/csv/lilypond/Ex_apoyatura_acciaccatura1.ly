\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
    \relative c' {
\grace {d8(} c4)
\tweak extra-offset #'(-2 . 1)
^\markup \small {apoyatura}
\grace {c8(} b4)
d8([
\tweak extra-offset #'(0 . -1)
^\markup \teeny {Se toca:} c8)]
c( b)
\grace {a16( b} c4)
\tweak extra-offset #'(0 . 0)
^\markup \small {doble} ^\markup \small {apoyatura}
\grace {d16 c(} b4)
a16(
\tweak extra-offset #'(0 . 1)
^\markup \teeny {Se toca:} b c8)
d16( c b8)
\slashedGrace {d8(} c4)
\tweak extra-offset #'(-3 . 0)
^\markup \small {acciaccatura}
\slashedGrace {c8(} b4)
\tweak extra-offset #'(0 . -1)
d64([^\markup \teeny {Se toca:} c) ~ c8..]
c64( b) ~ b8..
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

