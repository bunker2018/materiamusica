\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

X = \markup \bold {*}
upper = \relative c' {
  \override Score.SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/2)
  s1 s  s  s e^\X g bes^\X c d^\X e fis^\X g
}
lower = \relative c, {
  c1 
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"1º"}
  c' 
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"2º"}
  g' 
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"3º"}
  c  
     \tweak extra-offset #'(0 . 0.4)
     ^\markup \italic {"4º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"5º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"6º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"7º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"8º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"9º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"10º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"11º"}
  s  
     \tweak extra-offset #'(0 . 1.8)
     ^\markup \italic {"12º"}
}
		\markup { \fill-line {
		\score {
  	\layout { ragged-right = ##t indent = 0 }
		 \new PianoStaff <<
    \new Staff {
    \override Score.BarLine.stencil = ##f
    \override Staff.TimeSignature.stencil = ##f
    \override Staff.BarLine.allow-span-bar = ##f
    \upper
    \revert Score.BarLine.stencil
    \revert Staff.BarLine.allow-span-bar
    \bar "|"
    }
    \new Staff {
    \clef "bass"
    \override Score.BarLine.stencil = ##f
    \override Staff.TimeSignature.stencil = ##f
    \override Staff.BarLine.allow-span-bar = ##f
    \lower
    \revert Score.BarLine.stencil
    \revert Staff.BarLine.allow-span-bar
    \bar "|"
    }
		>>
  \layout { \context { \Staff \omit TimeSignature } }
} } }

