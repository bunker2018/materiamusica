\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2 \key e \minor
    \spacingWider
    \partial 4 {
      << {
        \relative c' {
          \voiceOne {
            e8-0 fis-1 ^\fbII
          }
        }
      } \\ {
        \relative c {
          \voiceTwo {
            g8-2 fis-1
          }
        }
      } >>
    }
    << {
      \relative c'' {
        \voiceOne {
          g4-3
          \tweak extra-offset #'(0 . 2)
          ^\markup \bold { "(a)" }
          fis8-2
          \tweak extra-offset #'(0 . 0.4)
          ^\markup \bold { "(b)" }
          e-0
          dis4-4
          \tweak extra-offset #'(0 . 3)
          ^\markup \bold { "(c)" }
        }
      }
    } \\ {
      \relative c, {
        \voiceTwo {
          e4-0 a-0 b-1
        }
      }
    } >>
  }
}

