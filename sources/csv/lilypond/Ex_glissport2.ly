\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
\relative c' {
c4-1(
\glissando
\slashedGrace {g'8)}
g4
c,4-1
\glissando
\once \override TextScript #'extra-offset = #'(-4.8 . 6.4)
_\markup \italic \teeny {port.}
g'-1
		  \once \hide NoteHead
		  \once \hide Stem
%		  \once \override NoteHead.no-ledgers = ##t
%		  \once \override Glissando.bound-details.left.padding = #0.3
			b,8 \glissando
			\slashedGrace {g'8} g4
}
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

