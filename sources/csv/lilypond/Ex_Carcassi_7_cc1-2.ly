\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 4/4
%    \spacingWider
    << {
      \relative c {
        \voiceOne {
          a16 a'-2_\rha a_\rhm a_\rhi
          c, a'_\rha a_\rhm a_\rhi
          f a_\rha a_\rhm a_\rhi
          e a_\rha a_\rhm a_\rhi
          d, a'-2_\rha f'-1_\rhi a,_\rha
          c, a'-2 e' a,
          b, gis'-1 d'-4_\rhm gis,
          a, a'-3 c-1 a
        }
      }
    } \\ {
      \relative c {
        \voiceTwo {
          a4_\rhp c-3_\rhp f-4_\rhp e-1_\rhp
          d_\rhp c-3 b-2 a
        }
      }
    } >>
    \bar "|"
  }
}

