\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 4/4
    \spacingWider
    \relative c {
<g-1 d'-3 f-1 b-2 f'-4 g-1>1 ^\fbIII
^\markup \small { Cejilla }
<g'-3 bes-1 d-1 g-1>1 ^\bbIII
^\markup \small { Media cejilla }
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

