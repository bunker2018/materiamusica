\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 4/4
    \spacingWider
    \relative c {
	<b\5-1 g'\4-4>1
	^\markup \small {Longitudinal}
	<fis\6-1 gis''\1-1>
	^\markup \small {Transversal}
	<fis''\1-1 a,,\6-4>
	^\markup \small {Transversal invertida}
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

