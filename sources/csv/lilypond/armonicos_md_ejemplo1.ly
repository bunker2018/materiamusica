\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
\new Staff {
\spacingWider
\override TimeSignature.style = #'numbered
<< {
\override TextSpanner #'direction = #up
\override TextSpanner #'(bound-details left text) = \markup \small \italic { arm. \concat { 8 \super {va.}}}
\override TextSpanner.style = #'line
\override TextSpanner.bound-details.right.text
      = \markup { \draw-line #'(0 . -1) }
\override TextSpanner.bound-details.left.padding = #-1
\override TextSpanner.bound-details.right.padding = #-1

s2 \startTextSpan s4 \stopTextSpan
}{
\relative c' {
e4-0
_\markup \teeny {XII} 
fis-2
_\markup \teeny {XIV} 
g2-3
_\markup \teeny {XV} 
} } >> } }

