\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
<<
\relative c'' {
g4 s4^\markup \small {Puede tocarse:} g4
} \\ \relative c, {
\set tieWaitForNote = ##t
e4 s4 \grace {e16 ~ s8 } e4
}
>>
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

