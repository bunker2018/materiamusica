\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
    \spacingWider
    \spacingWider
    \relative c {
      \rhRight
      \lhLeft
      <a-0\rhp e'-2\rhi a-3\rhm e'-0\rha>2
      <a-0\rhp e'-2\rhi c'-1\rhm e-0\rha>2
      <e-0\rhp b'-2\rhi g'-0\rhm e'-0\rha>2
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

