\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
    \spacingWider
    \spacingWider
    \relative c {
      \stemUp
      \rhDown
      <e\rhi a\rhm c\rha>8
        <e a c> <e a c> <e a c>
      <e\rhi a\rhm>8
        <e a> <e a> <e a>
      <a\rhm c\rha>8
        <a c> <a c> <a c>
      <e\rhi c'\rha>8
        ^\markup \bold { "(*)" }
        <e c'> <e c'> <e c'>
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

