\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
    \spacingWider
    \spacingWider
    \relative c {
      \rhDown
      <a\rhp e'\rhi a\rhm c\rha>8
        <a e' a c> <a e' a c> <a e' a c>
      <a\rhp e'\rhi a\rhm>8
        <a e' a> <a e' a> <a e' a>
      <a\rhp a'\rhm c\rha>8
        <a a' c> <a a' c> <a a' c>
      <a\rhp e'\rhi c'\rha>8
        ^\markup \bold { "(*)" }
        <a e' c'> <a e' c'> <a e' c'>
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

