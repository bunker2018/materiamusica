\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 1.4)
  ^\markup \bold { \concat {"3ª cuerda en fa" \super \sharp } }
}
\relative c {
fis1_\flageolet
fis'
  _\markup \teeny {XII}
cis'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
fis
  _\markup \teeny {V}
ais,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }
