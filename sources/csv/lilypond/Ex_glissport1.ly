\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
\relative c' {
c4-1(
\glissando
g'-1)
c,4-1
\glissando
\once \override TextScript #'extra-offset = #'(-4.8 . 6.4)
_\markup \italic \teeny {gliss.}
g'-1
			s8
		  \once \hide NoteHead
		  \once \hide Stem
		  \once \override NoteHead.no-ledgers = ##t
		  \once \override Glissando.bound-details.left.padding = #0.3
			\once \override TextScript #'extra-offset = #'(-1.8 . 8.4)
			\grace { <b, d f >8_\markup \italic \teeny {gliss.} \glissando }
			<e-3 g-2 b-1>2
}
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

