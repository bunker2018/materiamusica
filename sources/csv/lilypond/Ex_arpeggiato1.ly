\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
    \relative c, {
<e b' e gis b e>4\arpeggio
s8.
s16^\markup \small {Se toca:}
\set tieWaitForNote = ##t
\grace { e32\rhp ~ b'\rhp ~ e\rhp ~ gis\rhi ~ b\rhm ~ e\rha ~ }
<e,, b' e gis b e>4
    }
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

