\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/4
    \spacingWider
    \relative c' {
      \rhUp
      \override HorizontalBracket.direction = #UP
      \override HorizontalBracket.staff-padding = #3
      <e\rha>16_>\startGroup <e\rhm> <e\rhi> <e\rhm>
      <e\rhi>_>\stopGroup <e\rha>\startGroup <e\rhm> <e\rhi>
      <e\rhm>_> <e\rhi>\stopGroup <e\rha>\startGroup <e\rhm>
      <e\rhi>_> <e\rhm> <e\rhi>\stopGroup <e\rha>
      ^\markup { "(...)" }
    }
  }
  \layout {
    \context {
      \Voice
      \consists "Horizontal_bracket_engraver"
    }
  }
}

