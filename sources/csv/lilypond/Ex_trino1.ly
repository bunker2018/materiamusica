\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 2/2
    \spacingWider
\relative c' {
c4\trill _\markup \small {Barroco} s
d32(^\markup \small {Se toca:} c d c) ~ c8
s4
c4\trill _\markup \small {Clásico} s
c32(^\markup \small {Se toca:} d c d c8)
}
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

