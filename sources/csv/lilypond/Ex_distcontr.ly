\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 4/4
    \spacingWider
\spacingWider
\relative c {
<e-2 b'-4 c-1 g'-3>1
^\markup \small {Distensión}
<e-1 bes'-3 cis-2 g'-4>1
^\markup \small {Contracción}
}
    \bar "|"
  }
  \layout { \context { \Staff \omit TimeSignature } }
}

