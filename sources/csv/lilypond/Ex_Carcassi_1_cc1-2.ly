\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
  \new Staff {
    \clef "treble_8" \time 4/4
%    \spacingWider
    << {
      \relative c {
        \voiceOne {
          c8 c'-1 b-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
            a-2 g-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
            f-4 e-2 d-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
          c
            e'-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
            d-4 c-1 b-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
            a-2 g-0
            \tweak extra-offset #'(0.4 . 0)
            ^\markup \bold \small { "*" }
            f-3
          e
        }
      }
    } \\ {
      \relative c {
        \voiceTwo {
          c4-3 r4 r2
          c4-3 r4 r2
          e4-2
        }
      }
    } >>
  }
}

