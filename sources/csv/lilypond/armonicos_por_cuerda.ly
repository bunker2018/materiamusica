\version "2.20.0"

%#(set-global-staff-size 15)

\include "/usr/lib/lymd/lilypond/include/global.ly"
\include "/usr/lib/lymd/lilypond/include/guitardefinitions.ly"

\paper{
  indent=0\mm
  line-width=136\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

\markup { \vspace #1 }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . -0.8)
  ^\markup \bold {1ª cuerda}
}
\relative c' {
e1^\flageolet
e'
  _\markup \teeny {XII}
b
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
e
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
gis
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 0.4)
  ^\markup \bold {2ª cuerda}
}
\relative c' {
b1^\flageolet
b'
  _\markup \teeny {XII}
fis'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
b,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
dis
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 1.4)
  ^\markup \bold {3ª cuerda}
}
\relative c' {
g1_\flageolet
g'
  _\markup \teeny {XII}
d'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
g,
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {V}
b
  ^\markup \italic \concat {8 \super{va}}
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {4ª cuerda}
}
\relative c {
d1_\flageolet
d'
  _\markup \teeny {XII}
a'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
d
  _\markup \teeny {V}
fis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {5ª cuerda}
}
\relative c {
a1_\flageolet
a'
  _\markup \teeny {XII}
e'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
a
  _\markup \teeny {V}
cis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

\score {
\new Staff {
\override Score.BarLine.stencil = ##f
\override Staff.TimeSignature.stencil = ##f
\clef "treble_8"
\grace { s16
  \tweak extra-offset #'(-4 . 2)
  ^\markup \bold {6ª cuerda}
}
\relative c, {
e1_\flageolet
e'
  _\markup \teeny {XII}
b'
  _\markup \teeny {VII,}
  _\markup \teeny {XIX}
e
  _\markup \teeny {V}
gis
  _\markup \teeny {IV, IX,}
  _\markup \teeny {XVI}
}
\revert Score.BarLine.stencil
\bar "|"
} }

