- Similitud con el toque *pizzicato* del contrabajo y el cello.
- Apagar con el filo de la palma; similitud con el *palm mute* de la guitarra eléctrica.
- Ni demasiado apagado ni demasiado poco --- *quasi pizzicato*.
- Construcción de la posición: dejar caer el filo de la palma sobre la unión de las cuerdas y el puente;
extender el pulgar hacia la 6ª cuerda;
buscar el ángulo adecuado y la colocación óptima.
- Deslizar hacia la primera cuerda, buscando mantener la calidad del sonido.
- Toque apoyado de yema de pulgar.
- Pizzicato con *i m a*: experimentar hasta encontrar la colocación óptima.
