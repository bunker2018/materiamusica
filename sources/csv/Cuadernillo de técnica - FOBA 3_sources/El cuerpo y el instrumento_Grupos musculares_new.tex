\label{gruposmusculares}

Analicemos ahora el concepto de \textit{grupos musculares}.
Pensemos, por ejemplo, qué es lo que pasa cuando montamos una bicicleta.
Imaginemos que pedaleamos sentados:
la parte superior del cuerpo, de la cintura para arriba, no interviene en absoluto en el trabajo realizado,
que queda a cargo exclusivamente de las piernas.
Esto nos permite alcanzar una cierta velocidad.
Si por algún motivo necesitamos pedalear con más fuerza,
por ejemplo en una pendiente o si tenemos viento en contra,
probablemente nos paremos sobre los pedales, prácticamente en forma instintiva,
de forma tal que todo el peso del cuerpo caiga sobre los pedales
y de esta manera poder alcanzar no sólo una velocidad mayor, sino además con mucho menos esfuerzo.

Podemos describir esta situación de la siguiente manera:
al pararnos sobre los pedales,
el \textit{grupo muscular} que participa del trabajo físico es mayor que cuando pedaleamos sentados,
lo que determina que el trabajo pueda realizarse con menos esfuerzo.
En la posición de sentados, el grupo muscular que realiza el trabajo lo constituyen las piernas,
mientras que al pararnos sobre los pedales el grupo muscular pasa a ser el cuerpo en su totalidad.
Estamos reemplazando un grupo muscular \textit{menor} por otro \textit{mayor}.
Este concepto, si bien en apariencia teórico, es relevante y muy importante para la percepción
de los mecanismos técnicos, y tiene implicaciones muy valiosas en el estudio de la técnica instrumental.

Cuando realizamos un trabajo físico determinado, por ejemplo pulsar una cuerda,
podemos identificar el grupo de músculos que interviene directamente en el trabajo.
Pensemos en lo que ocurre cuando pulsamos una cuerda con el pulgar, articulando desde la primera falange:
¿cuál es el grupo muscular que interviene en el movimiento?
Evidentemente, se trata del segmento del pulgar comprendido entre la articulación y la yema;
si, en cambio, articulamos desde la unión del pulgar con la mano,
el grupo muscular pasa a ser la extensión completa del pulgar.
Estamos efectuando el toque, entonces, mediante el grupo muscular menor en el primer caso,
y utilizando el grupo muscular inmediatamente mayor en el segundo caso.

Despeguemos ahora el antebrazo del aro de la guitarra, de manera de liberar el brazo,
y pulsemos la cuerda con el pulgar dirigiendo el movimiento desde el hombro.
El grupo muscular que interviene en el mecanismo, en este caso, pasaría a ser el brazo en toda su extensión,
un grupo muscular claramente mayor que en los casos anteriores.
¿Cuál es el resultado sonoro en cada caso?
Podemos notar que al realizar el toque utilizando todo el brazo,
resulta un sonido con más volumen, prácticamente sin esfuerzo,
que al realizar el toque articulando desde el pulgar.
Probablemente percibamos también un sonido más lleno, esto es, una diferente calidad tímbrica,
dado que al aplicar todo el peso del brazo estamos excitando la cuerda con más energía
y por lo tanto permitiendo que vibren más armónicos.

Podemos concluir de este experimento
que al utilizar un grupo muscular de orden mayor
el mecanismo se efectúa con menor esfuerzo físico;
además, podemos relacionar en el caso del toque,
un grupo muscular mayor con una dinámica también mayor.
Cuando estudiemos los diferentes toques de mano derecha
tendremos oportunidad de aplicar este concepto según la dinámica deseada y en función de diferentes calidades tímbricas.
Esto será relevante también en el rasgueo, en la técnica de ligados, en los casos de distensión y contracción,
y en muchos otros.
En principio, retengamos el concepto de grupo muscular como un aspecto relevante de la técnica,
y observemos en cada ejercicio cuál es el grupo muscular que interviene en el mecanismo.

\subsection{Concepto de fijación}
\label{fijacion}

Analicemos ahora en más detalle qué ocurre cuando pasamos de un grupo muscular menor a otro mayor.
Volviendo al ejemplo del toque de pulgar,
al realizar el toque articulando desde la unión del pulgar con la mano,
podemos percibir cómo los músculos del pulgar comienzan a trabajar como una unidad.
Decimos en este caso que el dedo comienza a \textit{fijarse}.
El cuerpo hace esto de manera espontánea, ya que estamos apelando a un reflejo adquirido a una edad muy temprana;
podríamos decir que es algo que el cuerpo ya sabe hacer.
Podemos describir este mecanismo como si bloqueáramos o trabáramos las articulaciones
de manera que el grupo muscular trabaje en conjunto;
esto no implica necesariamente que se produzcan tensiones,
más bien se trata de un desplazamiento de la sensación neuromotora al grupo muscular más amplio.

Pensemos en el ejemplo de la bicicleta: al pararnos sobre los pedales,
para que el peso del cuerpo se traslade a los pedales
es necesario que las articulaciones de las rodillas y de la cadera estén fijas,
de otro modo seguirían siendo las piernas las que realizan el trabajo.
Imaginemos que nos paramos sobre los pedales
y con el cuerpo en el aire pedaleamos flexionando las rodillas de la misma forma que lo haríamos estando sentados:
claramente el efecto no es el que buscamos,
ya que no sólo el esfuerzo físico sería mayor, sino que además estaríamos mucho más incómodos.
Esto puede parecer antinatural, de hecho el cuerpo instintivamente traba las articulaciones
sin necesidad de que lo hagamos conscientemente.
Lo que se produce es una \textit{fijación} de las articulaciones
que habilita al grupo muscular de orden mayor, en este caso el cuerpo,
a intervenir en el trabajo realizado.

El concepto de \textit{fijación}, entonces,
se refiere a un reflejo que consiste en fijar o bloquear las articulaciones
de manera que la sensación neuromotora, y por lo tanto el trabajo físico,
se desplace de un grupo muscular a otro de orden mayor.
Nuevamente, este concepto será relevante para el estudio de los mecanismos técnicos
que impliquen la acción de grupos musculares de diferente orden.

