- Resumen del texto de E.F.
- Características de la posición apropiada:
    - la posición de tocar debe ser estable;
    - debe ser posible mantenerla sin un esfuerzo excesivo;
    - debe mantener el instrumento estable con respecto al cuerpo;
    - debe permitir realizar cómodamente todos los movimientos necesarios para la ejecución;
    - los brazos no deberían usarse para sostener la guitarra.
- Recomendaciones generales: altura de la silla, ángulos de las piernas, inclinación de la guitarra.
- Breve descripción del experimento.
- Uso de franelas o paños para que el instrumento no resbale.
- Alternativas al banquito.
- Importancia del registro de la posición correcta a la hora de trabajar en posiciones improvisadas.
