- Experimento I: tomar el acorde y rotar la mano.
- Experimento II: tomar el acorde y pulsar de arriba hacia abajo.
- Experimento III: combinar ambos movimientos en un solo gesto.
- _Arpeggiato_ lento: igual a un arpegio ascendente rápido y controlado.
- Gesto técnico suave, análogo al resultado musical esperado.
