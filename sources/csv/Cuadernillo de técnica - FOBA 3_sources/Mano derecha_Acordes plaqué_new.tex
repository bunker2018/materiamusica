%%% Cuadernillo de técnica - FOBA 3_sources/Mano derecha_Acordes plaqué_new

En los acordes \textit{plaqué}
utilizamos los dedos de la mano derecha para hacer sonar las notas del acorde simultáneamente,
a diferencia del \textit{arpeggiato},
que implica tocar las notas en rápida sucesión\footnote{
  La indicación \textit{non arpeggiato}
  prescribe expresamente que el acorde en cuestión
  debe tocarse \textit{plaqué}.
}.

Para construir el mecanismo básico,
presentamos la mano derecha en la posición correspondiente al \textit{toque libre} (ver \ref{toquelibre});
juntamos los dedos \textit{i m a} de manera que las yemas de los dedos queden alineadas,
y articulamos con los tres dedos simultáneamente
desde la segunda falange hacia el centro de la palma,
de la misma forma que en el caso del toque libre,
de manera que los dedos funcionen en bloque, como una unidad.
Podemos percibir que la sensación neuromotora se ubica en el centro de la palma;
repetimos el movimiento hasta registrar e internalizar el mecanismo
y la fijación del grupo muscular.

A continuación, incorporamos el pulgar al mecanismo:
simultáneamente con la articulación del toque de \textit{i m a},
hundimos el pulgar en el ``tubo'' imaginario,
tal como lo hicimos al construir el toque libre.
Notemos que el mecanismo consiste en articular desde las falanges,
sin intervención de la mano o el brazo.

Luego de unas cuantas repeticiones,
estamos en condiciones de llevar el mecanismo al instrumento:
presentamos la mano derecha
de manera que el pulgar quede enfrentado a la 4ª cuerda,
y los dedos \textit{i m a}, en bloque,
enfrentados a las cuerdas 3ª, 2ª y 1ª respectivamente.
A continuación,
reproducimos el mecanismo del toque
buscando que las cuatro cuerdas suenen simultáneamente.
Prestemos atención, además,
a la dinámica de las notas individuales del acorde:
las cuatro cuerdas deberían sonar con igual intensidad,
sin que ninguna se destaque o suene más débilmente que el resto.
Repetimos el ejercicio cuantas veces sea necesario,
corrigiendo la colocación de la mano
hasta lograr el resultado óptimo.

Es muy importante
controlar
que no aparezca ningún movimiento parasitario de la mano o el brazo;
por ejemplo, es muy habitual
tocar acordes plaqué
``tirando'' de las cuerdas hacia afuera,
en sentido perpendicular a la tapa.
Como ya hemos visto,
este movimiento hace que las cuerdas repercutan sobre la tastiera,
lo que sólo contribuye a estropear el timbre del sonido resultante.
Cuidemos, entonces,
que el mecanismo se afiance sin ningún ``rebote'' de la mano,
sobre todo cuando tocamos acordes repetidos en \hspace{0.2em}{\textnormal{\large{\textbf{\textit{f}}}}}.

\subsection{Configuraciones de mano derecha}

En situaciones musicales concretas,
lo más probable es que nos encontremos con diferentes configuraciones de mano derecha;
incorporaremos entonces como ejercicio
la ejecución de acordes \textit{plaqué}
utilizando las diferentes combinaciones de dedos de la mano derecha, en diferentes cuerdas.

\lilyfigure{H}{Ex_acordes_plaque_con_pulgar}{Configuraciones de acordes \textit{plaqué} utilizando el pulgar.}{exAcPlaqueConPulgar}
\lilyfigure{H}{Ex_acordes_plaque_sin_pulgar}{Configuraciones de acordes \textit{plaqué} sin utilizar el pulgar.}{exAcPlaqueSinPulgar}

En la figura \ref{exAcPlaqueConPulgar}
se muestran las posibles variantes de digitación de mano derecha
combinando el pulgar con los dedos \textit{i m a};
la figura \ref{exAcPlaqueSinPulgar}
utiliza sólo los dedos \textit{i m a}.
Estas configuraciones pueden practicarse en diferentes cuerdas,
variando el acorde o aplicándolas a acordes en sucesión,
como paso previo a la lectura de estudios y obras.
Asimismo, el pulgar puede trabajar sobre diferentes cuerdas graves,
como se muestra en la figura \ref{exAcPlaqueAltPulgar}.

\lilyfigure{H}{Ex_acordes_plaque_alt_pulgar}{Acordes \textit{plaqué}: trabajo del pulgar en diferentes cuerdas.}{exAcPlaqueAltPulgar}

En las figuras \ref{exAcPlaqueConPulgar} y \ref{exAcPlaqueSinPulgar},
las configuraciones marcadas con \textbf{(*)}
presentan una dificultad adicional,
ya que al no estar en contacto los dedos que realizan el toque
se hace más trabajoso
lograr la sensación de bloque, o grupo muscular.
Esto es particularmente notable en los casos de
\textit{acordes con separación de dedos},
que requieren un trabajo técnico diferenciado.
La figura \ref{exAcPlaqueSepDedos}
muestra algunos ejemplos típicos de este tipo de configuración.

\lilyfigure{H}{Ex_acordes_plaque_sep_dedos}{Acordes \textit{plaqué} con separación de dedos.}{exAcPlaqueSepDedos}

El desafío, en estos casos,
consiste en lograr que los dedos trabajen en bloque
aun sin estar en contacto entre sí.
La medida del éxito de este trabajo
estará en la capacidad de controlar la simultaneidad del toque
y de igualar la intensidad de las notas individuales.

