**Acentos (>)**

- Contexto (acentos en **_p_** y en **_f_**).
- Toque apoyado como alternativa.
- Experimento: acentuar diferentes subdivisiones en una sucesión (regular o no) de semicorcheas.
- *Marcato*: acento fuerte + *staccato*.
