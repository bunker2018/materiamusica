
\begin{quote}
Parece lógico comenzar el estudio del mecanismo por la consideración de la posición más adecuada.
Esto implica, de hecho, considerar qué relación debe tener la guitarra con nuestro cuerpo. La frase de Carlevaro,
“La guitarra debe ajustarse al cuerpo, y no el cuerpo a la guitarra”, parece en principio un punto de partida
razonable. De acuerdo a las consideraciones expuestas en la Introducción, se tratará de describir la posición,
siempre que sea posible, exclusivamente en términos de las sensaciones neuromotoras experimentadas.
¿Qué requisitos debería cumplir una posición para ser considerada óptima? Parecería sensato establecer que:

\begin{itemize}
\item[-]{la posición de tocar debe ser estable;}
\item[-]{debe ser posible mantenerla sin un esfuerzo excesivo;}
\item[-]{debe mantener el instrumento estable con respecto al cuerpo;}
\item[-]{debe permitir realizar cómodamente todos los movimientos necesarios para la ejecución.}
\end{itemize}

Y ahora, aunque pueda parecer redundante, creo conveniente tratar de ir reduciendo poco a poco las
 posiciones posibles. Los requisitos establecidos nos llevarán a descartar desde el principio las posiciones que
 impliquen que el ejecutante toque de pie, incluso si se sostiene el instrumento con una banda colgada del
 cuello, ya que si bien la posición del cuerpo es estable, no permite mantener el instrumento estable con
 respecto al cuerpo, y esto dificulta el trabajo de la mano izquierda además de obligar al brazo derecho a
 invertir parte de su esfuerzo en el simple mantenimiento de la posición.Veamos más bien si es posible
 encontrar una posición en la cual los brazos tengan que realizar el menor esfuerzo posible para mantener el
 instrumento en su lugar; si no es así, los músculos utilizados para evitar que el instrumento pierda estabilidad
 no estarán disponibles para la ejecución propiamente dicha.
 
 El intento de Aguado de establecer una nueva posición por medio del uso de un aparato que sostiene
 el instrumento mientras que el ejecutante está de pie, no ha tenido históricamente éxito, a pesar de su sustancial
 lógica y del apoyo entusiasta de nada menos que Fernando Sor, y no parece posible que un cambio tan
 sustancial en el hardware de nuestro oficio se imponga generalmente. Consideremos, entonces, diversas
 variantes de posición en las cuales el ejecutante está sentado.
 
 A lo largo de la historia ha habido distintas sugerencias en este sentido, desde el cojín para apoyar el
 pie izquierdo recomendado por Ferdinando Carulli hasta los modernos artefactos que se apoyan sobre el
 muslo izquierdo del ejecutante. En este último caso, probablemente la situación no es muy diferente de
 cuando se utiliza un banquillo para apoyar el pie izquierdo y levantar el muslo izquierdo. Esta última posición
 es hoy en día utilizada por una abrumadora mayoría de guitarristas, y no está de más preguntarse cuáles son
 las razones de esta popularidad. Es claro que si se adopta una posición sentada, y no se eleva el instrumento
 (por el medio que sea) la mano derecha se verá constreñida en su acción por el esfuerzo que el brazo debe
 realizar para mantenerla en la cercanía de las cuerdas; o bien, supuesto que el brazo derecho está colocado de
 modo que la mano pueda actuar más o menos libremente, al no estar elevado el instrumento, la acción de la
 mano izquierda se verá afectada negativamente; o aún, si las piernas se colocan de modo que el punto de
 contacto del instrumento con ellas sea sobre el muslo izquierdo, y se eleva este punto cambiando la posición
 de las piernas (por ejemplo cruzándolas), el cuerpo estará en una situación tan incómoda como para hacer la
 ejecución bastante dificultosa al cabo de algún tiempo. Es necesario, en efecto, tener en cuenta que debe ser
 posible tocar durante un período relativamente prolongado sin que se produzcan dolores o tensiones
 musculares.
 
 La guitarra, entonces, debe ser elevada de alguna manera para que ambas manos puedan trabajar sin
 dificultad. Históricamente, se ha tendido a elevarla del lado izquierdo del ejecutante, o bien a elevar de algún
 modo el muslo izquierdo del guitarrista, lo que en resumidas cuentas es muy similar. No es difícil ver por
 qué: si nos planteamos la posibilidad de elevar la pierna derecha, veremos que el brazo derecho quedará en
 una posición sumamente tensa, y, aún peor, el instrumento quedará en una posición decididamente inestable,
 obligando a que el brazo izquierdo colabore en sostenerlo y dificultando así el desplazamiento de una posición
 a otra de la mano izquierda durante la ejecución.
 
 Hemos llegado entonces a la conclusión de que la posición más conveniente implica tocar sentados,
 apoyando el pie izquierdo sobre un banquillo y la guitarra sobre el muslo izquierdo, o bien con algún artefacto
 o cojín colocado sobre el muslo izquierdo. Ahora bien, ¿cuál de entre las diversas posiciones que cumplen
 con estos requisitos adoptaremos?
 
 Para elegir la posición que más nos convenga, realicemos un primer experimento. Como éste requerirá
 que prestemos atención a nuestro cuerpo y a nuestras sensaciones cenestésicas, es quizás recomendable
 dedicarle un cierto tiempo y proceder con mucha calma.
 
 Sentémonos en una silla plana, sin descansar la espalda en el respaldo. Examinemos nuestras
 sensaciones musculares, tomándonos todo el tiempo que necesitemos (cerrar los ojos puede ayudar a
 concentrarnos). Permanezcamos en la posición adoptada un cierto tiempo, el que sea necesario para comprobar
 si se producen tensiones o si sentimos cansancio en alguna parte del cuerpo. Si hay tensiones, intentemos
 eliminarlas tensando voluntariamente el músculo en cuestión y relajándolo luego, o intentando algún cambio
 en la posición. La vivencia de la sensación corporal no puede ser suplida con palabras, y es necesario
 experimentarla. Continuemos con este proceso hasta estar seguros de haber eliminado las tensiones tanto
 como nuestra fineza de percepción y la condición preestablecida de estar sentados nos lo permitan.
 
 La posición así conseguida será nuestra base. Coloquemos ahora un banquillo para apoyar el pie
 izquierdo en él. Si mantenemos la sensación básica de posición alcanzada en el experimento anterior, muy
 probablemente percibamos un cambio en la constelación neuromotora. Para compensar este cambio, puede
 ser conveniente colocar el pie derecho algo más atrás y desplazarlo hacia la derecha. Esto, a su vez, puede
 hacer necesario colocarse algo más a la derecha en la silla. Repitamos ahora el experimento inicial, tratando
 de eliminar cualquier tensión que pueda haberse producido, y de refinar nuestra sensación neuromotora de
 la posición alcanzada. Es conveniente repetir este proceso algunas veces, para que la sensación correspondiente
 quede archivada en nuestra memoria.
 
 Coloquemos ahora el instrumento en posición. La curvatura del aro inferior quedará apoyada sobre
 el muslo izquierdo, y el borde derecho del aro quedará apoyado contra la parte interior del muslo derecho.
 Dependiendo de la altura del banquillo, la de la silla y la longitud de las piernas del ejecutante, lo más probable
 es que la parte superior-posterior del aro se apoye suavemente sobre el pecho del ejecutante.
 Observaremos que el instrumento no queda colocado en posición horizontal, sino inclinado en un
 ángulo que hace que el clavijero quede más elevado que el puente. Esta inclinación variará de acuerdo con la
 longitud de las piernas del ejecutante, la altura del banquillo, y la de la silla.
 Apoyemos ahora el brazo derecho, entre la muñeca y el codo, sobre el aro superior-anterior. El brazo
 debe quedar apoyado exclusivamente por su propio peso, su punto de contacto con el instrumento debe
 permitir a la mano derecha alcanzar las cuerdas con comodidad (probablemente el punto de contacto se sitúe
 más bien cerca del codo). Repitamos nuestra comprobación de la sensación muscular, eliminando las tensiones
 que se puedan haber producido. El brazo izquierdo debe estar completamente relajado durante este proceso.
 
 Podría ocurrir que en este proceso de construcción de la posición hubiéramos llegado a una posición
 bastante diferente de aquella a la cual estamos habituados. En ese caso, sería interesante intentar colocarnos
 en nuestra posición habitual y comparar la constelación de sensaciones neuromotoras a la que ella corresponde
 con la que acabamos de construir.
 
 Descansemos algunos minutos, y repitamos la construcción de la posición; probablemente esta
 segunda vez el proceso sea notoriamente más rápido.
 
 A esta altura quedará probablemente claro que no hay ni puede haber dos posiciones absolutamente
 idénticas, dada la diferencia en las proporciones corporales entre las distintas personas. No es para nada
 seguro que la posición apropiada para el señor X sirva para la señora Y. Si se intenta copiar una posición
 ajena, lo que se obtendrá será casi seguramente una constelación diferente de sensaciones neuromotoras.
 Dicho de otro modo, posiciones idénticas (vistas desde afuera) se vivenciarán de maneras totalmente diferentes.
 En el fondo, la posición es tan personal como el propio cuerpo, y por eso es necesario que cada ejecutante
 construya la suya.

\flushright{\textit{(Extracto del libro `Técnica, Mecanismo, Aprendizaje' de Eduardo Fernández)}}
\end{quote}
