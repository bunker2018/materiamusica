#!/bin/bash

while true; do
  f="$1"
  [ -n "$f" ] || break
  d="$(echo "$f" | sed -e 's@___NOTFOUND/@@').md"
  mv -v "$f" "$d"
  shift
done

