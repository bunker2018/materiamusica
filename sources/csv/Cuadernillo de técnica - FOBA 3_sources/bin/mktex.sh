#!/bin/bash

while true; do case $1 in
  -v|--vim|-e|--edit) DO_EDIT=1; shift;;
  --) shift; break;;
  *) break;;
esac; done

while true; do
  f="$1"
  [ -n "$f" ] || break
  d="$(echo "$f" | sed -e 's@___NOTFOUND/@@').tex"
  shift
  mv -v "$f" "$d"
done

if [ $DO_EDIT ]
then vim "$d"
fi
