- Golpe seco sobre el punto exacto (lo más cerca posible del traste).
- Acción perpendicular al plano de las cuerdas.
- Acción de los grupos musculares según la movilidad disponible.
- Experimento: dar vuelta la guitarra; percutir con la yema del dedo.
- Ligado **3 4** más difícil.
