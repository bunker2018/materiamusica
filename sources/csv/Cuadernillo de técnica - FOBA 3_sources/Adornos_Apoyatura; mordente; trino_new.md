- Lo más rápido posible, _pero no siempre_ (ref. Apéndice).
- Sincronización, _no_ velocidad.
- Practicarlo demasiado lento no funciona, salvo para comprender el movimiento.

**Mordente**

- Un solo gesto integrando sendos ligados.
- Movimiento breve, controlado, pero enérgico.

**Trino**

- Comenzar con pocas repercusiones, a velocidad moderada.
- No superar el límite tras el cual la tensión se acumula en exceso.
