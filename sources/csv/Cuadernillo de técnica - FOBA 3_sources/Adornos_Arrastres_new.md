- Diferentes variantes (ref. Apéndice):
    - toco y deslizo;
    - toco, deslizo y toco.
- Variantes de velocidad: arrastre lento, rápido, variable.
- Mantener la presión sobre la cuerda pero no en exceso.
- Ojo la posición del pulgar durante el arrastre.
