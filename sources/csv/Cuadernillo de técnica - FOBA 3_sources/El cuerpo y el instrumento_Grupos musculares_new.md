- Concepto, si bien en apariencia teórico, importantísimo para la percepción de la técnica.
- Definición de grupos musculares mayores y menores. Concepto: usar el mayor grupo muscular disponible.
- Ejemplos: empujar un auto, pararnos sobre los pedales de la bicicleta, ...
- Ejemplos simples relacionados a la guitarra: toque apoyado, ligado descendente, rasgueo, distensión.

### Concepto de fijación

- Grupos musculares trabajando como una unidad.
- Localización de la sensación en la articulación.
- Fijación sin tensión.
