- Dedos aproximadamente paralelos al traste.
- Codo arriba, sin tensionar el hombro.
- No quebrar la muñeca, mantener la linealidad del brazo. Pulgar bien situado detrás de la mano.

**Presentación transversal invertida**

- Dedo 1 más alto que el dedo 4 (similar a la presentación transversal con el ángulo opuesto).
- Codo contra el cuerpo, manteniendo la linealidad de la muñeca y el brazo.

