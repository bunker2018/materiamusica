- En la cuerda de nylon, vibramos en el sentido de la cuerda, no en el sentido
  del traste como en la cuerda de acero.
- Experimento: despegar el pulgar y balancear la mano hasta sentir la variación de altura.
- Probar a diferentes velocidades.
- Caracterización expresiva de los diferentes vibratos: nervioso, calmo, etc.
- Expresividad del _non vibrato_.
