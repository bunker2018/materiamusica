* Referencias bibliográficas:
    - Eduardo Fernández: *Técnica, Mecanismo, Aprendizaje*.
* La observación de la sensación neuromotora.
* Técnica:
    - estable (debemos poder tocar durante un tiempo prolongado);
    - eficiente (economía de esfuerzo y de movimientos);
    - adaptada a las características de cada guitarrista.
* Técnica básica, no técnica académica.
* Para cada aspecto técnico:
    - Descripción.
    - Experimentos.
    - Ejercicios.
    - Lecturas, obras, estudios, etc.
* Apéndices: profundización de determinados conceptos o áreas de estudio.
