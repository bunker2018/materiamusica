\label{toquelibre}
%%% Cuadernillo de técnica - FOBA 3_sources/Mano derecha_Toques_Toque libre

En el \textit{toque libre},
los dedos \textit{i m a} articulan desde la segunda falange en dirección al centro de la palma;
este es el toque más comúnmente usado en la ejecución de arpegios y líneas melódicas.
Tiene la particularidad de que la mano trabaja ``en el aire'',
sin hacer contacto más que con la cuerda que se pulsa,
y por lo tanto sin interrumpir la vibración de las demás cuerdas.

Para efectuar el toque, partimos de la que llamamos \textit{posición de ataque},
esto es, el dedo que pulsa la cuerda enfrentado a la misma a unos pocos milímetros de distancia;
desde esta posición, articulamos pulsando la cuerda en un movimiento seco y preciso,
culminando el movimiento en la \textit{posición de salida},
idealmente a unos pocos milímetros de la cuerda que acabamos de pulsar,
es decir que el movimiento no debe ser demasiado amplio,
y en ningún caso nos permitiremos rozar la cuerda adyacente.
Por ejemplo, si pulsamos la 2ª cuerda,
la posición de salida se ubicará en algún punto entre la 2ª y la 3ª cuerdas,
sin llegar a rozar esta última.

Generalmente el toque se realiza alternando dedos, por ejemplo \textit{i m i m}.
Lo ideal, y esto cobra especial relevancia en pasajes rápidos,
es que los movimientos de articular una nota y preparar el dedo para la articulación de la siguiente nota
se produzcan en forma simultánea.
Dicho de otro modo,
cuando el dedo \textit{i} llega a la posición de salida,
el dedo \textit{m} ya se halla ubicado en la posición de ataque correspondiente.
Es conveniente practicar esto muy lentamente hasta incorporar el mecanismo de forma natural.

Es importante que en esta alternancia de toques
la mano no proceda ``a saltos'';
idealmente, la mano debería mantenerse prácticamente inmóvil,
salvo para corregir la presentación en función del pasaje.
Es muy común que en pasajes de acordes plaqué o notas repetidas,
principalmente en \hspace{0.2em}{\textnormal{\large{\textbf{\textit{f}}}}},
la mano tienda a ``rebotar'':
estos movimientos parasitarios representan un gasto innecesario de energía,
además de comprometer la calidad del toque y la precisión y el control de los mecanismos.
De hecho, el movimiento de ``rebote'' de la mano le imprimirá a la cuerda una vibración
en sentido perpendicular al plano de las cuerdas,
en detrimento de la calidad del sonido resultante (ver el capítulo \ref{mdconstruccion}).
Es recomendable trabajar el mecanismo en diferentes dinámicas y \textit{tempi}
hasta lograr que la mano se mantenga naturalmente en la posición correcta,
sin rigidez ni tensiones innecesarias.

El toque libre con uña puede efectuarse presentando el dedo en diferentes ángulos en relación a la cuerda:
si el toque se realiza en sentido perpendicular a la cuerda,
tomando la cuerda con todo el ancho de la uña,
resultará un sonido más brillante (rico en armónicos)
que si presentamos el dedo en un ángulo, pongamos por ejemplo, de 45$^{\circ}$.
La variación tímbrica es bastante similar a la que procede de tocar cerca del puente o de la boca,
sólo que mucho más sutil.

\subsubsection{Toque libre de pulgar}

Para realizar el toque libre de pulgar,
partimos de los mismos principios básicos que en el toque libre con dedos \textit{i m a}:
presentamos la uña o la yema del pulgar a unos pocos milímetros de la cuerda
(\textit{posición de ataque}),
y efectuamos el toque en un solo movimiento seco y preciso \textit{hacia abajo}
(en el sentido del plano de las cuerdas),
sin llegar a rozar la cuerda adyacente.

Una vez efectuado el toque,
buscaremos colocar el pulgar frente a la cuerda que queremos tocar a continuación,
lo más rápida y naturalmente que sea posible.
Para esto, continuamos el movimiento del dedo en forma circular,
como si dibujáramos un círculo en el aire con la yema del pulgar,
hasta que el dedo se halle en la posición de ataque que buscamos.
Es importante practicar este mecanismo hasta lograr un movimiento fluido y continuo;
en particular, cuando trabajemos arpegios o pasajes a dos voces,
nos ocuparemos de efectuar este movimiento de preparación de la posición del pulgar
en forma simultánea con el trabajo de los demás dedos,
hasta desarrollar la independencia necesaria.

Podemos incorporar el movimiento básico
tocando repetidamente una cuerda grave con el pulgar,
describiendo un círculo con la yema entre toque y toque,
en un solo movimiento continuo,
hasta internalizar el mecanismo y poder realizarlo sin necesidad de controlarlo en forma consciente.

En el caso del pulgar, a diferencia de los dedos \textit{i m a},
disponemos de dos articulaciones para efectuar el toque:
articulando desde la segunda falange,
o bien desde la unión del pulgar con la mano (\textit{fijando} la segunda falange).
En el segundo caso, estaremos utilizando un \textit{grupo muscular} mayor
(ver \ref{gruposmusculares}),
con el correspondiente efecto en el timbre y la dinámica resultantes.

