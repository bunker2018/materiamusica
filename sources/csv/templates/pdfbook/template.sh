#!/bin/bash

#; Example template - short description here
#: Template documentation here

me="$1"
input="$2"
wdir="$3"
mydir="$4"
sdir="${wdir}_sources"

parsed_input="${wdir}/LaTeX-output.tex"
predefined_header="${sdir}/LaTeX-header.tex"
predefined_footer="${sdir}/LaTeX-footer.tex"
msg2 "Extracting parsed file $parsed_input"

output="${wdir}/book.tex"

echo -n >"$output"

{
  if [ -f "$predefined_header" ]
    then cat "$predefined_header";
    else cat "${mydir}/header.tex"; fi;
  cat "$parsed_input"
  if [ -f "$predefined_footer" ]
    then cat "$predefined_footer";
    else cat "${mydir}/footer.tex"; fi;
} >"$output"

msg2 "Running pdflatex"

if [ $DEBUG ]
  then env LANG=es_AR.UTF-8 pdflatex "$output";
  else env LANG=es_AR.UTF-8 pdflatex "$output" >/dev/null 2>&1; fi;

