#!/bin/bash

. ./lib/colors.sh

msg() { echo -e "$@" >> /dev/stderr; }
plainmsg() { msg "${plain_prompt}${@}${plain_postprompt}${reset}"; }
msg1() { msg "${msg1_prompt}${@}${msg1_postprompt}${reset}"; }
msg2() { msg "${msg2_prompt}${@}${msg2_postprompt}${reset}"; }
msg3() { msg "${msg3_prompt}${@}${msg3_postprompt}${reset}"; }
plainmsg() { msg "${plain_prompt}${@}${plain_postprompt}${reset}"; }
infomsg() { msg "${info_prompt}${@}${info_postprompt}${reset}"; }
info1msg() { msg "${info1_prompt}${@}${info1_postprompt}${reset}"; }
info2msg() { msg "${info2_prompt}${@}${info2_postprompt}${reset}"; }
info3msg() { msg "${info3_prompt}${@}${info3_postprompt}${reset}"; }
warnmsg() { msg "${warn_prompt}${@}${warn_postprompt}${reset}"; }
errmsg() { msg "${err_prompt}${@}${err_postprompt}${reset}"; }
abort() { errmsg "$2"; exit $1; }

