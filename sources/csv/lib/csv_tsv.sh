#!/bin/bash

run_parser() {
  parser=$1
  input="$2"
  wdir="$3"
  if [ -d ./parsers/$parser ]
  then # Run parser from ./parsers/
    pdir=./parsers/$parser
    msg1 "Running parser '$parser'"
    info1msg "Input file: '$input'"
    info1msg "Working directory: '$wdir/'"
    info1msg "Parser directory: '$pdir/'"
    . ${pdir}/parser.sh "$parser" "$input" "$wdir" "$pdir"
  else # Parser not found
    warnmsg "Parser '$parser' not found"
  fi
}

apply_template() {
  template=$1
  input="$2"
  wdir="$3"
  if [ -d ./templates/$template ]
  then # Run template from ./templates/
    tdir=./templates/$template
    msg1 "Applying template '$template'"
    info1msg "Input file: '$input'"
    info1msg "Working dir: '$wdir/'"
    info1msg "Template directory: '$tdir/'"
    . ${tdir}/template.sh "$template" "$input" "$wdir" "$tdir"
  else # Template not found
    warnmsg "Template '$template' not found"
  fi
}

parse_file() {
  f="$1"
  if [ ! -f "$f" ]
    then warnmsg "$f: file not found"; return; fi;
  plainmsg "Parsing $f"

  ftag="${f/.csv/}"
  mkdir -p "$ftag"
  ftitle="$(echo "$ftag" | cut -d'-' -f1 | sed -e 's/ $//')"
  fsubtitle="$(echo "$ftag" | cut -d'-' -f2 | sed -e 's/ //')"
  run_parser $PARSER "$f" "$ftag"
  apply_template $TEMPLATE "$f" "$ftag"
}

parse_filelist() {
  cat .filelist | while read f
  do
    dos2unix "$f"
    parse_file "$f"
  done
}

