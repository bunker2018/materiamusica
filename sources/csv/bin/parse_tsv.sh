#!/bin/bash

# Load helper code

. ./lib/common.sh
. ./lib/csv_tsv.sh

# Defaults

ACTION=parse
PARSER=LaTeX
TEMPLATE=pdfbook
DEBUG=

# Default configuration

plain_prompt="${bold}${cyan} :: "
msg1_prompt="${bold}${green} => ${bold}${white}"
msg2_prompt="${green}  -> ${bold}${white}"
msg3_prompt="${bold}${blue}   -> ${white}"
info_prompt="${blue}${bold} * ${reset}${blue}"
info1_prompt="${blue}${bold}    * ${reset}${blue}"
info2_prompt="${blue}${bold}      * ${reset}${blue}"
info3_prompt="${blue}${bold}        * ${reset}${blue}"
warn_prompt="${yellow}${bold} WARNING: ${white}"
err_prompt="${red}${bold} ERROR: ${white}"
plain_postprompt=
msg1_postprompt=
msg2_postprompt=
msg3_postprompt=
info_postprompt=
info1_postprompt=
info2_postprompt=
info3_postprompt=
warn_postprompt=
err_postprompt=

# Load config if exists

if [ -f ./config.sh ]
  then . ./config.sh; fi;

# Command line options

while true; do case $1 in

-p|-parse) ACTION=parse; shift;;
-a|--action) ACTION=$2; shift 2;;

--debug) DEBUG=1; shift;;
-h|--help|-?) ACTION=help; shift;;

--) shift; break;;

*) break;;

esac; done

# Set environment

unset HAVE_FILES
echo -n >.filelist

# Pick arguments

for f in "$@"
do
  # .filelist: one line per input file
  echo $f >> .filelist
  HAVE_FILES=1
done

case $ACTION in
  help)
    show_help; exit 0;;
  parse)
    if [ ! $HAVE_FILES ]
      then abort 1 "No arguments"; fi;
    parse_filelist;;
  *)
    abort 1 "Nothing to do";;
esac

msg1 "Done."

