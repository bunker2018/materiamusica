#!/bin/bash

#; Example parser - short description here
#: Parser documentation here

include_tex_source() {
  _this_tag="${appendix}${this_chapter}_${this_section}_${this_subsection}"
  if [ -n "$2" ]
    then tex_source="${sources}/${2}"; _this="$2";
    else tex_source="${sources}/${_this_tag}"; _this="$_this_tag"; fi;
  if [ -f "${tex_source}.tex" ]
  then
    info3msg "Inserting LaTeX source file: '${tex_source}.tex'"
    echo; echo "%%% File: ${tex_source}.tex";
    cat "${tex_source}.tex";
    echo "%%% End of file ${tex_source}.tex"; echo;
  elif [ -f "${tex_source}.md" ]
  then
    info3msg "Inserting Markdown source file: '${tex_source}.md'"
    echo; echo "%%% File: ${tex_source}.md";
    pandoc -r markdown -w latex "${tex_source}.md";
    echo "%%% End of file ${tex_source}.md"; echo;
  else
    echo "%%% Source file '${tex_source}' not found"; fi;
    echo "%%% ${tex_source}" >"${empty_sources}/$_this";
}

include_tex_subsource() {
  if [ -n "$2" ]
  then
    _this_tag="${2}"
  else
    _this_tag="${appendix}${this_chapter}_${this_section}_${this_subsection}_${1}"
  fi

  tex_source="${sources}/${_this_tag}"
  if [ -f "${tex_source}.tex" ]
  then
    info3msg "Inserting LaTeX source file: '${tex_source}.tex'"
    if [ -f "${mydir}/${1}.tcolorbox" ]
      then echo "\\vspace{1em}\\begin{tcolorbox}$(< "${mydir}/${1}.tcolorbox")";
      else echo "\\vspace{1em}\\begin{tcolorbox}"; fi;
    echo "\\subsubsection{$1}"
    echo; echo "%%% File: ${tex_source}.tex";
    cat "${tex_source}.tex";
    echo "%%% End of file ${tex_source}.tex"; echo;
    echo "\\end{tcolorbox}"
  elif [ -f "${tex_source}.md" ]
  then
    info3msg "Inserting Markdown source file: '${tex_source}.md'"
    if [ -f "${mydir}/${1}.tcolorbox" ]
      then echo "\\vspace{1em}\\begin{tcolorbox}$(< "${mydir}/${1}.tcolorbox")";
      else echo "\\vspace{1em}\\begin{tcolorbox}"; fi;
    echo "\\subsubsection{$1}"
    echo; echo "%%% File: ${tex_source}.md";
    pandoc -r markdown -w latex "${tex_source}.md";
    echo "%%% End of file ${tex_source}.md"; echo;
    echo "\\end{tcolorbox}"
  else
    echo "%%% Subsource file '${tex_source}' not found"; fi;
    echo "%%% ${tex_source}" >"${empty_sources}/$_this_tag";
}

make_chapter() {
  if [ $appendix ]
    then info2msg "New appendix: '$1'";
    else info2msg "New chapter: '$1'"; fi;
  this_chapter="$1"
  this_section="new"
  this_subsection="new"
  echo "\\chapter{${1}}"
  include_tex_source
}

make_section() {
  info2msg "New section: '$1'"
  this_section="$1"
  this_subsection="new"
  echo "\\section{${1}}"
  include_tex_source
}

make_subsection() {
  info2msg "New subsection: '$1'"
  this_subsection="$1"
  echo "\\subsection{${1}}"
  include_tex_source
}

make_abstract() {
  tex_source="${sources}/abstract";
  if [ -f "${tex_source}.tex" ]
  then
    info3msg "Inserting abstract from LaTeX: '${tex_source}.tex'"
    echo "\\chapter*{Prefacio}"
    echo; echo "%%% File: ${tex_source}.tex";
    cat "${tex_source}.tex";
    echo "%%% End of file ${tex_source}.tex"; echo;
  elif [ -f "${tex_source}.md" ]
  then
    info3msg "Inserting abstract from Markdown source: '${tex_source}.md'"
    echo "\\chapter*{Prefacio}"
    echo; echo "%%% File: ${tex_source}.md";
    pandoc -r markdown -w latex "${tex_source}.md";
    echo "%%% End of file: ${tex_source}.md"; echo;
  else
    echo "%%% Abstract '${tex_source}' not found"; fi;
    echo "%%% ${tex_source}" >"${empty_sources}/abstract";
}

me="$1"
input="$2"
wdir="$3"
mydir="$4"

msg2 "Parsing input"

sources="${wdir}_sources"
empty_sources="${sources}/___NOTFOUND"
mkdir -p "$empty_sources"
output="${wdir}/LaTeX-output.tex"
echo -n >"$output"
headers="$(head -n1 "$input")"
this_chapter="new"
this_section="new"
this_subsection="new"
appendix=

make_abstract >>"$output"

l=1
cat "$input" | sed -n 2,\$p | while read line
do
  l=$(($l+1))
  
  if echo "$line" | grep EOF >/dev/null 2>&1
  then break; fi

  if echo "$line" | grep APPENDIX >/dev/null 2>&1
  then
    info1msg "Opening appendix section"
    echo "\\appendix" >>"$output"
    echo >>"$output"
    appendix="APPENDIX_"
    continue
  fi

  chapter="$(echo "$line" | cut -d, -f1)"
  section="$(echo "$line" | cut -d, -f2)"
  subsection="$(echo "$line" | cut -d, -f3)"

  if [ -n "$chapter" ]
    then make_chapter "$chapter" >>"$output"; fi;
  if [ -n "$section" ]
    then make_section "$section" >>"$output"; fi;
  if [ -n "$subsection" ]
    then make_subsection "$subsection" >>"$output"; fi;
  
  c=3
  while true
  do
    c=$(($c+1))
    _subsub="$(echo "$headers" | cut -d, -f$c)"
    if [ -z "$_subsub" ]
      then break; fi;
    _subsubdef="$(echo "$line" | cut -d, -f$c)"
    include_tex_subsource "$_subsub" "$_subsubdef" >>"$output"
  done
done
