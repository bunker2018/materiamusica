---
documentclass: extarticle
fontsize: 12pt
---

## 2.3 MANO IZQUIERDA

_(Extracto del libro **Técnica, Mecanismo, Aprendizaje** de Eduardo Fernández)_

### 2.3.1 Presentación básica (longitudinal) y construcción del traslado

La estrategia que seguiremos en el tratamiento del mecanismo de la mano izquierda será relativamente
simple; partiremos de una posición base, que llamaremos presentación 1ongitudinal. En esta presentación,
los cuatro dedos estarán situados en espacios adyacentes y en una misma cuerda. Una vez establecida esta
posición, ampliaremos luego nuestro estudio al campo de las otras presentaciones posibles y a las diversas
acciones de la mano izquierda.

#### 2.3.1.1 La construcción de la presentación longitudinal

En esta construcción partiremos de un doble postulado básico; la presentación debe ser a la vez
estable (lo que implica que sea posible mantenerla por un tiempo prolongado sin realizar un esfuerzo excesivo)
y eficiente (las acciones necesarias para colocar y mantener la presentación deben realizarse con el mínimo
esfuerzo muscular posible).

Para que la condición de eficiencia se satisfaga nos conviene que la presión ejercida por los dedos sobre la cuerda sea mínima.
De aquí se desprenden dos conclusiones:

1. cuanto más cerca del traste se coloque el dedo, más eficiente será la acción, y
2. la fuerza debe ejercerse en sentido perpendicular al plano de las cuerdas.

Si el esfuerzo se realizara en una dirección diagonal, la cuerda se
desplazaría hacia arriba o abajo, modificando además la entonación. Ejercer la fuerza en dirección perpendicular
al plano de las cuerdas conviene además a la estabilidad de la presentación; la misma acción ayuda a mantener
el dedo en posición.

Si aceptamos este razonamiento, parece lógico que para que el esfuerzo se ejerza en la dirección
deseada es conveniente que la última falange de cada dedo (o sea, aquella que está en contacto con la cuerda)
llegue a la cuerda en una dirección perpendicular al diapasón. Dada la gran diversidad anatómica entre las
manos, parece claro que no es posible ni deseable describir una postura general de la mano con demasiada
precisión. La idea de un tubo imaginario nos puede ser útil, como también las similitudes de la acción de los
dedos de la mano izquierda con la que se opera en un teclado, musical o no (por ejemplo, el de una máquina
de escribir).

Tendremos en cuenta, además, el papel que juega el pulgar de la mano izquierda en presentar y
mantener la posición. Por el momento,
podemos considerar que las acciones del pulgar serán un apoyo para la acción de los dedos directamente
operantes (1, 2, 3 y 4).

Una definición es necesaria antes de comenzar el primer ejercicio de mano izquierda: llamaremos
**posición** al número del espacio _en el cual está colocado el dedo 1_.

#### Ejercicio de colocación

1. Imagínese que la mano izquierda está enrollada alrededor de un tubo, de tal modo que las últimas
falanges de los dedos 1, 2, 3 y 4 queden vueltas hacia el cuerpo en una dirección más o menos horizontal. El
pulgar se encontrará en oposición a los dedos 1, 2, 3 y 4, y el lugar exacto de su ubicación dependerá de las
características anatómicas de cada mano. Dicho de otro modo, el pulgar estará
situado en algún lugar frente a los dedos. Manteniendo esta presentación, rótese la mano de manera que las
puntas de los dedos queden apuntando al suelo. Déjese descansar las yemas de los dedos 1, 2, 3 y 4 en el
borde de una mesa de una altura cómoda; el pulgar quedará por debajo del borde.

2. Ejerzamos ahora presión con todos los dedos, mantengámosla un momento, y aflojemos en seguida.
Repitamos esta acción hasta adquirir una sensación de acción en bloque, o sea, hasta que la acción se perciba
como una sola acción de la mano, y no como una suma de acciones de los dedos.

3. Coloquemos ahora la guitarra sobre la misma mesa, o aún sobre las rodillas. El instrumento debe
quedar en posición horizontal, con las cuerdas y la tapa hacia arriba. Repitamos la acción de apretar y aflojar,
utilizando ahora el diapasón en vez de la mesa. La palma de la mano debe estar dirigida hacia abajo, o sea, no
se trata de la presentación “normal” utilizada durante la ejecución. No importa demasiado dónde elijamos
realizar el ejercicio, siempre que la posición del brazo nos resulte cómoda. Notaremos probablemente que la
zona del pulgar en contacto con la madera del diapasón será cercana a la cara interna del mismo (esto depende
naturalmente de la configuración de la mano).

4. Coloquemos ahora la guitarra verticalmente entre las piernas y con la tapa hacia adelante, imitando
la posición de un cellista con su instrumento. Repitamos el ejercicio, situando ahora la presentación en una
posición no inferior a la **V**, y en una cuerda grave (5ª o 6ª). El objetivo, al igual que en las fases previas, es
obtener la sensación de la acción en bloque.
¿Qué ocurre con el brazo en todo este proceso? El brazo tenderá naturalmente a facilitar la acción de
los dedos, elevando o bajando el codo, o modificando el ángulo de su articulación hasta que la muñeca se
encuentre en una curvatura suave (en verdad, el comienzo de una curva que termina en las puntas de los
dedos 1, 2, 3, y 4) y forme un ángulo de 180 grados con el antebrazo. Como observamos en el caso de la
mano derecha, es en ese ángulo que la acción de los dedos opera con mayor libertad.

5. Llevemos ahora la guitarra a su posición normal, coloquemos la presentación en **V** posición, y
repitamos el ejercicio.

6. Puede ser conveniente en este punto comprobar la ausencia de tensiones excesivas. Un ejercicio
posible sería tensar y relajar todo el brazo izquierdo (incluyendo el hombro). Otra ejercicio recomendable es
el siguiente:

    - Dejando la guitarra por el momento, sentémonos en la posición de ejecución, incluyendo la colocación
    de la mano izquierda (por así decir, estaremos tocando una guitarra imaginaria). Relajemos el brazo izquierdo
    hasta que caiga por su propio peso, y luego coloquémoslo de nuevo en posición. ¿Dónde se localiza la
    sensación de esta colocación? El lugar probable está en la región del bíceps más cercana al codo, entre el
    codo y el hombro. Repitamos varias veces el ejercicio hasta que la sensación esté perfectamente localizada.
    - Otro punto importante a mencionar, es que cuando se toque en posiciones superiores a la IX la
    presentación no puede ser la misma que para posiciones más bajas. El brazo, y aún a veces todo el cuerpo,
    debe ayudar para que las puntas de los dedos lleguen a la cuerda en dirección perpendicular al plano de las
    cuerdas; en este caso, la posición deberá necesariamente ajustarse.

7. Coloquemos ahora la posición habitual con la guitarra, incluyendo la presentación longitudinal de
mano izquierda, pero sin dejar que los dedos toquen las cuerdas ni que el pulgar presione sobre el mástil. La
sensación de colocación del brazo izquierdo debería ser igual a la que descubrimos en la fase 5, más arriba (si
no lo es, nuestro mecanismo previamente aprendido se está haciendo cargo de las cosas, y conviene volver a
realizar el ejercicio de la fase 5).

8. Permitamos ahora que los dedos toquen la cuerda escogida, que ejerzan presión sobre ella, y que el
pulgar actúe sobre el mástil. Al hacer esto, esto es, al colocar efectivamente la presentación, la sensación de
trabajo del brazo debería prácticamente desaparecer, y el nuevo centro de acción debería pasar a colocarse en
una franja en la parte superior de la palma, debajo del nacimiento de los dedos 1, 2, 3 y 4, al igual que en la
base del pulgar.
Ya que el desplazamiento de la sensación, o, dicho de otro modo, el desplazamiento del lugar en que
se percibe el centro de la acción, será necesario en etapas a venir, es conveniente practicarlo hasta que se
adquiera;  la adquisición y el control de la sensación deben ser el único objetivo. Puede incluso ser recomendable
realizar este ejercicio con ojos cerrados, para poder sintonizar nuestras sensaciones con más precisión. Cuando
lo único que se perciba al realizar el ejercicio sea el desplazamiento de lugar del centro de acción, estaremos
listos para continuar.

#### 2.3.1.2 La construcción del traslado longitudinal

Llamaremos traslado longitudinal al movimiento que desplaza la mano desde una posición a otra,
sobre la misma cuerda.

En un ejercicio preparatorio del traslado, las fases a seguir serían aproximadamente las siguientes:

- Colocar la presentación longitudinal en una posición cualquiera;
- Sin cambiar la presentación, dejar presente sólo la sensación del dedo 1 (y, por supuesto, la del
pulgar). El resultado será que sólo el dedo 1 quedará actuando sobre la cuerda, y los demás estarán presentados
en el aire frente a sus espacios respectivos - pero, una vez más, lo importante es la adquisición de la sensación.
- Desplazar la sensación al punto del brazo descubierto más arriba, al colocar la posición sin la guitarra.
El resultado será que toda la mano quedará presentada en el aire.
- Volver a localizar la sensación en la base del dedo 1 y del pulgar, y repetir varias veces todo este
proceso. El objetivo es adquirir la sensación de bloque de la colocación.

El pasaje de una posición a otra, o sea, el traslado propiamente dicho, será una acción de manejo de
sensaciones neuromotoras, al igual que todas las otras acciones del mecanismo. En nuestra subjetividad, que
es lo único con que contamos, la acción de un traslado no se puede describir diciendo que vamos de una
posición N a otra posición M; tal descripción sólo puede hacerse desde un punto de vista exterior. En cambio,
lo que hacemos al ejecutar un traslado es partir de un lugar construído por nuestras sensaciones neuromotoras
y visuales, y llegar a otro lugar de nuestra geografía mental, igualmente construído previamente.

Una consecuencia lógica de esto parece ser que para realizar un traslado debemos conocer
sensorialmente el lugar de partida y el de llegada. Según esta idea, es conveniente (como vimos más arriba)
que el brazo y la mano izquierda se ocupen de explorar todo el diapasón, construyendo así un equivalente
neuromotor del mismo. Este equivalente es el verdadero campo de acción del mecanismo, y no el instrumento
físico.

Apliquemos esta idea a un traslado longitudinal. Supongamos la **V** posición como punto de partida,
y cualquier posición más elevada como punto de llegada (por ejemplo la **VII** posición).
Coloquemos ambas posiciones de antemano y como ejercicios aparte, de forma de construir sensorialmente ambos puntos.

Con estos elementos establecidos, el traslado se hará recorriendo los siguientes pasos:

- colocamos la presentación longitudinal en el lugar inicial (**V**), y trasladamos la sensación al dedo 1;
- miramos el punto de llegada (por ejemplo, **VII**);
- desplazamos la sensación al brazo;
- hacemos que la presentación vaya a **VII**. Nuestro aparato neuromotor está entrenado para llevar el
brazo donde se mira desde una edad muy temprana, así que no debe preocuparnos el establecer
específicamente este movimiento;
- volvemos a desplazar la sensación al dedo 1.
- repetimos toda la secuencia hasta que se sienta como una unidad; en otras palabras, construimos un
bloque de acción para el movimiento de traslado.

Es claro que al realizar el traslado de este modo, no habrá fricción de los dedos sobre la cuerda y por
lo tanto el movimiento no será sólo eficiente sino además silencioso. Creo necesario insistir en que el manejo
de la sensación es la clave para el aprendizaje, y ruego al estudiante que se abstenga de tratar de ver el
movimiento “desde afuera”.

El ejercicio complementario del recién expuesto, o sea, el traslado longitudinal descendente, no es
difícil de construir siguiendo el mismo procedimiento.

Algunos pasos para la ampliación y generalización del traslado longitudinal pueden ser los siguientes:

1. Realizar el traslado ascendente con un dedo y el descendente con otro, en una secuencia repetible;
por ejemplo, 1 en III - 1 en V - 3 en V - 3 en III.
2. Realizar secuencias que incluyan el empleo de dos o más dedos; por ejemplo, 1 en V - 3 en VII - 2 en
VII - 2 en V.
3. Crear secuencias que utilicen todos los dedos; por ejemplo; 1 en IV - 3 en IV - 3 en VIII - 2 en VIII
- 4 en VIII - 4 en IV. En general, es conveniente a efectos de estudio que las secuencias utilizadas sean
repetibles, y por lo tanto, que se vuelva a la posición inicial).

### 2.3.2 El traslado transversal

Llamaremos traslado transversal al movimiento que desplaza una misma presentación a otra cuerda
o grupo de cuerdas; en sentido estricto usaremos este nombre sólo para traslados que se realizan dentro de
una misma posición.

Para construir este traslado seguiremos nuestro método habitual de trabajar primero fuera del
instrumento, construyendo gradualmente la sensación neuromotora deseada.

Coloquemos la mano izquierda en el borde de una mesa, tal como hicimos más arriba al tratar de la
presentación longitudinal. El pulgar quedará por debajo del borde de la mesa, y los dedos 1, 2, 3 y 4 quedarán
presentados como sobre un teclado.

Imaginemos ahora (o dibujemos sobre un papel) las seis cuerdas dibujadas sobre la mesa, al alcance de nuestros dedos. Sin desplazar
el pulgar, hagamos que los dedos pasen de una cuerda a otra (manteniendo la presentación). Observemos
que no se experimenta sensación alguna de esfuerzo en los dedos, sino que el movimiento se percibe como
originándose en el brazo y hombro.

Sentémonos ahora en la posición habitual para tocar, sin el instrumento, usando el antebrazo derecho
en lugar del diapasón. Hagamos que los dedos vayan de una “cuerda” (imaginaria) a otra, siempre sin desplazar
el pulgar, al menos por el momento. Esta sensación se localizará en la parte
superior del brazo, cerca del hombro. Es conveniente estar atentos a que no se produzcan tensiones innecesarias
en el hombro, y prontos a utilizar el aprendizaje negativo para corregirlas si se presentan.

Ya podemos realizar el traslado transversal en el instrumento. Es conveniente comenzar con un traslado
lo más amplio posible, yendo de la 1ª a la 6ª y viceversa. En general, para adquirir una nueva sensación es
conveniente comenzar por el caso más amplio del movimiento. También en el caso del traslado transversal es
necesario tener presente el desplazamiento de la sensación de la palma de la mano al brazo y viceversa.
Los pasos a seguir, que deben repetirse hasta que formen un bloque de acción, serán entonces:

- Colocar la presentación longitudinal en 6ª cuerda;
- Desplazar la sensación de la mano al brazo;
- Hacer el movimiento de traslado transversal a la 1ª cuerda;
- Retornar la sensación a la palma de la mano.

Al menos en este estado elemental del aprendizaje del traslado transversal, y por razones de mejor
aprendizaje de la sensación neuromotora, es deseable usar el pulgar apenas como un punto fijo, como un eje
del movimiento.

Para construir un mapa personal de los traslados transversales lo más completo posible, es deseable
que se los practique en todas las posiciones y desde y hacia todas las cuerdas.
Ya que los elementos del mecanismo están siempre interrelacionados en la práctica por la misma
naturaleza del mecanismo de ser un sistema, una vez adquirido un nuevo elemento es necesario (y en realidad,
indispensable) relacionarlo lo antes posible con los ya familiares. Siguiendo este criterio, es conveniente
hacer actuar a la mano derecha en las diferentes situaciones que surgen de los traslados, por ejemplo, utilizando
los ejercicios de arpegios en combinación con los de traslados. También es necesario establecer conexiones
explícitas entre los elementos del mecanismo de la mano izquierda. En este caso, podemos comenzar a
hacerlo combinando ambos tipos de traslados, el longitudinal y el transversal.

Es necesario que los ejercicios que se practiquen, en esta etapa de interrelación, sean siempre diferentes,
y que se evite la repetición demasiado sistemática de una misma secuencia, ya que en ese caso la interrelación
que buscamos conseguir entre los elementos del mecanismo se dará solamente para el caso aislado de la
secuencia estudiada.

### 2.3.3. Otras presentaciones; cambios de presentación

La presentación longitudinal es apenas una de entre las muchas presentaciones posibles, y no es nada
seguro que sea la más común en la práctica.
Si hemos elegido la presentación longitudinal como
punto de partida, es solamente porque se trata de la más sencilla de construir. A partir de ella iremos
construyendo otros tipos de presentación.
Hay al menos dos tipos de presentaciones que surgen naturalmente de la longitudinal. Las
denominaremos presentaciones en diagonal y presentación transversal.

#### 2.3.3.1 Presentaciones en diagonal

A partir de una presentación longitudinal es fácil construir dos tipos diferentes de presentaciones
diagonales. Las denominaremos presentaciones diagonales del tipo A y presentaciones diagonales del tipo B.

Para la construcción de una presentación diagonal del tipo A, procedamos de la siguiente manera:

- coloquemos una presentación longitudinal en la 4ª cuerda, en V posición;
- transfiramos la sensación de colocación, situada en la franja de la palma de la mano inmediatamente
  debajo de los dedos 1, 2, 3 y 4, a la zona situada debajo del dedo 1. Resultado: todos los dedos, excepto el 1,
  se aflojan y quedan presentados sobre la cuerda pero sin tocarla;
- sin que los dedos cambien su presentación, o sea, sin que ninguna de sus falanges cambien de
  ángulo, rotemos el brazo izquierdo levantando el codo hacia la izquierda. La sensación de esta acción se
  ubicará en la cara externa del brazo, entre codo y hombro. Detengamos el movimiento cuando los dedos
  queden colocados del siguiente modo: 1 en 4ª, 2 en 3ª, 3 en 2ª, 4 en lª. Este es un ejemplo de presentación
  diagonal del tipo A.

Es conveniente repetir algunas veces el proceso de construcción para fijar la sensación, y para crear
un bloque de acción. Una vez hecho esto, es una buena idea ejercitar la nueva presentación interrelacionándola
con los traslados longitudinales y transversales.

Para la construcción de una presentación diagonal del tipo B, el procedimiento es el siguiente:

- coloquemos una presentación longitudinal en lª cuerda, V posición;
- transfiramos la sensación de colocación a la zona situada debajo del dedo 1, en la palma de la mano.
  **Resultado:** todos los dedos, excepto el 1, se aflojan, y quedan presentados sobre la cuerda pero sin tocarla;
- sin que los dedos varíen los ángulos establecidos entre sus falanges, rotemos el brazo izquierdo de
	manera que el codo realice un movimiento hacia la derecha. La sensación correspondiente a este movimiento
	se ubicará en la cara interna del brazo, entre hombro y codo. Detengamos el movimiento cuando los dedos
	queden colocados de la siguiente manera: 1 en lª, 2 en 2ª, 3 en 3ª, 4 en 4ª. Esta es una típica presentación
	diagonal del tipo B.

Al igual que en el trabajo con el tipo A, es muy conveniente repetir varias veces el proceso hasta que
se forme un bloque de acción, y luego ejercitarlo combinándolo con otros aspectos del mecanismo;
específicamente, con los traslados longitudinales y transversales o con una combinación de ambos.

Si bien estos procedimientos son muy breves de describir, su dominio requiere bastante tiempo de
práctica y mucha dedicación, además de la paciencia necesaria para establecer una pequeña tabla de ejercicios.
Creo que es preferible que no se den ejemplos específicos de tales tablas posibles, por dos razones: el estudiante
debe, en lo que sea posible, establecer su propio camino en el aprendizaje, y no simplemente seguir el que se
le marque; y, en segundo lugar, es mi opinión que el seguir una rutina de ejercicios impresa y aparentemente
inamovible tendería a hacer perder de vista el objetivo básico de esos mismos ejercicios: el dominio y
ejercitamiento de la sensación neuromotora. Es el mismo estudiante quien debe decidir cuándo esa sensación
está adquirida y cuántos ejercicios debe hacer, y no el autor del libro de ejercicios.

#### 2.3.3.2 Presentación transversal

Llamaremos presentación transversal a aquella en la cual todos los dedos están colocados en un
mismo espacio. Esto ocurrirá normalmente como un caso extremo de la presentación del tipo A, o sea, el
orden de los dedos 1, 2, 3 y 4 corresponderá a una dirección descendente del número ordinal de la cuerda.[^1]

[^1]: Un ejemplo de esta presentación es la posición básica del acorde de La mayor.

Para colocar una presentación transversal, se parte de una presentación longitudinal y se siguen las pasos
señalados antes para la presentación diagonal del tipo A; la única diferencia es que esta vez el movimiento
continuará hasta que todos los dedos estén colocados en el mismo espacio.

#### 2.3.3.3 Presentaciones no rectilíneas

Un elemento común a todos los tipos de presentación que hemos visto hasta ahora (longitudinal,
diagonal, transversal) es que en todos ellos las puntas de los dedos 1, 2, 3 y 4 se encuentran sobre una línea
recta imaginaria. En la práctica concreta del trabajo de una obra, se verá con facilidad que esta situación no
es necesariamente la más común. Dada la frecuencia de tales presentaciones no rectilíneas, merecen
no sólo una categoría aparte, sino un tratamiento propio.

A fin de practicar algunos de los casos posibles y frecuentes de estas presentaciones,
puede ser útil construir una tabla del siguiente tipo:

- Consideremos los dedos 1, 2, 3, 4 en el ámbito de un cuadrado formado por cuatro espacios y
	cuatro cuerdas adyacentes. Es claro que hay muchas posibilidades de distribución de los dedos. Para
	sistematizar estas posibilidades, propongo el siguiente criterio: nombremos siempre en primer lugar al dedo
	que está en la 4ª, en segundo lugar al que está en la 3ª, en tercer lugar al de la 2ª y por fin al que está en la 1ª.
	Naturalmente, es posible decidir utilizar otras cuerdas, adyacentes o no. Utilizando todas las permutaciones
	posibles de 1, 2, 3 y 4 tendremos al mismo tiempo todas las presentaciones posibles en un ámbito de cuatro
	cuerdas, que en este ejemplo suponemos ser las cuatro primeras.
	Cada una de estas presentaciones requiere una actitud levemente distinta del brazo y los dedos, y por
	ende una sensación diferente, o más bien una constelación diferente de sensaciones.
	Es ciertamente indispensable
	la práctica de al menos algunas de ellas; un modo posible de hacerlo sería, en primer lugar, construir una tabla
	como la descrita más arriba, y en segundo, hacer una selección arbitraria de un cierto número de ellas para
	cada sesión de trabajo.

### 2.3.4. Cambios de presentación

El pasaje de una presentación cualquiera a otra del mismo tipo, combinado o no con un cambio de
posición, debe practicarse como un caso especial de traslado, donde todo el aparato motor del brazo izquierdo
actuará como un conjunto. Si se ha adquirido la sensación de presentación, y el bloque de acción de los
traslados, la integración de ambos elementos no debería ser problemática.

El pasaje de una presentación cualquiera a otra de un tipo diferente no requiere mayor comentario,
dado que por el proceso de construcción que hemos realizado todos los bloques de presentación y de acción
estarán incorporados. Desde luego no hay ningún inconveniente en practicarlos de un modo sistemático,
para lo cual sería deseable la construcción de una tabla.

Es quizás en el caso especial del pasaje de una presentación rectilínea a otra no rectilínea, o viceversa,
cuando más necesaria sea la construcción de una tabla de ejercicios. Si se ha construído la tabla parcial de
presentaciones no rectilíneas mencionada antes, se tratará simplemente de relacionar cada caso con una
presentación rectilínea, y analizar el cambio de una a otra en términos de descomposición de detalles.
De hecho, si quisiéramos cubrir todas las acciones posibles de la mano izquierda, tendríamos que
pasarnos tanto tiempo en construir las tablas y en estudiarlas que muy probablemente no nos quedaría
tiempo para trabajar obras.
La escala de acción del mecanismo es generalmente microscópica, en cuanto se
ocupa del estudio y de la adquisición de elementos aislados, relacionándolos luego entre sí. No bien éstos se
interrelacionan en una obra, el mecanismo debe ceder el lugar a otro tipo de trabajo: el técnico, que trata de
una jerarquía de significación diferente y más amplia, y que no comprende sólo elementos físicos sino
básicamente musicales.

