---
documentclass: extarticle
fontsize: 12pt
---

## 2.2. MANO DERECHA

_(Extracto del libro **Técnica, Mecanismo, Aprendizaje** de Eduardo Fernández)_

### 2.2.1 Construcción de la posición

La construcción detallada de la posición del brazo derecho requiere decidir previamente sobre algunas
opciones básicas. Dos de las más fundamentales son si se debe atacar las cuerdas con las uñas o con las
yemas de los dedos, y en qué ángulo deberán los dedos atacar las cuerdas.

El antiguo debate entre los partidarios de la uña y los de la yema, cuestión aún candente hace una o
dos generaciones, parece hoy estar superado. Hoy en día, al menos para los ejecutantes
de instrumentos modernos, el toque con uña se ha impuesto de manera prácticamente universal; y se puede
presentar un muy buen argumento para su uso incluso en obras compuestas en el siglo XIX. Desde luego,
para los ejecutantes de instrumentos históricos el toque con yema es la regla. Como en esta obra nos
ocuparemos de la guitarra moderna, supondremos adoptado el toque con uña.

La segunda opción a decidir, o sea, en qué ángulo deben los dedos atacar las cuerdas, parece ofrecer
un margen mucho mayor para la diferencia de opiniones. Es sin embargo importante que el resultado sonoro
de la acción de los dedos sea bueno, y para esto son necesarios algunos requisitos: que las cuerdas se ataquen
sobre su propio plano, o sea, de abajo hacia arriba para i, m, a, y de arriba hacia abajo por parte del pulgar (y
no en una dirección perpendicular a la tapa), que los dedos puedan utilizar todas sus falanges con absoluta
libertad, y todos los músculos estén en una situación óptima de rendimiento. No existen dos manos derechas
idénticas, pero todos podemos oír los resultados.

En lo que sigue, sin establecer un ángulo determinado para la acción de los dedos, trataremos de
establecer estos requisitos. Esto requerirá que la mano derecha quede en una posición similar a la que adoptaría
para agarrar un tubo cilíndrico, con los dedos índice, mayor, anular y meñique siguiendo la curva del tubo, y
con el pulgar apoyado aproximadamente en el centro del mismo, en la superficie superior. De acuerdo a esto,
la acción de índice, mayor y anular se realizará hacia la palma de la mano, y el pulgar actuará atacando la
cuerda aproximadamente con su esquina superior izquierda (con uña, yema u opcionalmente una u otra
según la forma escogida por el ejecutante de limar esa uña). En este caso, todas las acciones de ataque harán
vibrar las cuerdas predominantemente en un plano básicamente paralelo al de la tapa. Una línea que pase por
la articulación entre la mano y los dedos, o sea, por los nudillos, quedará así colocada en un plano
aproximadamente paralelo al de la tapa.

### 2.2.2 Acción

Imaginemos el tubo mencionado más arriba, y coloquemos la posición de la mano derecha en el aire
(sin el instrumento), concentrándonos en percibir la sensación neuromotora que ella implica. Una vez
sintonizada ésta con claridad, dejemos que la mano derecha caiga sobre las cuerdas por su propio peso, sin
modificar la posición. Observemos en dónde se ubica naturalmente el punto de contacto del brazo derecho
con el instrumento, comprobando siempre que no existan tensiones. Como vemos, no es necesario que la
muñeca se tuerza a la derecha para que los dedos queden en una posición aproximadamente perpendicular a
las cuerdas. De hecho, cualquier torsión les quitaría movilidad: compruébese esta aserción fuera del
instrumento, con y sin torsión de la muñeca. En este caso, como siempre que se experimentan elementos de
mecanismo fuera del instrumento, es recomendable que el movimiento no se realice hasta que la sensación
neuromotora sea percibida con toda claridad.

Coloquemos ahora la mano de manera que los dedos formen la siguiente presentación, la que
llamaremos “posición 1”:

- a en la lª cuerda;
- m en la 2ª cuerda;
- i en la 3ª cuerda;
- p en la 4ª cuerda.

Exploremos ahora la constelación de sensaciones neuromotoras correspondiente a esta posición,
siempre asegurándonos de que no haya tensiones innecesarias o excesivas.

Una manera práctica de comprobar si la mano derecha, y por extensión todo el brazo derecho) está
en buena posición es intentar el siguiente experimento: coloquemos

- a en 1ª cuerda;
- m en 2ª cuerda; (posición 2)
- i en 4ª cuerda;
- p en 3ª cuerda.

Si ahora tocamos la tercera cuerda con el pulgar en forma repetida, tendremos la sensación exacta
tanto de la posición como del ataque con el pulgar. Una vez sintonizada esa sensación, podemos retornar a la
posición 1 para el ejercicio que sigue más abajo. La sensación correspondiente a la acción del pulgar se
localizará en la base de éste. Si hubiera alguna dificultad en localizarla con precisión, sugiero intentar alguno
de los procedimientos que siguen, o ambos:

- Visualícese el tubo imaginario del que hablamos al describir la presentación. Trátese de hundir el
pulgar dentro de ese tubo imaginario, cuya superficie se puede imaginar como elástica.
- Sin variar la presentación de la mano derecha, colóquese la mano izquierda debajo del pulgar de la
mano derecha, e inténtese empujar la mano izquierda hacia abajo (la mano izquierda debe oponerse a ello
para que el experimento resulte).

Todos los ejercicios que siguen deberían practicarse durante varios días cada uno para obtener el
máximo provecho de ellos. El objetivo básico, vincular las sensaciones neuromotoras a las acciones deseadas,
puede ser conseguido en un plazo sorprendentemente corto, que naturalmente variará en cada estudiante. Es
sin embargo recomendable continuar practicándolos durante algunos días para afirmar y refinar lo ya
aprendido.

#### Ejercicio 1

1. Pongamos ahora en funcionamiento la posición, tocando las cuatro primeras cuerdas en un acorde,
usando la posición 1. El acorde debe ser repetido en un tempo que sea cómodo de realizar y al mismo tiempo
relativamente rápido, para poder automatizar la acción de toque. Observemos la sensación producida, y la
localización de esa sensación en la mano, sin dejar de tocar. Debería percibirse la sensación del pulgar, ya
localizada, y también la que provoca la acción de los dedos a, m, i, y que se sitúa en la palma de la mano,
inmediatamente debajo de cada dedo.
2. Siempre sin dejar de tocar, realicemos un crescendo lo más gradual y prolongado que sea posible.
Nótese que a medida que el acorde suena más fuerte (siempre y cuando no ejerzamos ninguna tensión
innecesaria) la mano tiende a levantarse de las cuerdas luego de cada acorde, para caer de nuevo en su
posición anterior (posición l). Esto debe observarse, pero no provocarse deliberadamente. A lo largo del
crescendo, se podrán distinguir algunos momentos identificables en el continuo de sensaciones neuromotoras:

- al inicio, la localización de la sensación neuromotora de esfuerzo será donde describimos más arriba
(si no está presente, debe continuarse ejecutando el acorde hasta que se sintonice con claridad)
- en un segundo tramo se percibirá la sensación de control aproximadamente en el dorso de la mano;
- en un tercer tramo, la sensación se desplazará a una zona situada en el antebrazo.

Si realizamos ahora un diminuendo, igualmente gradual y prolongado, observaremos que los tres
momentos se suceden en orden inverso.

El brazo izquierdo debe permanecer relajado durante todo el ejercicio; y en cuanto al brazo derecho
debe prestarse particular atención a mantener la ausencia de tensiones innecesarias (por ejemplo, en el hombro)
a lo largo de todo el proceso descrito.

Si en cualquier momento nos sucede perder la sintonía de la sensación neuromotora, simplemente
recomencemos el proceso, y si esa pérdida de sintonía estuviera causada por tensiones imprevistas en alguna
zona muscular no utilizada, sería entonces una buena ocasión para practicar el aprendizaje negativo: sin dejar
de tocar, provoquemos la tensión deliberadamente y luego relajemos esa zona.

Este ejercicio debería ser repetido hasta que la sensación neuromotora esté “archivada”, o sea, hasta
que no nos sea necesario hacer un esfuerzo de concentración en ella para percibirla con claridad. El objetivo
del ejercicio es conseguir que sea el oído el controlador del crescendo y del diminuendo.

#### El concepto de fijación

Analicemos ahora con un poco más de minuciosidad qué es lo que ocurre a lo largo del ejercicio 1.

Mencionamos que la sensación neuromotora asociada a la acción de los dedos i, m, a se localiza
aproximadamente en una franja en la palma de la mano, inmediatamente debajo de la unión de los dedos con
la palma, y que a medida que se va produciendo el crescendo se despierta otra sensación similar en el dorso
de la mano, o se desplaza la sensación original a ese lugar. ¿Cuál es la razón de que esto suceda? A medida
que el aumento en dinámica requiere más fuerza en el ataque, las tres falanges comienzan a actuar como una
unidad. Para utilizar el vocabulario introducido por Carlevaro, los dedos comienzan a _fijarse_. Esto se produce
de manera completamente espontánea, ya que es un reflejo aprendido por nuestra mano en la vida cotidiana.
Al comenzar a producirse la fijación, los músculos flexores y los extensores comienzan a trabajar
simultáneamente, y es esa participación de los extensores lo que produce la sensación neuromotora de acción
en el dorso de la mano derecha.

Hablábamos más arriba de tres etapas diferenciables sensorialmente a lo largo del ejercicio 1; una en
la cual la sensación está localizada en la palma de la mano inmediatamente debajo de los dedos, otra en la
cual se superpone a ella la sensación en el dorso de la mano (desplazando así el foco de control), y una
tercera en la que la sensación predominante está localizada en el antebrazo (naturalmente, el proceso se
revierte en el diminuendo).

Es prácticamente imposible (si el crescendo
es realmente gradual) determinar cuándo comienza a pasarse de una a otra;
los objetivos de este ejercicio son la combinación de estos dos controladores con la percepción
de la sensación neuromotora, y, una vez que esta última está automatizada, la realización del proceso de
crescendo-diminuendo por medio de una atención exclusiva al sonido.

#### Ejercicio 2

Una vez asimilado el ejercicio 1, realicemos el mismo proceso utilizando solamente a, m, i en las tres
primeras cuerdas; de este modo podremos concentrarnos mejor en la sensación correspondiente al trabajo
de estos tres dedos. Observemos que ellos funcionan mejor si están en contacto entre sí en el momento del
ataque.

La sensación que tendremos al realizar tanto el ejercicio 1 como el 2 no será la de estar utilizando los
dedos, sino la de un bloque de acción. Este concepto de acción en bloque, que por así decir nos permite
redefinir la mano para cada acción, será fundamental en toda nuestra concepción del mecanismo, tanto en lo
referente a la mano derecha como en cuanto a la mano izquierda.

#### Ejercicio 3 _(corresponde al Ejercicio 4 en el libro)_

Más que un ejercicio propiamente dicho, éste consiste en una familia de ejercicios que abarca todas
las permutaciones de acción de _p_, _i_, _m_ y _a_, en arpegios. Las 24 permutaciones posibles se reducen a algunas
menos si consideramos idénticas aquellas que presentan el mismo ciclo de dedos con diferencia de fase, por
ejemplo _p i m a_ con _i m a p_. He aquí la lista de permutaciones posibles:


--------- --------- ---------	---------
 p i m a 	 i m a p 	 m a p i 	 a p i m 

 p i a m 	 i m p a 	 m a i p 	 a p m i 

 p m a i 	 i a p m 	 m i p a 	 a i p m 

 p m i a 	 i a m p 	 m i a p 	 a i m p 

 p a i m 	 i p m a 	 m p i a 	 a m p i 

 p a m i 	 i p a m 	 m p a i 	 a m i p 
---------	---------	---------	---------

Si producimos los arpegios en forma continua, y sin introducir acentos,
entonces alcanzaría con considerar cualquiera de las filas para que todos los arreglos
de p, i, m y a se produzcan (ya que las restantes contienen los mismos arreglos desfasados). Desde un punto
de vista más musical, es deseable practicar todas las permutaciones del ejercicio integradas en un compás de
cualquier metro de base 4 (2/4, 3/4, 4/4), sea en cuerdas adyacentes -por las que ciertamente es deseable
comenzar- o no adyacentes, y finalmente incluso en la misma cuerda.

La sensación de bloque de presentación que hemos encontrado más arriba debe estar
siempre presente mientras realizamos los arpegios.
Es también necesario que si se generan tensiones o si
percibimos que el sonido producido cambia de calidad (en el caso de que este cambio no nos agrade) se
retorne al ejercicio anterior, o bien se recupere la sensación de bloque trabajando un momento fuera del
instrumento. No es necesario que miremos la mano derecha; más bien recomiendo abstenerse de hacerlo.

No sólo es necesario sino también conveniente colocarse en una actitud de juego frente al instrumento;
no es casual que en varios idiomas “jugar” sea sinónimo de “tocar” un instrumento. Una actitud lúdica
durante el estudio, combinada con una sana curiosidad por nuestras sensaciones cenestésicas, es la más
productiva, además de ser la protección más adecuada contra la “hipocondría del mecanismo” que se puede
observar en muchos instrumentistas principiantes, y aún avanzados.

#### Ejercicio 5

Las mismas permutaciones que utilizamos en el ejercicio 4 se pueden emplear con repeticiones del
pulgar, o sea, con dos intervenciones del pulgar en cada permutación, donde al menos una de ellas sea
simultánea con la acción de uno de los otros dedos. Por ejemplo:

    p a m i    a m i p
      p p      p   p

Otras posibilidades de enriquecimiento del esquema son:
 
- cambios de ritmo;
- unión de dedos adyacentes para formar acordes de dos notas;
- inserción del pulgar, actuando solo, dentro de las permutaciones;
- repeticiones de dedos no consecutivas dentro de una permutación (por ejemplo, p i m a m - pero no
p i m m a);
- o cualquier otra variante que la fantasía del estudiante le sugiera.

Es recomendable escribir previamente las variantes a usar antes de practicarlas; este trabajo se realiza
fácilmente en unos minutos y sus ventajas son enormes, como podrá el lector apreciar en la práctica.

#### Ejercicio 6

Las permutaciones pueden también servir de base al trabajo sobre una sola cuerda; en ese caso, es
claro que el pulgar debe de todos modos ubicarse en una cuerda más grave (trabajar con el pulgar en la
misma cuerda que los demás dedos, si bien tendría teóricamente cierto interés, no nos proporcionaría una
habilidad demasiado útil en el trabajo de una obra).[^1]

[^1]: Esta es la opinión del autor; para el propósito de este curso, sugiero en cambio practicar las permutaciones de arpegios en una sola cuerda, como ejercicio adicional; personalmente encuentro valioso el control de la regularidad dinámica y tímbrica que este ejercicio nos proporciona.

Otra posible variante sería el trabajo sobre tres cuerdas, con repetición de una de ellas. En ese caso
debe tenerse presente que es conveniente evitar los cruces de dedos, o sea, que el índice no debe tocar una
cuerda más aguda que el mayor, y la misma condición debe cumplirse entre el mayor y el anular.

### Apéndice

La práctica de permutaciones de arpegios "en abstracto" (es decir, fuera de un contexto musical)
presenta utilidad sólo como ejercicio mecánico y muscular. Una vez alcanzado cierto grado de
control y familiaridad con el mecanismo, conviene aplicarlo a algún estudio u obra apropiados;
a continuación se sugiere una lista de estudios que pueden servir para este propósito:

- Estudio Sencillo Nº6 _(Leo Brouwer)_
- Estudio Sencillo Nº8 _(Leo Brouwer)_ **Parte B**
- Estudios Nº12 y 13 _(M. Carcassi)_
- Preludio en Re menor _(J. S. Bach)_
- Campo abierto (estilo pampeano) _(A. Yupanqui)_
- El trébol en tu piel _(Pablo López Minnucci)_ **Introducción**
- Preludio Nº4 _(H. Villa-Lobos)_ **Animato**
- Hoy por hoy _(E. Méndez)_

Todas estas piezas las pueden encontrar en la biblioteca del sitio.

